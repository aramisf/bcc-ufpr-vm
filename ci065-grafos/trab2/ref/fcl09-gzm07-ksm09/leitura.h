#include <stdio.h>
#include <stdlib.h>
#include "estrutura.h"

#ifndef H_LEITURA_H
#define H_LEITURA_H

/* Função de leitura genérica */
void leitura ( FILE *arq, grafo * g );

#endif
