#include "leitura.h"

/* Função de leitura de matriz padrão */
void leitura ( FILE *entrada, grafo *g )
{
    unsigned int i, j;
    unsigned char v;
    unsigned char *linha1 = malloc( 1 * sizeof( unsigned char ) );
    
    g->nVertices = 0;

    v = getc(entrada);
    while (v != '\n') {
        g->nVertices++;
        linha1 = (unsigned char *) realloc(linha1, g->nVertices * sizeof(unsigned char));
        linha1[g->nVertices - 1] = v - 48;
        v = getc(entrada);
    }

    // Alocando a matriz:	
    g->adjacencia = (unsigned char **) malloc(g->nVertices * sizeof(unsigned char *));
    for (i = 0; i < g->nVertices; i++)
        g->adjacencia[i] = (unsigned char *) malloc(g->nVertices * sizeof(unsigned char));
    
    // Preenchendo a primeira linha da matriz:
    for (i = 0; i < g->nVertices; i++)
        g->adjacencia[0][i] = linha1[i];

    // Preenchendo o restante da matriz:
    for (i = 1; i < g->nVertices; i++) {
        for (j = 0; j < g->nVertices; j++)
            g->adjacencia[i][j] = getc(entrada) - 48;
        getc(entrada); //consome o '\n' de cada linha
    }

    free(linha1);
    return;
}
