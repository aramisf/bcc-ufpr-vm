#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "biblioteca_k-conexo.h"

main(int argc, char **argv)
{
  FILE *f = fopen(argv[1], "r");
  int **mat;
  int n;
  int i;
	int k;
	int vertices;

	fscanf(f, "%d", &k);
	if (k <= 1)
	{
		printf ("1\n");
		exit(1);
	}

	k--;
	mat = mapeia_matriz(mat, f, &n);
	
	if ( (kconexo(mat, n, k )) == 1)
		printf ("0\n");
	else
		printf ("1\n");	
}


