#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int **mapeia_matriz(int **matriz, FILE *arq, int *n);
int num_arestas(int **matriz, int tamanho);
void calcula_graus(int *vertices, int **m, int tamanho);
int teste_dos_graus(int **matriz1, int **matriz2, int tamanho);
void verifica_isomorfismo(int **matriz1, int **matriz2, int n, int *vertices);
void permuta_vertices(int **matriz1, int **matriz2, int n, int *vertices, int inicio);
