#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "biblioteca_isomorfos.h"

/*Calcula os gaus dos vertices da matriz de adjacencia passada como parametro*/
void calcula_graus(int *vertices,int **m,int tamanho)
{
  int i;
  int j;
  int cont=0;
  
  for(i=0;i<tamanho;i++)
    {
      for(j=0;j<tamanho;j++)
	if(m[i][j]==1)
	  cont++;
      vertices[i]=cont;
      cont=0;
    }
}

/*Conta o numero de arestas na matriz de adjacencia passada como parametro*/
int num_arestas(int **matriz,int tamanho)
{
  int i;
  int j;
  int cont=0;

  for(i=0;i<tamanho;i++)
    for(j=i+1;j<tamanho;j++)
      if(matriz[i][j]==1)
        cont++;      
  return(cont);
}

/*Testa as duas matrizes de adjacencia vereficando se amabas tem o mesmo grau. Caso sim, retorna 1, caso contrario 0*/
int teste_dos_graus(int **matriz1,int **matriz2,int tamanho)
{
  int *graus1;
  int *graus2;
  int i=0;
  int j=0;
  int achou=1;

  graus1=(int *)malloc(tamanho*sizeof(int));
  graus2=(int *)malloc(tamanho*sizeof(int));
  calcula_graus(graus1,matriz1,tamanho);
  calcula_graus(graus2,matriz2,tamanho);
  while((achou)&&(i<tamanho))
    {
      achou=0;
      j=0;
      while((!achou)&&(j<tamanho))
	{
	  if(graus1[i]==graus2[j])
	    {
	      graus2[j]=-1;
	      achou=1;
	      j++;
	    }
	  i++;
	}
    }
  if(achou)
    return(1);
  else
    return(0);
}

/*Verifica se ambas a matrizes de adjacencia sao isomorfas. Caso sim, imprime 1 na saida padrao e finaliza a execucao do programa*/
void verifica_isomorfismo(int **matriz1,int **matriz2,int n,int *vertices)
{
  int i=0;
  int j=0;
  int isomorfo=1;

  while((isomorfo)&&(i<n))
    {
      j=0;
      while((isomorfo)&&(j<n))
	{
	  if(matriz1[i][j]==1)
	    {
	      if(matriz2[vertices[i]][vertices[j]]==1)
		isomorfo=1;
	      else
		isomorfo=0;
	    }
	  j++;
	}
      i++;
    }
  if(isomorfo)
    {
      printf("1\n");
      exit(0);
    }
}

void permuta_vertices(int **matriz1,int **matriz2,int n,int *vertices,int inicio)
{
  int i;
  
  if(inicio==n-1) 
    verifica_isomorfismo(matriz1,matriz2,n,vertices);
  else 
    {
      for(i=inicio;i<n;i++) 
	{
	  int tmp=vertices[i];
      vertices[i]=vertices[inicio];
      vertices[inicio]=tmp;
      permuta_vertices(matriz1,matriz2,n,vertices,inicio+1);
      vertices[inicio]=vertices[i];
      vertices[i]=tmp;
	}
    }
}

int **mapeia_matriz(int **matriz,FILE *arq,int *n)
{
  char buffer[100];
  char aux[2];
  int tamanho;
  int i;
  int j;
  
  aux[1]='\0';
  fgets(buffer,100,arq);
  tamanho=strlen(buffer);
  fseek(arq,-tamanho,SEEK_CUR);
  if(buffer[tamanho-1]=='\n')
    tamanho=tamanho-1;
  matriz=(int **)malloc(tamanho*sizeof(int *));
  for(i=0;i<tamanho;i++)
    matriz[i]=(int *)malloc(tamanho*sizeof(int ));
  for(i=0;i<tamanho;i++)
    {
      fgets(buffer,100,arq);
      for(j=0;j<tamanho;j++)
	{
	  aux[0]=buffer[j];
	  matriz[i][j]=atoi(aux);
	}
    }
  *n=tamanho;
  return(matriz);
}
