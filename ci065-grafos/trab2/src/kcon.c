/**
 * @file kcon.c
 * @brief Arquivo para o problema de k-conexidade de um grafo.
 */

#include "main.h"

/**
 * @brief Lê um grafo a partir da entrada padrão.
 * 
 *        Leitura de arquivo para teste de uma matriz de adjacência com
 *        conexidade k. O inteiro k é lido na primeira linha, seguido pela
 *        leitura da matriz.
 *
 * @param g    Grafo a ser lido.
 * @param argc Número de parâmetros passado na execução.
 * @param argv Parâmetros passados na execução.
 * @param k    Número de conexidade a ser lido.
 */
void kcon_read (graph g, int argc, char** argv, int *k) {
    
    FILE *input = stdin;
    if (argc > 1) {
        if (!(input = fopen (argv[1], "r"))) {
            printf ("Erro na leitura do arquivo");
            exit(1);
        }
    }

    int i, j;
    char c;
    char *l = malloc (1 * sizeof(int));
    
    g->n = 0;

    // Lê K
    fscanf (input, "%d\n", k);

    c = getc (input);
    while (c != '\n') {
        g->n++;
        l = realloc (l, g->n * sizeof(int));
        l[g->n - 1] = c - 48;
        c = getc (input);
    }

    g->m = malloc (g->n * sizeof(int));
    for (i = 0; i < g->n; i++)
        g->m[i] = malloc (g->n * sizeof(int));
    
    for (i = 0; i < g->n; i++)
        g->m[0][i] = l[i];

    for (i = 1; i < g->n; i++) {
        for (j = 0; j < g->n; j++)
            g->m[i][j] = getc(input) - 48;
        getc(input);
    }

    g->v = malloc (g->n * sizeof(int));
    for (i = 0 ; i <= g->n ; i++)
        g->v[i] = 1;

    free(l);
    return;
}

/**
 * @brief Verifica a existência de um caminho entre dois vértices.
 * 
 *        Função recursiva que parte de um vértice na matriz de adjacência
 *        de um grafo até alcançar um destino.
 * 
 * @param g  Grafo a ser testado.
 * @param v  Vetor dos vértices visitados.
 * @param n  Conexidade atingida pelos caminhos encontrada.
 * @param v1 Vértice de partida.
 * @param v2 Vértice de destino.
 * @return   0 se o caminho não for encontrado.
 * @return   1 se o caminho for encontrado.
 */
int kcon_path (graph g, int* v, int *n, int v1, int v2) {

    int i;

    // Vértice já visitado
    if (v[v1] == 1)
        return 0;
    v[v1] = 1;
    
    // Caminho entre v1 e v2 existe
    if (v1 == v2) {
        (*n)++;
        return 1;
    }
    
    // Tenta um próximo vértice
    for (i = 0; i < g->n; i++) {
        if (g->m[v1][i] == 1) {
            if (kcon_path (g, v, n, i, v2) == 1) {
                g->m[v1][i] = 0;
                return 1;
            }
        }
    }

    return 0;
}

/**
 * @brief Obtém o grau de conexidade de um grafo.
 * 
 *        A função obtém consecutivos caminhos entre as combinações de
 *        seus vértices a fim de verificar o grau de conexidade do
 *        grafo.
 * 
 * @param g Grafo a ser testado.
 * @return  Grau de conexidade encontrado.
 */
int kcon_test (graph g) {

    int n = 0, run = 0, i;
    int *visited = malloc (g->n * sizeof(int));

    // Testa caminhos possíveis incrementando a conexidade
    run = kcon_path (g, visited, &n, 0, 1);
    while (!run) {
        for (i = 0; i < g->n; i++)
            visited[i] = 0;
        run = kcon_path (g, visited, &n, 0, 1);
    }

    free (visited);
    return n;
}

/**
 * @brief Função principal.
 * @param argc Número de argumentos.
 * @param argv Argumentos.
 */
int main (int argc, char **argv) {

    int k;
    graph g = malloc (sizeof(struct _graph));
    kcon_read (g, argc, argv, &k);
    
    if (kcon_test (g) == k-1)
        printf ("1\n");
    else
        printf ("0\n");

    return 0;
}


