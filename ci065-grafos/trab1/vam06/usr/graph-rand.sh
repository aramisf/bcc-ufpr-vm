#!/bin/bash

NODES=$1
SATURATION=$2

echo $NODES
for i in `seq 1 $(($NODES*$SATURATION))`
do
	j=`echo $(($RANDOM % $NODES))`
    k=`echo $(($RANDOM % $NODES))`
    if [ $j -eq 0 ] ; then j=1; fi
    if [ $k -eq 0 ] ; then k=1 ; fi
    echo $j $k
done
