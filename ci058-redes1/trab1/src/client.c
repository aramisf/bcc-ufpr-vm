#include "main.h"

void client_loop (int socket) {

	printf("client: rodando em modo cliente\n");
	printf("client: escutando a interface %s\n", TRANSFER_DEVICE_CLIENT);

    int i = 0;
    char c;
    char *option = (char*) calloc (sizeof(char**), 300);
    char **opt = (char**) calloc (sizeof(char**), 20);

	while (1) {

		if (DEBUG)
			printf("debug: proximo pacote %d\n", _seq);

		i = 0;
		printf ("$ ");
		while ((c = getchar ()) != '\n')
			option[i++] = c;
		option[i] = '\0';

		i = 0;
		opt[i] = strtok (option, " ");
		while ((opt[++i] = strtok (NULL, " ")));

		if (strcmp(opt[0], "exit") == 0)
			break;

		else if (strcmp(opt[0], "cd") == 0)
			client_cd (socket, opt[1]);

		else if (strcmp(opt[0], "ls") == 0)
			client_ls (socket, opt[1]);

		else if (strcmp(opt[0], "put") == 0)
			file_send (socket, opt[1]);

		else if (strcmp(opt[0], "get") == 0)
			client_get (socket, opt[1]);

        else if (strcmp(opt[0], "lcd") == 0)
            client_lcd (opt[1]);

        else if (strcmp(opt[0], "lls") == 0)
            client_lls ();

		else
			client_help ();

		if (DEBUG)
			printf("debug: esperando comando\n");

	}
}

void client_help () {
	printf("help: comandos disponiveis:\n");
	printf("  cd <dir>\n");
	printf("  ls -<la>\n");
	printf("  put <arquivo>\n");
	printf("  get <arquivo>\n");
	printf("  help\n");
	printf("  exit\n");
}

void client_cd (int socket, char *folder) {

	if (!folder) {
		printf("cd: forneca o nome de um diretorio\n");
		return;
	}

	packet p = packet_mount (_seq, TYPE_CD,
		(unsigned char*) folder, strlen (folder));

	if (packet_send (socket, p))
		p = packet_recv (socket);

	if (p->type == TYPE_SHOW)
		printf("cd: novo diretorio %s\n", folder);
	else if (p->type == TYPE_ERROR)
		printf("cd: diretorio nao disponivel\n");

}

void client_ls (int socket, char *opt) {

    int optlen = (opt) ? strlen(opt) : 0;
    packet p = packet_mount (_seq, TYPE_LS, (unsigned char*) opt, optlen);
	packet_send (socket, p);
	p = packet_recv (socket);

	if (p->type == TYPE_SHOW)
		printf("%s\n", p->data);

}

void client_get (int socket, char *file) {

	packet p = packet_mount (_seq, TYPE_GET,
		(unsigned char*) file, strlen(file));

	if (packet_send (socket, p))
		file_recv (socket, file);

}

void client_lls (void) {

	struct dirent *dp;

	char cwd[1024];
    char *out = malloc (1);
	getcwd (cwd, sizeof(cwd));

	DIR *dir = opendir (cwd);
	while ((dp = readdir (dir)) != NULL) {
		if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
			out = realloc (out, sizeof(char) * (strlen (out) + strlen (dp->d_name)));
			out = strcat (out, dp->d_name);
            sprintf (out, "%s\n", out);
		}
	}
	closedir(dir);
	out[strlen (out)] = '\0';

    printf ("%s", out);
    free (out);

}

void client_lcd (char* dir) {

	char cwd[1024];

	if ((chdir ((char*) dir)) == 0) {
		getcwd (cwd, sizeof(cwd));
		if (DEBUG)
			printf ("lcd: novo diretorio: %s\n", dir);
	} else if (DEBUG) {
			printf ("lcd: %s\n", strerror(errno));
	}

}
