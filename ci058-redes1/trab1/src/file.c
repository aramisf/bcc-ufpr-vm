#include "main.h"

void file_send (int socket, char *file) {

	char animation = 'O';
    unsigned char* buffer = malloc (PACKET_DATA);
    packet p;

    FILE *fp;
    if ((fp = fopen (file, "r")) == NULL) {
        printf("file: arquivo inexistente\n");
        return;
    }

	fseek (fp, 0, SEEK_END);
	long int filesize = ftell(fp);
	int bufflen = 1;
	rewind (fp);

	p = packet_mount (_seq, TYPE_PUT, (unsigned char*) file, strlen (file));
	if (!packet_send (socket, p))
		return;

	while (1) {

		bufflen = filesize - ftell(fp);

		if (bufflen <= 0)
			break;

		if (bufflen > PACKET_DATA)
			bufflen = PACKET_DATA;
		else
			buffer[bufflen] = '\0';

		fread (buffer, 1, bufflen, fp);

		p = packet_mount (_seq, TYPE_DATA, buffer, bufflen);
		if (!packet_send (socket, p))
			return;

		animation = ext_progress (animation);
		if (!DEBUG)
			printf ("\rfile: enviando %s %3ld%% %c",
				file, ftell(fp) * 100 / filesize, animation);

	}

	fclose (fp);

	char* filesize_uc = malloc (PACKET_DATA);
	sprintf (filesize_uc, "%ld", filesize);

	p = packet_mount (_seq, TYPE_END,
		(unsigned char*) filesize_uc, strlen (filesize_uc));
	if (!packet_send (socket, p))
		return;

	if (!DEBUG)
		printf ("\n");

}

void file_recv (int socket, char *file) {

	long int filesize;
	packet p = packet_mount(_seq, 0, NULL, 0);
	FILE* fp;

	/**
     * @var Evita que um arquivo seja sobrescrito em testes locais
     * sprintf ((char*) file, "%s.tmp", file);
     * */

    if ((fp = fopen ((char*) file, "w")) == NULL) {
        printf ("put: nao foi possivel criar arquivo\n");
        return;
    }

    printf ("put: recebendo arquivo: %s\n", file);

	while (1) {
		p = packet_recv (socket);

		if (p->type == TYPE_DATA)
			fwrite (p->data, 1, p->size - 2, fp);

		else if (p->type == TYPE_END) {
			filesize = atol ((char *) p->data);
			break;
		}
	}

    fseek (fp, 0, SEEK_END);
    if (filesize != ftell(fp))
		printf ("put: arquivos com tamanhos diferentes\n");
	else
		printf ("put: transferencia finalizada\n");

	fclose (fp);

}
