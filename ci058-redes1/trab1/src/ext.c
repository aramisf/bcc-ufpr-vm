#include "main.h"

char ext_progress (char c) {
	if (c == 'O')
		return 'o';
	else if (c == 'o')
		return '.';
	else
		return 'O';
}

void ext_cdtousr () {
	int i;
	char dir[1024];

	ext_getexename (dir, sizeof(dir));
	for (i = strlen(dir); dir[i] != '/'; i--);
	dir[i] = '\0';

	char *cwd = malloc (snprintf(NULL, 0, "%s%s", dir, "../usr/") + 1);
	sprintf(cwd, "%s%s", dir, "/../usr/");
	chdir(cwd);
	getcwd (dir, sizeof(dir));

	printf ("ext: diretorio %s\n", dir);
}

char* ext_getexename(char* buf, size_t size) {
    char linkname[64]; /* /proc/<pid>/exe */
    pid_t pid;
    int ret;

    /* Get our PID and build the name of the link in /proc */
    pid = getpid();

    if (snprintf(linkname, sizeof(linkname), "/proc/%i/exe", pid) < 0)
        {
        /* This should only happen on large word systems. I'm not sure
           what the proper response is here.
           Since it really is an assert-like condition, aborting the
           program seems to be in order. */
        abort();
        }


    /* Now read the symbolic link */
    ret = readlink(linkname, buf, size);

    /* In case of an error, leave the handling up to the caller */
    if (ret == -1)
        return NULL;

    /* Report insufficient buffer size */
    if (ret >= size)
        {
        errno = ERANGE;
        return NULL;
        }

    /* Ensure proper NUL termination */
    buf[ret] = 0;

    return buf;
}
