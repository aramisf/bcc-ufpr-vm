#include "main.h"

int _seq = 1;

packet packet_mount (int seq, int type, unsigned char *data, int data_size) {

	packet p = malloc (PACKET_MAXSIZE);
    p->data = malloc (data_size);

	p->init = PACKET_INIT;
	p->size = PACKET_MINSIZE + data_size;
	p->seq = seq;
	p->type = type;
	p->data = data;
	p->par = packet_parity (p);

	return p;
}

unsigned char *packet_raw (packet p) {

	int i;
	unsigned char *r = malloc ((p->size + 4) * sizeof (unsigned char));
	r[0] = p->init;
	r[1] = p->size;
	r[2] = p->seq;
	r[3] = p->type;
	for (i = 0; i <= p->size - 3; i++)
		r[i + 4] = p->data[i];
	r[i + 4] = p->par;
	i = 0;

	return r;
}

packet packet_unraw (unsigned char* r) {

	int i;
	packet p = malloc (r[1] - 2);

	p->init = r[0];
	p->size = r[1];
	p->seq = r[2];
	p->type = r[3];
	p->data = malloc (p->size - 2);
    for (i = 0; i <= p->size - 3; i++)
		p->data[i] = r[i + 4];
    p->par = packet_parity (p);

	return p;
}

int packet_parity (packet p) {

    int i = 0;
    int par = 0;
    unsigned char *r = packet_raw (p);

    for (i = 0; i < r[1]; i++)
		par = par ^ r[i];

    return par;
}

int packet_send (int socket, packet p) {

	int i = 0;
	long int random;

	int status;
	struct pollfd pfd;
	pfd.fd = socket;
	pfd.events = POLLIN;

	unsigned char* q = packet_raw (p);
	packet r = malloc (sizeof(char) * PACKET_MAXSIZE);

	srand(time(0));
	while (i++ < PACKET_TRIES) {

		random = (int) pow (2, i) <= 1024 ? (int) pow (2, i) : 1024;
		random = rand() % (random + 1);

		if ((send (socket, q, PACKET_MAXSIZE, 0)) == -1) {
			printf("erro: send: %s\n", strerror(errno));
			usleep (random * PACKET_TIME);
			continue;
		}

		if (DEBUG && p->type != TYPE_ACK) {
			printf("debug: enviada tentativa %d\t", i);
			debug_packet (p);
		}

		if (p->type == TYPE_ACK)
			packet_seq ();

		if (p->type == TYPE_ACK || p->type == TYPE_NACK)
			return 1;

		if ((status = poll (&pfd, 1, (int) random)) < 0 ) {
			printf("erro: poll: %s\n", strerror(errno));
			usleep (random * PACKET_TIME);
			continue;
		}

		if ((r = packet_recv(socket)) == 0) {
			printf("erro: packet_recv: packet_recv: erro no recebimento");
			usleep (random * PACKET_TIME);
			continue;
		}

		if (r->type == TYPE_NACK) {
			printf("erro: packet_recv: nack retornado\n");
			i = 0;
			continue;
		}

		return 1;
	}

	printf("erro: packet_send: falha na conexao\n");
	return 0;
}

packet packet_recv (int socket) {

    unsigned char *r = malloc (PACKET_MAXSIZE);
    packet p = malloc (PACKET_MAXSIZE);

    while (1) {

		if (DEBUG)
			printf("debug: aguardando pacote %d\n", _seq);

		if ((recv (socket, r, PACKET_MAXSIZE, 0)) < 0) {
			printf("erro: recv: %s\n", strerror(errno));
			continue;
		}

		p = packet_unraw (r);

		if (p->init != PACKET_INIT)
			continue;

		if (p->seq != _seq)
			continue;

        if (packet_parity(p) != p->par)
            continue;

		if (p->type == TYPE_ACK) {
			packet_seq ();
			return p;
		}

		if (DEBUG) {
			printf("debug: mensagem recebida\t");
			debug_packet (p);
		}

		if (p->type != TYPE_ACK && p->type != TYPE_NACK) {
			packet q = packet_mount (_seq, TYPE_ACK, NULL, 0);
			if (packet_send (socket, q))
				return p;
		}

	}
}

int packet_seq () {

	if (_seq < PACKET_SEQ)
		++_seq;
	else
		_seq = 1;

	return _seq;
}
