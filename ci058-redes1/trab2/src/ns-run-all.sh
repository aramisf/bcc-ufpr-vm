#!/bin/bash

mps=1
tries=1
out="ns-results.dat"

while [ $mps -lt 10001 ]
do

	while [ $tries -le 3 ]
	do
		file="out-$mps-$tries"
		bash -c "ns ns-simulation.tcl $RANDOM $mps $file"

		r=`grep "^r" $file.nam | wc -l`
		d=`grep "^d" $file.nam | wc -l`
		echo -e "$mps\t$tries\tentrega\t$(echo scale=10\; $r/\($r+$d\) | bc)" >> $out

		r=0
		d=0
		n=1000
		tottime=0

		line=`grep -n "\-i $n " $file.nam | tail -1 | cut -d":" -f1`
		info=`head -$line $file.nam`

		while [ $r -lt $n ]
		do
			message=`echo "$info" | grep "\-i $r "`
			if [ ! `echo "$message" | grep "^d"` ]
			then
				initime=`echo "$message" | grep "^+" | head -1 | cut -d" " -f3`
				endtime=`echo "$message" | grep "^r" | tail -1 | cut -d" " -f3`
				if [ $initime ] && [ $endtime ]
				then
					tottime=`echo "scale=10; $tottime + $endtime - $initime" | bc`
				fi
			else
				d=$(($d+1))
			fi
			p=`echo "scale=2; ($r/$n)*100" | bc`
			r=$(($r+1))
			echo -e "$file: $p%"
		done

		echo -e "$mps\t$tries\ttempo\t$(echo scale=10\; $tottime/\($r-$d\) | bc)" >> $out

		tries=$(($tries+1))
		rm $file.nam
		rm $file.tr
	done

	mps=$(($mps * 10))
	tries=1
done
