#!/bin/bash

file=$1

r=`grep "^r" $file | wc -l`
d=`grep "^d" $file | wc -l`
echo "entrega\t$(echo scale=10\; $r/\($r+$d\) | bc)"

n=`grep "\-i" $file | tail -1 | cut -d" " -f15`

r=0
d=0
t=0

for i in `seq 1 $n`
do
    message=`cat $file | grep "\-i $i "`
    if [ -z `echo "$message" | grep "^d"` ]
    then
        init=`echo "$message" | grep "^+" | head -1 | cut -d" " -f3`
        end=`echo "$message" | grep "^r" | tail -1 | cut -d" " -f3`
        if [ ! -z $init ] && [ $end ]
        then
            t=`echo "scale=10; $t + $end - $init" | bc`
        fi
        r=$(($r+1))
        echo "-i $i -r $r -t $t" >> "$file.log"
    fi
done 2> /dev/null

echo "tempo\t$(echo scale=10\; $t/$r | bc)"
