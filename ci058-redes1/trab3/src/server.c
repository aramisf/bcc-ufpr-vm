#include "main.h"

int server_mount_socket () {

	int sock;

    if ((sock = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
        perror("socket");

	return sock;
}

void server_mount_host (struct sockaddr_in *host, char *address, char *port) {

    struct hostent *hp;
	host->sin_family = AF_INET;

	if ((hp = gethostbyname (address)) <= 0)
		printf ("erro: nao pode encontrar o servidor\n");

	memcpy ((char *) &host->sin_addr, (char *) hp->h_addr, hp->h_length);
	host->sin_port = htons (atoi (port));

	printf("host: conectado ao servidor %s na porta %d\n", inet_ntoa (host->sin_addr), host->sin_port);
}

void server_mount_local (struct sockaddr_in *local, char *port, int sock) {

	int length;
	length = sizeof (struct sockaddr_in);
	bzero (local, length);
	local->sin_family = AF_INET;
	local->sin_addr.s_addr = INADDR_ANY;
	local->sin_port = htons(atoi(port));
	bzero (&(local->sin_zero), 8);

	if (bind (sock, (struct sockaddr *) local, length) < 0)
		perror ("bind");

	printf ("local: ouvindo a porta %d\n", local->sin_port);
}

void server_loop (int sock_local, struct sockaddr_in *local,
                  int sock_host,  struct sockaddr_in *host,
                  int token) {

	int i;
	int random;
    char *recvbuffer = (char*) malloc (BUFFER_LENGTH * sizeof (char));
    char *sendbuffer = (char*) malloc (BUFFER_LENGTH * sizeof (char));

    while (i++ < PACKET_TRIES) {

        if (token > 0) {
			printf ("$ ");
			bzero (sendbuffer, BUFFER_LENGTH);
			fgets (sendbuffer, BUFFER_LENGTH, stdin);
			sendbuffer = packet_format (TYPE_MESSAGE, sendbuffer);
			packet_send (sock_host, host, sendbuffer);
		}

		if ((recvbuffer = packet_recv (sock_local, local))) {

			if (recvbuffer[0] == TYPE_TOKEN) {
				token = 1;
				continue;
			}

			if (strcmp (recvbuffer, sendbuffer) == 0) {
				token = 0;
				recvbuffer[0] = TYPE_TOKEN;
				printf ("local: passando bastao...\n");
			} else {
				packet_print (recvbuffer);
			}

			sleep (PACKET_DELAY);
			packet_send (sock_host, host, recvbuffer);
			i = 0;
			continue;

		}

		random = (int) pow (2, i) <= 1024 ? (int) pow (2, i) : 1024;
		random = rand() % (random + 1);
		usleep (PACKET_TIME * random);

    }
}
