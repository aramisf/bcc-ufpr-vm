#define TAM_MSG 256

#define MSG 'm'
#define NEG 'n'
#define BAT 'b'

void checksyntax_server (int argc, char **argv);
void checksyntax_client (int argc, char **argv);
void checksyntax_chat (int argc, char **argv);
void error(char *msg);

int cria_socket (void);
void servidor_local (struct sockaddr_in *server, char *port, int sock);
void servidor_remoto (struct sockaddr_in *server, char *address, char *port);
char *proxima_mensagem (void);
int envia (int sock, char *msg, struct sockaddr_in *server);
char *recebe (int sock, struct sockaddr_in *from);

int eh_bastao(char *msg);
int eh_mensagem(char *msg);
void imprime_msg(char *msg);
void negocia_bastao(int sock_cli, struct sockaddr_in *cli, int sock_srv, struct sockaddr_in *srv);
