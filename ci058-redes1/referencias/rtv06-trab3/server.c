#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include "aux.h"

int main(int argc, char **argv) {
	int sock, n;
	struct sockaddr_in server;
	struct sockaddr_in from;
	char *msgin, *msgout = "mRecebi sua mensagem";

	checksyntax_server(argc, argv);

	sock = cria_socket();
	servidor_local(&server, argv[1], sock);
	do {
		msgin = recebe(sock, &from);
		n = envia(sock, msgout, &from);
	} while ( strcmp(msgin, "exit") != 0 );

	return 0;
}
