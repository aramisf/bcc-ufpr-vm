#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <pthread.h>
#include <semaphore.h>

#include "aux.h"
#include "fila.h"

#define PAUSA_REENVIO 2
#define INTER_THREAD 0
sem_t uso_fila;

void **traduz_parms(int *sock_ouve, struct sockaddr_in *addr_ouve,
									 int *sock_envia, struct sockaddr_in *addr_envia,
									 struct fila *f) {
	void **parms;

	if ( !(parms = malloc(5 * sizeof(void *))) ) return NULL;
	parms[0] = (void *) sock_ouve;
	parms[1] = (void *) addr_ouve;
	parms[2] = (void *) sock_envia;
	parms[3] = (void *) addr_envia;
	parms[4] = (void *) f;

	return parms;
}

void *le_usuario (void *fila) {
	char *msg;

	do {
		msg = proxima_mensagem();
		// Enquanto consegue enfilar mensagens do usuario, verifica se FIM
		sem_wait(&uso_fila);
		if ( !enfila((struct fila *)fila, msg) )
			printf("Fila cheia! Aguarde liberacao da fila.\n");
		sem_post(&uso_fila);
	} while ( strcmp(msg, "mexit") != 0 );
}

void *conversacao (void **parms) {
	int sock_ouve, sock_transmite;
	int sair = 0;
	struct sockaddr_in *addr_ouve, *addr_transmite;
	struct fila *f;
	char *recebido, *transmitir;

	// Traducao dos parametros
	sock_ouve = (int) *((int *)parms[0]);
	addr_ouve = (struct sockaddr_in *) parms[1];
	sock_transmite = (int) *((int *)parms[2]);
	addr_transmite = (struct sockaddr_in *) parms[3];
	f = (struct fila *) parms[4];

	do {
		recebido = recebe(sock_ouve, addr_ouve);
		if ( strcmp(recebido, "mexit") != 0 ) sair = 1;

		// Se o que foi recebido eh o bastao, envia mensagem da fila (caso haja), e transmite bastao
		if ( eh_bastao(recebido) ) {
			printf("Recebi bastao para falar\n");
			sem_wait(&uso_fila);
			if ( !vazia_fila(f) ) {
				printf("Oba! Tenho mensagem para enviar. Uma a menos...\n");
				transmitir = desenfila(f);
				envia(sock_transmite, transmitir, addr_transmite);
				printf("Foi... sera que ela volta? ");
				recebe(sock_ouve, addr_ouve);
				printf("Claro que volta!! :)\n");
			} else printf("Nada a falar por enquanto... proximo!!!\n");
			sem_post(&uso_fila);
			printf("Tchau bastao...\n");
			envia(sock_transmite, recebido, addr_transmite);
		}

		// Se nao for o bastao imprime e retransmite
		else {
			printf("Recebi mensagem, devo imprimir e repassar\n");
			imprime_msg(recebido); printf("\n");

			// Pausa para reenvio de mensagem determinado na especificação
			printf("Albini mandou esperar 2 segundos... ");
			sleep(PAUSA_REENVIO);
			envia(sock_transmite, recebido, addr_transmite);
			printf("Pronto! Toca a ficha!\n");
		}

	} while (!sair);
}

int main(int argc, char **argv) {
	int sock_cli, sock_srv;
	struct sockaddr_in srv_remoto, srv_local;
	struct fila *f;
	pthread_t usr, com;

	// Checa se todos os parametros de entrada estao corretos
	checksyntax_chat(argc, argv);
	printf("UFPR - Universidade Federal do Parana\n");
	printf("CI058 - Redes de Computadores I\n");
	printf("Trabalho 03 - Chat em rede Token Ring\n");
	printf("-------------------------------------\n\n");

	// Abre socket para ouvir
	sock_srv = cria_socket();
	servidor_local(&srv_local, argv[1], sock_srv);

	// Abre socket para transmitir
	sock_cli = cria_socket();
	servidor_remoto(&srv_remoto, argv[2], argv[3]);

	printf("Ouve no socket %d | Transmite no socket %d\n\n",	sock_srv, sock_cli);

	// Negociação do bastão
	negocia_bastao(sock_cli, &srv_remoto, sock_srv, &srv_local);

	// Cria fila e semaforo para ler mensagens do usuario
	if ( !(f = cria_fila()) ) return -1;
	sem_init(&uso_fila, INTER_THREAD, 1);

	// Cria threads (interacao com usuario e comunicacao)
	if ( pthread_create (&usr, NULL, le_usuario, (void *)f) != 0 ) return -1;
	if ( pthread_create (&com, NULL, conversacao,	
					traduz_parms(&sock_srv, &srv_local,
											&sock_cli, &srv_remoto, f)) != 0 ) return -1;

	// Retorna threads ao processo principal, destroi fila e finaliza programa
	pthread_join (usr, NULL);
	pthread_join (com, NULL);
	destroi_fila(f);

	return 0;
}
