#include <stdio.h>
#include <stdlib.h>

#include "fila.h"

struct fila *cria_fila (void) {
	struct fila *f;

	if ( !(f = malloc(sizeof(struct fila *))) ) return NULL;
	f->tamanho = 0;
	f->primeiro = NULL;

	return f;
}

int enfila (struct fila *f, char *msg) {
	struct no *n, *ultimo;

	if (!f) return 0;
	if ( !(n = malloc(sizeof(struct no *))) ) return 0;
	n->msg = msg;
	n->prox = NULL;

	if ( vazia_fila(f) ) { f->primeiro = n; }
	else {
		for (ultimo=f->primeiro; ultimo->prox!=NULL; ultimo=ultimo->prox);
		ultimo->prox = n;
	}

	f->tamanho++;

	return 1;
}

char *desenfila (struct fila *f) {
	struct no *no_ret = f->primeiro;
	char *ret = no_ret->msg;

	f->primeiro = no_ret->prox;
	free(no_ret);

	f->tamanho--;

	return ret;
}

void destroi_fila (struct fila *f) {
	while ( !vazia_fila(f) ) desenfila(f);
	free (f);
}

int vazia_fila (struct fila *f) { return f->tamanho == 0 ? 1 : 0; }

void imprime_fila (struct fila *f) {
	struct no *n;
	int i;

	if (f) {
		if ( vazia_fila(f) ) printf("Fila vazia!\n");
		else {
			printf("Tamanho da fila: %d\n", f->tamanho);
			for (n=f->primeiro; n!=NULL; n=n->prox)
				printf("Mensagem %d: %s\n", ++i, n->msg);
		}
	}
}
