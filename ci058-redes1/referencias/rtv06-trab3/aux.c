#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <poll.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "aux.h"

void remove_newline(char *texto) {
	if ( texto[strlen(texto)-1] == 10 ) bzero(&texto[strlen(texto)-1], 1);
}

void checksyntax_server (int argc, char **argv) {
	if (argc != 2) {
		printf("Erro de sintaxe! Esperado:\n");
		printf("\t%s <porta>\n\n", argv[0]);
		_Exit (-1);
	}
}

void checksyntax_client (int argc, char **argv) {
	if (argc != 3) {
		printf("Erro de sintaxe! Esperado:\n");
		printf("\t%s <servidor> <porta>\n\n", argv[0]);
		_Exit (-1);
	}
}

void checksyntax_chat (int argc, char **argv) {
	if (argc != 4) {
		printf("Erro de sintaxe! Esperado:\n");
		printf("\t%s <porta_servir> <servidor> <porta>\n\n", argv[0]);
		_Exit (-1);
	}
}

void error(char *msg) {
	printf("Erro: %s\n", msg);
	_Exit (-1);
}

int cria_socket (void) {
	int sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0) error("Falha ao criar socket");
	return sock;
}

void servidor_local (struct sockaddr_in *server, char *port, int sock) {
	int length;

	length = sizeof(struct sockaddr_in);
	bzero(server, length);

	server->sin_family = AF_INET;
	server->sin_addr.s_addr=INADDR_ANY;
	server->sin_port = htons(atoi(port));
	bzero(&(server->sin_zero), 8);

	if ( bind(sock, (struct sockaddr *)server, length) < 0 ) error("binding");

	printf("Ouvindo na porta %d\n", server->sin_port);
}

void servidor_remoto (struct sockaddr_in *server, char *address, char *port) {
	struct hostent *hp;

	server->sin_family = AF_INET;

	hp = gethostbyname(address);
	if (hp == 0) error("Unknown host");
	memcpy((char *)&server->sin_addr, (char *)hp->h_addr, hp->h_length);
	server->sin_port = htons(atoi(port));

	printf("Mirando em %s porta %d\n", inet_ntoa(server->sin_addr), server->sin_port);
}

char *proxima_mensagem (void) {
	char *texto;

	if ( !(texto = malloc(TAM_MSG * sizeof(char))) ) return NULL;
	bzero(texto, TAM_MSG);
	texto[0] = MSG;

	printf("msg> ");
	fgets(&texto[1], TAM_MSG - 2, stdin);
	remove_newline(texto);

	if ( strcmp(texto, "fim") == 0 ) { free(texto); return NULL; }
	return texto;
}

int envia (int sock, char *msg, struct sockaddr_in *server) {
	int n, length;

	length = sizeof(struct sockaddr_in);
	if ( (n = sendto(sock, msg, strlen(msg), 0, (struct sockaddr *)server, length)) < 0 ) return 0;
	return n;
}

char *recebe (int sock, struct sockaddr_in *from) {
	int n, length;
	char *texto;

	if ( !(texto = malloc(TAM_MSG * sizeof(char))) ) return NULL;
	length = sizeof(struct sockaddr_in);
	if ( (n = recvfrom(sock, texto, TAM_MSG, 0, (struct sockaddr *)from, (socklen_t *)&length)) < 0) return NULL;

	if (eh_mensagem(texto)) {
		printf("Recebida mensagem: ");
		imprime_msg(texto); printf("\n");
	}
	return texto;
}

int eh_bastao(char *msg) { return msg[0] == 'b' ? 1 : 0; }
int eh_mensagem(char *msg) { return msg[0] == 'm' ? 1 : 0; }
void imprime_msg(char *msg) { printf("%s", &msg[1]); }

void negocia_bastao(int sock_cli, struct sockaddr_in *cli, int sock_srv, struct sockaddr_in *srv) {
	char *msgin;
	char myid;
	char negociacao[2] = "nX", bastao[2] = "bX";
	int termina = 0, cria_bastao = 1;

	negociacao[1] = myid = (char)getpid();

	do {
		envia(sock_cli, negociacao, cli);
		msgin = recebe(sock_srv, srv);

		if ( msgin[0] == NEG ) {
			if ( msgin[1] > negociacao[1] ) negociacao[1] = msgin[1];

			if ( msgin[1] == myid ) {
				cria_bastao = 1;
				termina = 1;
			}
		}
		else if ( (eh_bastao(msgin)) && (msgin[1] != myid) ) {
			cria_bastao = 0;
			termina = 1;
		}
	} while ( !termina );

	if ( cria_bastao ) { envia(sock_cli, bastao, cli); printf("Criei bastao inicial...\n"); }
	if ( (eh_bastao(msgin)) && (msgin[1] != myid) ) { envia(sock_cli, msgin, cli); printf("Recebi bastao inicial... vou retransmitir\n"); }
}
