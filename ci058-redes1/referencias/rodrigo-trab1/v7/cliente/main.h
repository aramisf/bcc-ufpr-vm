# include <arpa/inet.h>
# include <errno.h>
# include <netpacket/packet.h>
# include <net/ethernet.h>										// Cabecalho Ethernet
# include <net/if.h>											// Estrutura ifr
# include <netinet/in.h>											// Definicao de protocolos
# include <memory.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <sys/ioctl.h>
# include <sys/socket.h>
# include <sys/types.h>
# include <unistd.h>

# define TAM_BUFFER 256											// Tamanho do buffer de string utilizado em diversas funcoes
# define TAM_STRING 10											// Tamanho maximo de cada string que contem o tamanho de cada mensagem enviada

int Rawsocket (const char *device);

void Modo_Cliente ();
int Manda_Mensagem (int c, char* mensagem);
