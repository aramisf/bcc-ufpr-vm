# include "main.h"

void Modo_Cliente ()
{
	int c;
	char *mensagem = (char*) malloc (sizeof (char) * TAM_BUFFER);

	c = Rawsocket ("eth0");
	if (!Manda_Mensagem (c, mensagem))
		printf ("\nLeitura finalizada!\n");
}

int Manda_Mensagem (int c, char* mensagem)
{
	int fim_while = 0, resultado, tamanho_mensagem, i;
	char ch, *str_tamanho_mensagem = (char*) malloc (sizeof (char) * TAM_STRING), *fim;

	while (!fim_while)												// Enquanto a string fim nao e' digitada
	{
		i = 0;
		ch = getchar ();
		while (ch != '\n')											// Monta a mensagem que sera enviada ao outro computador
		{
			mensagem[i] = ch;
			ch = getchar ();
			i++;
		}
		mensagem[i] = ch;											// Coloca '\n' na penultima posicao da string
		mensagem[i + 1] = '\0';										// Finaliza a montagem da mensagem
		if (strcasecmp (mensagem, "fim") == 0)
			fim_while = 1;
		if (!fim_while)											// Envia a mensagem para o outro computador
		{
			tamanho_mensagem = strlen (mensagem) + 1;					// Artificio que ajudara na montagem da mensagem no servidor
			snprintf (str_tamanho_mensagem, TAM_STRING, "%d", tamanho_mensagem);				// Converte variavel inteira para string
			str_tamanho_mensagem[tamanho_mensagem] = '\0';				// Finaliza o artificio citado acima

			resultado = send (c, str_tamanho_mensagem, TAM_STRING, 0);
			if (resultado == -1)									// Envia o tamanho da mensagem
			{
				perror ("Erro na funcao \"send\".\n");
				strerror (errno);
				return 0;
			}
			resultado = send (c, mensagem, tamanho_mensagem, 0);
			if (resultado == -1)									// Envia a mensagem
			{
				perror ("Erro na funcao \"send\".\n");
				strerror (errno);
				return 0;
			}
		}
		if (strcasecmp (mensagem, "fim\n") == 0)
			return 0;
		mensagem[0] = '\0';											// Reseta a mensagem, para uma nova mensagem ser lida
	}

	return 1;
}

int main (int argc, char **argv)
{
	if (strcmp (argv[1], "--cliente") == 0)
		Modo_Cliente ();
	
	return 0;
}
