# include "main.h"

/* Cria um pacote que sera enviado do Cliente para o Servidor. Retorna este pacote. */
protocolo Monta_Pacote (int sequencia, int tipo, char *dados)
{
	printf ("\t\t\t>>>>> Monta_Pacote\n");
	protocolo pacote;
	int paridade_calculada;
	char *campos_agrupados;
	uchar campo_duplo;

	pacote.marca = MARCA;
	campo_duplo = Codifica_Tamanho_Sequencia (strlen (dados) + 5, sequencia);
	pacote.tamanho_sequencia = campo_duplo;							// Tamanho(34) = Tipo(1) + Dados(30) + Paridade(3)
	pacote.tipo = tipo;
	strcpy (pacote.dados, dados);
	campos_agrupados = Agrupa_Campos (campo_duplo, tipo, dados);
	paridade_calculada = Calcula_Paridade (campos_agrupados);
	pacote.paridade = paridade_calculada;							// Paridade = Tamanho + Sequencia + Tipo + Dados

//	printf ("\t\tPacote de %s, tamanho %d, sequencia %d, tipo %d, dados %s e paridade %d foi montado com sucesso.\n", Retorna_Tipos (tipo), strlen (dados) + 5, sequencia, pacote.tipo, paridade_calculada);
	printf ("\t\tPacote de %s foi montado com sucesso.\n", Retorna_Tipos (tipo));
	return pacote;
}

/* Envia uma mensagem qualquer. Retorna 1 caso a mensagem seja enviada com sucesso, e 0 caso
contrario. */
int Envia_Pacote (int c, protocolo pacote)
{
	printf ("\t\t\t>>>>> Envia_Pacote\n");
	struct pollfd pfd;
	int retorno_poll, numero_tentativas = 1, tamanho = 5 + strlen (pacote.dados), resultado, i;
	char buffer[TAM_BUFFER];
	uchar *mensagem;

	pfd.fd = c;												// Indica a conexao ao qual o TIMEOUT ira se referir
	pfd.events = POLLOUT;										// POLLIN = tem dados para ler
	mensagem = (uchar*) calloc (tamanho, sizeof (uchar));				// Aloca espaco para a mensagem
	mensagem[0] = MARCA;
	mensagem[1] = pacote.tamanho_sequencia;
	mensagem[2] = pacote.tipo;
	mensagem[3] = pacote.paridade;
	for (i = 0; i < strlen (pacote.dados); i++)						// Coloca os dados na mensagem
		mensagem[i + 5] = pacote.dados[i];

	printf ("\t\t>>> >>> >>> >>> >>> Enviando pacote...\n");
	do
	{
		resultado = send (c, mensagem, tamanho, 0);					// Envia a mensagem para outro computador
		if (resultado == -1)
		{
			Erro_Envio_Arquivo (pacote.tipo, numero_tentativas, 0);
			return 0;
		}
		retorno_poll = poll (&pfd, 1, TEMPO_TIMEOUT);				// Verifica o TIMEOUT com a funcao poll
//		printf ("\t\tRetorno da funcao poll = %d.\n", retorno_poll);
		if (retorno_poll < 0)
		{
			fprintf (stderr, "\t\tErro na funcao poll. A verificacao de TIMEOUT nao pode ser realizada.\n");
			return 0;
		}
		if (retorno_poll > 0)
			printf ("\t\tPacote enviado com sucesso apos %d tentativas.\n", numero_tentativas);
//		printf ("\t\tNumero de tentativas: %d.\n", numero_tentativas);
		numero_tentativas++;
	} while (retorno_poll == 0 && numero_tentativas <= NUMERO_TENTATIVAS);
	if (retorno_poll == 0)
	{
		fprintf (stderr, "\t\tConexao perdida. Pacote de %s nao pode ser entregue pois ocorreu TIMEOUT.\n", Retorna_Tipos (pacote.tipo));
		return 0;
	}

	return 1;
}

/* Recebe algum pacote. Retorna 1 caso a funcao tenha recebido algum pacote, e 0 caso contrario. */
protocolo Recebe_Pacote (int c)
{
	printf ("\t\t\t>>>>> Recebe_Pacote\n");
	protocolo pacote;
	int recebidos, tamanho = TAM_PACOTE + 5, tamanho_dados, i, tamanho_dados_dentro_da_mensagem;
	uchar mensagem[tamanho], *dados;

	printf ("\t\t<<< <<< <<< <<< <<< Recebendo pacote...\n");
	do
	{
		recebidos = recv (c, mensagem, tamanho, 0);					// Recebe mensagem enviada
		if (recebidos > 0)
			printf ("\t\tPacote recebido com sucesso! Foram recebidos %d bytes.\n", recebidos);
		if (recebidos < 0)
			PACOTE_RECEBIDO = 0;
		if (recebidos == 0)
			printf ("\t\tConexao perdida!\n");
	}  while (mensagem[0] != MARCA);								// Enquanto nao receber mensagem, espera ela chegar
	if (mensagem[5] == '0')										// Verifica se tipo recebido e' do TIPO_E, TIPO_N, TIPO_Y ou TIPO_Z
		tamanho_dados = 2;
	else
		tamanho_dados = strlen (mensagem) - 5;						// Obtem o tamanho dos dados recebidos
	pacote.marca = mensagem[0];
	pacote.tamanho_sequencia = mensagem[1];
	pacote.tipo = mensagem[2];
	pacote.paridade = mensagem[3];
	tamanho_dados_dentro_da_mensagem = mensagem[4];
	dados = (uchar*) calloc (tamanho_dados, sizeof (uchar));
	for (i = 0; i < tamanho_dados_dentro_da_mensagem; i++)				// Obtem os dados da mensagem enviada
		dados[i] = mensagem[i + 5];
	strcpy (pacote.dados, dados);
//	printf ("\t\tPacote de %s, dados %s e paridade %d foi recebido com sucesso!\n", Retorna_Tipos (pacote.tipo), dados, pacote.paridade);
	printf ("\t\tPacote de %s foi recebido com sucesso!\n", Retorna_Tipos (pacote.tipo));

	return pacote;
}

/* Desmonta o pacote recebido pelo cliente. Retorna diretamente os dados recebidos, e indiretamente
o tamanho, o numero da sequencia, o tipo e a paridade. */
char *Desmonta_Pacote (protocolo pacote, int *tamanho, int *sequencia, int *tipo, int *paridade)
{
	int *campo_duplo;
	char *dados;

	campo_duplo = (int*) calloc (2, sizeof (int));					// Aloca espaco para o campo_duplo
	campo_duplo = Decodifica_Tamanho_Sequencia (pacote.tamanho_sequencia);
	*tamanho = campo_duplo[0];									// Obtem tamanho do pacote
	*sequencia = campo_duplo[1];									// Obtem numero de sequencia do pacote
	*tipo = pacote.tipo;
	dados = strdup (pacote.dados);
	*paridade = pacote.paridade;

	return dados;
}

/* Agrupa os campos Tamanho, Sequencia, Tipo e Dados, a fim de calcular a paridade destes (que deve
ser calculada para todos esses campos de forma agrupada). Retorna a string gerada por este
agrupamento. */
char *Agrupa_Campos (int campo_duplo, int tipo, char *dados)
{
	int i, paridade = 0;
	char s_campo_duplo[TAM_CONVERSAO], s_tipo[TAM_CONVERSAO], buffer[TAM_BUFFER], *campos_agrupados;

	snprintf (s_campo_duplo, TAM_CONVERSAO, "%d", campo_duplo);			// Converte o inteiro campo_duplo para a string s_campo_duplo
	snprintf (s_tipo, TAM_CONVERSAO, "%d", tipo);					// Converte o tipo para a string s_tipo
	strcpy (buffer, s_campo_duplo);								// Agrupa os campos
	strcat (buffer, s_tipo);
	strcat (buffer, dados);
	campos_agrupados = strdup (buffer);

	return campos_agrupados;
}

/* Calcula e retorna a paridade de uma mensagem. */
int Calcula_Paridade (char *dados)
{
	int i, paridade = 0;
	
	for (i = 0; i < strlen (dados); i++)
		paridade = paridade ^ dados[i];

	return paridade;
}

/* Coloca em apenas 8 bits (1 byte) o tamanho e o numero da sequencia de um pacote. Retorna estes 8
bits na forma de uma variavel do tipo uchar. */
uchar Codifica_Tamanho_Sequencia (int tamanho, int sequencia)
{
	uchar campo_duplo, aux;

	aux = tamanho << 3;
	campo_duplo = aux | sequencia;

	return campo_duplo;
}

/* Divide o campo duplo nos 2 inteiros que o formam, que sao o tamanho e o numero da sequencia do
pacote. Retorna o tamanho e o numero da sequencia. */
int *Decodifica_Tamanho_Sequencia (uchar campo_duplo)
{
	int aux, *retorno, tamanho, sequencia;

	retorno = (int*) calloc (2, sizeof (int));						// Aloca espaco para o retorno da funcao
	tamanho = campo_duplo >> 3;									// Obtem o tamanho do pacote
	aux = campo_duplo << 5;										// Obtem o numero da sequencia do pacote
	sequencia = aux >> 5;
	retorno[0] = tamanho;
	retorno[1] = sequencia;

	return retorno;
}

/* Imprime na tela uma mensagem informando que um erro ocorreu, e retorna indiretamente o valor
retorno para a variavel erro. */
void Erro_Leitura_Arquivo (int *erro, int retorno, char *arquivo)
{
	fprintf (stderr, "\t\tNao foi possivel abrir o arquivo %s devido a ocorrencia de erro.\n", arquivo);
	*erro = retorno;
}

/* Imprime na tela uma mensagem informando que algum pacote nao chegou ao seu destino. */
void Erro_Envio_Arquivo (int tipo, int tentativa, int modo)
{
	char *destino, *s_tipo;

	if (modo)													// Seta o destino do pacote que nao pode ser enviado
		destino = strdup ("servidor");
	else
		destino = strdup ("cliente");
	s_tipo = strdup (Retorna_Tipos (tipo));							// Seta o tipo de pacote que nao pode ser enviado
	fprintf (stderr, "\t\tErro na funcao send. Pacote de %s, na %da tentativa de envio, nao chegou ao %s.\n", s_tipo, tentativa, destino);
}

/* Retorna uma string com o tipo de pacote passado como entrada. */
char *Retorna_Tipos (int tipo)
{
	char *s_tipo;

	switch (tipo)												// Escolhe um tipo e coloca em s_tipo a forma escrita dele
	{
		case 0:
			s_tipo = strdup ("TIPO_A");
			break;
		case 1:
			s_tipo = strdup ("TIPO_C");
			break;
		case 2:
			s_tipo = strdup ("TIPO_D");
			break;
		case 3:
			s_tipo = strdup ("TIPO_E");
			break;
		case 4:
			s_tipo = strdup ("TIPO_F");
			break;
		case 5:
			s_tipo = strdup ("TIPO_G");
			break;
		case 6:
			s_tipo = strdup ("TIPO_L");
			break;
		case 7:
			s_tipo = strdup ("TIPO_N");
			break;
		case 8:
			s_tipo = strdup ("TIPO_W");
			break;
		case 9:
			s_tipo = strdup ("TIPO_X");
			break;
		case 10:
			s_tipo = strdup ("TIPO_Y");
			break;
		case 11:
			s_tipo = strdup ("TIPO_Z");
			break;
		default:
			s_tipo = strdup ("tipo desconhecido");
			break;
	}
	return s_tipo;
}

/* Esta funcao elimina a quebra de linha de uma string lida com a funcao fgets. */
char *Inicializa_Buffer (int opcao)
{
	char buffer[TAM_BUFFER], *dados;

	__fpurge (stdin);
	fgets (buffer, TAM_BUFFER, stdin);
	dados = strdup (buffer);
	if (opcao)												// Se o ENTER deve ser eliminado
		dados[strlen (dados) - 1] = '\0';

	return dados;
}

/* Conta o numero de linhas de um arquivo. Retorna o numero de linhas. */
int Conta_Linhas (char *arquivo)
{
	FILE *fp;
	int cont = 0, i = 0;
	char ch;

	if (fp = fopen (arquivo, "r"))
	{
		while (!(feof (fp)))
		{
			fscanf (fp, "%c", &ch);
			if (ch == '\n')
				cont++;
			i++;
		}
		fclose (fp);
		return (cont - 1);
	}	
	else
		fprintf (stderr, "\t\tNao foi possivel abrir o arquivo %s devido a ocorrencia de erro.\n", arquivo);
}
