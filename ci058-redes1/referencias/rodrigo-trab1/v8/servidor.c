# include "main.h"

/* Executa as acoes do servidor. */
void Modo_Servidor (int c, int modo)
{
	printf ("\t\t\t>>>>> Modo_Servidor\n");
	int tipo_anterior = -1, enviou = 0;

	switch (modo)
	{
		case 1:
			printf ("\t\tOpcao %d do Modo_Servidor foi escolhida.\n", modo);
			Recebe_Pacote_Do_Cliente (c, tipo_anterior);
			Recebe_Pacote_Do_Cliente (c, tipo_anterior);
			break;
		case 2:
			printf ("\t\tOpcao %d do Modo_Servidor foi escolhida.\n", modo);
			Recebe_Pacote_Do_Cliente (c, tipo_anterior);
			Recebe_Pacote_Do_Cliente (c, tipo_anterior);
			Recebe_Pacote_Do_Cliente (c, tipo_anterior);
			tipo_anterior = TIPO_G;
			Recebe_Pacote_Do_Cliente (c, tipo_anterior);
			break;
		case 3:
			while (!enviou)
			printf ("\t\tOpcao %d do Modo_Servidor foi escolhida.\n", modo);
			{
				printf ("[1] -> enviou? %d.\n", enviou);
				Recebe_Pacote_Do_Cliente (c, tipo_anterior);
				puts ("[2]");
				Recebe_Pacote_Do_Cliente (c, tipo_anterior);
				puts ("[3]");
				tipo_anterior = TIPO_A;
				puts ("[4]");
				enviou = Recebe_Pacote_Do_Cliente (c, tipo_anterior);
				printf ("[5] -> enviou? %d.\n", enviou);
			}
			break;
		case 4:
			printf ("\t\tOpcao %d do Modo_Servidor foi escolhida.\n", modo);
			Recebe_Pacote_Do_Cliente (c, tipo_anterior);
			break;
		case 5:
			printf ("\t\tOpcao %d do Modo_Servidor foi escolhida.\n", modo);
			Recebe_Pacote_Do_Cliente (c, tipo_anterior);
			break;
	}
}

/* Recebe pacote de aviso do cliente. Envia como resposta um pacote informando se o pacote enviado
foi recebido com sucesso, sem sucesso ou com erro. Retorna 1 caso todos os sub-pacotes tenham sido
enviados, e 0 caso contrario. */
int Recebe_Pacote_Do_Cliente (int c, int tipo_anterior)
{
	printf ("\t\t\t>>>>> Recebe_Pacote_Do_Cliente\n");
	protocolo pacote, pacote_resposta;
	ssize_t recebidos;
	int tamanho, sequencia, tipo, paridade, paridade_calculada, resultado;
	char buffer[TAM_BUFFER], *campos_agrupados, *arquivo, *dados, *vazio = strdup ("0");
	uchar campo_duplo;

	if (!PACOTE_RECEBIDO)
		fprintf (stderr, "\t\tErro na funcao recv. Nenhum pacote foi recebido pelo cliente.\n");
	PACOTE_RECEBIDO = 1;										// Reseta variavel global PACOTE_RECEBIDO
	pacote = Recebe_Pacote (c);									// Recebe pacote de controle (atributo) do protocolo
	dados = strdup (Desmonta_Pacote (pacote, &tamanho, &sequencia, &tipo, &paridade));	// Desmonta o pacote recebido
	campo_duplo = Codifica_Tamanho_Sequencia (tamanho, sequencia);
	campos_agrupados = Agrupa_Campos (campo_duplo, tipo, dados);
	paridade_calculada = Calcula_Paridade (campos_agrupados);
	switch (tipo)
	{
		case TIPO_A:
			Descritor_Arquivo (c, paridade, paridade_calculada, pacote_resposta, dados, vazio);
			break;
		case TIPO_C:
			Descritor_Diretorio (0, c, paridade, paridade_calculada, pacote_resposta, dados, vazio);
			break;
		case TIPO_D:
			if (paridade == paridade_calculada)					// Se pacote recebido esta correto
			{
				if (tipo_anterior == TIPO_A)
				{
					resultado = Acrescenta_Dados (ARQUIVO, dados);
					switch (resultado)							// Verifica como pacote de dados foi recebido pelo servidor
					{
						case 2:	
							pacote_resposta = Monta_Pacote (-1, TIPO_Z, vazio);
							Envia_Pacote (c, pacote_resposta);
							break;
						case 1:
							pacote_resposta = Monta_Pacote (-1, TIPO_Y, vazio);
							Envia_Pacote (c, pacote_resposta);
							break;
						case 0:
							pacote_resposta = Monta_Pacote (-1, TIPO_E, vazio);
							Envia_Pacote (c, pacote_resposta);
							break;
					}
					if (strlen (dados) < TAM_PACOTE)
						return 1;
					else
						return 0;
				}
				else
				{
					resultado = Edita_Linha (ARQUIVO, dados, LINHA);
					switch (resultado)							// Verifica como pacote de dados foi recebido pelo servidor
					{
						case 2:
							pacote_resposta = Monta_Pacote (-1, TIPO_Z, vazio);
							Envia_Pacote (c, pacote_resposta);
							break;
						case 1:
							pacote_resposta = Monta_Pacote (-1, TIPO_Y, vazio);
							Envia_Pacote (c, pacote_resposta);
							break;
						case 0:
							pacote_resposta = Monta_Pacote (-1, TIPO_E, vazio);
							Envia_Pacote (c, pacote_resposta);
							break;
					}
					if (strlen (dados) < TAM_PACOTE)
						return 1;
					else
						return 0;
				}
				pacote_resposta = Monta_Pacote (-1, TIPO_Y, vazio);
				Envia_Pacote (c, pacote_resposta);
			}
			else
			{
				pacote_resposta = Monta_Pacote (-1, TIPO_N, vazio);
				Envia_Pacote (c, pacote_resposta);
			}
			ARQUIVO = strdup (vazio);							// Reseta variavel global ARQUIVO
			break;
		case TIPO_F:
			ARQUIVO = strdup (dados);							// Inicializa variavel global ARQUIVO
			Descritor_Arquivo (c, paridade, paridade_calculada, pacote_resposta, dados, vazio);
			break;
		case TIPO_G:
			LINHA = atoi (dados);								// Inicializa variavel global LINHA
			if (paridade == paridade_calculada)					// Se pacote recebido esta correto
			{
				pacote_resposta = Monta_Pacote (-1, TIPO_Y, vazio);
				Envia_Pacote (c, pacote_resposta);
			}
			else
			{
				pacote_resposta = Monta_Pacote (-1, TIPO_N, vazio);
				Envia_Pacote (c, pacote_resposta);
			}
			break;
		case TIPO_L:
			Descritor_Diretorio (1, c, paridade, paridade_calculada, pacote_resposta, dados, vazio);
			break;
		case TIPO_W:
			Descritor_Arquivo (c, paridade, paridade_calculada, pacote_resposta, dados, vazio);
			break;
		case TIPO_X:
			if (Erro_1 (dados) || Erro_2 (dados))					// Verifica se arquivo existe ou tem permissoes validas
			{
				pacote_resposta = Monta_Pacote (-1, TIPO_E, vazio);
				Envia_Pacote (c, pacote_resposta);
			}
			else
			{
				if (paridade == paridade_calculada)				// Se pacote recebido esta correto
				{
					if (!Mostra_Conteudo (dados))					// Verifica se ocorreu erro do tipo 3
					{
						pacote_resposta = Monta_Pacote (-1, TIPO_E, vazio);
						Envia_Pacote (c, pacote_resposta);
					}
					else
					{
						pacote_resposta = Monta_Pacote (-1, TIPO_Y, vazio);
						Envia_Pacote (c, pacote_resposta);
					}
				}
				else
				{
					pacote_resposta = Monta_Pacote (-1, TIPO_N, vazio);
					Envia_Pacote (c, pacote_resposta);
				}
			}
			ARQUIVO = strdup (vazio);							// Reseta variavel global ARQUIVO
			break;
	}
	return 1;
}

void Descritor_Arquivo (int c, int paridade, int paridade_calculada, protocolo pacote_resposta, char *dados, char *vazio)
{
	printf ("\t\t\t>>>>> Descritor_Arquivo\n");
	if (Erro_1 (dados) || Erro_2 (dados))							// Verifica se arquivo existe ou tem permissoes validas
	{
		pacote_resposta = Monta_Pacote (-1, TIPO_E, vazio);
		Envia_Pacote (c, pacote_resposta);
	}
	else														// Verifica se pacote recebido esta correto
	{
		if (paridade == paridade_calculada)
		{
			pacote_resposta = Monta_Pacote (-1, TIPO_Y, vazio);
			Envia_Pacote (c, pacote_resposta);
		}
		else
		{
			pacote_resposta = Monta_Pacote (-1, TIPO_N, vazio);
			Envia_Pacote (c, pacote_resposta);
		}
	}
}

void Descritor_Diretorio (int modo, int c, int paridade, int paridade_calculada, protocolo pacote_resposta, char *dados, char *vazio)
{
	printf ("\t\t\t>>>>> Descritor_Diretorio\n");
	if (Erro_2 (dados) || Erro_4 (dados))							// Verifica se diretorio existe ou tem permissoes validas
	{
		pacote_resposta = Monta_Pacote (-1, TIPO_E, vazio);
		Envia_Pacote (c, pacote_resposta);
	}
	else
	{
		if (paridade == paridade_calculada)						// Se pacote recebido esta correto
		{
			switch (modo)
			{
				case 0: 		
					if (!Executa_CD (dados))						// Verifica se ocorreu erro do tipo 3
					{
						pacote_resposta = Monta_Pacote (-1, TIPO_E, vazio);
						Envia_Pacote (c, pacote_resposta);
					}
					else
					{
						pacote_resposta = Monta_Pacote (-1, TIPO_Y, vazio);
						Envia_Pacote (c, pacote_resposta);
					}
					break;
				case 1:
					if (!Executa_LS (dados))						// Verifica se ocorreu erro do tipo 3
					{
						pacote_resposta = Monta_Pacote (-1, TIPO_E, vazio);
						Envia_Pacote (c, pacote_resposta);
					}
					else
					{
						pacote_resposta = Monta_Pacote (-1, TIPO_Y, vazio);
						Envia_Pacote (c, pacote_resposta);
					}
					break;
			}
		}
		else
		{
			pacote_resposta = Monta_Pacote (-1, TIPO_N, vazio);
			Envia_Pacote (c, pacote_resposta);
		}
	}
}
