# include "main.h"

/* Variaveis globais. */
int PACOTE_RECEBIDO = 1;
char *ARQUIVO;
int LINHA = 0;

/* Programa principal. */
int main (int argc, char **argv)
{
	protocolo pacote;
	int c, opcao, sequencia = 0, linha = 0, numero_de_linhas;
	char *buffer, s_opcao[TAM_CONVERSAO], *arquivo, *dados, *diretorio;

	system ("clear");											// Limpa a tela
	c = Raw_Socket ("eth0");										// Cria uma conexao de Raw Socket
	if (argc == 2)												// Verifica os parametros de entrada do programa
	{
		while (1)
		{
			if (strcasecmp (argv[1], "--cliente") == 0)				// Executa como modo cliente
			{
				printf ("1. Mostrar na tela o conteudo de um arquivo.\n");
				printf ("2. Editar linha de um arquivo.\n");
				printf ("3. Acrescentar dados a um arquivo.\n");
				printf ("4. Entrar em um diretorio.\n");
				printf ("5. Listar conteudo de um diretorio.\n");
				printf ("6. Finalizar programa.\n");
				printf ("\t");
				scanf ("%d", &opcao);
				snprintf (s_opcao, TAM_CONVERSAO, "%d", opcao);		// Converte a opcao para a string s_opcao
				pacote = Monta_Pacote (-1, -1, s_opcao);
				switch (opcao)
				{
					case 1:
						Envia_Pacote (c, pacote);				// Informa ao servidor qual opcao de menu foi escolhida
						printf ("\tEntre com o nome do arquivo: ");
						buffer = strdup (Inicializa_Buffer (1));
						arquivo = strdup (buffer);
						if (!Modo_Cliente (c, 1, sequencia, arquivo, linha, dados))
							printf ("\tO conteudo do arquivo %s nao pode ser mostrado na tela.\n", arquivo);
						free (arquivo);
						break;
					case 2:
						Envia_Pacote (c, pacote);				// Informa ao servidor qual opcao de menu foi escolhida
						printf ("\tEntre com o nome do arquivo: ");
						buffer = strdup (Inicializa_Buffer (1));
						arquivo = strdup (buffer);
						printf ("\tEntre com a linha a ser editada: ");
						scanf ("%d", &linha);
						numero_de_linhas = Conta_Linhas (arquivo);
						if ((linha - 1) > numero_de_linhas)
						{
							printf ("\t\tA linha %d nao pode ser acessada, pois este arquivo possui apenas %d linhas.\n", linha, numero_de_linhas);
							break;
						}
						printf ("\tDigite os dados a serem colocados na linha %d do arquivo %s: ", linha, arquivo);
						buffer = strdup (Inicializa_Buffer (0));
						dados[strlen (dados) - 1] = '\0';
						dados = strdup (buffer);
						if (!Modo_Cliente (c, 2, sequencia, arquivo, linha - 1, dados))
							printf ("\t\tA linha %d do arquivo %s nao pode ser editada.\n", linha, arquivo);
						free (arquivo);
						break;
					case 3:
						Envia_Pacote (c, pacote);				// Informa ao servidor qual opcao de menu foi escolhida
						printf ("\tEntre com o nome do arquivo: ");
						buffer = strdup (Inicializa_Buffer (1));
						arquivo = strdup (buffer);
						printf ("\tDigite os dados a serem acrescentados: ");
						buffer = strdup (Inicializa_Buffer (0));
						dados = strdup (buffer);
						if (!Modo_Cliente (c, 3, sequencia, arquivo, linha, dados))
							printf ("\tOs dados digitados nao puderam ser acrescentados ao arquivo.\n");
						break;
						free (arquivo);
						free (dados);
					case 4:
						Envia_Pacote (c, pacote);				// Informa ao servidor qual opcao de menu foi escolhida
						printf ("\tEntre com o nome do diretorio: ");
						buffer = strdup (Inicializa_Buffer (1));
						diretorio = strdup (buffer);
						if (!Modo_Cliente (c, 4, sequencia, diretorio, linha, dados))
							printf ("\tO diretorio %s/ nao pode ser explorado.\n", diretorio);
						break;
						free (diretorio);
					case 5:
						Envia_Pacote (c, pacote);				// Informa ao servidor qual opcao de menu foi escolhida
						printf ("\tEntre com o nome do diretorio: ");
						buffer = strdup (Inicializa_Buffer (1));
						diretorio = strdup (buffer);
						if (!Modo_Cliente (c, 5, sequencia, diretorio, linha, dados))
							printf ("\tO conteudo do diretorio %s/ nao pode ser listado.\n", diretorio);
						break;
						free (diretorio);
					case 6:
						printf ("\tPrograma finalizado.\n");
						Envia_Pacote (c, pacote);				// Informa ao servidor qual opcao de menu foi escolhida
						return 0;
					default:
						printf ("\tDigite apenas as opcoes listadas na tela!\n");
						break;
				}
				puts ("");
			}
			else
			{
				if (strcasecmp (argv[1], "--servidor") == 0)			// Executa como modo servidor
				{
					printf ("Modo servidor ativado.\n");
					pacote = Recebe_Pacote (c);					// Recebe do cliente a opcao de menu que foi escolhida
					strcpy (s_opcao, pacote.dados);				// Obtem a opcao
					opcao = atoi (s_opcao);
					printf ("\tOpcao do servidor: %d.\n", opcao);
					if (opcao == 6)
					{
						printf ("\tPrograma finalizado.\n");
						exit (0);
					}
					else
						Modo_Servidor (c, opcao);
				}
				else
				{
					printf ("\tOs modos de execucao deste programa sao apenas --cliente e --servidor.\n");
					return 0;
				}
			}
		}
	}
	else
	{
		printf ("\tOs modos de execucao deste programa sao apenas --cliente e --servidor.\n");
		return 0;
	}

	return 1;
}
