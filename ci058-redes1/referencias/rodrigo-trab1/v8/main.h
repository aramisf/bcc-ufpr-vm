/* BIBLIOTECAS */
# include <arpa/inet.h>
# include <errno.h>
# include <netpacket/packet.h>
# include <net/ethernet.h>										// Cabecalho Ethernet
# include <net/if.h>											// Estrutura ifr
# include <netinet/in.h>											// Definicao de protocolos
# include <memory.h>
# include <poll.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <sys/ioctl.h>
# include <sys/socket.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <unistd.h>

/* DEFINICAO DE VARIAVEIS EXTERNAS */
# ifndef _A_
	# define EXT extern
# else
	# define EXT
# endif

/* ESTRUTURA DO PROTOCOLO */
struct estrutura
{
	int marca;
	unsigned char tamanho_sequencia;								// Tamanho = Tipo + Dados + Paridade
	int tipo;													// Paridade = Tamanho + Sequencia + Tipo + Dados
	char dados[30];
	int paridade;
};
typedef struct estrutura protocolo;
typedef unsigned char uchar;

/* CONSTANTES */
# define NUMERO_TENTATIVAS 16										// Numero maximo de tentativas
# define TEMPO_TIMEOUT 1000										// Tempo em ms
# define TAM_CONVERSAO 10										// Tamanho da string gerada apos a conversao de uma variavel inteira para string
# define TAM_LISTAGEM 20											// Numero maximo de linhas mostrado na tela (por vez)
# define TAM_BUFFER 256											// Tamanho do buffer de string utilizado em diversas funcoes
# define TAM_PACOTE 29											// Tamanho maximo de cada pacote
# define MARCA 126												// Valor decimal relativo a marca 0111 1110 (em binario), ou 0x7e (em hexadecimal)
# define TIPO_A 0												// Acrescentar dados ao arquivo
# define TIPO_C 1												// Entrar em outro diretorio (cd)
# define TIPO_D 2												// Envio de dados
# define TIPO_E 3												// Erro
# define TIPO_F 4												// Descritor de arquivo
# define TIPO_G 5												// Linha a ser editada
# define TIPO_L 6												// Listar os arquivos do diretorio (ls)
# define TIPO_N 7												// NACK
# define TIPO_W 8												// Arquivo a ser editado
# define TIPO_X 9												// Mostrar na tela o conteudo do arquivo
# define TIPO_Y 10												// ACK
# define TIPO_Z 11												// Final de 1 arquivo

/* VARIAVEIS GLOBAIS */
EXT int PACOTE_RECEBIDO;
EXT char *ARQUIVO;
EXT int LINHA;

/* FUNCOES */
/* rawsocket.c */
int Raw_Socket (const char *device);

/* protocolo.c */
int Acrescenta_Dados (char *arquivo, char *dados);
char *Divide_Pacote (char **dados);
int Erro_1 (char *arquivo);
int Erro_2 (char *entrada);
int Erro_4 (char *diretorio);
int Executa_CD (char *diretorio);
int Executa_LS (char *diretorio);
int Mostra_Conteudo (char *arquivo);
int Edita_Linha (char *arquivo, char *dados, int linha);

/* cliente.c */
int Modo_Cliente (int c, int modo, int sequencia, char *arquivo, int linha, char *dados);
int Manda_Pacote_Ao_Servidor (int c, int sequencia, int tipo, char *entrada);
int Recebe_Pacote_Do_Servidor (int c);
int Opcao1_Cliente (int c, int sequencia, char *arquivo);
int Opcao2_Cliente (int c, int sequencia, char *arquivo, int linha, char *dados);
int Opcao3_Cliente (int c, int sequencia, char *arquivo, char *dados);
int Opcao4_Cliente (int c, int sequencia, char *diretorio);
int Opcao5_Cliente (int c, int sequencia, char *diretorio);

/* servidor.c */
void Modo_Servidor (int c, int modo);
int Recebe_Pacote_Do_Cliente (int c, int tipo_anterior);
void Descritor_Arquivo (int c, int paridade, int paridade_calculada, protocolo pacote_resposta, char *dados, char *vazio);
void Descritor_Diretorio (int modo, int c, int paridade, int paridade_calculada, protocolo pacote_resposta, char *dados, char *vazio);

/* funcoes.c */
protocolo Monta_Pacote (int sequencia, int tipo, char *dados);
int Envia_Pacote (int c, protocolo pacote);
protocolo Recebe_Pacote (int c);
char *Desmonta_Pacote (protocolo pacote, int *tamanho, int *sequencia, int *tipo, int *paridade);
char *Agrupa_Campos (int campo_duplo, int tipo, char *dados);
int Calcula_Paridade (char *dados);
uchar Codifica_Tamanho_Sequencia (int tamanho, int sequencia);
int *Decodifica_Tamanho_Sequencia (uchar campo_duplo);
void Erro_Leitura_Arquivo (int *erro, int retorno, char *arquivo);
void Erro_Envio_Arquivo (int tipo, int tentativa, int modo);
char *Retorna_Tipos (int tipo);
char *Inicializa_Buffer (int opcao);
int Conta_Linhas (char *arquivo);
