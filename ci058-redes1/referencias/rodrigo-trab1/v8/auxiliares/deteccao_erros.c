# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# define TAM 256

int main ()
{
	char *diretorio1 = strdup ("servidor/"), *diretorio2 = strdup ("diretorio/"), *comando1, *comando2, *arquivo1 = strdup ("LEIAME"), *arquivo2 = ("arquivo.txt");

	comando1 = (char*) calloc (TAM, sizeof (char));
	comando2 = (char*) calloc (TAM, sizeof (char));
	strcpy (comando1, "cat ");
	strcpy (comando2, "cat ");

	// Verifica erros do comando cd, usando funcao chdir
	printf ("> Executando comando cd %s\n", diretorio1);
	if (chdir (diretorio1) != 0)									// Muda de diretorio
		fprintf (stderr, "> Erro no comando cd. Diretorio %s nao existe\n", diretorio1);
	else
		printf ("> Diretorio %s acessado com sucesso\n", diretorio1);

	printf ("> Executando comando cd %s\n", diretorio2);
	if (chdir (diretorio2) != 0)									// Muda de diretorio
		fprintf (stderr, "> Erro no comando cd. Diretorio %s nao existe\n", diretorio2);
	else
		printf ("> Diretorio %s acessado com sucesso\n", diretorio2);

	// Verifica erros do comando cat, usando funcao system
	printf ("> Executando comando cat %s\n", arquivo1);
	strcat (comando1, arquivo1);
	if (system (comando1) == 0)									// Mostra conteudo do arquivo na tela
		printf ("> Arquivo %s mostrado com sucesso\n", arquivo1);
	else
		fprintf (stderr, "> Erro no comando cat. Arquivo %s nao existe\n", arquivo1);

	printf ("> Executando comando cat %s\n", arquivo2);
	strcat (comando2, arquivo2);
	if (system (comando2) == 0)									// Mostra conteudo do arquivo na tela
		printf ("> Arquivo %s mostrado com sucesso\n", arquivo2);
	else
		fprintf (stderr, "> Erro no comando cat. Arquivo %s nao existe\n", arquivo2);
}
