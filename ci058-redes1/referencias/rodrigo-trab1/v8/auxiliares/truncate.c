/*
> man 2 truncate

graffiti75@graffiti75-desktop:~/Desktop/vina$ ll
total 196
-rwxr-xr-x 1 graffiti75 graffiti75   7313 2008-06-06 19:47 a.out
-rw------- 1 graffiti75 graffiti75   1038 2008-06-06 19:49 backup_copiando.c
-rw------- 1 graffiti75 graffiti75   1038 2008-06-03 17:09 copiando.c
-rw-r--r-- 1 graffiti75 graffiti75  19195 2008-06-06 13:46 exemplo_de_lista.c
-rw-r--r-- 1 graffiti75 graffiti75      0 2008-04-29 16:48 man 3 getopt
-rw-r--r-- 1 graffiti75 graffiti75  14528 2008-06-06 19:24 stat.txt
-rw-r--r-- 1 graffiti75 graffiti75    366 2008-06-06 19:48 truncate.c
-rw-r--r-- 1 graffiti75 graffiti75     55 2008-04-25 14:25 ufsc.txt
-rw-r--r-- 1 graffiti75 graffiti75   3742 2008-06-06 19:44 utime.c
-rw-r--r-- 1 graffiti75 graffiti75   7196 2008-06-06 19:28 vina.c
-rw-r--r-- 1 graffiti75 graffiti75 111709 2008-06-03 15:24 vina.pdf
graffiti75@graffiti75-desktop:~/Desktop/vina$ gcc truncate.c
graffiti75@graffiti75-desktop:~/Desktop/vina$ ./a.out backup_copiando.c
graffiti75@graffiti75-desktop:~/Desktop/vina$ ll
total 192
-rwxr-xr-x 1 graffiti75 graffiti75   7313 2008-06-06 19:50 a.out
-rw------- 1 graffiti75 graffiti75     10 2008-06-06 19:50 backup_copiando.c
-rw------- 1 graffiti75 graffiti75   1038 2008-06-03 17:09 copiando.c
-rw-r--r-- 1 graffiti75 graffiti75  19195 2008-06-06 13:46 exemplo_de_lista.c
-rw-r--r-- 1 graffiti75 graffiti75      0 2008-04-29 16:48 man 3 getopt
-rw-r--r-- 1 graffiti75 graffiti75  14528 2008-06-06 19:24 stat.txt
-rw-r--r-- 1 graffiti75 graffiti75    366 2008-06-06 19:48 truncate.c
-rw-r--r-- 1 graffiti75 graffiti75     55 2008-04-25 14:25 ufsc.txt
-rw-r--r-- 1 graffiti75 graffiti75   3742 2008-06-06 19:44 utime.c
-rw-r--r-- 1 graffiti75 graffiti75   7196 2008-06-06 19:28 vina.c
-rw-r--r-- 1 graffiti75 graffiti75 111709 2008-06-03 15:24 vina.pdf

Antes: 1038 bytes

-rw------- 1 graffiti75 graffiti75   1038 2008-06-06 19:49 backup_copiando.c

Depois: 10 bytes

-rw------- 1 graffiti75 graffiti75     10 2008-06-06 19:50 backup_copiando.c
*/

# include <stdio.h>
# include <string.h>
# include <unistd.h>
# include <sys/types.h>

int main (int argc, char **argv)
{
	FILE *fp;
	char *arquivo;

	if (fp = fopen (argv[1], "wb"))									// Detecta erro na abertura do arquivo
	{
		arquivo = strdup (argv[1]);
		truncate (arquivo, 10);
	}
	else
		printf ("Erro na abertura do arquivo %s.\n", argv[1]);
}
