# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <unistd.h>
# include <time.h>

/*
struct stat
{
	mode_t    st_mode;    // Permissoes
	uid_t     st_uid;     // ID usuario do proprietario
	gid_t     st_gid;     // ID grupo do proprietario
	off_t     st_size;    // Tamanho total, em bytes
	time_t    st_atime;   // Hora de ultimo acesso
	time_t    st_mtime;   // Hora de ultima modificacao
	time_t    st_ctime;   // Hora de ultima modificacao de status
};
*/

int main(int argc, char **argv)
{
	int i;	
	FILE *fp, *sp;
	struct stat status;
	char *arquivo, *permissoes, buffer[7];
	char data_modificacao[11], data_ult_acesso[11], data_ult_status[11], hora_modificacao[9], hora_ult_acesso[9], hora_ult_status[9];
	// As variaveis de tipo "struct tm" serao inicializadas com o prefixo "tm_"	
	struct tm tm_modificacao, tm_ult_acesso, tm_ult_status;
	// As variaveis de tipo "time_t" serao inicializadas com os prefixos "dt_" e "hr_"	
	time_t dt_modificacao, dt_ult_acesso, dt_ult_status, hr_modificacao, hr_ult_acesso, hr_ult_status;

	puts ("");

	if (fp = fopen (argv[1], "rb"))			// Detecta erro na abertura do arquivo
	{
		arquivo = strdup (argv[1]);
		if (!stat (arquivo, &status))
		{
			// Cria um arquivo para armazenar a permissao do arquivo lido (com o intuito final de converte-lo de octal para uma string)
			sp = fopen ("permissao.txt", "wb");
			fprintf (sp, "%o", status.st_mode);
			fclose (sp);

			// Abre o arquivo criado anteriormente e armazena o conteudo da permissao do arquivo na variavel "buffer"
			sp = fopen ("permissao.txt", "rb");
			fscanf (sp, "%s", &buffer);
			fclose (sp);

			permissoes = (char*)calloc (4, sizeof (char));			// Aloca espaco para a variavel permissao

			for (i = 3; buffer[i]; i++)			// Monta a string "permissao"
				permissoes[i - 3] = buffer[i];
	
			remove ("permissoes.txt");			// Remove o arquivo temporario gerado na conversao de tipo octal para string	

			// Converte a data de modificacao do arquivo, de time (que e' um tipo de variavel) para string
			dt_modificacao = status.st_mtime;			
			dt_ult_acesso = status.st_atime;
			dt_ult_status = status.st_atime;
			hr_modificacao = status.st_mtime;
			hr_ult_acesso = status.st_atime;
			hr_ult_status = status.st_atime;

			// Exemplo de sintaxe para capturas os atributos de cada arquivo:
			// < variavel "struct tm" > = *localtime (& < variavel "time_t" >;
			// strftime(< variavel "string" >, sizeof (< variavel "string" >, formato (vide "man strftime"), < variavel "struct tm" >);

			tm_modificacao = *localtime (&dt_modificacao);			// Data em que o arquivo foi modificado
			strftime (data_modificacao, sizeof (data_modificacao), "%d/%m/%Y", &tm_modificacao);

			tm_modificacao = *localtime (&hr_modificacao);			// Hora em que o arquivo foi modificado
			strftime (hora_modificacao, sizeof (hora_modificacao), "%H:%M:%S", &tm_modificacao);

			tm_ult_acesso = *localtime (&dt_ult_acesso);			// Data em que o arquivo foi acessado pela ultima vez
			strftime (data_ult_acesso, sizeof (data_ult_acesso), "%d/%m/%Y", &tm_ult_acesso);

			tm_ult_acesso = *localtime (&hr_ult_acesso);			// Hora em que o arquivo foi acessado pela ultima vez
			strftime (hora_ult_acesso, sizeof (hora_ult_acesso), "%H:%M:%S", &tm_ult_acesso);

			tm_ult_status = *localtime (&dt_ult_status);			// Data em que o status do arquivo foi alterado pela ultima vez
			strftime (data_ult_status, sizeof (data_ult_status), "%d/%m/%Y", &tm_ult_status);

			tm_ult_status = *localtime (&hr_ult_status);			// Hora em que o status do arquivo foi alterado pela ultima vez
			strftime (hora_ult_status, sizeof (hora_ult_status), "%H:%M:%S", &tm_ult_status);

			printf ("Eis a data e a hora de modificacao do arquivo <%s>: %s, %s.\n", arquivo, data_modificacao, hora_modificacao);
			printf ("Eis a data e a hora de ultimo acesso do arquivo <%s>: %s, %s.\n", arquivo, data_ult_acesso, hora_ult_acesso);
			printf ("Eis a data e a hora de ultima alteracao de status do arquivo <%s>: %s, %s.\n", arquivo, data_ult_status, hora_ult_status);
			printf ("UID do usuario do arquivo <%s>: %d.\n", arquivo, status.st_uid);
			printf ("Permissoes do arquivo <%s>: %s.\n", arquivo, permissoes);

			fclose (fp);
		}
		else
			printf ("Erro ao ler os atributos do arquivo <%s>!\n", arquivo);
	}
	else
		printf ("Erro na abertura do arquivo %s!\n", arquivo);
	puts ("");
}
