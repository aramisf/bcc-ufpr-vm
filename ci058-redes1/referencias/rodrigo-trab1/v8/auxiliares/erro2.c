/*
Erro 2. Sem permissao
*/
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <unistd.h>

int main(int argc, char **argv)
{
	FILE *fp_in, *fp_out;
	struct stat status;
	int i, tipo = 2, tamanho;
	char *arquivo, *permissoes, buffer[7];

	arquivo = strdup (argv[1]);
	if (fp_in = fopen (argv[1], "rb"))							 	// Detecta erro na abertura do arquivo
	{
		if (!stat (arquivo, &status))
		{
			fp_out = fopen ("permissoes.txt", "wb");				// Cria um arquivo auxiliar para armazenar a permissao do arquivo de entrada (com o
			fprintf (fp_out, "%o", status.st_mode);					// intuito final de converte-lo de octal para string)
			fclose (fp_out);

			fp_out = fopen ("permissoes.txt", "rb");				// Abre o arquivo criado anteriormente e armazena o conteudo da permissao do arquivo de
			fscanf (fp_out, "%s", buffer);						// entrada na variavel buffer
			fclose (fp_out);

			permissoes = (char*) calloc (4, sizeof (char));			// Aloca espaco para a string permissao
			tamanho = strlen (buffer);
			if (tamanho == 6)									// Se tamanho = 6, permissoes sao de um arquivo. Caso contrario, de diretorio
				tipo = 3;

			for (i = tipo; buffer[i]; i++)						// Cria a string permissoes
				permissoes[i - tipo] = buffer[i];
	
			remove ("permissoes.txt");							// Remove o arquivo temporario
			if (tipo == 3)
				printf ("> As permissoes do arquivo %s sao %s\n", arquivo, permissoes);
			else
				printf ("> As permissoes do diretorio %s/ sao %s\n", arquivo, permissoes);
			fclose (fp_in);
		}
		else
			printf ("> Erro ao ler atributos do arquivo %s\n", arquivo);
	}
	else
		printf ("> Erro na abertura do arquivo %s\n", arquivo);
}
