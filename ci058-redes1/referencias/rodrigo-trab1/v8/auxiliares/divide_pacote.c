# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# define TAM_BUFFER 256
# define TAM_PACOTE 30

/* Divide pacote original em um sub-pacote de tamanho TAM_PACOTE. Retorna este sub-pacote e,
indiretamente, o conteudo do pacote original (sem o conteudo do sub-pacote). */
char *Divide_Pacote (char **dados)
{
	int i = 0;
	char *copia = strdup (dados[0]), *pacote = strdup ("");

	strncat (pacote, dados[0], TAM_PACOTE);							// Cria a string que sera retornada pela funcao
	do dados[0][i] = copia[i++ + TAM_PACOTE];
	while (dados[0][i]);

	return pacote;
}

int main ()
{
	int i, numero_pacotes;
	char buffer[TAM_BUFFER], *dados, **matriz;
	
	printf ("Entre com a string: ");
	fgets (buffer, TAM_BUFFER, stdin);
	dados = strdup (buffer);										// Inicializa os dados

	numero_pacotes = strlen (dados) / TAM_PACOTE + 1;
	printf ("Numero de pacotes: %d.\n", numero_pacotes);
	matriz = (char**) calloc (numero_pacotes, sizeof (char*));			// Aloca espaco para a matriz que contera os pacotes divididos
	for (i = 0; i < numero_pacotes; i++)
	{
		matriz[i] = (char*) calloc (TAM_PACOTE, sizeof (char));		// Inicializa linha da matriz com um tamanho de TAM_PACOTE
		strcpy (matriz[i], Divide_Pacote (&dados));					// Coloca o pacote dividido na linha atual da matriz
		printf ("Linha[%d] (%.2d caracteres): %s\n", i, strlen (matriz[i]), matriz[i]);
	}

	return 1;
}
