/*
Erro 1. Arquivo inexistente
*/
# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# define TAM 256

int main (int argc, char **argv)
{
	int resultado;
	char *comando, *arquivo = strdup (argv[1]);

	comando = (char*) calloc (TAM, sizeof (char));
	strcpy (comando, "test -e ");

	// Verifica erros do comando test -e, usando funcao system
	printf ("> Executando comando test -e %s\n", arquivo);
	strcat (comando, arquivo);
	resultado = system (comando);
	printf ("> Resultado %d\n", resultado);
	if (resultado == 0)											// Testa se arquivo existe
		printf ("> Arquivo %s existe\n", arquivo);
	else
		fprintf (stderr, "> Erro no comando test -e. Arquivo %s nao existe\n", arquivo);
}
