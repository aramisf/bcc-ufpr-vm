# include <stdio.h>
# include <string.h>

# define TAM_BUFFER 256
# define TAM_PACOTE 30

int main ()
{
	char buffer[TAM_BUFFER], *dados, *copia = strdup ("");

	printf ("Digite uma frase: ");
	fgets (buffer, TAM_BUFFER, stdin);
	dados = strdup (buffer);
	strncat (copia, dados, TAM_PACOTE);
	printf ("Copia (%d caracteres): %s\n", strlen (copia), copia);
}
