# include <stdio.h>
# include <stdlib.h>
# include <string.h>

int Conta_Linhas (char *arquivo)
{
	FILE *fp;
	int cont = 0, i = 0;
	char ch;

	if (fp = fopen (arquivo, "r"))
	{
		while (!(feof (fp)))
		{
			fscanf (fp, "%c", &ch);
			if (ch == '\n')
				cont++;
			i++;
		}
		fclose (fp);
		return (cont - 1);
	}	
	else
		fprintf (stderr, "Erro ao abrir arquivo %s.\n", arquivo);
}

int main ()
{
	int linhas;

	linhas = Conta_Linhas ("uchar.txt");
	printf ("Numero de linhas equivale a %d.\n", linhas);
}
