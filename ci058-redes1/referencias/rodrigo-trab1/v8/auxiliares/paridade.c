/*
OU-EXCLUSIVO
A	B	Saída
0	0	0
0	1	1
1	0	1
1	1	0

Na paridade par, se o total de bits 1 é:
	Par, paridade recebe 0.
	Ímpar, paridade recebe 1.
Na paridade ímpar, se o total de bits 1 é:
	Ímpar, a paridade recebe 0.
	Par, a paridade recebe 1.

Caractere		ASCII		Binário		Paridade-Par		Paridade-Ímpar
A			41			1000001		1000001	0		1000001	1
B			42			1000010		1000010	0		1000010	1
C			43			1000011		1000011	1		1000011	0

frodo@ubuntu:~/UFPR/2010/2010-1/CI058/CI058-trab1-rc05-2009-1/rodrigo_cericatto/v8$ ./paridade BDFHJL
mensagem[0] = B, paridade = 66
mensagem[1] = D, paridade = 6
mensagem[2] = F, paridade = 64
mensagem[3] = H, paridade = 8
mensagem[4] = J, paridade = 66
mensagem[5] = L, paridade = 14

Binário		Decimal	Hexa		Glifo
0100 0010 	66	 	42	 	B
0100 0100 	68	 	44	 	D
0100 0110 	70	 	46	 	F
0100 1000 	72	 	48	 	H
0100 1010		74	 	4A	 	J
0100 1100	 	76 		4C	 	L
---- ----
0000 1110
*/
# include <stdio.h>
# include <string.h>

int main (int argc, char **argv)
{
	int i, paridade = 0;
	char *mensagem = strdup (argv[1]);

	for (i = 0; i < strlen (mensagem); i++)
	{
		paridade = paridade ^ mensagem[i];
		printf ("mensagem[%d] = %c, paridade = %d\n", i, mensagem[i], paridade);
	}
}
