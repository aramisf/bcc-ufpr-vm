    INPP                # program exemplo7 (input, output)
    AMEM 2              # var x [0,0], y [0,1]
    DSVS L1
L2: NADA                # procedure p
    ENPR 1
    AMEM 1              # var z [1,0]
    CRVL 0,0
    ARMZ 1,0            # x := z
    CRVL 0,0
    CRCT 1
    SUBT
    ARMZ 0,0
    CRVL 1,0
    CRCT 1
    CMMA                # if z > 1
    DSVF L3             # then
    CHPR L2             # p
L3: NADA                # else
    CRCT 1
    ARMZ 0,1            # y := 1
    CRVL 0,1
    CRVL 1,0
    MULT
    ARMZ 0,1            # y := y * z
    DMEM 1
    RTPR 1              # end p
L4: NADA                # procedure q
    ENPR 1
    AMEM 2              # var s [1,0], t [1,1]
    CRVL 0,0
    ARMZ 1,0            # s := x
    CRVL 0,0
    CRCT 1
    SUBT
    ARMZ 1,1            # t := x - 1
    CRVL 1,1
    ARMZ 0,0            # x := t
    CRVL 1,0
    CRCT 0
    CMIG                # if s = 0
    DSVF L5             # then
    CRCT 1
    ARMZ 0,1            # y := 1
L5: NADA                # else
    CRVL 1,0
    CRCT 2
    DIVI                # s div 2
    CRCT 2
    MULT                # (s div 2) * 2
    CRVL 1,0
    CMIG                # if ((s div 2) * 2) = s
    DSVF L6             # then
    CHPR L4             # q
L6: NADA                # else
    CHPR L2             # p
    CRVL 0,1
    CRVL 1,0
    MULT
    ARMZ 0,1            # y := y * z
L1: NADA                # begin
    LEIT
    ARMZ 0,0            # read(x)
    CHPR L4             # q
    CRVL 0,1
    IMPR                # write(y)
    DMEM 2
    PARA                # end
