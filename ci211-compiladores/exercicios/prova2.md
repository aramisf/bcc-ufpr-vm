# Exercícios Para Prova 2

## Análise Sintática Descendente Recursiva (LL)

Parser que analisa uma entrada do topo para baixo.

### Exercício 1

Gramática

G = { S' -> S #
      S  -> A S | B A
      A  -> a B | C
      B  -> b A | d
      C  -> c }

Tabela de Próximos

        psip     psip*    psi
    S     AB   ABaCbdc   abdc
    A     aC       aCc     ac
    B     bd        bd     bd
    C      c         c      c

Programa do Parser:

    void Si () {
        S();
        if (token != '#')
            error ();
    }

    void S () {
        switch (token) {
            'a', 'c': A(); S(); break;
            'b', 'd': B(); A(); break;
        }
    }

    void A () {
        switch (token) {
            'a': next(); B(); break;
            'c': C(); break;
        }
    }

    void B () {
        switch (token) {
            'b': next(); A(); break;
            'd': next(); break;
        }
    }

    void C () {
        next();
    }

Árvore De Cima Para baixo

    abcdad
    S ---+
    |    |
    A    S
    |
    a

    bcdad
    S -------+
    |        |
    A ---+   S
    |    |
    a    B
         |
         b

    cdad
    S -------+
    |        |
    A ---+   S
    |    |
    a    B ---+
         |    |
         b    A
              |
              C
              |
              c

    dad
    S -------------+
    |              |
    A ---+         S
    |    |         |
    a    B ---+    B
         |    |    |
         b    A    d
              |
              C
              |
              c

    ad
    S -------------+
    |              |
    A ---+         S
    |    |         |
    a    B ---+    B ---+
         |    |    |    |
         b    A    d    A
              |         |
              C         a
              |
              c

    d
    S -------------+
    |              |
    A ---+         S ---+
    |    |         |    |
    a    B ---+    B    A ---+
         |    |    |    |    |
         b    A    d    a    B
              |         |    |
              C         a    d
              |
              c

    S' ---+
    |     |
    |     #
    |
    S -------------+
    |              |
    A ---+         S ---+
    |    |         |    |
    a    B ---+    B    A ---+
         |    |    |    |    |
         b    A    d    a    B
              |         |    |
              C         a    d
              |
              c

### Exercício 2

Gramática

G = { S -> A #
      A -> b B | C a
      B -> d A | e C
      C -> c }

Tabela de Próximos

          psip    psip*    psi
    S        A
    A       bC      bCc    bc
    B       de       de    de
    C        c        c    c

Programa

    void S() {
        A();
        if (token != '#')
            error();
    }

    void A() {
        switch (token) {
            'b': next(); B(); break;
            'c': C(); next(); break;
        }
    }

    void B() {
        switch (token) {
            'd': next(); A(); break;
            'e': next(); C(); break;
        }
    }

    void C() {
        next();
    }


### Exercício 3

Refatoração de Gramática: Recursão à Esquerda

G = { A -> A a | A B | a
      B -> b }

G = { A -> a X
      X -> e | A | B X
      B -> b }

Tabela de Próximos

        psip    psip*    psi
    A      a        a      a
    X     AB     ABab     ab
    B      b        b      b

Programa:

    void A() {
        switch (token) {
            case 'a' : next(); X(); break;
        }
    }

    void X() {
        switch (token) {
            case 'e' : finish(); break;
            case 'a' : A(); break;
            case 'b' : B(); X(); break;
        }
    }

    void B() {
        next();
    }

Teste de Entrada

    aabab
    A ---+
    |    |
    a    X
         |
         A ---+
         |    |
         a    X ---+
              |    |
              B    X
              |    |
              b    A ---+
                   |    |
                   a    X ---+
                        |    |
                        B    X
                        |    |
                        b    e

### Exercício 4

Refatoração de Gramática: Ambiguidade

G = { S -> a A B | a B A
      A -> b | c S
      B -> d | e S }

G = { S -> a X
      X -> A B | B A
      A -> b | c S
      B -> d | e S }


### Exercício 5

Refatoração de Gramática: Recursão à Esquerda

G = { E  -> E + T | T
      T  -> T * F | F
      F  -> a | b | c }

G = { E  -> T EX
      E' -> e | + T E'
      T  -> F T'
      T' -> e | * F T'
      F  -> a | b | c }

Programa:

    void E() {
        T(); E'();
    }

    void EX() {
        case 'e' : finish(); break;
        case '+' : next(); T(); E'(); break;
    }

    void T() {
        F(); T'();
    }

    void TX() {
        case 'e' : finish(); break;
        case '*' : next(); F(); T'(); break;
    }

    void F() {
        next();
    }

### Exercício 4.1 (pág 69 Tomasz)

G = { E -> T + E | T
      T -> F * T | F
      F -> a | b | ( E ) }

Teste de entrada para `a + b * a`

         E
         |
    T -- + -- E
    |         |
    F         T
    |         |
    a    F -- * -- T
         |         |
         b         F
                   |
                   a

### Exercício 4.5 (pág 69 Tomasz)

G = { A -> B - A | B
      B -> B u C | C
      C -> C ^ D | D
      D -> E | ~ E
      E -> a | ( A ) }

Eliminação da Recursão à Esquerda

G = { A  -> B - A | B
      B  -> C B1
      B1 -> e | u C B1
      C  -> D C1
      C1 -> e | ^ D C1
      D  -> E | ~ E
      E  -> a | ( A ) }

Teste de Entrada: `a u ~ a - a - ~ a`

    A -------------------+----+
    |                    |    |
    B ---+               -    A ---+----+
    |    |                    |    |    |
    C    B1 ---+              B    -    B
    |    |     |              |         |
    D    u     C              C         B1
    |          |              |         |
    E          D ---+         D         C
    |          |    |         |         |
    a          ~    E         E         D ---+
                    |         |         |    |
                    a         a         ~    E
                                             |
                                             a

## Analisador Sintático Ascendente (SLR)

Referência:

* LR Parsing -- Etec, Johnson
  ACM Computing Services, 1974

Tem-se uma gramática ambígua quando duas árvores podem ser geradas a partir da
mesma entrada. Para que o analisador funcione, é necessário uma gramática não
ambígua.

Aplicar roteiro:

1. Construção da árvore
2. Funcionamento do LR
3. Construção das tabelas

`@` representa um novo estado

### Exercício 1

Gramática:

G = { L' -> L #     (0)
      L  -> L , E   (1)
      L  -> E       (2)
      E  -> a       (3)
      E  -> b }     (4)


Estados:

T( L',   ) = { [ L' -> . L # ],
               [ L  -> . L , E ]
               [ L  -> . E ]
               [ E  -> . a ]
               [ E  -> . b ] } = e0 @

T( e0, L ) = { [ L' -> L . # ]
               [ L  -> L . , E ] } = e1 @

T( e0, E ) = { [ L  -> E . ] } = e2 @

T( e0, a ) = { [ E  -> a . ] } = e3 @

T( e0, b ) = { [ E  -> b . ] } = e4 @

T( e1, , ) = { [ L  -> L , . E ]
               [ E  -> . a ]
               [ E  -> . b ] } = e5 @

T( e5, E ) = { [ L  -> L , E . ] } = e6 @

T( e5, a ) = e3

T( e5, b ) = e4


Tabela de Ações e Desvios:

         L     E     a     b     ,     #
    0    1     2     E     E
    1                            E,5   A
    2                            r2    r2
    3                            r3    r3
    4                            r4    r4
    5          6     3     4
    6                            r1    r1


Teste de entrada:

    a , b , a #

                   +-------- L'
                   |         |
         +-------- L ---+    |
         |         |    |    |
    +--- L ---+    |    |    |
    |    |    |    |    |    |
    L    |    |    |    |    |
    |    |    |    |    |    |
    E    |    E    |    E    |
    |    |    |    |    |    |
    a    ,    b    ,    a    #


### Exercício 2

Gramática:

G = { E' -> E #      (0)
      E  -> + E E    (1)
      E  -> * E E    (2)
      E  -> a        (3)
      E  -> b }      (4)


Estados:

T( E',   ) = { [ E' -> . E # ]
               [ E  -> . + E E ]
               [ E  -> . * E E ]
               [ E  -> . a ]
               [ E  -> . b ] } = e0 @

T( e0, E ) = { [ E  -> E . # ] } = e1 @

T( e0, + ) = { [ E  -> + . E E ]
               [ E  -> . + E E ]
               [ E  -> . * E E ]
               [ E  -> . a ]
               [ E  -> . b ] } = e2 @

T( e0, * ) = { [ E  -> * . E E ]
               [ E  -> . + E E ]
               [ E  -> . * E E ]
               [ E  -> . a ]
               [ E  -> . b ] } = e3 @

T( e0, a ) = { [ E  -> a . ] } = e4 @

T( e0, b ) = { [ E  -> b . ] } = e5 @

T( e2, E ) = { [ E  -> + E . E ]
               [ E  -> . + E E ]
               [ E  -> . * E E ]
               [ E  -> . a ]
               [ E  -> . b ] } = e6 @

T( e2, + ) = e2

T( e2, * ) = e3

T( e2, a ) = e4

T( e2, b ) = e5

T( e3, E ) = { [ E  -> * E . E ]
               [ E  -> . + E E ]
               [ E  -> . * E E ]
               [ E  -> . a ]
               [ E  -> . b ] } = e7 @

T( e3, + ) = e2

T( e3, * ) = e3

T( e3, a ) = e4

T( e3, b ) = e5

T( e6, E ) = { [ E  -> + E E . ] } = e8 @

T( e6, + ) = e2

T( e6, * ) = e3

T( e6, a ) = e4

T( e6, b ) = e5

T( e7, E ) = { [ E  -> * E E . ] } = e9 @

T( e7, + ) = e2

T( e7, * ) = e3

T( e7, a ) = e4

T( e7, b ) = e5


Tabela de Desvios:

        E    +    *    a    b    #
    0   1    2    3    4    5
    1                            A
    2   6    2    3    4    5
    3   7    2    3    4    5
    4
    5
    6   8    2    3    4    5
    7   9    2    3    4    5
    8
    9


Tabela de Ações:

        +    *    a    b    #
    0   E    E    E    E
    1                       A
    2   E    E    E    E
    3   E    E    E    E
    4   r3   r3   r3   r3
    5   r4   r4   r4   r4
    6   E    E    E    E
    7   E    E    E    E
    8   r1   r1   r1   r1
    9   r2   r2   r2   r2


Teste de Entrada: `+ * a + b a a #`

    E' --------------------------------+
    |                                  |
    E --------+-------------------+    |
    |         |                   |    |
    |    +--- E --------+         |    |
    |    |    |         |         |    |
    |    |    |    +--- E ---+    |    |
    |    |    |    |    |    |    |    |
    |    |    E    |    E    E    E    |
    |    |    |    |    |    |    |    |
    +    *    a    +    b    a    a    #


### Exercício 3

Gramática:

G = { G' -> E #     (0)
      E  -> E + T   (1)
      E  -> T       (2)
      T  -> T * F   (3)
      T  -> F       (4)
      F  -> ( E )   (5)
      F  -> a }     (6)


Estados:

T( G',   ) = { [ G' -> . E # ]
               [ E  -> . E + T ]
               [ E  -> . T ]
               [ T  -> . T * F ]
               [ T  -> . F ]
               [ F  -> . ( E ) ]
               [ F  -> . a ] } = e0 @

T( e0, E ) = { [ E  -> E . # ]
               [ E  -> E . + T ] } = e1 @

T( e0, T ) = { [ E  -> T . ]
               [ T  -> T . * F ] } = e2 @

T( e0, F ) = { [ T  -> F . ] } = e3 @

T( e0, a ) = { [ F  -> a . ] } = e4 @

T( e0, ( ) = { [ F  -> ( . E ) ]
               [ E  -> . E + T ]
               [ E  -> . T ]
               [ T  -> . T * F ]
               [ T  -> . F ]
               [ F  -> . ( E ) ]
               [ F  -> . a ] } = e5 @

T( e1, + ) = { [ E  -> E + . T ]
               [ T  -> . T * F ]
               [ T  -> . F ]
               [ F  -> . ( E ) ]
               [ F  -> . a ] } = e6 @

T( e2, * ) = { [ T  -> T * . F ]
               [ F  -> . ( E ) ]
               [ F  -> . a ] } = e7 @

T( e5, E ) = { [ F  -> ( E . ) ]
               [ E  -> E . + T ] } = e8 @

T( e5, T ) = { [ E  -> T . ]
               [ T  -> T . * F ] } = e2

T( e5, F ) = e3

T( e5, a ) = e4

T( e5, ( ) = e5

T( e6, T ) = { [ E  -> E + T . ]
               [ T  -> T . * F ] } = e9 @

T( e6, F ) = e3

T( e6, a ) = e4

T( e6, ( ) = e5

T( e7, F ) = { [ T  -> T * F . ] } = e10 @

T( e7, a ) = e4

T( e8, ) ) = { [ F -> ( E ) . ] } = e11 @

T( e8, + ) = e6

T( e9, * ) = e7


Tabela de Desvios:

         E    T    F    a    +    *    (    )    #
    0    1    2    3    4              5
    1                        6
    2                             7
    3
    4
    5    8    2    3    4              5
    6         9    3    4              5
    7             10    4
    8                        6             11
    9                             7
    10
    11


Tabela de Ações:

         a    +    *    (    )    #
    0    E              E
    1         E                   A
    2        r2 r2,E   r2   r2   r2
    3        r4   r4   r4   r4   r4
    4
    5    E
    6    E              E
    7    E              E
    8         E              E
    9        r1 r1,E   r1   r1   r1
    10       r3   r3   r3   r3   r3
    11       r5   r5   r5   r5   r5

Conflito no estado 2

  [ E -> T . ] e [ E -> T . * F ]

Conflito no estado 9

  [ E  -> E + T . ] e [ T  -> T . * F ]

Para eliminar os conflitos pode-se aplicar SLR(1) nos pontos
especificados e não realizar a redução caso o próximo símbolo
seja um símbolo terminal. A tabela resultante neste caso é:

         a    +    *    (    )    #
    0    E              E
    1         E                   A
    2        r2    E   r2   r2   r2
    3        r4   r4   r4   r4   r4
    4
    5    E
    6    E              E
    7    E              E
    8         E              E
    9        r1    E   r1   r1   r1
    10       r3   r3   r3   r3   r3
    11       r5   r5   r5   r5   r5
