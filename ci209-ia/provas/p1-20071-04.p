e_primo(N) :-
    e_primo2(N, N).
e_primo2(_, 2).
e_primo2(N, N1) :-
    N2 is N1 - 1,
    MOD is N mod N2,
    MOD > 0,
    e_primo2(N, N2).

soma_de_2_naturais(N, L) :-
    soma_de_2_naturais2(0, SR, N, L),
    write(SR), nl,
    SR =:= N.
soma_de_2_naturais2(S, _, N, [H|T]) :-
    e_primo(H),
    S1 is S + H,
    soma_de_2_naturais2(S1, S1, N, T).
