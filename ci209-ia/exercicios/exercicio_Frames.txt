
Exerc�cio de Intelig�ncia Artificial sobre Frames
=================================================

� dada a base de Frames abaixo onde: (1) o atributo "instancia"
representa um canal de racioc�nio por heran�a; (2) "i_instancia"
representa a liga��o inversa; (3) "real" e "default" representam duas
facetas de valor do atributo de um frame. Utilize a linguagem e o
pacote de ger�ncia de Frames apresentado em:

http://www.inf.ufpr.br/alexd/ia/frames.txt

Com ele, aloque a mem�ria de representa��o da base de Frames abaixo e
depois responda �s perguntas que seguem.

 [passaro [instancia [real [animal] ]]
         [i_instancia [real [pardal]]]
         [sangue [real [quente]]]
         [locomocao [real [voa]]]]
 [leao [instancia [real [mamifero]]]
         [gestacao [real [108 dias]]]]
 [jose [instancia [real [humano]]]
          [gestacao [real [7 meses]]]
          [gosta_de [real [tomate]]]]
 [maria [instancia [real [elefante]]]
          [gosta_de [real [tomate]]]]
 [humano [instancia [real [mamifero] ]]
          [num_pernas [default [2]]]
          [gestacao [real [9 meses]]]
          [i_instancia [real [jose]]]]
 [jumbo [instancia [real [elefante]]]
          [gosta_de  [real [amendoim]]]]
 [pardal [instancia [real [passaro]]]]
 [animal [i_instancia [real [mamifero] [reptil] [passaro]]]]
 [mamifero [i_instancia [real [leao] [girafa] [elefante] [humano]]]
           [instancia [real [animal]]]
           [num_pernas [default [4]]]
           [sangue [real [quente]]]
           [locomocao [real [anda]]]]
 [girafa [instancia [real [mamifero]]]
            [gestacao [real [450 dias]]]]
 [reptil [instancia [real [animal]]]
            [i_instancia [real [crocodilo]]]
            [sangue [real [frio]]]
            [locomocao [real [anda]]]]
 [elefante [instancia [real [mamifero]]]
             [i_instancia [real [jumbo] [maria]]]]
             [gestacao [real [22 meses]]]]
 [crocodilo [instancia [real [reptil]]]]


1- Por racioc�nio de "default" combinado com o de "heranca", qual deve
ser o valor do atributo "num_pernas" do Frame "maria" ?

2- Por racioc�nio de "criterialidade", diga o nome do Frame
correspondente ao seguinte conjunto de pares atributo/valor:

   <gosta_de, amendoim>
   <gestacao, 22 meses>
