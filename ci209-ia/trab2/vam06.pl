%
% Vinicius Massuchetto (GRR 20063784 - vam06)
%
% Trabalho 2 de Inteligência Artificial
% Implementação de Sistemas Especialistas
%
% Emycin
%
% Para rodar da linha de comando:
%   $ swipl -s vam06.pl -g "diagnostico( <atributo> )" -t halt.
%
% Para rodar da linha de comando com monitoramento:
%   $ swipl -s vam06.pl -g "assertz( monitoramento ), diagnostico( <atributo> )" -t halt.
%
% Para rodar do shell do Prolog:
%   ?- consult( vam06 ).
%   ?- diagnostico( <atributo> ).
%
% Para rodar com monitoramento das ações:
%   ?- consult( vam06 ).
%   ?- assertz( monitoramento ).
%   ?- diagnostico( <atributo> ).

% Base de regras fornecido no enunciado do trabalho

base_de_regras( [
    [[idade, jovem], [especie, humana], [sexo, masculino], -> , 1.0, [chamado, menino]],
    [[idade, jovem], [especie, humana], [sexo, feminino], -> , 1.0, [chamado, menina]],
    [[idade, adulto], [especie, humana], [sexo, masculino], -> , 1.0, [chamado, homem]],
    [[idade, adulto], [especie, humana], [sexo, feminino], -> , 1.0, [chamado, mulher]],
    [[idade, jovem], [especie, cao], ->, 1.0, [chamado, filhote]],
    [[idade, adulto], [especie, cao], [sexo, feminino], ->, 1.0, [chamado, cadela]],
    [[idade, adulto], [especie, cao], ->, 0.5, [chamado, cao]],
    [[pernas, 2], ->, 1.0, [especie, humana]],
    [[pernas, 4], ->, -1.0, [especie, humana]],
    [[pernas, 4], ->, 0.5, [especie, cao]] ,
    [[altura, baixa], ->, 0.5, [especie, cao]],
    [[altura, alta], ->, 0.5, [especie, humana]]
] ).

% Entrada principal do programa

diagnostico( Atributo ) :-
    retractall( base_de_fatos( _ ) ),
    assert( base_de_fatos( [ ] ) ),
    retractall( atributo( _ ) ),
    assert( atributo( Atributo ) ),
    mensagem_descobrindo( Atributo ),
    base_de_regras( Regras ),
    verifica_regras( Regras ).

% Limiar de exatidão utilizada para aceitar as hipóteses resultantes

limiar_de_exatidao( 0.2 ).

% Mensagens de monitoramento que podem ser mostradas ao longo da execução. Para
% habilitar a exibição é necessário que o predicado nulário `monitoramento`
% esteja presente no programa:
%   ?- assertz( monitoramento ).

mensagem_descobrindo( Valor ) :-
    current_predicate( monitoramento/0 ),
    write('>> Descobrindo sobre '), write( Valor ), write( '.' ), nl.
mensagem_descobrindo( _ ).

mensagem_exatidao( Parametro, Valor, Exatidao ) :-
    current_predicate( monitoramento/0 ),
    write( '>> A exatidão de ' ),
    write( Parametro ), write( ' = ' ), write( Valor ),
    write( ' passou a ser '),
    write( Exatidao ), write( '.' ), nl,
    base_de_fatos( Fatos ),
    write( '>> Base de fatos: '), write( Fatos ), write( '.' ), nl.
mensagem_exatidao( _, _, _ ).

mensagem_regra_entra( Regra ) :-
    current_predicate( monitoramento/0 ),
    write( '>> Tentando a regra: ' ), write( Regra ), write( '.' ), nl.
mensagem_regra_entra( _ ).

mensagem_regra_sai( Atributo ) :-
    current_predicate( monitoramento/0 ),
    write( '>> Abandonando esta regra para ' ), write( Atributo ), write( '.' ), nl.
mensagem_regra_sai( _ ).

mensagem_fim( Atributo ) :-
    current_predicate( monitoramento/0 ),
    write( '>> Fim da execução para ' ), write( Atributo ),
    write( '.' ), nl, nl.
mensagem_fim( _ ).

% Predicados de propósito geral para manipulação de listas

% Pega o primeiro elemento de uma lista

primeiro( [ X | _ ], X ).

% Pega o último elemento de uma lista

ultimo( [ Item ], Item ).
ultimo( [ _ | Resto ], Ultimo ) :-
	ultimo( Resto, Ultimo ).

% Verifica se um elemento é membro de uma lista

membro( X,[ X | _ ] ).
membro( X,[ _ | Z ] ) :-
    membro( X, Z ).

% Seleciona e remove um elemento de uma lista

seleciona( H, [ H | T ], T ).
seleciona( X, [ H | T ], [ H | T1 ] ) :-
    seleciona( X, T, T1 ).

% Concatena duas listas

concatena( [ ], Ys, Ys ).
concatena( [ X | Xs ], Ys, [ X | Zs ] ) :-
    concatena( Xs, Ys, Zs ).

% Seleciona a exatidão de uma lista (no caso, uma regra de inferência)

exatidao( [ Primeiro | Resto ], Exatidao ) :-
    ( float( Primeiro ), Exatidao = Primeiro );
    exatidao( Resto, Exatidao ).

% Cálculo central do Emycin:
%
%   Se antiga > 0 e nova > 0 Entao
%       resultante = antiga + nova - (antiga * nova)
%   Se antiga < 0 e nova < 0 Entao
%       resultante = antiga + nova + (antiga * nova)
%   Senao
%       (antiga + nova) / (1 - min(abs(antiga),abs(nova)))
%   Fim-Se

calcula_exatidao( A, N, R ) :-
    A >= 0,
    N >= 0,
    R is A + N - ( A * N ).
calcula_exatidao( A, N, R ) :-
    A < 0,
    N < 0,
    R is A + N + ( A * N ).
calcula_exatidao( A, N, R ) :-
    R is ( A + N ) / ( 1 - min( abs( A ), abs( N ) ) ).

% Atualiza um fato na base de fatos

atualiza_fato( Fatos, Parametro, Valor, Exatidao ) :-
    membro( [ Parametro, Valores ], Fatos ),
    membro( [ Valor, E ], Valores ),
    calcula_exatidao( E, Exatidao, ExatidaoNova ),
    seleciona( [ Valor, _ ], Valores, Valores2 ),
    seleciona( [ Parametro, _ ], Fatos, Fatos2 ),
    concatena( [ [ Valor, ExatidaoNova ] ], Valores2, ValoresNovos ),
    concatena( [ [ Parametro, ValoresNovos ] ], Fatos2, FatosNovos ),
    retractall( base_de_fatos( _ ) ),
    assert( base_de_fatos( FatosNovos ) ),
    mensagem_exatidao( Parametro, Valor, ExatidaoNova ).

% Insere um fato na base de fatos, e se este fato já existir, aplica
% o cálculo de prabilidades do Emycin nos dois valores

insere_fato( [ ], Parametro, Valor, Exatidao ) :-
    retractall( base_de_fatos( _ ) ),
    FatosNovos = [ [ Parametro, [ [ Valor, Exatidao ] ] ] ],
    assert( base_de_fatos( FatosNovos ) ),
    mensagem_exatidao( Parametro, Valor, Exatidao ).
insere_fato( Fatos, Parametro, Valor, Exatidao ) :-
    atualiza_fato( Fatos, Parametro, Valor, Exatidao ).
insere_fato( Fatos, Parametro, Valor, Exatidao ) :-
    membro( [ Parametro, Valores ], Fatos ) ->
    (   seleciona( [ Parametro, Valores ], Fatos, Fatos2 ),
        concatena( Valores, [ [ Valor, Exatidao ] ], Valores2 ),
        concatena( [ [ Parametro, Valores2 ] ], Fatos2, FatosNovos ),
        retractall( base_de_fatos( _ ) ),
        assert( base_de_fatos( FatosNovos ) ),
        mensagem_exatidao( Parametro, Valor, Exatidao )
    );
    concatena( Fatos, [ [ Parametro, [ [ Valor, Exatidao ] ] ] ], FatosNovos ),
    retractall( base_de_fatos( _ ) ),
    assert( base_de_fatos( FatosNovos ) ),
    mensagem_exatidao( Parametro, Valor, Exatidao ).

% Perguntas para o usuário são feitas da seguinte forma:
%
%   1. Pergunta-se um valor ou exatidão de um atributo
%   2. Se a resposta for 'porque', exibe a regra atual e pergunta-se sobre o
%      atributo novamente
%   3. Se a resposta não for 'porque', continua a execução

% Pergunta o valor de um atributo para o usuário

pergunta_valor( Regra, Condicao, Valor ) :-
    primeiro( Condicao, Parametro ),
    write( 'Me informe sobre ' ), write( Parametro ), write( ' ? ' ),
    read( ValorLido ),
    pergunta_valor2( Regra, Condicao, ValorLido, ValorNovo ),
    Valor = ValorNovo.
pergunta_valor2( Regra, Condicao, ValorLido, ValorNovo ) :-
    ValorLido = 'porque',
    ultimo( Regra, Implicacao ),
    write( 'Tentando usar a regra: ' ),
    write( Condicao ), write( ' -> ' ), write( Implicacao ), nl,
    pergunta_valor( Regra, Condicao, ValorNovo ).
pergunta_valor2( _, _, Valor, Valor ).

% Pergunta a exatidão de um atributo para o usuário

pergunta_exatidao( Regra, Condicao, Exatidao ) :-
    write( 'Qual é o seu grau de exatidão (um numero entre -1 e 1) ? '),
    read( ExatidaoLida ),
    pergunta_exatidao2( Regra, Condicao, ExatidaoLida, ExatidaoNova ),
    Exatidao = ExatidaoNova.
pergunta_exatidao2( Regra, Condicao, ExatidaoLida, ExatidaoNova ) :-
    ExatidaoLida = 'porque',
    ultimo( Regra, Implicacao ),
    write( 'Tentando usar a regra: ' ),
    write( Condicao ), write( ' -> ' ), write( Implicacao ), nl,
    pergunta_exatidao( Regra, Condicao, ExatidaoNova ).
pergunta_exatidao2( _, _, Exatidao, Exatidao ).

% Pergunta os valores de um atributo quando se deparar com uma condição

pergunta_condicao( Regra, Condicao, Valor, Exatidao ) :-
    primeiro( Condicao, Parametro ),
    mensagem_descobrindo( Parametro ),
    nl,
    pergunta_valor( Regra, Condicao, Valor ),
    pergunta_exatidao( Regra, Condicao, Exatidao ),
    nl.

% Iteração dentre as perguntas necessárias para as regras

pergunta_fatos( _, [ Condicao | _ ] ) :-
    Condicao = '->'.
pergunta_fatos( _, [ Condicao | _ ] ) :-
    base_de_fatos( Fatos ),
    primeiro( Condicao, Parametro ),
    membro( [ Parametro, _ ], Fatos ).
pergunta_fatos( Regra, [ Condicao | Resto ] ) :-
    pergunta_condicao( Regra, Condicao, Valor, Exatidao ),
    base_de_fatos( Fatos ),
    primeiro( Condicao, Parametro ),
    insere_fato( Fatos, Parametro, Valor, Exatidao ),
    pergunta_fatos( Regra, Resto ).

% Iteração dentre as condições das regras e se houver necessidade, faz as
% perguntas para o usuário. Retira também a menor exatidão fornecida dentre as
% condições presentes, e não utiliza a regra se a menor exatidão fornecida for
% menor do que o limiar de exatidão exigido por `limiar_de_exatidao`.

verifica_condicoes( Regra, [ Condicao | _ ], Menor ) :-
    Condicao = '->',
    verifica_condicoes2( Regra, [ Condicao | _ ], Menor ).
verifica_condicoes( _, [ Condicao | _ ], _ ) :-
    base_de_fatos( Fatos ),
    primeiro( Condicao, Parametro ),
    ultimo( Condicao, Valor ),
    membro( [ Parametro, Valores ], Fatos ),
    not( membro( [ Valor, _ ], Valores ) ).
verifica_condicoes( Regra, [ Condicao | Resto ], Menor ) :-
    base_de_fatos( Fatos ),
    primeiro( Condicao, Parametro ),
    ultimo( Condicao, Valor ),
    membro( [ Parametro, Valores ], Fatos ),
    membro( [ Valor, Exatidao ], Valores ),
    ( float( Menor ) ->
        Menor is Menor
    ;   Menor is 1
    ),
    ( Exatidao < Menor ->
        MaisMenor is Exatidao
    ;   MaisMenor is Menor
    ),
    verifica_condicoes( Regra, Resto, MaisMenor ).

verifica_condicoes2( _, _, Menor ) :-
    limiar_de_exatidao( Limiar ),
    Menor < Limiar.
verifica_condicoes2( Regra, _, Menor ) :-
    exatidao( Regra, Exatidao ),
    ExatidaoRegra is Exatidao * Menor,
    base_de_fatos( Fatos ),
    ultimo( Regra, Implicacao ),
    primeiro( Implicacao, Atributo ),
    ultimo( Implicacao, AtributoValor ),
    insere_fato( Fatos, Atributo, AtributoValor, ExatidaoRegra ).

% Iteração principal dentre as regras. Utiliza somente as regras que possuem a
% implicação de interesse segundo o atributo fornecido em `diagnostico`

verifica_regras( [ ] ) :-
    base_de_fatos( Fatos ),
    atributo( Atributo ),
    nl,
    ( membro( [ Atributo, Hipoteses ], Fatos ) ->
        write( 'Hipóteses: ' ), write( Hipoteses )
    ;   write( 'Não foi possível concluir nenhuma hipótese.' )
    ),
    nl, nl,
    mensagem_fim( Atributo ).
verifica_regras( [ Regra | Resto ] ) :-
    atributo( Atributo ),
    ultimo( Regra, Implicacao ),
    ( membro( Atributo, Implicacao ) ->
        mensagem_regra_entra( Regra ),
        pergunta_fatos( Regra, Regra ),
        verifica_condicoes( Regra, Regra, _ ),
        mensagem_regra_sai( Atributo ),
        verifica_regras( Resto )
    ;   verifica_regras( Resto )
    ).
