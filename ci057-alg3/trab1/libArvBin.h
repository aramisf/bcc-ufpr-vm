#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define OK    1
#define ERRO -1

/* Estruturas fornecidas */

typedef long TipoChave;
typedef struct Registro {
	TipoChave Chave;
} Registro;

typedef struct No *Apontador;
typedef struct No {
	Registro  Reg;
	Apontador Esq;
	Apontador Dir;
	int       AltEsq;
	int       AltDir;
} No;

extern Apontador nodoNull;

/* Funções fornecidas */

Apontador criaNo     ( Registro r, Apontador esq, Apontador dir );
Apontador Pesquisa   (TipoChave k, Apontador p);
void      Inicializa (Apontador *raiz);
void      imprimeArv (Apontador p);

void rotacaoEsquerda (Apontador *p);
void rotacaoDireita  (Apontador *p);

/* Funções implementadas */

void RotDirComAlt (Apontador *p);
void RotEsqComAlt (Apontador *p);

Apontador noEsq (Apontador p);
Apontador noDir (Apontador p);
Apontador noSuc (Apontador p);

Apontador Busca  (Registro r, Apontador  p);
int       Insere (Registro r, Apontador *p);
int       Remove (Registro x, Apontador  raiz, Apontador *p, Apontador *pai);


