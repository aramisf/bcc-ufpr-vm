#include "libArvBin.h"

int main (int argc, char *argv[]) {

	No *Dicionario;
	Registro x;

	int status = 0;
	char opt;

	Inicializa (&Dicionario);

	while (scanf("%c %ld\n", &opt, &(x.Chave)) != EOF) {

		printf ("%c %ld\n", opt, x.Chave);

		if (opt == 'i') {
			status = Insere (x, &Dicionario);
			imprimeArv(Dicionario);

		} else if (opt == 'b') {
			Busca (x, Dicionario);

		} else if (opt == 'r') {
			Remove (x, Dicionario, &Dicionario, &nodoNull);
			imprimeArv(Dicionario);
		}

		if (status == ERRO)
			printf ("ERRO: %ld ja' existe\n", x.Chave);

		printf ("\n");

	}

	return 0;

}
