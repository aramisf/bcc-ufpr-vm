
#ifndef N_MIN
	#define N_MIN 0
#endif

#ifndef N_MAX
	#define N_MAX 31
#endif

#ifndef N_BITS
	#define N_BITS 4
#endif

struct _patr_no { int valor, bit; struct _patr_no *esq, *dir; };
typedef struct _patr_no *patr_no;

int patr_digito (int, int);
void patr_imprimir (patr_no);
void patr_buscar (patr_no, int);
patr_no patr_criar (int, int);
patr_no patr_dividir (patr_no, patr_no, int, int);
patr_no patr_inserir (patr_no, int);
