#ifndef N_MIN
	#define N_MIN 0
#endif

#ifndef N_MAX
	#define N_MAX 31
#endif

#ifndef N_BITS
	#define N_BITS 4
#endif

struct _trie_no { int valor; struct _trie_no *esq, *dir; };
typedef struct _trie_no *trie_no;

int trie_digito (int, int);
int trie_no_valido (trie_no);
trie_no trie_criar_no (int);
trie_no trie_dividir_no (trie_no, trie_no, int);
trie_no trie_inserir (trie_no, int, int);
void trie_imprimir (trie_no);
void trie_buscar (trie_no, int, int);
