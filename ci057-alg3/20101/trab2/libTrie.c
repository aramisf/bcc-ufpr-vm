#include <stdio.h>
#include <stdlib.h>

#include "libTrie.h"

int trie_digito (int dec, int bit) {
	int i = 0, bin[N_BITS];

	while (i <= N_BITS) {
		bin[i] = dec % 2;
		// Se for o bit desejado
		if (N_BITS - i == bit)
			return bin[i];
		dec /= 2;
		i++;
	}

	// Imprime o binário correspondente
	if (bit == -1)
		while (i > 0)
			printf("%d", bin[--i]);

	return -1;
}

trie_no trie_criar (int valor) {
	trie_no no = (trie_no) malloc (sizeof(struct _trie_no));
	no->valor = valor;
	no->esq = no->dir = NULL;
	return no;
}

trie_no trie_dividir (trie_no no1, trie_no no2, int i) {

	trie_no novo_no = trie_criar (-1);

	int d1 = trie_digito (no1->valor, i);
	int d2 = trie_digito (no2->valor, i);

	if (d1 == d2) {
		if (d1 == 0)
			novo_no->esq = trie_dividir (no1, no2, ++i);
		else
			novo_no->dir = trie_dividir (no1, no2, ++i);
	} else {
		if (d1 == 0) {
			novo_no->esq = no1;
			novo_no->dir = no2;
		} else {
			novo_no->esq = no2;
			novo_no->dir = no1;
		}
	}

	return novo_no;
}

trie_no trie_inserir (trie_no no, int valor, int i) {

	if (no == NULL)
		return trie_criar (valor);

	if (no->valor == valor)
		return no;

	if (no->valor > 0)
		return trie_dividir (trie_criar (valor), no, i);
	else {
		if (trie_digito(valor, i) == 0)
			no->esq = trie_inserir (no->esq, valor, ++i);
		else
			no->dir = trie_inserir (no->dir, valor, ++i);
	}

	return no;
}

void trie_imprimir (trie_no no) {
	if (no) {
		printf ("(");
		trie_imprimir (no->esq);

		if (no->valor > 0)
			printf ("%d", no->valor);

		trie_imprimir (no->dir);
		printf (")");
	}
}

void trie_buscar (trie_no no, int valor, int i) {

	int j = 0;

	while (j <= i)
		printf("%d", trie_digito(valor, j++));
	printf(", ");

	if (no) {
		if (no->valor >= 0) {
			printf("%d\n", no->valor);
			return;

		} else {

			if (trie_digito(valor, i) == 0)
				trie_buscar (no->esq, valor, ++i);
			else
				trie_buscar (no->dir, valor, ++i);
		}
	}
}

int trie_no_valido (trie_no no) {
	if (no)
		if (no->valor >= 0)
			return 1;
	return 0;
}
