void imprimirNo (Apontador p) {
	if (p != NULL) {
		printf("Alt:%d\tChave: (%ld) ==> ", p->altura, p->Reg.Chave);
		if (p->Esq != NULL)
			printf("FE: %ld\t", (p->Esq)->Reg.Chave);
		if (p->Dir != NULL)
			printf("FD: %ld\t", (p->Dir)->Reg.Chave);
		printf("\n");
	}
}

int Balanco (Apontador p) {
	if ((p == NULL) || (p->Esq == NULL && p->Dir == NULL))
		return 0;
	else
		return p->balanceamento;
}

Apontador RotEsqComBal (Apontador p) {
	Apontador t;
	t = p->Esq;
	p->Esq = t->Dir;
	t->Dir = p;

	p->balanceamento = Balanco(p->Esq) - Balanco(p->Dir);
	t->balanceamento = Balanco(t->Esq) - p->balanceamento;

	return t;
}

Apontador RotDirComBal (Apontador p) {
	Apontador t;
	t = p->Dir;
	p->Dir = t->Esq;
	t->Esq = p;

	p->balanceamento = Balanco(p->Esq) - Balanco(p->Dir);
	t->balanceamento = Balanco(t->Dir) - p->balanceamento;

	return t;
}

int Inserir (Registro r, Apontador *p,
	Apontador *paiP, Apontador *anc) {

	Apontador q;

	if (*p == NULL) {

		*p = criaNo (r);
		q = *anc;

		while (q != *p) {
			if (r.Chave < q->Reg.Chave) {
				q->balanceamento += 1;
				q = esquerda (q);
			} else {
				q->balanceamento -= 1;
				q = direita (q);
			}
		}

		if ((*anc)->balanceamento < 0) {

			// Rotação única para a direita
			if (r.Chave > q->Dir->Reg.Chave) {
				q = RotDirComBal (q);

			// Rotação dupla esquerda-direita
			} else {
				q->Dir = RotEsqComBal (q->Dir);
				q = RotDirComBal (q);
			}

		} else if ((*anc)->balanceamento > 0) {

			// Rotação única para a esquerda
			if (r.Chave < q->Esq->Reg.Chave) {
				q = RotEsqComBal (q);

			// Rotação dupla direita-esquerda
			} else {
				q->Esq = RotDirComBal (q->Esq);
				q = RotEsqComBal (q);
			}
		}

		return 1;

	} else {

		if ((*p)->balanceamento != 0 )
			anc = p;

		if (r.Chave == (*p)->Reg.Chave)
			return ERRO;

		if (r.Chave < (*p)->Reg.Chave)
			return Insere (r, &(*p)->Esq, p, anc);
		else
			return Insere (r, &(*p)->Dir, p, anc);
	}
}

void carregarExemplo (int n, Apontador *p) {
	int i, s;
	Registro t;
	for (i = 0; i < n; i++) {
		t.Chave = rand()/10000000;
		s = Inserir (t, p, NULL, p);
	}
}

Apontador Buscar (TipoChave k, Apontador p) {
	if (p != NULL) {
		printf("%ld", p->Reg.Chave);

		if (k == p->Reg.Chave) {
			printf("\n");
			return p;

		} else if (k < p->Reg.Chave) {
			if (p->Esq != NULL)
				printf(",");
			return Buscar (k, esquerda(p));

		} else {
			if (p->Dir != NULL)
				printf(",");
			return Buscar (k, direita(p));
		}
	}
	return NULL;
}

Apontador menorFilho (Apontador p) {
	if (p == NULL)
		return NULL;
	if (p->Esq == NULL)
		return p;
	menorFilho (p->Esq);
	return NULL;
}

int Remover (TipoChave k, No** p) {
	No* t;
	int s;

	// Árvore vazia ou chave não existente
	if (*p == NULL)
		return 0;

	// Chave encontrada
	else if ((*p)->Reg.Chave == k) {

		// Sem nenhum filho
		if ((*p)->Esq == NULL && (*p)->Dir == NULL) {
			free (*p);
			*p = NULL;

		// Com filho esquerdo
		} else if ((*p)->Esq != NULL && (*p)->Dir == NULL) {
			t = (*p)->Esq;
			free (*p);
			*p = t;

		// Com filho direito
		} else if ((*p)->Esq == NULL && (*p)->Dir != NULL) {
			t = (*p)->Dir;
			free (*p);
			*p = t;

		// Com dois filhos
		} else if ((*p)->Esq != NULL && (*p)->Dir != NULL) {

			long i;
			if (!(t = menorFilho ((*p)->Dir)))
				t = (*p)->Esq;

			i = t->Reg.Chave;
			Remover (i, p);
			(*p)->Reg.Chave = i;
		}

		return 1;

	// Continua procurando
	} else if (k > (*p)->Reg.Chave) {
		s = Remover (k, &(*p)->Dir);

		// Realiza rotações no nó
		if (s != 1) {
			if ((*p)->balanceamento < 0) {

				// Rotação única para a direita
				if (k > (*p)->Dir->Reg.Chave) {
					*p = RotDirComBal (*p);

				// Rotação dupla esquerda-direita
				} else {
					(*p)->Dir = RotEsqComBal ((*p)->Dir);
					*p = RotDirComBal (*p);
				}

			} else if ((*p)->balanceamento > 0) {

				// Rotação única para a esquerda
				if (k < (*p)->Esq->Reg.Chave) {
					*p = RotEsqComBal (*p);

				// Rotação dupla direita-esquerda
				} else {
					(*p)->Esq = RotDirComBal ((*p)->Esq);
					*p = RotEsqComBal (*p);
				}
			}
		}

	} else {
		s = Remover (k, &(*p)->Esq);

		// Realiza rotações no nó
		if (s != 1) {
			if ((*p)->balanceamento < 0) {

				// Rotação única para a direita
				if (k > (*p)->Dir->Reg.Chave) {
					*p = RotDirComBal (*p);

				// Rotação dupla esquerda-direita
				} else {
					(*p)->Dir = RotEsqComBal ((*p)->Dir);
					*p = RotDirComBal (*p);
				}

			} else if ((*p)->balanceamento > 0) {

				// Rotação única para a esquerda
				if (k < (*p)->Esq->Reg.Chave) {
					*p = RotEsqComBal (*p);

				// Rotação dupla direita-esquerda
				} else {
					(*p)->Esq = RotDirComBal ((*p)->Esq);
					*p = RotEsqComBal (*p);
				}
			}
		}

	}

	return 0;

}
