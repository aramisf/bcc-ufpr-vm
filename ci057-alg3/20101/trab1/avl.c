#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define OK    1
#define ERRO -1

/* Biblioteca com as funções fornecidas */
#include "avl.h"

/* Biblioteca com as funções implementadas */
#include "avl-tree.h"

int main(int argc, char *argv[]) {
	int s;
	No *d;
	Registro x;
	char c,op;

	Inicializa (&d);

	// Lê parâmetros
	while ((c = getopt (argc, argv, "t")) != -1)
	switch (c) {
		// Inserção randômica para teste
		case 't':
			carregarExemplo (10, &d);
			imprimeArv (d);
			printf("\n");
		break;
	}
	
	if (argv[1]) {
		printf("Modo de uso: \"./avl < entrada > saida\"\n");
		exit(1);
	}

	// Começa o sequenciamento de operações
	while (scanf("%c %ld\n", &op, &(x.Chave)) != EOF) {

		printf("%c %ld\n", op, (x.Chave));

		if (op == 'i') {
			s = Inserir (x ,&d, NULL, &d);
			imprimeArv (d);
		}

		if (op == 'r') {
			Remover (x.Chave, &d);
			imprimeArv (d);
		}

		if (op == 'b') {
			Buscar (x.Chave, d);
		}

		printf("\n");

	}

	printf("\n");
	return 0;

}
