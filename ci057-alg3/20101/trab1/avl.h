typedef long TipoChave;
typedef struct Registro {
  TipoChave Chave;
  /* outros componentes */
} Registro;

typedef struct No *Apontador;
typedef struct No {
  Registro Reg;
  Apontador Esq, Dir;
  int balanceamento;
  int altura;
} No;

Apontador esquerda( Apontador p ){
  if (p != NULL)
    return p->Esq;
  return NULL;
}

Apontador direita( Apontador p ){
  if (p != NULL)
    return p->Dir;
  return NULL;
}

Apontador criaNo( Registro r ){
  Apontador p;

  p = (Apontador)malloc(sizeof(No));
  p->Reg = r;
  p->Esq = NULL;
  p->Dir = NULL;
  p->balanceamento = 0;
  return p;
}


void imprimeArv(Apontador p){

  if( p == NULL )
    printf("()");
  else{
    printf("(");
    imprimeArv( esquerda(p) );
    printf("% ld[%d] ", p->Reg.Chave, p->balanceamento);
    imprimeArv( direita(p) );
    printf(")");
  }
  return;
}

Apontador Pesquisa(TipoChave k, Apontador p) {
  if (p == NULL || k == p->Reg.Chave)
    return p;
  if (k < p->Reg.Chave)
    return Pesquisa(k, esquerda(p));
  else
    return Pesquisa(k, direita(p));
}

void rotacaoDireita( Apontador *p ) {
  Apontador fe, netoDir;
  fe = esquerda( *p );
  netoDir = direita( fe );
  (*p)->Esq = netoDir;
  fe->Dir = *p;
  *p = fe;
  return;
}

void rotacaoEsquerda( Apontador *p ) {
  Apontador fd, netoEsq;
  fd = direita( *p );
  netoEsq = esquerda( fd );
  (*p)->Dir = netoEsq;
  fd->Esq = *p;
  *p = fd;
  return;
}

void balanceia( Apontador *p ) {
  Apontador  pai, f, n;

	pai = *p;

	/* esta' desbalanceado para a direita */
	if( pai->balanceamento < 0 ) {
		f = direita( *p );
		if ( f->balanceamento < 0) {
			rotacaoEsquerda( p );
			pai->balanceamento = 0;
			f->balanceamento = 0;
		} else {
			n = esquerda( f );
			rotacaoDireita( &(*p)->Dir );
			rotacaoEsquerda( p );
			if( n->balanceamento == 0 ) {
				pai->balanceamento = 0;
				f->balanceamento = 0;
			} else if ( n->balanceamento > 0 ) {
				pai->balanceamento = 0;
				f->balanceamento = -1;
			} else{
				pai->balanceamento = 1;
				f->balanceamento = 0;
			}
			n->balanceamento = 0;
		}
	
	/* esta' desbalanceado para a esquerda */
	} else {
		f = esquerda( *p );
		if( f->balanceamento > 0) {
			rotacaoDireita( p );
			pai->balanceamento = 0;
			f->balanceamento = 0;
		} else {
			n = direita( f );
			rotacaoEsquerda( &(*p)->Esq );
			rotacaoDireita( p );
			if( n->balanceamento == 0 ){
				pai->balanceamento = 0;
				f->balanceamento = 0;
			} else if ( n->balanceamento > 0 ) {
				pai->balanceamento = -1;
				f->balanceamento = 0;
			} else {
				pai->balanceamento = 0;
				f->balanceamento = 1;
			}
			n->balanceamento = 0;
		}
	}
	
	return;
}
 
int Insere(Registro r, Apontador *p, Apontador *paiP, Apontador *anc){
  /* anc contem o no mais prox. da folha que pode ficar desbalanceado */

  Apontador q;

  if (*p == NULL){        /* cria no' e acerta balanceamento dos ancestrais */
     *p = criaNo (r);
     q = *anc;
     while( q != *p ){
       if( r.Chave < q->Reg.Chave ){
	 q->balanceamento += 1;
	 q = esquerda( q );
       }
       else{
	 q->balanceamento -= 1;
	 q = direita( q );
       }
     }
     if( (*anc)->balanceamento > 1 ||
	 (*anc)->balanceamento < -1){
       balanceia( anc );
     }
     return OK;
  }
  else {
    if( (*p)->balanceamento != 0 )
      anc = p;
    if( r.Chave == (*p)->Reg.Chave )    /* chave ja' existe */
      return ERRO;

    if (r.Chave < (*p)->Reg.Chave)
      return Insere(r, &(*p)->Esq, p, anc );
    else
      return Insere(r, &(*p)->Dir, p, anc);
  }
}

void Inicializa(Apontador *raiz){
  *raiz = NULL;
}
