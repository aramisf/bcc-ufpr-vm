#include "libPatricia.h"

void patriz_print         (node p);
void patriz_search        (node p, int key, int bit);
node patriz_search_node   (node p, int key, int bit, node parent);
node patriz_insert        (node p, int key);
node patriz_insert_node   (node p, int key, int bit, node parent);
node patriz_from_patricia (node p);
void patriz_cost          (node p);
