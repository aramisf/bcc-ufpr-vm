#include "libPatriz.h"

int* buffer;
int  count;
int  count2;

int apl_digit (int key, int bit) {
    unsigned int *bin = malloc (WORDSIZE * sizeof(int));
    bin = apl_dec2bin (key);
    return bin[bit];
}

int apl_bin2dec (int bin) {

    int i, d = 0, t = 0;
    for (i = 0; i < WORDSIZE; i++) {
        d = bin % 10;
        t += pow (2, i) * d;
        bin /= 10;
    }

    return t;
}

unsigned int* apl_dec2bin (int dec) {

    int i;
    unsigned int *bin = malloc (WORDSIZE * sizeof (int));

    /**
     * @note Supressão de erros de inicialização
     *       de vetor no valgrind.
     */
    for (i = 0; i < WORDSIZE-1; i++) bin[i] = 0;

    for (i = WORDSIZE - 1; dec > 0; i--) {
        bin[i] = dec % 2;
        dec >>= 1;
    }

    return bin;
}

void apl_free_buffer () {
    count = 0;
    count2 = 0;
    free (buffer);
    buffer = malloc (BUFFSIZE * sizeof(int));
}

int main (int argc, char *argv[]) {

	node patricia = NULL;
    node patriz = NULL;
	char opt;
    int k;

    /**
     * @note Modos de exeucução:
     *       'P' para chamadas na árvore Patricia
     *       'p' para chamadas na árvore Patriz
     */
    int mode;
    if (strcmp (argv[0], "./Patri") == 0)
        mode = 'P';
    else
        mode = 'p';

	while (scanf("%c ", &opt) != EOF) {

        printf ("%c ", opt);

        if (opt != 't' && opt != 'c') {
            scanf ("%d\n", &k);
            printf ("%05d", k);
        }

        printf ("\n");

        k = apl_bin2dec (k);

        /**
         * @note Verificação de comandos válidos para Patricia
         */
        if (mode == 'P') {

            if (opt == 'i') {
                patricia = patricia_insert (patricia, k);
                patricia_print (patricia, -1);

            } else if (opt == 'b') {
                apl_free_buffer ();
                patricia_search (patricia, k, -1);

            } else if (opt == 't') {
                patriz = NULL;
                patriz = patriz_from_patricia (patricia);
                patriz_print (patriz);
                mode = 'p';

            } else if (opt == 'c') {
                apl_free_buffer ();
                patricia_cost (patricia, -1);
                printf("%d bytes\n", count);
                printf("%d visitas", count2);
            }

        /**
         * @note Verificação de comandos válidos para Patriz
         */
        } else if (mode == 'p') {

            if (opt == 'i') {
                patriz = patriz_insert (patriz, k);
                patriz_print (patriz);

            } else if (opt == 'b') {
                apl_free_buffer ();
                patriz_search (patriz, k, -1);

            } else if (opt == 't') {
                patricia = NULL;
                patricia = patricia_from_patriz (patriz);
                patricia_print (patricia, -1);
                mode = 'P';

            } else if (opt == 'c') {
                apl_free_buffer ();
                patriz_cost (patriz);
                printf("%d bytes\n", count);
                printf("%d visitas", count2);
            }
        }

		printf ("\n");

	}

	return 0;

}
