#include "libPatriz.h"

/**
 * @brief Insere nó em árvore Patriz
 */
node patriz_insert (node p, int key) {

    /**
     * @note Cria a árvore caso ela não exista
     */
    if (!p) {
        p = node_create (-1, 0);
        if (apl_digit (key, 0) == 0)
            p->left = node_create (key, 1);
        else
            p->right = node_create (key, 1);
        return p;
    }

	return patriz_insert_node (p, key, 0, NULL);
}

/**
 * @brief Insere um nó recursivamente em uma árvore Patriz
 */
node patriz_insert_node (node p, int key, int bit, node parent) {

    int i;

    /**
     * @note Verifica se deve criar um novo nó
     *       ou prosseguir navegando
     */
    int new = 0;
    if (!p) {
        new = 1;
        i = bit;
    } else if (p->key > 0) {
        new = 1;
        for (i = 0; i < WORDSIZE &&
            (apl_digit (key, i) == apl_digit (p->key, i)); i++);
    }

    if (new) {

        node n = node_create (-1, i);
        node o = node_create (key, i+1);

        if (apl_digit (key, i) == 0) {
            n->left = o;
            n->right = p;
        } else {
            n->left = p;
            n->right = o;
        }
        return n;

    /**
     * @note Continua navegando para os nós ancestrais
     */
    } else {
        if (apl_digit(key, p->bit) == 0)
            p->left = patriz_insert_node (p->left, key, bit+1, p);
        else
            p->right = patriz_insert_node (p->right, key, bit+1, p);
        return p;
    }
}

/**
 * @brief Busca um nó em uma árvore Patriz
 */
void patriz_search (node p, int key, int bit) {

	if (!p) {
        printf("nao achou");
        return;
    }

    if (p->key == key) {
        int i;
        for (i = 0; i < count; i++)
            printf("[%d], ", buffer[i]);

        /**
         * @note Mostra o nó e encerra a busca caso
         *       ele seja encontrado
         */
        printf("(%d)", p->key);
        return;
    } else if (p->bit <= bit) {
        printf ("nao achou");
        return;
    }

    /**
     * @note Guarda os nós visitados em um buffer para
     *       mostrá-los caso o nó desejado seja encontrado.
     */
    buffer[count++] = p->bit;

    if (apl_digit(key, p->bit) == 0)
        patriz_search (p->left, key, p->bit);
    else
        patriz_search (p->right, key, p->bit);

}

/**
 * @brief Imprime uma árvore Patriz
 */
void patriz_print (node p) {
    if (!p) {
        printf ("()");
        return;
    }

    if (p->key > 0) {
        printf("(%d)", p->key);
    } else {
        printf ("([%d] ", p->bit);
        patriz_print (p->left);
        patriz_print (p->right);
        printf (")");
    }
}

/**
 * @brief Percorre uma árvore Patricia, preenchendo o
 *        buffer com os valores dos nós
 */
void patricia_in_order (node p, int bit) {

    if (!p)
        return;

    if (p->bit <= bit)
        return;

    patricia_in_order (p->left, p->bit);
    buffer[count++] = p->key;
    patricia_in_order (p->right, p->bit);
}

/**
 * @brief Constrói uma árvore Patriz a partir de uma Patricia
 */
node patriz_from_patricia (node patricia) {

    apl_free_buffer ();
    patricia_in_order (patricia, -1);

    int i;
    node patriz = node_create (-1, 0);
    for (i = 0; i < count; i++)
        patriz = patriz_insert (patriz, buffer[i]);

    return patriz;
}

/**
 * @brief Conta o número de nós visitados e soma
 *        os tamanhos da estrutura que os nós são compostos
 */
void patriz_cost (node p) {

    if (!p)
        return;

    count += sizeof (struct _node);
    count2++;
    patriz_cost (p->left);
    patriz_cost (p->right);
}
