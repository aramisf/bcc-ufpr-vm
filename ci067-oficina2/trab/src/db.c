/**
 * @file db.c
 * @brief Manipulação do banco de dados.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <err.h>
#include "util.h"
#include "err.h"
#include "img.h"
#include "mos.h"
#include "db.h"

/**
 * @brief Busca o idenficador de imagem @p id no banco de dados.
 * @param db Banco de dados.
 * @param id Idenficador para ser encontrado.
 * @return Retorna a posição do @p id em db::table ou @c -1 caso não encontre.
 */
static int
db_id_search (struct db *db, int id)
{
    int i;

    for (i = 0; i < db->entries; i++)
        if (db->table[i].id == id)
            return i;

    return -1;
}

/**
 * @brief Comparação para ordenamento crescente por @c id de imagens.
 * @param aarg Primeiro elemento.
 * @param barg Segundo elemento.
 * @return A diferença entre o @c id do primeiro e o @c id do segundo elemento.
 */
static int
db_id_cmp (const void *aarg, const void *barg)
{
#define a ((struct db_table *) aarg)
#define b ((struct db_table *) barg)
    return a->id - b->id;
#undef a
#undef b
}

/**
 * @brief Ordena o mosaico de acordo com o @c id de cada imagem.
 * @param db Banco de dados.
 */
static void
db_id_sort (struct db *db)
{
    qsort (db->table, db->entries, sizeof (*db->table), db_id_cmp);
}

/**
 * @brief Calcula a posição no arquivo onde inicia-se a escrita do elemento @p h
 *        de db::table.
 * @param db Banco de dados.
 * @param h Elemento de db::table.
 * @return Posição no arquivo de banco de dados.
 */
static long
db_meta_position (struct db *db, int h)
{
    long pos = (db->size - ((db->entries - h) * sizeof (*db->table) +
                            sizeof (db->entries)));

    if (pos < 0)
        return 0;
    else
        return pos;
}

/**
 * @brief Lê metainformação do banco de dados.
 * @param db Banco de dados.
 *
 * Posiciona o ponteiro de arquivo na última posição do banco de dado. Com isso
 * é possível medir o tamanho do arquivo. Caso o arquivo esteja vazio, grava @c
 * db::num como @c zero e retorna.
 *
 * Se o arquivo não estiver vazio, lê o último inteiro do arquivo, que contém a
 * informação de quantas imagens existem no banco de dadose grava em @c db::num.
 * Posiciona o ponteiro de arquivo na posição do início da tabela de informações
 * sobre as imagens e grava estas informações em @c db::table.
 *
 * Caso alguma movimentação do ponteiro de arquivo resulte em erro, interrompe o
 * programa com mensagem de erro sobre o formato dos dados.
 */
static void
db_meta_read (struct db *db)
{
    long pos;

    err_fseek (db->file, 0L, SEEK_END);
    db->size = err_ftell (db->file);

    if (db->size == 0)
    {
        db->entries = 0;
        db->table = NULL;
        return;
    }

    err_fseek (db->file, -1L * sizeof (db->entries), SEEK_END);

    if (fread (&db->entries, sizeof (db->entries), 1, db->file) < 1)
    {
        errno = EBFONT;
        err (EXIT_FAILURE, "`%s'", db->filename);
    }

    if (db->entries < 0)
    {
        errno = EBFONT;
        err (EXIT_FAILURE, "`%s'", db->filename);
    }

    pos = db_meta_position (db, 0);
    err_fseek (db->file, pos, SEEK_SET);
    db->table = err_malloc (db->entries * sizeof (*db->table));

    if (fread (db->table, sizeof (*db->table), db->entries,
               db->file) < db->entries)
    {
        errno = EBFONT;
        err (EXIT_FAILURE, "`%s'", db->filename);
    }
}

/**
 * @brief Escreve metainformações no banco de dados.
 * @param db Banco de dados.
 * @param h Elemento de db::table.
 *
 * Se @p h é negativo, escreve as informações de todos os elementos. Caso
 * contrário, escreve apenas as informações do elemento @p h.
 */
static void
db_meta_write (struct db *db, int h)
{
    if (!db->entries)
        return;

    if (h < 0)
    {
        err_fwrite (db->table, sizeof (*db->table), db->entries, db->file);
        err_fwrite (&db->entries, sizeof (db->entries), 1, db->file);
    }
    else
    {
        err_fwrite (&db->table[h], sizeof (*db->table), 1, db->file);
    }
}

/**
 * @brief Adiciona um elemento a tabela de metainformações.
 * @param db Banco de dados.
 * @param id Identificador da imagem.
 * @param col Coluna da imagem no mosaico.
 * @param row Linha da imagem no mosaico.
 * @param pos Posição da imagem do elemento @p h no arquivo de banco de dados.
 * @param size Tamanho da imagem do elemento @p h no arquivo de banco de dados.
 */
static void
db_meta_add (struct db *db, int id, int col, int row, long pos, long size)
{
    int h = db->entries++;

    db->table = err_realloc (db->table, db->entries * sizeof (*db->table));

    /* A estrutura db->table pode ser maior, em bytes, do que a soma de
     * todos os seus elementos devido ao alinhamento de memória feito pelo
     * compilador. Os bytes que sobram precisam ser preenchidos para evitar
     * os avisos de memória não iniciada no valgrind(1). */
    memset (db->table + h, 0, sizeof (*db->table));

    db->table[h].id = id;
    db->table[h].col = col;
    db->table[h].row = row;
    db->table[h].pos = pos;
    db->table[h].size = size;
}

/**
 * @brief Atualiza informações de um elemento.
 * @param db Banco de dados.
 * @param h Elemento de db::table.
 * @param col Coluna da imagem no mosaico.
 * @param row Linha da imagem no mosaico.
 */
static void
db_meta_update (struct db *db, int h, int col, int row)
{
    db->table[h].col = col;
    db->table[h].row = row;
}

/**
 * @brief Remove um elemento da tabela de metainformações.
 * @param db Banco de dados.
 * @param h Elemento de db::table.
 */
static void
db_meta_remove (struct db *db, int h)
{
    int i;
    long size;

    db->size -= db->table[h].size + sizeof (db->table[h]);
    size = db->table[h].size;

    for (i = h + 1; i < db->entries; i++)
    {
        db->table[i - 1].id = db->table[i].id;
        db->table[i - 1].col = db->table[i].col;
        db->table[i - 1].row = db->table[i].row;
        db->table[i - 1].pos = db->table[i].pos - size;
        db->table[i - 1].size = db->table[i].size;
    }

    db->entries--;
    db->table = err_realloc (db->table, db->entries * sizeof (*db->table));
}

/**
 * @brief Adiciona imagem ao banco de dados.
 * @param db Banco de dados.
 * @param img Imagem a ser adicionada.
 * @return Tamanho da imagem escrita no banco de dados.
 */
static long
db_img_write (struct db *db, struct img *img)
{
    long amount, chunk, size;
    unsigned char buf[BUFSIZ];

    err_fwrite (&img->header, sizeof (img->header), 1, db->file);
    amount = img->bytes_per_pixel * img->pixels;
    size = amount + sizeof (img->header);

    while (amount > 0)
    {
        chunk = img_read (img, buf, sizeof (buf), amount);
        err_fwrite (buf, sizeof (*buf), chunk, db->file);
        amount -= chunk;
    }

    return size;
}

/**
 * @brief Remove imagem do banco de dados.
 * @param db Banco de dados.
 * @param h Elemento de db::table.
 */
static void
db_img_remove (struct db *db, int h)
{
    long amount, chunk, offset;
    unsigned char buf[BUFSIZ];

    /* O arquivo será truncado diretamente. */
    if (db->entries == 1 || h == db->entries - 1)
        return;

    /* Cópia de bytes, então a delimatação de imagens dentro do arquivo não
     * importa. Os bytes são transferidos em pacotes de BUFSIZ. */

    amount = (db->table[db->entries - 1].pos +
              db->table[db->entries - 1].size -
              db->table[h + 1].pos);

    while (amount > 0)
    {
        err_fseek (db->file, db->table[h].size, SEEK_CUR);
        chunk = (amount > sizeof (buf) ? sizeof (buf) : amount);
        err_fread (buf, sizeof (*buf), chunk, db->file);
        offset = db->table[h].size + chunk;
        err_fseek (db->file, -offset, SEEK_CUR);
        err_fwrite (buf, sizeof (*buf), chunk, db->file);
        amount -= chunk;
    }
}

/**
 * @brief Inicia o banco de dados.
 * @param db Estrutura de banco de dados.
 * @param database Arquivo de banco de dados.
 *
 * Caso @p database seja diferente de @c NULL, atribui esse valor ao arquivo de
 * banco de dados.  Se @p database for @c NULL, atribui o valor da variável de
 * ambiente @c MOSAICO ao nome do banco de dados. Caso não exista nenhum valor
 * definido na variável de ambiente, usa o nome padrão @ref DB_FILENAME. Abre o
 * arquivo do banco de dados e lê as metainformações ou cria um novo arquivo
 * caso não exista.
 *
 * Abre o arquivo de banco de dados @c db::filename e grava as metainformações
 * na estrutura @ref db. Caso o arquivo não exista, cria um novo arquivo.
 *
 * O arquivo do banco de dados permanece aberto até a chamada de db_finish().
 */
void
db_init (struct db *db, const char *database)
{
    db->filename = (char *) database;

    if (!db->filename || !strlen (db->filename))
        db->filename = getenv ("MOSAICO");

    if (!db->filename || !strlen (db->filename))
        db->filename = DB_FILENAME;

    if (!(db->file = fopen (db->filename, "r+")))
        db->file = err_fopen (db->filename, "w+");

    db_meta_read (db);
    
    debug ("file:%s entries:%d db size:%ld", db->filename, db->entries,
           db->size);
}

/**
 * @brief Finaliza o banco de dados.
 * @param db Banco de dados.
 *
 * Libera todo o espaço alocado e fecha o arquivo.
 */
void
db_finish (struct db *db)
{
    fclose (db->file);
    free (db->table);
}

/**
 * @brief Adiciona uma imagem ao mosaico.
 * @param db Banco de dados.
 * @param id Identificador da imagem.
 * @param col Coluna da imagem no mosaico.
 * @param row Linha da imagem no mosaico.
 * @param filename Arquivo com a imagem a ser adicionada ao mosaico.
 * @param force Não confirmar sobrescrita da imagem no mosaico caso já exista o
 *              mesmo @p id.
 */
void
db_add (struct db *db, int id, int col, int row, const char *filename,
        unsigned char force)
{
    struct img img;
    long pos, size;
    int h = db_id_search (db, id);

    if (h >= 0)
    {
        if (!force)
            yesno_ask (&force, "sobrescrever a imagem `%d'?", id);

        if (!force)
            return;

        db_remove (db, id, 1);
    }

    pos = db_meta_position (db, 0);
    err_fseek (db->file, pos, SEEK_SET);
    img_init (&img, filename);
    size = db_img_write (db, &img);
    img_finish (&img);
    db_meta_add (db, id, col, row, pos, size);
    db_meta_write (db, -1);

    debug ("add h:%d id:%d entries:%d db size:%ld", h, id, db->entries,
           db->size);
}

/**
 * @brief Move a posição de uma imagem no mosaico.
 * @param db Banco de dados.
 * @param id Identificador da imagem.
 * @param col Nova coluna da imagem no mosaico.
 * @param row Nova linha da imagem no mosaico.
 * @param force Confirmação de deslocamento de imagem.
 *
 * Procura pelo identificador de imagem @p id na tabela de informações sobre
 * imagens e modifica os valores da coluna @p col e linha @p row da imagem no
 * mosaico e escreve a atualização no banco de dados.
 *
 * Caso não seja econtrado o idenficador @p id, interrompe a execução com
 * mensagem de erro.
 */
void
db_move (struct db *db, int id, int col, int row, unsigned char force)
{
    long pos;
    int h = db_id_search (db, id);

    if (h < 0)
        errx (EXIT_FAILURE, "`%d': Identificador não encontrado", id);

    if (!force)
        yesno_ask (&force, "mover a imagem `%d' de %d,%d para %d,%d?",
                   id, db->table[h].col, db->table[h].row, col, row);

    if (!force)
        return;

    debug ("move h:%d id:%d col:%d row:%d --> col:%d row:%d",
           h, id, db->table[h].col, db->table[h].row, col, row);

    pos = db_meta_position (db, h);
    err_fseek (db->file, pos, SEEK_SET);
    db_meta_update (db, h, col, row);
    db_meta_write (db, h);
}

/**
 * @brief Remove uma imagem do mosaico.
 * @param db Banco de dados.
 * @param id Identificador da imagem.
 * @param force Confirmação de remoção.
 *
 * Se @p force é diferente de @c zero, remove diretamente, sem confirmação.
 */
void
db_remove (struct db *db, int id, unsigned char force)
{
    long pos;
    int fd;
    int h = db_id_search (db, id);

    if (h < 0)
        errx (EXIT_FAILURE, "`%d': Identificador não econtrado", id);

    if (!force)
        yesno_ask (&force, "remover imagem `%d'?", id);

    if (!force)
        return;

    debug ("remove h:%d id:%d pos:%ld size:%ld new db size:%ld", h, id,
           db->table[h].pos, db->table[h].size, db->size);

    err_fseek (db->file, db->table[h].pos, SEEK_SET);
    db_img_remove (db, h);
    db_meta_remove (db, h);
    db_meta_write (db, -1);
    pos = ftell (db->file);
    fd = err_fileno (db->file);
    err_ftruncate (fd, pos);
}

/**
 * @brief Remove todas as figuras do banco de dados.
 * @param db Banco de dados.
 * @param force Confirmação de remoção.
 *
 * Se @p force é diferente de @c zero, remove diretamente, sem confirmação.
 */
void
db_clean (struct db *db, unsigned char force)
{
    int fd;

    if (!db->entries)
        return;

    if (!force)
        yesno_ask (&force, "remover todas imagens?");

    if (!force)
        return;

    db->entries = 0;
    fd = err_fileno (db->file);
    err_ftruncate (fd, 0);
}

/**
 * @brief Imprime mosaico em arquivo.
 * @param db Banco de dados.
 * @param filename Arquivo onde deve ser impresso o mosaico.
 * @param bg Cor de fundo da imagem. Se negativo, será determinado
 *           automaticamente como a média de @c maxval de todas as
 *           imagens do mosaico.
 */
void
db_print (struct db *db, const char *filename, unsigned short bg)
{
    struct mos mos;

    if (!db->entries)
        errx (EXIT_FAILURE, "`%s': Nenhuma imagem no mosaico", db->filename);

    db_id_sort (db);

    mos_init (&mos, db, filename, bg);
    mos_header_print (&mos);
    mos_imgs_print (&mos);
    mos_bg_print (&mos);
    mos_finish (&mos);
}

/**
 * @brief Lista as informações das imagens contidas no mosaico.
 * @param db Banco de dados.
 */
void
db_list (struct db *db)
{
    int i;
    struct img img;

    if (db->entries)
    {
        /* *INDENT-OFF* */
        printf ("+---------+---------+---------+---------+---------+---------+---------+---------+---------+\n");
        printf ("|   id    | coluna  |  linha  | posição | tamanho | formato | largura | altura  | maxval  |\n");
        printf ("+---------+---------+---------+---------+---------+---------+---------+---------+---------+\n");
        /* *INDENT-ON* */

        for (i = 0; i < db->entries; i++)
        {
            err_fseek (db->file, db->table[i].pos, SEEK_SET);
            err_fread (&img.header, sizeof (img.header), 1, db->file);

            /* *INDENT-OFF* */
            printf ("| %7d | %7d | %7d | %7ld | %7ld | %-7s | %7d | %7d | %7d |\n",
                    db->table[i].id, db->table[i].col, db->table[i].row,
                    db->table[i].pos, db->table[i].size, img.header.magic,
                    img.header.cols, img.header.rows, img.header.maxval);
            /* *INDENT-ON* */
        }

        /* *INDENT-OFF* */
        printf ("+---------+---------+---------+---------+---------+---------+---------+---------+---------+\n");
        /* *INDENT-ON* */
    }

    printf ("arquivo: %s  imagens: %d\n", db->filename, db->entries);
}

/* vim: set et ts=4 sw=4 sts=4 tw=80 cino+=(0,w1,u0,t0 fdm=marker: */
