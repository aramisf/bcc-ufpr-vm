/**
 * @file debug.h
 * @brief Funções de ajuda de desenvolvimento.
 */

#ifndef __DEBUG_H__
#define __DEBUG_H__

#ifndef _STDIO_H
# include <stdio.h>
#endif

#ifndef _STDLIB_H
# include <stdlib.h>
#endif

#ifndef _STRING_H
# include <string.h>
#endif

#ifndef _ERRNO_H
# include <errno.h>
#endif

#define __FLF__       __FILE__,__LINE__,__FUNCTION__
#define __FLF_ARGS__  __file__,__line__,__function__
#define __FLF_PROTO__ const char *__file__,int __line__,const char *__function__

#ifdef NDEBUG
# define debug_init(args...)
# define debug(args...)
# define err_err(s,args...) err(s,args)
#else /* not NDEBUG */

unsigned char debug_mode;
FILE *debug_file;
int debug_errno;

# define debug_init(mode,filename)                                     \
    do {                                                               \
        debug_mode = mode;                                             \
        if (debug_mode) {                                              \
            if (filename == NULL) {                                    \
                debug_file = stderr;                                   \
            } else if (strcmp (filename, "stderr") == 0) {             \
                debug_file = stderr;                                   \
            } else if ((debug_file = fopen (filename, "w")) == NULL) { \
                fprintf (stderr, "%s: ERRO DE PROGRAMA: %s: %s\n",     \
                         program_name, filename,                       \
                         strerror (errno));                            \
                exit (EXIT_FAILURE);                                   \
            }                                                          \
            setlinebuf (debug_file);                                   \
            debug_errno = 0;                                           \
        }                                                              \
    } while (0)

# define debug(args...)                                                \
    do {                                                               \
        if (debug_mode) {                                              \
            fprintf (debug_file, "%s:%d: %s(): ", __FLF__);            \
            fprintf (debug_file, ## args);                             \
            if (debug_errno != 0)                                      \
                fprintf (debug_file, " [%s]", strerror (debug_errno)); \
            fputc ('\n', debug_file);                                  \
        }                                                              \
    } while (0)

# define err_err(s,args...)                                            \
    do {                                                               \
        debug_errno = errno;                                           \
        if (debug_mode) {                                              \
            fprintf (debug_file, "%s:%d: %s(): ", __FLF_ARGS__);       \
            fprintf (debug_file, ## args);                             \
            if (debug_errno != 0)                                      \
                fprintf (debug_file, " [%s]", strerror (debug_errno)); \
            fputc ('\n', debug_file);                                  \
        }                                                              \
        debug_errno = 0;                                               \
        err(s,args);                                                   \
    } while (0)

# define err(s,args...)    \
  do {                     \
      debug_errno = errno; \
      debug(args);         \
      debug_errno = 0;     \
      err(s,args);         \
  } while (0)

#endif /* NDEBUG */

#endif /* __DEBUG_H__ */

/* vim: set et ts=4 sw=4 sts=4 tw=80 cino+=(0,w1,u0,t0 fdm=marker: */
