/**
 * @file img.c
 * @brief Manipulação de imagens.
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <err.h>
#include "util.h"
#include "err.h"
#include "img.h"

/**
 * @brief Lê os dois primeiros caracteres de um arquivo e verifica se é um
 *        tipo de imagem válido.
 * @param img Imagem.
 */
static void
img_magic_read (struct img *img)
{
    char a, b;

    a = getc (img->file);
    b = getc (img->file);

    /* Aceitar apenas P5 e P6 */
    if (a != 'P' || (b != '5' && b != '6'))
    {
        errno = EBFONT;
        err (EXIT_FAILURE, "`%s'", img->filename);
    }

    img->header.magic[0] = a;
    img->header.magic[1] = b;
    img->header.magic[2] = '\0';
}

/**
 * @brief Lê o cabeçalho da imagem após o <i>magic number</i> e antes do
 *        conteúdo.
 * @param img Imagem.
 */
static void
img_header_read (struct img *img)
{
    img->header.cols = getci (img->file);

    if (errno)
        err (EXIT_FAILURE, "`%s'", img->filename);

    img->header.rows = getci (img->file);

    if (errno)
        err (EXIT_FAILURE, "`%s'", img->filename);

    img->header.maxval = getci (img->file);

    /* Verificar limitações para MAXVAL. */
    if (img->header.maxval < 0 || img->header.maxval > 65535)
        errno = ERANGE;

    if (errno)
        err (EXIT_FAILURE, "`%s'", img->filename);

    img->pixels = IMG_PIXELS (img);
    img->levels = IMG_LEVELS (img);
    img->bytes_per_level = IMG_BYTES_PER_LEVEL (img);
    img->bytes_per_pixel = IMG_BYTES_PER_PIXEL (img);
}

/**
 * @brief Inicia a estrutura de uma imagem.
 * @param img Estrutura de imagem.
 * @param filename Arquivo com uma imagem nos formatos PBM, PGM ou PPM.
 *
 * @note O arquivo da imagem permanece aberto até a chamada de img_finish().
 */
void
img_init (struct img *img, const char *filename)
{
    /* Preencher os bytes de alinhamento para evitar os avisos de memória não
     * iniciada no valgrind(1). */
    memset (&img->header, 0, sizeof (img->header));

    img->filename = filename;
    img->file = err_fopen (filename, "r");

    img_magic_read (img);
    img_header_read (img);

    debug ("img file:%s magic:%s cols:%d rows:%d maxval:%d "
           "pixels:%d levels:%d bytes_per_level:%d bytes_per_pixel:%d",
           filename, img->header.magic, img->header.cols, img->header.rows,
           img->header.maxval, img->pixels, img->levels, img->bytes_per_level,
           img->bytes_per_pixel);
}

/**
 * @brief Finaliza uma imagem.
 * @param img Imagem.
 *
 * Fecha o arquivo da imagem.
 */
void
img_finish (struct img *img)
{
    fclose (img->file);
}

/**
 * @brief Lê uma imagem em formato binário.
 * @param img Imagem.
 * @param buf Buffer onde o pedaço lido deve ser salvo.
 * @param bufsiz Tamanho do buffer @p buf.
 * @param amount Quantidade máxima de bytes que pode ser lida da imagem.
 * @return Quantidade de bytes lidos.
 *
 * @warning Só pode ser usada depois de img_init().
 */
long
img_read (struct img *img, unsigned char *buf, long bufsiz, long amount)
{
    long chunk = (amount > bufsiz ? bufsiz : amount);
    err_fread (buf, sizeof (*buf), chunk, img->file);
    return chunk;
}

/* vim: set et ts=4 sw=4 sts=4 tw=80 cino+=(0,w1,u0,t0 fdm=marker: */
