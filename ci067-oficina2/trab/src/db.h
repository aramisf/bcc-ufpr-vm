/**
 * @file db.h
 * @brief Manipulação do banco de dados.
 */

#ifndef __DB_H__
#define __DB_H__

#ifndef _STDIO_H
# include <stdio.h>
#endif

/** @brief Nome padrão para o arquivo de banco de dados. */
#define DB_FILENAME ".mosaico"

/**
 * @brief Informações sobre o conteúdo do banco de dados.
 */
struct db_table
{
    /** @brief Identificador da imagem. */
    int id;
    /** @brief Coluna da imagem no mosaico. */
    int col;
    /** @brief Linha da imagem no mosaico. */
    int row;
    /** @brief Posição da imagem no banco de dados. */
    long pos;
    /** @brief Tamanho da imagem no banco de dados (em bytes). */
    long size;
};

/**
 * @brief Estrutura principal do banco de dados.
 */
struct db
{
    /** @brief Nome do arquivo de banco de dados. */
    char *filename;
    /** @brief @e Stream do arquivo de banco de dados. */
    FILE *file;
    /** @brief Tamanho do arquivo de banco de dados (em bytes). */
    long size;
    /** @brief Número de registros no banco de dados. */
    int entries;
    /** @brief Informação das imagens. */
    struct db_table *table;
};

extern void db_init (struct db *, const char *);
extern void db_finish (struct db *);
extern void db_add (struct db *, int, int, int, const char *, unsigned char);
extern void db_move (struct db *, int, int, int, unsigned char);
extern void db_remove (struct db *, int, unsigned char);
extern void db_clean (struct db *, unsigned char);
extern void db_list (struct db *);
extern void db_print (struct db *, const char *, unsigned short);

#endif /* __DB_H__ */

/* vim: set et ts=4 sw=4 sts=4 tw=80 cino+=(0,w1,u0,t0 fdm=marker: */
