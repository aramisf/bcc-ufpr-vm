/**
 * @file err.c
 * @brief Funções empacotadoras de erros.
 *
 * Empacotadores das funções da biblioteca padrão. Em caso de erro imprimem as
 * mensagens de erro para a saída @c stderr e interrompem a execução do
 * programa imediatamente.
 *
 * Todas as funções recebem os parâmetros que suas versões originais, sem o @c
 * err_ além dos dados de onde a função foi chamado (arquivo, linha e função).
 * As três variáveis de compilação, @c __FILE__, @c __LINE__ e @c __FUNCTION__,
 * são passadas automaticamente na chamada das funções @c err_ pelas macros @c
 * err_, definidas em @ref err.h.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <unistd.h>
#include <sys/types.h>
#include <err.h>
#include "debug.h"
#include "args.h"

void *
err_malloc (__FLF_PROTO__, size_t size)
{
    void *p = malloc (size);

    if (size && !p)
        err_err (EXIT_FAILURE, "malloc()");

    return p;
}

void *
err_realloc (__FLF_PROTO__, void *p, size_t size)
{
    p = realloc (p, size);

    if (size && !p)
        err_err (EXIT_FAILURE, "realloc()");

    return p;
}

char *
err_strdup (__FLF_PROTO__, const char *s)
{
    char *p = strdup (s);

    if (!p)
        err_err (EXIT_FAILURE, "strdup()");

    return p;
}

FILE *
err_fopen (__FLF_PROTO__, const char *path, const char *mode)
{
    FILE *file = fopen (path, mode);

    if (!file)
        err_err (EXIT_FAILURE, "fopen(): `%s'", path);

    return file;
}

long
err_ftell (__FLF_PROTO__, FILE * stream)
{
    long offset = ftell (stream);

    if (offset < 0)
        err_err (EXIT_FAILURE, "ftell()");

    return offset;
}

void
err_fseek (__FLF_PROTO__, FILE * stream, long offset, int whence)
{
    if (fseek (stream, offset, whence) < 0)
        err_err (EXIT_FAILURE, "fseek()");
}

size_t
err_fread (__FLF_PROTO__, void *ptr, size_t size, size_t nmemb, FILE * stream)
{
    size_t retval = fread (ptr, size, nmemb, stream);

    if (retval < nmemb && ferror (stream) && !feof (stream))
        err_err (EXIT_FAILURE, "fread()");

    return retval;
}

size_t
err_fwrite (__FLF_PROTO__, const void *ptr, size_t size, size_t nmemb,
            FILE * stream)
{
    size_t retval = fwrite (ptr, size, nmemb, stream);

    if (retval < nmemb)
        err_err (EXIT_FAILURE, "fwrite()");

    return retval;
}

int
err_fileno (__FLF_PROTO__, FILE * stream)
{
    int fd = fileno (stream);

    if (fd < 0)
        err_err (EXIT_FAILURE, "fileno()");

    return fd;
}

void
err_ftruncate (__FLF_PROTO__, int fd, off_t length)
{
    if (ftruncate (fd, length) < 0)
        err_err (EXIT_FAILURE, "ftruncate()");
}

unsigned int
err_atoui (__FLF_PROTO__, const char *str)
{
    char *endptr;
    unsigned int val;

    errno = 0;
    val = strtoul (str, &endptr, 10);

    if (errno)
        err_err (EXIT_FAILURE, "strtoul(): `%s'", str);

    if (*str == '\0' || *endptr != '\0')
    {
        errno = EINVAL;
        err_err (EXIT_FAILURE, "strtoul(): `%s'", str);
    }

    return val;
}

int
err_atoi (__FLF_PROTO__, const char *str)
{
    char *endptr = NULL;
    int val;

    errno = 0;
    val = strtol (str, &endptr, 10);

    if (errno)
        err_err (EXIT_FAILURE, "strtol(): `%s'", str);

    if (*str == '\0' || *endptr != '\0')
    {
        errno = EINVAL;
        err_err (EXIT_FAILURE, "strtoul(): `%s'", str);
    }

    return val;
}

/* vim: set et ts=4 sw=4 sts=4 tw=80 cino+=(0,w1,u0,t0 fdm=marker: */
