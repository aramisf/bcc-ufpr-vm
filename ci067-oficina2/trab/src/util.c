/**
 * @file util.c
 * @brief Funções com utilidades gerais.
 */

#include <stdio.h>
#include <errno.h>
#include <limits.h>

/**
 * @brief Lê o próximo caractere do @e stream, ignorando comentários.
 * @param file @e Stream.
 * @return Próximo caractere depois do comentário ou @c EOF.
 *
 * Se o próximo caractere for um sustenido, @c #, ignora todos os caracteres
 * entre este sustenido e a próxima quebra de linha (@c \\n) ou retorno de
 * carro (@c \\r) e retorna o próximo caractere.
 */
int
getcc (FILE * file)
{
    int c = getc (file);

    if (c == '#')
        do
            c = getc (file);
        while (c != '\n' && c != '\r');

    return c;
}

/**
 * @brief Lê o próximo inteiro de @p file.
 * @param file @e Stream.
 * @return O valor lido, convertido para inteiro.
 * 
 * Leva em conta comentários que possam aparecer (inclusive no meio de
 * <i>tokens</i>).
 *
 * Interrompe a execução do programa caso acontece erros de conversão ou os
 * valores sejam inválidos.
 */
int
getci (FILE * file)
{
    int c, x, val = 0;

    errno = 0;

    do
        c = getcc (file);
    while (c != EOF && (c == ' ' || c == '\t' || c == '\n' || c == '\r'));

    if (c < '0' || c > '9')
    {
        errno = EINVAL;
        return INT_MAX;
    }

    do
    {
        if (val > INT_MAX / 10)
        {
            errno = ERANGE;
            return INT_MAX;
        }

        val *= 10;
        x = c - '0';

        if (val > INT_MAX - x)
        {
            errno = ERANGE;
            return INT_MAX;
        }

        val += x;
        c = getcc (file);
    }
    while (c >= '0' && c <= '9');

    return val;
}

/**
 * @brief Lê uma linha da entrada padrão verifica se é uma resposta positiva.
 * @return @c 1, se a resposta for positiva e @c 0 em caso contrário.
 *
 * Toda resposta onde o primeiro caractere é @c s ou @c S é entendida como
 * positiva.
 */
unsigned char
yesno (void)
{
    unsigned char yes;
    int c = getchar ();

    yes = (c == 's' || c == 'S');

    while (c != '\n' && c != EOF)
        c = getchar ();

    return yes;
}

/**
 * @brief Imprime os bits de um valor @c unsigned short.
 * @param a Valor a ser impresso.
 */
void
showbits (unsigned short a)
{
    int i, k, mask;

    for (i = 15; i >= 0; i--)
    {
        mask = 1 << i;
        k = a & mask;

        if (k == 0)
            fprintf (stderr, "0");
        else
            fprintf (stderr, "1");

        if (i == 7)
            fprintf (stderr, " ");
    }

    fprintf (stderr, "\n");
}

/* vim: set et ts=4 sw=4 sts=4 tw=80 cino+=(0,w1,u0,t0 fdm=marker: */
