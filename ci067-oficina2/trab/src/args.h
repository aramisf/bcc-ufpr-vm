/**
 * @file args.h
 * @brief Leitura de argumentos da linha de comando.
 */

#ifndef __ARGS_H__
#define __ARGS_H__

/**
 * @brief Ações que podem ser feitas no mosaico.
 */
enum ACTIONS
{
    /** @brief Ação indefinida. */
    ACTION_NOTHING,
    /** @brief Adiciona imagem. */
    ACTION_ADD,
    /** @brief Remove imagem. */
    ACTION_REMOVE,
    /** @brief Move uma imagem. */
    ACTION_MOVE,
    /** @brief Imprime mosaico. */
    ACTION_PRINT,
    /** @brief Remove todas imagens. */
    ACTION_CLEAN
};

/**
 * @brief Opções passadas como argumentos por linha de comando.
 */
struct args
{
    /** @brief Tipo de ação executada. */
    enum ACTIONS action;
    /** @brief Nome do arquivo usado na ação. */
    char *filename;
    /** @brief Identificador da imagem. */
    int id;
    /** @brief Coluna do início da imagem no mosaico. */
    int col;
    /** @brief Linha do início da imagem no mosaico. */
    int row;
    /** @brief Arquivo de banco de dados. */
    char *database;
    /** @brief Cor de fundo do mosaico. */
    unsigned short bg;
    /** @brief Listar as imagens depois que ação é executada. */
    unsigned char list;
    /** @brief Forçar a remoção de imagens sem confirmação. */
    unsigned char force;
#ifndef NDEBUG
    /** @brief Ativar modo de desenvolvimento. */
    unsigned char debug;
#endif
};

/**
 * @brief Nome resumido pelo qual o programa foi executado.
 * 
 * Variável global definida em args_init().
 */
char *program_name;

/**
 * @brief Comando pelo qual o programa foi executado, incluindo diretórios.
 * 
 * Variável global definida em args_init().
 */
char *program_path;

extern void args_init (int, char **, struct args *);
extern void args_finish (struct args *);
extern void args_parse (int, char **, struct args *);

#endif /* __ARGS_H__ */

/* vim: set et ts=4 sw=4 sts=4 tw=80 cino+=(0,w1,u0,t0 fdm=marker: */
