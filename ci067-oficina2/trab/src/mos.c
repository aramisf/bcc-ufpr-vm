/**
 * @file mos.c
 * @brief Manipulação do mosaico.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <endian.h>
#include "util.h"
#include "err.h"
#include "mos.h"

/**
 * @brief Compara novo buraco com um da lista.
 * @param linf Borda inferior do buraco do item da lista.
 * @param lsup Borda superior do buraco do item da lista.
 * @param ninf Borda inferior do novo buraco.
 * @param nsup Borda superior do novo buraco.
 * @return Retorna @c zero caso já existe o buraco na lista, maior que @c
 *         zero caso o buraco deva ser inserido depois deste buraco da
 *         lista e menor que @c zero, caso deva ser inserido antes.
 *
 * @note O retorno igual a @c zero considera o caso em que o buraco da lista
 *       possa ser expandido para encaixar o novo buraco.
 */
static int
bgholes_cmp (long linf, long lsup, long ninf, long nsup)
{
    if ((ninf >= linf && nsup <= lsup) || (nsup >= linf && nsup <= lsup)
        || (ninf >= linf && ninf <= lsup) || (ninf <= linf && nsup >= lsup))
        return 0;
    else
        return ninf - linf;
}

/**
 * @brief Cria novo nó para a lista de buracos.
 * @param inf Borda inferior do buraco.
 * @param sup Borda superior do buraco.
 * @param next Próximo nó.
 * @return Novo nó de buracos.
 */
static struct bgholes *
bgholes_create (long inf, long sup, struct bgholes *next)
{
    struct bgholes *new = err_malloc (sizeof (*new));
    new->inf = inf;
    new->sup = sup;
    new->prev = NULL;
    new->next = next;
    return new;
}

/**
 * @brief Destrói uma lista de buracos.
 * @param bgholes Ponteiro para lista de buracos.
 */
static void
bgholes_destroy (struct bgholes *bgholes)
{
    struct bgholes *next;

    while (bgholes)
    {
        next = bgholes->next;
        free (bgholes);
        bgholes = next;
    }
}

/**
 * @brief Insere um buraco no fundo da imagem na lista de buracos.
 * @param bgholes Lista de buracos.
 * @param inf Borda inferior do buraco a ser inserido.
 * @param sup Borda superior do buraco a ser inserido.
 * @return Nova lista de buracos.
 */
static struct bgholes *
bgholes_insert (struct bgholes *bgholes, long inf, long sup)
{
    int cmp;
    struct bgholes *prev = NULL;
    struct bgholes *temp = bgholes;

    debug ("buraco:%ld,%ld", inf, sup);

    /* Lista vazia. */
    if (!bgholes)
    {
        debug ("lista vazia");
        return bgholes_create (inf, sup, NULL);
    }

    /* Compara com o primeiro elemento. */
    cmp = bgholes_cmp (temp->inf, temp->sup, inf, sup);
    debug ("cmp:%d %ld,%ld", cmp, temp->inf, temp->sup);

    /* Procura o lugar onde o buraco BGHOLE deve ser inserido. */
    while (temp->next && cmp > 0)
    {
        prev = temp;
        temp = temp->next;
        cmp = bgholes_cmp (temp->inf, temp->sup, inf, sup);
        debug ("cmp:%d %ld,%ld", cmp, temp->inf, temp->sup);
    }

    if (!cmp)
    {
        debug ("buraco:%ld,%ld", temp->inf, temp->sup);
        temp->inf = MIN (inf, temp->inf);
        temp->sup = MAX (sup, temp->sup);
        debug ("       %ld,%ld", temp->inf, temp->sup);

        inf = temp->inf;
        sup = temp->sup;
        prev = temp;
        temp = temp->next;
        cmp = 0;

        while (temp && cmp >= 0)
        {
            cmp = bgholes_cmp (temp->inf, temp->sup, inf, sup);
            debug ("cmp:%d hole:%ld,%ld procurando %ld,%ld",
                   cmp, temp->inf, temp->sup, inf, sup);

            if (!cmp)
            {
                prev->inf = MIN (inf, temp->inf);
                prev->sup = MAX (sup, temp->sup);
                prev->next = temp->next;
                free (temp);
                temp = prev->next;
            }
            else
            {
                temp = temp->next;
            }
        }

        return bgholes;
    }

    /* Insere no final da lista. */ 
    if (!temp->next && cmp > 0)
    {
        debug ("final da lista");
        temp->next = bgholes_create (inf, sup, NULL);
        return bgholes;
    }
    
    /* Insere no início da lista. */
    if (!prev)
    {
        debug ("início da lista");

        if (cmp)
            return bgholes_create (inf, sup, bgholes);
        else
            return bgholes;
    }
    else
    {
        debug ("meio da lista");

        if (cmp)
            prev->next = bgholes_create (inf, sup, temp);
        
        return bgholes;
    }
}

/**
 * @brief Inicia mosaico para impressão.
 * @param mos Estrutura de imagem que salva o mosaico.
 * @param bg Cor de fundo do mosaico.
 * @param db Banco de dados.
 * @param filename Arquivo onde deve ser impresso o mosaico.
 */
void
mos_init (struct mos *mos, struct db *db, const char *filename, unsigned short bg)
{
    int i, max, magic;
    struct img *img;                /* ponteiro para imagem em processamento */
    struct img *mosimg = &mos->img; /* ponteiro para imagem do mosaico */

    magic = '5';

    mosimg->header.cols = 0;
    mosimg->header.rows = 0;
    mosimg->header.maxval = 0;
    mosimg->filename = filename;
    mosimg->file = err_fopen (mosimg->filename, "w");

    mos->bg = 0;
    mos->db = db;
    mos->bgholes = NULL;
    mos->imgs = err_malloc (db->entries * sizeof (*mos->imgs));

    for (i = 0; i < db->entries; i++)
    {
        img = &mos->imgs[i];

        err_fseek (db->file, db->table[i].pos, SEEK_SET);
        err_fread (&img->header, sizeof (img->header), 1, db->file);

        /* Número de colunas do mosaico. */
        max = db->table[i].col + img->header.cols;

        if (mosimg->header.cols < max)
            mosimg->header.cols = max;

        /* Número de linhas do mosaico. */
        max = db->table[i].row + img->header.rows;

        if (mosimg->header.rows < max)
            mosimg->header.rows = max;

       /* Número MAXVAL do mosaico. */
        if (mosimg->header.maxval < img->header.maxval)
            mosimg->header.maxval = img->header.maxval;

        /* Magic number do mosaico. Escolher o maior entre P5 e P6. O
         * valor padrão gravado em MAGIC é '5'. */
        if (img->header.magic[1] > magic)
            magic = img->header.magic[1];

        img->pixels = img->header.cols * img->header.rows;
        img->levels = (img->header.magic[1] == '6' ? 3 : 1);
        img->bytes_per_level = (img->header.maxval < 256 ? 1 : 2);
        img->bytes_per_pixel = img->levels * img->bytes_per_level;

        /* Cor de fundo do mosaico é a média entre todos MAXVAL. */
        mos->bg += (img->header.maxval - mos->bg) / (i + 1);
    }

    /* Se algum valor não negativo foi passado como argumento, user o valor BG
     * em vez da média calculada. */
    if (bg >= 0)
        mos->bg = bg;

    mosimg->header.magic[0] = 'P';
    mosimg->header.magic[1] = magic;
    mosimg->header.magic[2] = '\0';

    mosimg->pixels = IMG_PIXELS (mosimg);
    mosimg->levels = IMG_LEVELS (mosimg);
    mosimg->bytes_per_level = IMG_BYTES_PER_LEVEL (mosimg);
    mosimg->bytes_per_pixel = IMG_BYTES_PER_PIXEL (mosimg);

    debug ("mosaico magic:%s cols:%d rows:%d maxval:%d "
           "pixels:%d levels:%d bytes_per_level:%d bytes_per_pixel:%d bg:%d ",
           mosimg->header.magic, mosimg->header.cols, mosimg->header.rows,
           mosimg->header.maxval, mosimg->pixels, mosimg->levels, mosimg->bytes_per_level,
           mosimg->bytes_per_pixel, mos->bg);
}

/**
 * @brief Finaliza o mosaico.
 * @param mos Mosaico.
 */
void
mos_finish (struct mos *mos)
{
    bgholes_destroy (mos->bgholes);
    free (mos->imgs);
    fclose (mos->img.file);
}

/**
 * @brief Imprime o cabeçalho do mosaico.
 * @param mos Mosaico.
 *
 * @note Guarda o tamanho do cabeçalho, em bytes, em mos::shift.
 */
void
mos_header_print (struct mos *mos)
{
    mos->shift = fprintf (mos->img.file, "%s\n%d %d\n%d\n",
                          mos->img.header.magic, mos->img.header.cols,
                          mos->img.header.rows, mos->img.header.maxval);
}

/**
 * @brief Imprime a cor de fundo em todo o mosaico.
 * @param mos Mosaico.
 */
void
mos_bg_print (struct mos *mos)
{
    int i;
    unsigned char buf[BUFSIZ];
    long amount, chunk;
    struct bgholes *prev = NULL;
    struct bgholes *node = mos->bgholes;
    struct img *mosimg = &mos->img; /* ponteiro para imagem do mosaico */

    /* Preenche o buffer BUF com o valor de fundo para ser usado na escrita. */
    if (mosimg->bytes_per_level == 1)
    {
        memset (&buf, mos->bg, sizeof (buf));
    }
    else
    {
        /* Este loop funciona também para BYTES_PER_LEVEL igual a 1, mas com o a
         * verificação explícita neste if-else, resolve-se tudo com um único
         * memset() quando BYTES_PER_LEVEL igual a 1. */
        for (i = 0; i < sizeof (buf); i += mosimg->bytes_per_level)
            memcpy (&buf[i], &mos->bg, mosimg->bytes_per_level);
    }

    /* Posiciona o ponteiro do arquivo no início da grade de pixels. */
    err_fseek (mosimg->file, mos->shift, SEEK_SET);

    /* Loop pela lista de buracos. */
    while (node)
    {
        if (prev)
            amount = node->inf - prev->sup - 1;
        else
            amount = node->inf;
        
        while (amount > 0)
        {
            chunk = (amount > sizeof (buf) ? sizeof (buf) : amount);
            err_fwrite (buf, sizeof (*buf), chunk, mosimg->file);
            amount -= chunk;
        }

        err_fseek (mosimg->file, node->sup - node->inf + 1, SEEK_CUR);

        prev = node;
        node = node->next;
    }

    /* Terminar a escrita do fundo em casos como este:
     * +--------------------------+
     * |                   xxxxxx |
     * |                   xxxxxx |
     * |                   xxxxxx |
     * | xxxxxx                   |
     * | xxxxxx                   |
     * | xxxxxx<-- este pedaço -->|
     * +--------------------------+
     */
    amount = mosimg->bytes_per_pixel * mosimg->pixels - prev->sup - 1;

    while (amount > 0)
    {
        chunk = (amount > sizeof (buf) ? sizeof (buf) : amount);
        err_fwrite (buf, sizeof (*buf), chunk, mosimg->file);
        amount -= chunk;
    }
}

/**
 * @brief Ajusta valores de @c MAXVAL.
 * @param buf Buffer com valores para ajuste.
 * @param buflen Tamanho do buffer @p buf.
 * @param bytes_per_level Tamando de cada valor a ser ajustado (1 ou 2).
 * @param factor Fator de ajuste.
 */
static void
mosaico_maxval_scale (unsigned char *buf, long buflen, int bytes_per_level,
                      double factor)
{
    long i;
    unsigned short temp;

    /* Caso o tamanho de cada valor guardado no buffer BUF tenha um byte,
     * aplicar o fator diretamente. */
    if (bytes_per_level == 1)
    {
        for (i = 0; i < buflen; i++)
            buf[i] *= factor;
    }
    else
    {
        /* Se o tamanho do valor for diferente de 1 byte, é preciso copiar os
         * todos os BYTES_PER_LEVEL bytes para uma única variável temporária,
         * aplicar a ajuste e retornar o valor para o buffer. Esse mesmo loop
         * funciona para BYTES_PER_LEVEL igual a 1. No entanto, para evitar duas
         * chamadas a memcpy() desnecessárias, o caso "1" é tratado
         * separadamente. */
        for (i = 0; i < buflen; i += bytes_per_level)
        {
            temp = buf[i + (bytes_per_level - 1)];
            temp *= factor;
            memcpy (&buf[i], &temp, bytes_per_level);

#if __BYTE_ORDER == LITTLE_ENDIAN
            /* Trocar a ordem dos bytes para little endian machine. */
            temp = buf[i];
            buf[i] = buf[i + 1];
            buf[i + 1] = temp;
#endif
        }
    }
}

/**
 * @brief Converte parte de imagem salva no buffer @p buf para se ajustar ao
 *        formato do mosaico @p mosimg.
 * @param buf Buffer.
 * @param buflen Quantidade ocupada do buffer @p buf.
 * @param mosimg Imagem do mosaico.
 * @param img Imagem.
 */
static void
mosaico_img_convert (unsigned char *buf, long buflen, struct img *mosimg,
                    struct img *img)
{
    int i, j, x, y;
    unsigned char ratio = mosimg->bytes_per_pixel / img->levels;

    for (i = buflen - 1; i >= 0; i--)
    {
        x = i / img->bytes_per_level;
        y = (i % img->bytes_per_level +
             mosimg->bytes_per_level - img->bytes_per_level);

        for (j = 0; j < ratio; j += mosimg->bytes_per_level)
            buf[x * ratio + y + j] = buf[i];
    }

    if (mosimg->bytes_per_level > img->bytes_per_level)
        for (i = 0; i < buflen * mosimg->bytes_per_level;
             i += mosimg->bytes_per_level)
            buf[i] = 0;
}

/**
 * @brief Imprime as imagens no mosaico e calcula os buracos do fundo.
 * @param mos Estrutura do mosaico.
 */
void
mos_imgs_print (struct mos *mos)
{
    int i, row, buflen, factor;
    long pos, read_amount, read_chunk;
    long write_amount, write_chunk, row_chunk, row_amount;
    unsigned char buf[BUFSIZ];
    double scale;
    long inf, sup;
    struct img *img;                /* ponteiro para imagem em processamento */
    struct db_table *table;         /* ponteiro para tabela da imagem */
    struct img *mosimg = &mos->img; /* ponteiro para imagem do mosaico */
    struct db *db = mos->db;        /* ponteiro para banco de dados */
    
    for (i = 0; i < db->entries; i++)
    {
        img = &mos->imgs[i];
        table = &db->table[i];

        debug ("imgs[%d] id:%d magic:%s cols:%d rows:%d maxval:%d "
               "bytes_per_pixel:%d levels:%d bytes_per_level:%d pixels:%d",
               i, table->id, img->header.magic, img->header.cols,
               img->header.rows, img->header.maxval, img->bytes_per_pixel,
               img->levels, img->bytes_per_level, img->pixels);

        /* Fator de conversão de valores máximos MAXVAL da imagem em relação ao
         * valor máximo MAXVAL do mosaico. */
        scale = mosimg->header.maxval / (double) img->header.maxval;
        debug ("scale:%lf", scale);

        /* Fator de contração do buffer. Caso a imagem precise ser expandida por
         * alguma conversão, esse valor indica quantas vezes a imagems será
         * expandida. */
        factor = mosimg->bytes_per_pixel / img->bytes_per_pixel;
        debug ("%d -> %d factor:%d", img->bytes_per_pixel,
               mosimg->bytes_per_pixel, factor);

        /* Posição do ponteiro dentro do banco de dados. */
        pos = table->pos + sizeof (img->header);
        err_fseek (db->file, pos, SEEK_SET);

        /* Posição do ponteiro dentro do mosaico */
        pos = MOS_POS (table->row, table->col, mos);
        err_fseek (mosimg->file, pos, SEEK_SET);

        /* Quantidade de bytes que são lidos para consumir toda a imagem no
         * banco de dados. */
        read_amount = table->size - sizeof (img->header);

        /* Linha no mosaico onde a imagem deve ser colocada. Durante a escrita
         * é incrementada. */
        row = table->row;

        /* Quantidade de bytes que forma uma linha, ou seja, número de colunas
         * da imagem vezes o tamanho de cada bloco do mosaico. */
        row_amount = img->header.cols * mosimg->bytes_per_pixel;

        /* Quantidade de bytes já escrita para o mosaico na linha atual. A cada
         * nova linha essa variável é zerada. */
        row_chunk = 0;

        while (read_amount > 0)
        {
            read_chunk = (read_amount > sizeof (buf) / factor ?
                          sizeof (buf) / factor : read_amount);

            err_fread (buf, sizeof (*buf), read_chunk, db->file);
            read_amount -= read_chunk;
            write_amount = read_chunk * factor;
            buflen = write_amount;

            /* Expandir os bytes da imagem para se ajustar ao mosaico. */
            if (factor != 1)
                mosaico_img_convert (buf, read_chunk, mosimg, img);

            /* Ajustar os valores desse pedaço da imagem para MAXVAL do
             * mosaico. */
            if (img->header.maxval != mosimg->header.maxval)
                mosaico_maxval_scale (buf, buflen, mosimg->bytes_per_level,
                                      scale);
                
            while (write_amount > 0)
            {
                /* Escrever uma linha por vez. */
                write_chunk = (write_amount > row_amount ?
                               row_amount : write_amount);
                
                /* Se o total que já foi escrito nesta linha, ROW_CHUNK,
                 * mais o total que será escrito agora, WRITE_CHUNCK, for maior
                 * que o tamaho total de cada linha, ROW_AMOUNT, então diminuir a
                 * quantidade que será escrita para que não ultrapasse o total
                 * da linha. */
                if (row_chunk + write_chunk >= row_amount)
                    write_chunk = row_amount - row_chunk;
            
                /* Preparar para próxima linha. */
                if (row_chunk >= row_amount)
                {
                    /* Inserir linha na lista de buracos. */
                    pos = MOS_POS (row, table->col, mos);
                    inf = pos - mos->shift;
                    sup = inf + row_amount - 1;
                    mos->bgholes = bgholes_insert (mos->bgholes, inf, sup);

                    row++;
                    row_chunk = 0;
                    pos = MOS_POS (row, table->col, mos);
                    err_fseek (mosimg->file, pos, SEEK_SET);
                }

                /* Ufa! Finalmente, escrever esse pedaço de linha (ou uma linha
                 * toda) para o mosaico. */
                err_fwrite (buf + buflen - write_amount, sizeof (*buf),
                            write_chunk, mosimg->file);
                write_amount -= write_chunk;
                row_chunk += write_chunk;
            }

            /* Inserir última linha na lista de buracos. */
            pos = MOS_POS (row, table->col, mos);
            inf = pos - mos->shift;
            sup = inf + row_amount - 1;
            mos->bgholes = bgholes_insert (mos->bgholes, inf, sup);
        }
    }
}

/* vim: set et ts=4 sw=4 sts=4 tw=80 cino+=(0,w1,u0,t0 fdm=marker: */
