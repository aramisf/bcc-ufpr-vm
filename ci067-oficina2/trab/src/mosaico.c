/**
 * @file mosaico.c
 * @brief Mosaico de imagens.
 */

#include <stdlib.h>
#include <stdio.h>
#include <err.h>
#include "args.h"
#include "db.h"

int
main (int argc, char *argv[])
{
    struct args args;
    struct db db;

    args_init (argc, argv, &args);
    args_parse (argc, argv, &args);
    db_init (&db, args.database);

    switch (args.action)
    {
    case ACTION_ADD:
        db_add (&db, args.id, args.col, args.row, args.filename, args.force);
        break;
    case ACTION_MOVE:
        db_move (&db, args.id, args.col, args.row, args.force);
        break;
    case ACTION_REMOVE:
        db_remove (&db, args.id, args.force);
        break;
    case ACTION_PRINT:
        db_print (&db, args.filename, args.bg);
        break;
    case ACTION_CLEAN:
        db_clean (&db, args.force);
        break;
    case ACTION_NOTHING:
        break;
    default:
        errx (EXIT_FAILURE, "ERRO DE PROGRAMA: ação `%d' não tratada.",
              args.action);
        break;
    }

    if (args.list)
        db_list (&db);

    db_finish (&db);
    args_finish (&args);

    return EXIT_SUCCESS;
}

/* vim: set et ts=4 sw=4 sts=4 tw=80 cino+=(0,w1,u0,t0 fdm=marker: */
