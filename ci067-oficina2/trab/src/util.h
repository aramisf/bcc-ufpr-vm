/**
 * @file util.h
 * @brief Funções com utilidades gerais.
 */

#ifndef __UTIL_H__
#define __UTIL_H__

#ifndef __ARGS_H__
# include "args.h"
#endif

#ifndef _STDIO_H
# include <stdio.h>
#endif

extern int getcc (FILE *);
extern int getci (FILE *);
extern unsigned char yesno (void);
extern void showbits (unsigned short);

#define MAX(a,b) ((a) > (b) ? (a) : (b))
#define MIN(a,b) ((a) < (b) ? (a) : (b))

#define yesno_ask(response,args...)             \
    do {                                        \
        fprintf (stderr, "%s: ", program_name); \
        fprintf (stderr, ## args);              \
        fprintf (stderr, " [s/N] ");            \
        fflush (stderr);                        \
        *response = yesno ();                   \
    } while (0)

#endif /* __UTIL_H__ */

/* vim: set et ts=4 sw=4 sts=4 tw=80 cino+=(0,w1,u0,t0 fdm=marker: */
