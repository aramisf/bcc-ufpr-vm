/**
 * @file img.h
 * @brief Manipulação de imagens.
 */

#ifndef __IMG_H__
#define __IMG_H__

/** @brief Número de pixels de uma imagem. */
#define IMG_PIXELS(img) ((img)->header.cols * (img)->header.rows)
/** @brief Níveis de cor uma imagem: 1 para escala de cinza e 3 para RGB. */
#define IMG_LEVELS(img) ((img)->header.magic[1] == '6' ? 3 : 1)
/** @brief Tamanho de cada nível de cor em bytes. */
#define IMG_BYTES_PER_LEVEL(img) ((img)->header.maxval < 256 ? 1 : 2)
/** @brief Tamanho de cada pixel em bytes. */
#define IMG_BYTES_PER_PIXEL(img) ((img)->levels * (img)->bytes_per_level)

/**
 * @brief Cabeçalho de uma imagem.
 */
struct img_header
{
    /** @brief <i>Magic Number</i> identificador de tipo. */
    unsigned char magic[3];
    /** @brief Número de colunas da imagem. */
    unsigned int cols;
    /** @brief Número de linhas da imagem. */
    unsigned int rows;
    /** @brief Valor máximo de cor. Deve ser menor que @c 65536. */
    unsigned short maxval;
};

/**
 * @brief Estrutura com dados de uma imagem.
 */
struct img
{
    /** @brief Nome do arquivo da imagem. */
    const char *filename;
    /** @brief @e Stream do arquivo da imagem. */
    FILE *file;
    /** @brief Informações de cabeçalho da imagem. */
    struct img_header header;
    /** @brief Número de pixels do vetor. */
    unsigned int pixels;
    /** @brief Número de elementos de cada pixel. */
    unsigned char levels;
    /** @brief Tamanho de cada elemento (em bytes). */
    unsigned char bytes_per_level;
    /** @brief Número de bytes de cada pixel (bytes per pixel). */
    unsigned char bytes_per_pixel;
};

extern void img_init (struct img *, const char *);
extern void img_finish (struct img *);
extern long img_read (struct img *, unsigned char *, long, long);

#endif /* __IMG_H__ */

/* vim: set et ts=4 sw=4 sts=4 tw=80 cino+=(0,w1,u0,t0 fdm=marker: */
