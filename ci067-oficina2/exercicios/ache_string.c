/*
* Dado um conjunto de strings e uma palavra, determinar o número de vezes que a
* palavra ocorre em cada string. Se for indicada a palavra FIM, o programa
* deve terminar. Exemplo:
* frase 1: ANA E MARIANA GOSTAM DE BANANA
* frase 2: MARIANA GOSTA DE CHICLETE
* frase 3: SONIA NAO GOSTA DE NADA
* palavra: ANA
* ANA ocorre 4 vezes na frase 1.
* ANA ocorre 1 vez na frase 2.
* palavra: BACANA
* BACANA nao ocorre em nenhuma das frases. (ache_string)
*/

#include <stdio.h>
#include <string.h>

int BuscaChave(char frases[][100], char chave[], int i) {
    int n_frase, n_chave, dif, cont, j, k, n = 0;

    n_frase = strlen(frases[i])-1;
    n_chave = strlen(chave)-1;

    dif = n_frase - n_chave;
    if (dif < 0) return 0;
    else {
        for (j = 0; j <= dif; j++) {
            cont = 0;
            if (frases[i][j] == chave[0]) {
               for (k = 0; k < n_chave; k++) if (chave[k] == frases[i][j+k]) cont++;
               if (cont == n_chave) n++;
            }
        }
        return n;
    }
}

int main() {
    int i = 1, n, n_vezes, cont;
    char frases[101][100], chave[100] = { 'N' };

	printf("Numero de frases: ");
	scanf("%d", &n);
	fgets(frases[0], 100, stdin);

    while (i<=n) {
		printf("Frase %d: ", i);
		fgets(frases[i],100,stdin);
		i++;
    }

    while (strcmp(chave,"FIM\n") != 0) {
		cont = 0;
		printf("Chave: ");
		fgets(chave, 100, stdin);

		for (i=1; i<=n; i++) {
            n_vezes = BuscaChave(frases, chave, i);
            if (n_vezes != 0) printf("%d vezes na frase %d.\n", n_vezes, i);
            else cont++;
        }
    }

    return 0;
}