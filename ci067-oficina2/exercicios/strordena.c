/*
* Escreva um programa que leia uma string e ordene seus caracteres em ordem
* crescente. NOTA: Uma letra maiúscula vem antes de sua correspondente
* minúscula, e ambas vem antes de sua letra sucessora. (Ex.: A vem antes de
* a, e ambas vem antes de B e b). (strordena)
*/

#include <stdio.h>

void TrocaLetras(char palavra[],int pos1, int pos2) {
	char aux;
	aux = palavra[pos1];
	palavra[pos1] = palavra[pos2];
	palavra[pos2] = aux;
}

int MedeTamanho(char palavra[]) {
	int i = 0;;
	while (palavra[i]) i++;
	return i;
}

void OrdenaPalavra(char palavra[],int n) {
	int i, j, menor, auxj, auxmenor;

	for (i = 0; i < n-1; i++) {
		menor = i;
		for (j = i+1; j < n; j++) {
			if (palavra[j] >= 97) auxj = -32; else auxj = 0;
			if (palavra[menor] >= 97) auxmenor = -32; else auxmenor = 0;
			if (palavra[j]+auxj == palavra[menor]+auxmenor) {
				if (auxj < 0) auxj = 1; else auxmenor = 1;
			}
			if (palavra[j]+auxj < palavra[menor]+auxmenor) menor=j;
		}
		TrocaLetras(palavra,i,menor);
	}
}

int main() {
	int tamanho;
	char palavra[100];

	printf("Entre com uma palavra: ");
	scanf("%s",palavra);
	tamanho = MedeTamanho(palavra);
	OrdenaPalavra(palavra,tamanho);
	printf("%s\n",palavra);
	return 0;
}