/*
* Faça uma função que conte o número de vogais existentes em uma string fornecida
* pelo usuário.  (contavogais)
*/

#include <stdio.h>

int ContaVogais(char palavra[]) {
	int i;
	int vogais = 0;

	for	(i=0; palavra[i]; i++)
		if (
			(palavra[i] == 'A') ||
			(palavra[i] == 'E') ||
			(palavra[i] == 'I') ||
			(palavra[i] == 'O') ||
			(palavra[i] == 'U') ||
			(palavra[i] == 'a') ||
			(palavra[i] == 'e') ||
			(palavra[i] == 'i') ||
			(palavra[i] == 'o') ||
			(palavra[i] == 'u')
		) vogais++;

	return vogais;
}

int main() {
	char palavra[100];

	printf("Entre com uma palavra: ");
	scanf("%s",palavra);
	printf("%d vogais\n",ContaVogais(palavra));
	return 0;
}