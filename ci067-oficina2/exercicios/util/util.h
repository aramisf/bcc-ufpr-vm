/*
Fazer uma biblioteca chamada util de forma que

   1. Ela possa ser utilizada na compilação de programas através da opções -I e -l do comando gcc(1).
   2. Esta biblioteca deve ser de propósito geral, isto é, as funções devem ser genéricas o bastante para serem úteis a quaisquer programas que você esteja fazendo ou venha a fazer;
   3. As funções que devem ser definidas nesta biblioteca são:

      void nflushin(FILE *stream)
          Esvazia buffer de stream de entrada;
      char *ngets (char *buf, int maxtam, FILE *stream)
          Mesmo que fgets(), mas retira do buffer o caracter ' $ \backslash n$ '  final que fgets() insere no buffer de entrada;
      char *ngetval(char *prompt, int maxtam)
          Apresenta prompt na saida padrão (stdout) e retorna string recebido pela entrada padrão (stdin);
      char *strtoupper(char *string)
          Converte todos os caracteres de uma string para MAIÚSCULA;
      char *strtolower(char *string)
          Converte todos os caracteres de uma string para minúscula.

   4. A contrução da biblioteca deve ser definida através de um arquivo Makefile apropriado.
      Neste arquivo, além dos targets de limpeza e faxina do diretório, deve constar também um target chamado instala, que após a geração da biblioteca a instale (copie) em um diretório apropriado em sua área, específico para ter bibliotecas para uso por outros programas (por exemplo, HOME/lib e HOME/include).
      Após ser gerada a biblioteca, uma mensagem deve ser emitida ao usuário orientando sobre o uso da biblioteca em compilação e na execução de programas que a usem.
*/

/**
 * @file util.h
 * @brief Funções com utilidades gerais.
 */

#ifndef __UTIL_H__
	#define __UTIL_H__

	#ifndef _STDIO_H
		# include <stdio.h>
	#endif

	extern void nflushin(FILE *stream);
	extern char *ngets (char *buf, int maxtam, FILE *stream);
	extern char *ngetval(char *prompt, int maxtam);
	extern char *strtoupper(char *string);
	extern char *strtolower(char *string);
#endif