/**
 * @file util.c
 * @brief Funções com utilidades gerais.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * @brief Esvazia buffer de stream de entrada
 * @param strem Stream de entrada
 */
void
nflushin (FILE *stream)
{
	int i = 0;
	while (((i = getc(stream)) != EOF) && (i != '\n')) continue;
}

/**
 * @brief Mesmo que fgets(), mas retira do buffer o caracter '\n ' final que fgets() insere no buffer de entrada
 * @param buf Buffer de entrada
 * @param maxtam Tamanho máximo de leitura
 * @param stream Stream de entrada
 * @return String lida por fgets mas sem o '\n' no final
 */
char
*ngets (char *buf, int maxtam, FILE *stream)
{
	fgets(buf, maxtam, stream);
	buf[maxtam] = '\0';
	return buf;
}

/**
 * @brief Apresenta prompt na saida padrão (stdout) e retorna string recebido pela entrada padrão (stdin)
 * @param prompt String apresentada antes da leitura pela entrada padrão
 * @param maxtam Tamanho máximo de leitura
 * @return String lida pela entrada padrão
 */
char
*ngetval(char *prompt, int maxtam)
{
	char *out = malloc(maxtam);
	printf("%s: ",prompt);
	fgets(out,maxtam,stdout);
	return out;
}

/**
 * @brief Converte todos os caracteres de uma string para maiúsculo
 * @param string String ANSI
 * @return string ANSI com todos os caracteres em maiúsculo
 */
char
*strtoupper (char *string)
{
	int i = 0;

	while (string[i]) {
		if (string[i] < 97)
			string[i] = string[i] + 32;
		i++;
	}

	return string;
}

/**
 * @brief Converte todos os caracteres de uma string para minúsculo
 * @param string String ANSI
 * @return string ANSI com todos os caracteres em minúsculo
 */
char
*strtolower (char *string)
{
	int i = 0;
	while (string[i]) {
		if (string[i] >= 97)
			string[i] = string[i] - 32;
		i++;
	}

	return string;
}