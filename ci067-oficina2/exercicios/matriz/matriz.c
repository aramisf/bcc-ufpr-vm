# include <stdlib.h>
# include <stdio.h>

float** matriz_cria (int cols, int rows) {
	int i;

	float **m = malloc (sizeof(float) * cols);
	for (i = 0; i < cols; i++) {
		m[i] = malloc (sizeof(float) * rows);
	}

	return m;
}

void matriz_destroi (float** m, int cols, int rows) {
	int i;
	for (i = 0; i < cols; i++) {
		free(m[i]);
	}
	free(m);
}

float** matriz_muda (float **m, int cols, int rows, int ncols, int nrows) {
	int i,j,tcols,trows;

	float **n = malloc	( sizeof(float) * ncols);
	for (i = 0; i < ncols; i++)
		n[i] = malloc( sizeof(float) * nrows);

	if (cols > ncols) tcols = ncols;
	else tcols = cols;
	
	if (rows > nrows) trows = nrows;
	else trows = rows;

	for (i = 0; i < tcols; i++) {
		for (j = 0; j < trows; j++) {
			n[i][j] = m[i][j];
		}
	}
	matriz_destroi(m,cols,rows);
	return n;
}

float** matriz_pedaco (float **m, int col1, int row1, int col2, int row2) {
	int i,j;
	int cols = col1 + col2;
	int rows = row1 + row2;

	float **n = malloc	(sizeof(float) * col2-col1);
	for (i = col1; i < cols; i++) {
		n[i] = malloc(sizeof(double) * row2-row1);
		for (j = row1; j < rows; j++) {
			n[i-col1][j-row1] = m[i][j];
		}
	}
	matriz_destroi(m,cols,rows);
	return n;
}

void matriz_transfere (
	float **m1, float **m2,
	int m1_col1, int m1_row1,
	int m1_col2, int m1_row2,
	int m2_col1, int m2_row1,
	int m2_col2, int m2_row2) {
	
	int i,j;
	
	for (i = 0; i < m2_col2 + m2_col1; i++) {
		for (j = 0; j < m2_row2 + m2_row1; j++) {
			m1[i + m1_col1][j + m1_row1] = m2[i + m2_col1][j + m2_row1];
		}
	}
}

void matriz_sobreescreve ( float **m, int col1, int row1, int col2, int row2, float val) {
	int i,j;
	for (i = col1; i < col1 + col2; i++) {
		for (j = row1; j < row1 + row2; j++) {
			m[i][j] = val;
		}
	}
}

void matriz_preenche (float **m, int cols, int rows) {
	int i,j;
	for (i = 0; i < cols; i++) {
		for (j = 0; j < rows; j++) {
			scanf("%f",&m[i][j]);
		}
	}
}

void matriz_imprime (float **m, int cols, int rows) {
	int i,j;
	for (i = 0; i < cols; i++) {
		for (j = 0; j < rows; j++) {
			if (m[i][j] == 0) printf("- | ");
			else printf("%7f | ",m[i][j]);
		}
		printf("\n");
	}
}
