# ifndef __MATRIZ_H__
	# define __MATRIZ_H__

	extern float** matriz_cria (int cols, int rows);
	extern void matriz_destroi (float** m, int cols, int rows);
	extern float** matriz_muda (float **m, int cols, int rows, int ncols, int nrows) {
	extern float** matriz_pedaco (float **m, int col1, int row1, int col2, int row2) {
	extern void matriz_transfere (float **m1, float **m2, int m1_col1, int m1_row1, int m1_col2, int m1_row2, int m2_col1, int m2_row1, int m2_col2, int m2_row2);
	extern void matriz_sobreescreve ( float **m, int col1, int row1, int col2, int row2, float val);
	extern void matriz_preenche (float **m, int cols, int rows);
	extern void matriz_imprime (float **m, int cols, int rows);

# endif
