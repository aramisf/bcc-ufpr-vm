#ifndef __LISTA_H__
	#define __LISTA_H__

	typedef struct item lista;
	struct item {
		void *val;
		struct item *prox;
	};

	extern lista* iniciar (void);
	extern lista* inserir (void *x, lista *lst);
	extern lista* remover (lista *lst);
	extern int e_vazia(lista *lst);
	extern void imprime (lista *lst);
#endif
