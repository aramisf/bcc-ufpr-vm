# include <stdlib.h>
# include <stdio.h>
# include "lista.h"

lista* iniciar (void) {
	return NULL;
}

lista* inserir (void *x, lista *lst) {
	lista *novo = malloc (sizeof(lista));
	novo->val = x;
	novo->prox = lst;
	return novo;
}

lista* remover (lista *lst) {
	lista *p = lst->prox;
	if (lst->val != NULL)
		lst->prox = p;
	return lst;
}

int e_vazia(lista *lst) {
	if (lst->val) return 1;
	else return 0;
}

int pega_tamanho (lista *lst) {
	int i = 0;
	lista *p;
	for (p = lst; p != NULL; p = p->prox) i++;
	return i;
}

int main () {
	lista *lst;
	lst = iniciar();
	//lst = preenche_lista(lst);
	//imprime (lst);
	//lst = sort(lst,pega_tamanho(lst));
	return 0;
}
