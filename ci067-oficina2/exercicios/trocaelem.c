/*
* Fa�a um programa que obtenha do usu�rio uma matriz quadrada (dimens�o m�xima de
* 10x10) e troque o maior elemento de cada linha com o elemento da diagonal.
* (trocaelem)
*/

#include <stdio.h>

void LeMatriz(int matriz[10][10], int n) {
	int i,j;

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
				scanf("%d",&matriz[i][j]);
		}
	}
}

void ImprimeMatriz(int matriz[10][10], int n) {
	int i,j;

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
				printf("%d ",matriz[i][j]);
		}
		printf("\n");
	}
}

void TrocaElementos(int matriz[10][10], int n) {
	int i,j,k,maior,jmaior;

	for (i = 0; i < n; i++) {
		maior = matriz[i][1];
		jmaior = 1;
		for (j = 0; j < n; j++) {
			if (matriz[i][j] > maior) {
				maior = matriz[i][j];
				jmaior = j;
			}
		}
		k = matriz[i][i];
		matriz[i][i] = matriz[i][jmaior];
		matriz[i][jmaior] = k;
	}
}

int main() {
	int dimensao,matriz[10][10];

	printf("Entre com a dimensao: ");
	scanf("%d", &dimensao);
	if (dimensao > 10) printf("Dimensao nao permitida.\n");
	else {
		printf("Entre com os %d elementos: ",(dimensao*dimensao));
		LeMatriz(matriz, dimensao);
		printf("Matriz original:\n");
		ImprimeMatriz(matriz,dimensao);
		TrocaElementos(matriz,dimensao);
		printf("Matriz com elementos trocados:\n");
		ImprimeMatriz(matriz,dimensao);
	}
	return 0;
}