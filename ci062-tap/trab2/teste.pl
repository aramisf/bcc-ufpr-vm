uses flavours;
load elevadorgraf.p;
load vam06.p;

vars e1 p1 p2 p3 p4 f1 f2 f3;

make_instance([ elevador nome e1 altura 8 andar 0 ]) -> e1;
make_instance([ pessoa nome p1 andar 3 meu_elevador e1 ]) -> p1;
make_instance([ pessoa nome p2 andar 3 meu_elevador e1 ]) -> p2;
make_instance([ pessoa nome p3 andar 6 meu_elevador e1 ]) -> p3;
make_instance([ pessoa nome p4 andar 7 meu_elevador e1 ]) -> p4;
make_instance([ funcionario nome f1 andar 5 meu_elevador e1 ]) -> f1;
make_instance([ funcionario nome f2 andar 4 meu_elevador e1 ]) -> f2;
make_instance([ funcionario nome f3 andar 0 meu_elevador e1 ]) -> f3;

p1 <- saia_do_predio;
p2 <- vai_para_casa;
p3 <- saia_do_predio;
p4 <- vai_para_casa;
f1 <- limpe_o_andar(7);
f2 <- saia_do_predio;
f3 <- limpe_o_andar(6);

e1 <- atenda_chamadas_e_requisicoes;
