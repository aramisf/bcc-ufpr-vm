homem(jose).
homem(mario).
homem(alberto).
homem(fabricio).

mulher(joana).
mulher(maria).
mulher(alberta).
mulher(fabricia).

pai(jose,mario).
pai(jose,maria).

mae(joana,mario).
mae(joana,maria).

irmaos(I1,I2) :-
	homem(P),
	pai(P,A),
	pai(P,I2),
	mulher(M),
	mae(M,I1),
	mae(M,I2).


