
% Diferença entre paradigma imperativista e lógico:
% O paradigma lógico emprega regras de inferência sobre fórmulas lógicas
% definidas até que uma ou mais respostas que atendam às estas regras
% sejam encontradas. Possui também uma forte natureza declarativa, com
% ordem de código não sendo tão importante quanto na programação
% imperativista.
% O paradigma imperativista (ou funcional) usa blocos de código
% denominados funções, que são como funções matemáticas que retornam
% valores usados em determinada ordem por outros blocos de código para
% gerar os resultados desejados.
% 
% Backtracking:
% Trata-se de um algoritmo geral que busca uma ou várias respostas para
% determinado problema de maneira incremental.
%
% Unificação:
% A unificação entre dois termos acontece quando os símbolos e a aridade
% das funções sob as quais eles ocorrem são idênticos, e se os parâmetros
% podem ser unificados simultaneamente.

conta([], 0).
conta([ _ | T ], TN) :-
	conta(T, N),
	TN is N + 1.

soma([], 0).
soma([ H | T ], TS) :-
	soma(T, S),
	TS is H + S.

media(L, M) :-
	conta(L, N),
	soma(L, S),
	M is S / N.

copia([], []).
copia([ H1 | T1 ], [ H1 | T2 ]) :-
	copia(T1, T2).

concatena([], B, B).
concatena([ AH | AT ], B, [ AH | L ]) :-
	concatena(AT, B, L).

listificada([],[]).
listificada([ X | L1 ], [[ X ] | L2 ]) :-
	listificada(L1, L2).

lista_dobro([],[]).
lista_dobro([ X | LS ], [ Y | LD ]) :-
	Y is X * 2,
	lista_dobro(LS, LD).

ocorrencias([],_,0).
ocorrencias([ X | T ], X, TN) :-
	ocorrencias(T, X, N),
	TN is N + 1.
ocorrencias([ X | T ], X, TN) :-
	ocorrencias(T, X, TN).

todas_antes(_, [], []).
todas_antes(P, [ P | _ ], []).
todas_antes(P, [ H | T ], [ H | T1 ]) :-
	todas_antes(P, T, T1).

todas_depois(P, L, []) :-
	not(pertence(L, P)).
todas_depois(P, [ P | T ], T).
todas_depois(P, [ _ | T ], T1 ) :-
	todas_depois(P, T, T1).

pega_elemento([ H | _ ], 1, H).
pega_elemento([ _ | T ], N, E) :-
	N > 1,
	NY is N - 1,
	pega_elemento(T, NY, E).

pertence(X, [ X | _ ]).
pertence(X, [ _ | L ]) :-
	pertence(X, L).

inverte(L, I) :-
	inv(L, I, []).
inv([], I, I) :- !.
inv([ X | Y ], I, A) :-
	inv(Y, I, [ X | A ]).

ord([], _).
ord([ H | T ], E) :-
	E < H,
	ord(T, H).

ordem([]).
ordem([ H | T ]) :-
	ord(T, H).
