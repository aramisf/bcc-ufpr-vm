
               TAP - T�cnicas Alternativas de Programa��o

  *** Calend�rio de provas e trabalhos do primeiro semestre de 2.010 ***

Primeiro Trabalho - prazo de entrega 3 de maio (segunda-feira)
  Entrega: siga as instru��es do enunciado;
  Observa��o: n�o deixe de perguntar d�vidas com anteced�ncia.

Primeira Prova - 04 de maio (ter�a-feira)
  Conte�do: Programa��o em L�gica (somente);
  Local: Sala de aula normal.

Segundo Trabalho - prazo de entrega 21 de junho (quarta-feira)
  Entrega: siga as instru��es do enunciado;
  Observa��o: n�o deixe de perguntar d�vidas com anteced�ncia.

Segunda Prova - 22 de junho (ter�a-feira)
  Conte�do: Programa��o Orientada a Objetos;
  Local: Sala de aula normal.

Prova de Reposi��o - 24 de junho (quinta-feira)
  Local: Sala de aula normal;
  Conte�do: Mat�ria toda;
  Observa��o: a falta em qualquer uma das provas anteriores deve ser justificada
              dentro das normas regimentais para que o aluno possa fazer a Prova
              de Reposi��o.

Prova Final - 06 de julho (ter�a-feira).
  Conte�do: Mat�ria toda;
  Local: Sala de aula normal.
