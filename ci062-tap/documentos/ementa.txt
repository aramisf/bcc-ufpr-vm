CI062 - T�CNICAS ALTERNATIVAS DE PROGRAMA��O

1. EMENTA - PR�-REQUISITOS - CARGA HOR�RIA

CR�DITOS: 04

CARGA HOR�RIA: 60 Aulas Te�ricas: 60 Aulas Pr�ticas: 00

EMENTA:  Programa��o   em  L�gica,   Programa��o  Funcionalista,   Programa��o
Orientada a Objetos. C�lculo Proposicional e Lambda Calculus.

OBJETIVOS: Promover uma vis�o aprofundada da pragm�tica dos princ�pios e das
t�cnicas de programa��o nos 3 (tr�s) paradigmas alternativos ao de programa��o
imperativa. Isto � feito com base no ensino dos referidos princ�pios e
t�cnicas, com a aplica��o destes em 3 (tr�s) linguagens de programa��o
diferentes, uma para cada paradigma.

PR�-REQUISITOS: Conhecimento aprofundado

EQUIVAL�NCIA:

PROGRAMA:

1 - Introdu��o � no��o de m�ltiplos paradigmas

2 - O paradigma de programa��o em l�gica e a linguagem Prolog

  2.1 - Introdu��o (conceitua��o, nota��o convencional)

  2.2 - C�lculos Proposicional e de Predicados (sem�ntica de operadores
e m�todos de infer�ncia)

  2.3 - Tipos de cl�usula l�gica (fato, regra)

  2.4- Controle  do processo de c�lculo sem�ntico por meio de
predicados especiais

  2.5 - Monitoramento do processo de interpreta��o

  2.6 - Exerc�cios

3 - O paradigma de programa��o orientada a objetos e a linguagem Flavours

  3.1 - Introdu��o (conceitua��o, nota��o convencional)

  3.2 - Estrutura de Programa (forma geral, objeto-m�todo)

  3.3 - Encapsulamento (abstra��o e oculta��o gradual de informa��o)

  3.4 - Heran�a simples e m�ltipla

  3.5 - Comunica��o entre objetos (passagem e recebimento de mensagens)

  3.6 - Exerc�cios

4 - O paradigma de programa��o funcionalista e a linguagem ML

  4.1 - Introdu��o (conceitua��o, nota��o convencional)

  4.2 - Tipos de Dados (n�mero, caracter, s�mbolo, lista, etc)

  4.3 - Estrutura de Programa (forma geral, fun��o, declara��o)

  4.4 - Estruturas  de controle  (atribui��o de valores,  ativa��o de  fun��o,
sequenciamento simples,  condicional,  itera��o, percorrimento  de  estruturas
complexas, multi-valora��o)

  4.5 - Express�es-Lambda

  4.6 - Exerc�cios


BIBLIOGRAFIA:

1 - Ulf Nilsson, Jan Maluszynski. Logic, Programming and Prolog (2ed). Previously
published by John Wiley & Sons Ltd. ( Agora em: http://www.ida.liu.se/~ulfni/lpp/ )

2  - Logic, Programming and Prolog (2nd ed.). Nilsson, U. and Maluszynski,  J.,
JOHN WILEY & SONS 1995.

3  -  Artificial  Intelligence  Techniques  in  Prolog.  Yoav  Shoham,  Morgan
Kaufmann, 1994

4 -  Prolog Programming  for Artificial  Intelligence (International  Computer
Science Series). Ivan Bratko, Addison-Wesley, 1993.

5  -  Clocksin,  W.   F.  &  Mellish  C.   S.  -  "Programming  in   Prolog" -
Springer-Verlag, 1987. 

6 - Isa�as Camilo Boratti. Programa��o Orientada a Objetos em Jave. Editora
Visual Books, 2009.

7 - On to Smalltalk. Patrick Henry Winston, Addison-Wesley Pub Co, 1998.

8- Object-Oriented  Languages,  Systems and  Applications.  G. Blair  and  J.
Gallagher and D. Hutchison and D. Shepherd, Pitman Publishing, 1991.

9- Analise Baseada em Objetos. Peter Coad e Edward Yourdon. Editora Campus.

10- Fundamentals of  Object-Oriented Design in  UML. Larry Meilir  Page-Jones.
Addison-Wesley Object Technology Series.

11- Design Patterns: Elements  of Reusable  Object-Oriented Software.  Erich
Gamma, et alii. Addison-Wesley Professional Computing.

12- An�lise e  Projeto de Sistemas  de Informa��o Orientados  a Objetos.  Raul
Sidnei Wazlawick. Editora Campus, 1994.

13 - ML for the Working  Programmer. Lawrence C. Paulson, Cambridge  University
Press, 1998 3a. Edi��o.

14 - Elements of Ml Programming : Ml97. Jeffrey D. Ullman, Prentice Hall, 1997.

15- Introdu��o � Programa��o  Funcional. Silvio R. de  L. Meira. VI Escola  de
Computa��o da Sociedade Brasileira de Computa��o - Campinas - SP, 1988.



Software de Apoio:

- Poplog: http://www.cs.bham.ac.uk/research/poplog/freepoplog.html

- SWI-Prolog: http://www.swi-prolog.org/



Avalia��o:

A avalia��o consiste de dois trabalhos e duas provas.
