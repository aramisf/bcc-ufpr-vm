Sistemas Operacionais
Prova 2 de 2010/2

1. a) Faça um escalonamento RR com quantum igual a 3 do seguinte esquema de
      processos.

      Processo   Tempo de Rajada     Tempo de Chegada
      A          8                   0
      B          3                   2
      C          9                   4
      D          2                   7

      0   3   6   9  12 14 16  19  22
      +---+---+---+---+--+--+---+---+
        A   B   A   C   D  A  C   C

      Estados das filas:

      [0]  A(8)
      [2]  B(3)
      [3]  B(3) A(5)
      [4]  A(5) C(9)
      [6]  A(5) C(9)
      [7]  C(9) D(2)
      [9]  D(2) A(2)
      [12] D(2) A(2) C(6)
      [14] A(2) C(6)
      [16] C(6)
      [19] C(3)
      [22] --

   b) Qual o tempo de espera para cada processo?

      A = (6 - 3) + (14 - 9) = 8
      B = (3 - 2) = 1
      C = (9 - 4) + (16 - 12) = 9
      D = (12 - 7) = 5

   c) Qual o tempo de espera médio?

      (T_A + T_B + T_C + T_C) / 4 =
      (8 + 1 + 9 + 5) / 4 = 5.7


2. a) Suponha um sistema operacional interativo, a ser usado tipicamente por
      vários usuários simultaneamente. Qual seria o critério mais importante
      para escalonamento nesse caso?
      
      Tempo de resposta.

   b) Que algoritmo de esclonamento seria mais adequadro para o sistema?

      Round Robin (RR).

   c) Por que este algoritmo é mais adequado?

      Quando possui um quantum baixo para execução dos processos. Em RR temos a
      garantia de que cada processos irá esperar no máximo (n - 1) * quantum
      unidades de tempo.

      Se este valor for muito baixo, chega-se próximo ao efeito de um
      processador com 1/n capacidade do processador original para n processos.


3. Questões de conceitos gerais não relacionadas com o grafo desenhado no
   quadro.

   a) Cite apenas as condições gerais necessárias para um deadlock.

   b) Dado um grafo de alocação de recursos (RAG), indique a condição
      *necessária* para deadlock.
   
   c) Dado um grafo de alocação de recursos (RAG), indique a condição
      *suficiente* para deadlock.
   
   Questões relacionadas com o grafo desenhado no quadro:
   
   d) O estado mostrado é seguro?
   
   e) Suponha que nesse estado chega requisição de P3 por 1 recurso tipo R5.
      Mostre outro RAG com a "fotografia" do sistema após a chegada desta
      requisição.
   
   f) O que o sistema faz com P3? Prove sua resposta justificando o raciocínio
      em termos do seu RAG em (e) segundo o método de prova presente no livro.
