#include <sys/types.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define MAXTICKET sizeof(int) * 1000

int s = 0;
int *tickets;

int get_ticket() {

    int i;
    pthread_t p;
    p = pthread_self();

    while (s) {
        printf("%ld TRY GET\n", p);
        sleep(1);
    }
    s = 1;

    for (i = 0; tickets[i] == 1; i++);
    tickets[i] = 1;
    printf("%ld GET %d\n", p, i);

    s = 0;
    sleep(1);
    return i;
}

void put_ticket(int t) {

    pthread_t p;
    p = pthread_self();

    while (s) {
        printf("%ld TRY PUT\n", p);
        sleep(2);
    }
    s = 1;

    tickets[t] = 0;
    printf("%ld PUT %d\n", p, t);
    sleep(1);

    s = 0;
}

void* ticket_routine_get() {
    int i;
    int ticket;

    for (i = 0; i <= 100; i++)
        ticket = get_ticket();

    return NULL;
}

void* ticket_routine_get_put() {
    int i;
    int ticket;

    for (i = 0; i <= 100; i++) {
        ticket = get_ticket();
        put_ticket(ticket);
    }

    return NULL;
}

int main () {

    int i;

    tickets = malloc(MAXTICKET);

    for (i = 0; i <= 1000; i++)
        tickets[i] = 0;

    pthread_t t1;
    pthread_t t2;

    pthread_create(&t1, NULL, ticket_routine_get, NULL);
    pthread_create(&t2, NULL, ticket_routine_get_put, NULL);

    while (1);
}
