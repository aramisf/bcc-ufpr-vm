CI215 - Sistemas Operacionais
Exercícios

3.3 O que acontecerá na linha A deste programa?

    #include <sys/types.h>
    #include <stdio.h>
    #include <unistd.h>

    int value = 5;

    int main() {
        pid_t pid;
        pid = fork();

        if (pid == 0) {
            value += 15;
        } else if (pid > 0) {
            wait(NULL);
            pintf("PARENT: value = %d", value); /* A */
            exit(0);
        }
    }

    --

    Do manual da função `fork()`:

    * [...] The entire virtual address space of the parent is replicated in the
      child, including the states of mutexes, condition variables, and other
      pthreads objects; [...]

    RETURN VALUE
      On success, the PID of the child process is returned in the parent, and 0
      is returned in the child.  On failure, -1 is returned in the parent, no
      child process  is  created,  and  errno  is  set appropriately.

    Ao sofrer um fork(), o programa filho adentra o `if`, enquanto o programa
    pai aguarda a sua execução terminar. Como o espaço virtual dentre os dois
    processos é diferente, o incremento na variável `value` que o filho faz não
    afeta a mesma variável no processo pai. Assim, `printf` exibe o valor 5.


