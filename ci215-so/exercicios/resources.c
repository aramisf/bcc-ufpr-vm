#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

#define NRES 7

int mutex = 0;
int mutex_print = 0;
int res = 0;
long int slots[NRES];

int res_alloc () {

    while (mutex && res >= 5);
    mutex = 1;
    sleep(2);

    pthread_t p;
    p = pthread_self();

    int i;
    for (i = 0; i < NRES; i++) {
        if (!slots[i]) {
            slots[i] = p;
            break;
        }
    }
    res++;

    mutex = 0;
    return i;
}

int res_free (int i) {

    while (mutex);
    mutex = 1;
    sleep(1);

    slots[i] = 0;
    res--;

    mutex = 0;
    return i;
}

void res_print_slots(char* text) {

    if (mutex_print)
        return;

    mutex_print = 1;

    int i;
    printf("%s\t|", text);
    for (i = 0; i < NRES; i++)
        printf(" %15ld |", slots[i]);
    printf("\n");

    mutex_print = 0;

}

void* res_init () {

    int r;
    time_t s;

    r = res_alloc();
    res_print_slots("ALLOC");

    // using resource
    srand(time(&s));
    sleep(rand() % 10);

    res_free(r);
    res_print_slots("FREE");

    return NULL;
}

int main (int argc, char** argv) {

    pthread_t p;

    while (1) {
        sleep(1);
        pthread_create (&p, NULL, res_init, NULL);
    }

    return 0;
}
