
  Aula 16/03

  Conceitos Iniciais de Banco de Dados
  ------------------------------------
    
    * Banco de Dados
      Foi notada a necessidade de separação do código das informações, e 
      como a recuperação das informações levava sempre ao mesmo 
      procedimento, também foi pensado em criar sistemas somente para 
      armazenamento das informações, com compartilhamento das informações e 
      múltiplas visões de acordo com cada usuário.
    
    * Modelo de Dados
      Formalismo da descrição: conjunto de regras para descrever um banco
      de dados. São eles:
      - Modelo Hierárquico
      - Modelo Relacional
      - Modelo Orientado a Objetos
    
    * Esquema
      Própria descrição dos dados. Não confundir com modelo, cujas 
      definições acadêmicas e corporativas são diferentes. Na indústria 
      define-se "modelagem" por montagem de esquemas.
    
    * Visão
      Uma visão de banco de dados é uma redução das informações presentes para
      um nível específico, geralmente definido para determinado usuário.
    
    * Instância
      Correspondência no mundo real para o que está no banco de dados.
    
    * População
      Conjunto de ocorrências e instâncias do banco de dados.
    
    * LDD e LDM
      Linguagem de descrição de esquemas, e linguagem de manipuação.
      Descrição: "CREATE table ( ..."
      Manipulação" "SELECT field FROM ...", "UPDATE table SET ..."
    
    * Completeza das Linguagens
      Capacidade de recuperação de qualquer relatório cujos atributos 
      existem no banco de dados, sem a necessidade de gerar redundâncias.

    * Identificador
      A chave, que é uma garantia de unicidade necessária a todos os modelos.
    
    * SGBD
      Sistema responsável por fazer a análise sintática, léxica e semântica 
      de uma linguagem, e então efetuar sua tradução e execução, fornecendo 
      os resultados esperados.


  Modelo Hierárquico
  ------------------

    Modelo composto por dataset (objeto), ponteiro (relação), propriedade 
    (campos).

    * Vantagens
      Bastante rápido para junções e útil para aplicações em tempo real. 
      Possui forte relação com sua linguagem hospedeira (cursores).
    
    * Desvantagens
      Deficitário para evolução de esquemas. Impossibilidade de ser cíclico. 
      Aproximação do nível físico e lógico, o que o torna dependente de 
      implementações. Ausência de padrões, dificultando migrações e fusões.


  Aula de 21/03
  
  Modelo Relacional
  -----------------
    
    * Exemplo: Domínio de inteiros de um byte.
      Todos os números que podem ser representados com um byte.
    
    * Relação
      Subconjunto do produto cartesiado de uma lista de domínios
     
    * Lista
      Sequência desordenada de domínios multivalorados.
    
    * Tupla
      Conjunto de dados únicos, normalmente com identificadores.
    
    * Atomicidade
      Manutenção de valores indivisíveis.
    
    * Monovalorização
      Cada átomo tem somente um tipo.


  Álgebra Relacional
  ------------------
  
    * Operações Básicas
      - Seleção             SELE  (relação, <atributo = 'valor'>)
      - Projeção            PROJ  (relação, [lista de domínios])
      - União               UNIAO (relação, relação)
      - Diferença           DIFF  (relação, relação)
      - Produto Cartesiano  PROD  (relação, relação)
    
    * Ver exercícios


  Aula 28/03
  
  Modelo Entidade-Relacionamento
  ------------------------------
  
    * Conceito
      Visão unificada dos modelos relacional e hierárquico utilizado para
      representação de esquemas.
      
      - Tipo entidade (TE)
      - Tipo abstrato (*TA*)
    
    * Cardinalidade
      Quantificação máxima e mínima de relacionamentos
      
               ALUNO [10:60] ---- *CURSAR* ---- [1:8] DISCIPLINA
      
      Um aluno pode cursar entre 1 e 8 disciplinas, enquanto uma disciplina
      pode ser cursada por no mínimo 10 e no máximo 60 alunos.
      

  Generalização e Especialização
  ------------------------------
  
    * Conceito de Herança
  
                                 ARTIGOS                    tipo G
                           código preço fornecedor       generalização
                                  [1:1]
                                    |
                                  [0:1]
               +--------------------+--------------------+         tipo E
               |                    |                    |      especialização
           ALIMENTAR         ELETRODOMÉSTICOS        VESTUÁRIO
            validade             garantia             tamanho
        valor nutricional        voltagem               sexo


  BD Orientado a Objetos
  ----------------------

    * Pensado na década de 80 sobre o conceito de esquemas variáveis,
      resultando em projetos de banco dedutivos, como o Datalog.
      
        <pessoa josé>
            <filho zezinho>
                <brinquedo bicicleta/>
                <brinquedo autorama/>
            </filho>
            <filho huguinho>
                <brinquedo peão/>
                <brinquedo bola/>
            </filho>
        </pessoa>
        <pessoa maria>
            <filho luizinho>
                <brinquedo bicicleta/>
                <brinquedo peão/>
            </filho>
        </pessoa>

  
  Aula 06/04

  Projetos de BD
  --------------
  
    * Dependência funcional A --> B: Cada valor em A determina um valor em B.
      
      CPF --> Nome
      CPF --> (Nome, Telefone)
      (RG, Estado) --> Pessoa
      
      CEP --> Rua
      (CEP, Código de Rua) --> Rua
      
    * Transitividade
    
      CEP --> Município --> Estado
      CEP --> Estado
    
    * Incoerências
    
      Nome    Cidade     Estado
      José    Curitiba   Paraná
      João    Curitiba   Paraná
      Maria   Curitiba   Paraná
                 +          +
                 |          +---> Caracteriza redundância porque Curitiba
                 |                sempre está no Paraná.
                 |
                 +---> Não caracteriza redundância porque a relação
                       Nome --> Cidade é sempre uma informação nova.

    * Validação de TE
      Se para um TE existe um identificador, então todos os atributos 
      dependem do identificador inteiro.
    
    * Validação do TA
      Conjunto de TEs que determina todos os atributos do TA.

    * Redundância
      Quando podemos encontrar associações correspondentes por composição
      de associações

        CURSO ---------------- *CURSA* ---------------- DISCIPLINA
                        colocação no vestibular
                        obrigatória ou eletiva
                               horário
                                  |
                                ALUNO
                                 nome

              ** -------------- ALUNO -------------- **
            horário                         colocação no vestibular
               |                                     |
           DISCIPLINA                              CURSO    
   
    * Peculiaridade de TEs
      - TEs sem interesse (quando não se tem consultas sobre eles) devem 
        virar atributos
      - TE com somente um atributo é em geral um erro de esquema
      - Um TE fraco é aquele que depende de um identificador alheio
        para existir, tal como os exemplares de um livro, ou as turmas de
        uma disciplina.
        
                     LIVRO ------ ** ------ EXEMPLAR
                      isbn                isbn + sequência
                    
            DISCIPLINA ---------- ** ---------- TURMA
              código                       código + [A, B, C, ... ]


    Aula 11/04

    Primeira Forma Normal
    ---------------------

    * DF Multivalorada
      Quando um atributo determina um conjunto de valores de um certo atributo
      CPF --> Nome
      CPF --> Dependentes
    
    Uma relação está na primeira forma normal se, e somente se, não possui
    atributos multivalorados.

    Cliente
      Código  Nome   Telefone              Endereço
      C001    José   9900-8800/9988-0000   Rua X, 85, Uberaba, 87770-020
      C002    João   9998-8544/9977-6523   Rua Y, 77, Uberaba, 87770-020
    
      - Atributos repetidos para endereço: Uberaba, CEP
      - Mais de um telefone por cliente
    
    * Conversão para forma normal:
    
      Cliente
        Código  Nome   Rua  Bairro   CEP
        C001    José   X    Uberaba  827700-020
        C002    João   Y    Uberaba  827700-010
    
      Telefone
        Código  Telefone
        C001    9900-8800
        C001    9988-0000
        C002    9998-8544
        C002    9977-6523

    Segunda Forma Normal
    --------------------
    
    Uma relação está na segunda forma normal se, e somente se, estiver na
    primeira forma normal e todo atributo não chave for dependente da
    chave primária
    
    Pedido
      Número  Código  Produto        Quantidade   Valor  Total
      001     1-663   Impressora HP  3            570    1500
      002     2-754   Monitor        1            350    350
      003     1-888   HD             2            500    1000
    
      - nome do produto depende do código do produto, não do pedido
    
    * Convero para a segunda forma normal
    
      Produto
        Código  Produto
        1-663   Impressora HP
        2-754   Monitor
        1-888   HD
    
    
    Terceira Forma Normal
    ---------------------
    
    Uma relação está na terceira forma normal se, estiver na segunda forma
    normal e cada atributo não chave não possuir dependência transitiva.

    O valor transitivo na tabela acima é "Total", já que ele é uma 
    dependência de quantidade * valor unitário.
    
    
    Exercício De Forma Normal
    ----------------------------

    Converter para a segunda e terceira forma normal:

      1. Projeto
        Cód  Descrição  CódFunc  NomeFunc  CargoFunc    SalFunc   DataInicio
        11   Alfa       1001     Antônio   Analista SR  1800      02/01/2008
        11   Alfa       1004     Daniela   Analista JR  1200      03/04/2005
        12   Beta       1003     Claudia   Analista JR  1800      10/02/2003

    * Segunda:

      1. ProjetoFuncionário
        Cód  Descrição  CódFunc
        11   Alfa       1001
        11   Alfa       1004
        12   Beta       1800

      2. Funcionário
        CódFunc  NomeFunc  CargoFunc    SalFunc   DataInicio
        1001     Antônio   Analista SR  1800      02/01/2008
        1004     Daniela   Analista JR  1200      03/04/2005
        1003     Claudia   Analista JR  1800      10/02/2003
    
    * Terceira:

      1. Projeto            2. Funcionário
        Cód  Descrição        CódFunc  NomeFunc  CargoFunc
        11   Alfa             1001     Antônio   Analista SR
        11   Alfa             1004     Daniela   Analista JR
        12   Beta             1003     Claudia   Analista JR
      
      3. Cargo                        4. ProjetoFuncionário
        Cargo          Salário         Cód   CódFunc   DataInício
        Analista SR    1800            11    1001      02/01/2008
        Analista JR    1200            11    1004      03/04/2005
        Analista JR    1800            12    1003      10/02/2003
