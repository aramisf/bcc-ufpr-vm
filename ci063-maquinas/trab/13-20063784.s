.data 
	V1:	.space 400
	V2: 	.space 400 
      mens1: 	.asciiz "Digite os numeros e finalize com enter: \n"
      mens2: 	.asciiz "Quantos algarismos tem a primeira sequencia?\n"
      mens3: 	.asciiz "Quantos algarismos tem a segunda sequencia?\n"
      mens4: 	.asciiz "Sequencias diferentes ou a é maior que a primeira. Tente novamente. \n"
      mens5:	.asciiz "Um a um entre com a primeira sequencia: \n"
      mens6:	.asciiz "Um a um entre com a segunda sequencia: \n"
      mens7:	.asciiz "A segunda sequencia está contida na primeira!"
      mens8:	.asciiz "A segunda sequencia não esáa contida na primeira!"

.text

main: 
	li $v0,4		# imprimir char
	la $a0,mens1		# imprimir mens1
	syscall			# imprime
	
numeros: li $v0,4		# imprimir char
	 la $a0,mens2		# imprimir mens2
	 syscall		# imprime

	li $v0,5		# ler inteiro
	syscall			# ler
	move $s0,$v0		# s0=inteiro

	li $v0,4		# imprimir char
	la $a0,mens3		# imprimir mens3
	syscall			# imprime
	
	li $v0,5		# ler inteiro
	syscall			# ler
	move $s1,$v0		# s1=inteiro
 
teste: 	sgt $t0,$s1,$s0		
	beq $t0,$0,fimv		# se t0=0 ir para fimv
	li $v0,4		# imprimir char
	la $a0,mens4		# imprimir mens4
	syscall			# imprime
	j numeros		# pular para numeros
fimv:
	li $v0,4		# imprimir char
	la $a0,mens5		# imprimir mens5
	syscall			# imprime
	
	li $s5,0		# indice do vetor
	li $s2,0		# contador
formV1: 
	slt $t0,$s2,$s0		
	beq $t0,$0,fimV1	# se t0=0 ir para fimV1
	li $v0,5		# ler inteiro
	syscall			# ler
	move $s7,$v0		# s7=v0
	sw $s7,V1($s5)
	add $s5,$s5,4		# s5=s5+4
	add $s2,$s2,1		# s2=s2+1
		
	j formV1		# ir para formV1
fimV1:
	li $v0,4		# imprimir char
	la $a0,mens6		# imprimir mens6
	syscall			# imprimir

	li $s5,0		# indice do vetor
	li $s2,0		# contador
	
formV2:
	slt $t0,$s2,$s1
	beq $t0,$0,fimV2	# se t0=0 ir para fimV2
	li $v0,5		# ler inteiro
	syscall			# ler
	move $s7,$v0		# s7=v0
	sw $s7,V2($s5)
	add $s5,$s5,4		# s5=s5+4
	add $s2,$s2,1		# s5=s5+4
	
	j formV2

fimV2:
	
	li $s6,0		# i:=1	
	li $s7,0		# contador
	# li $s3,0		# posicao de V1
	li $s4,0		# posicao de V2
	li $s2,-32117		
cont: 
	sle $t0,$s6,$s1
	beq $t0,$0,fcont	
	li $s5,1		# j:=1
	li $s3,0
	lw $t2,V2($s4)
cont2:
	sle $t0,$s5,$s0
	beq $t0,$0,fcont2

	lw $t1,V1($s3)	
if:
	bne $t2,$t1,fif
	sw $s2,V1($s3)
	add $s7,$s7,1
	j fcont2
fif:
	add $s5,$s5,1
	add $s3,$s3,4
	j cont2
fcont2:
	add $s6,$s6,1
	add $s4,$s4,4
	j cont
fcont:
	beq $s7,$s1,ok
	bne $s7,$s1,nok
ok:
	li $v0,4
	la $a0,mens7
	syscall
	j fim
nok:
	li $v0,4
	la $a0,mens8
	syscall	
fim:
	li $v0,10
	syscall
