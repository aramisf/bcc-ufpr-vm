.data
        V: 	.space 34
        A: 	.asciiz "Binarios t�m no máximo 32 bits. \n"
        B: 	.asciiz "Digite o binario: "
        C:	.asciiz "O numero ultrapassou 32 bits. Tente novamente. \n"
        D: 	.asciiz "N�mero inválido. \n"
        E: 	.asciiz "O numero em hexadecimal é: "
        n1: 	.asciiz "1"
        n10: 	.asciiz "2"
        n11: 	.asciiz "3"
        n100: 	.asciiz "4"
        n101: 	.asciiz "5"
        n110: 	.asciiz "6"
        n111: 	.asciiz "7"
        n1000: 	.asciiz "8"
        n1001: 	.asciiz "9"
        n1010: 	.asciiz "A"
        n1011: 	.asciiz "B"
        n1100: 	.asciiz "C"
        n1101: 	.asciiz "D"
        n1110: 	.asciiz "E"
        n1111: 	.asciiz "F"
.text

main:
	li $v0,4		# imprimir char
        la $a0,A		# imprimir A
        syscall                 # imprime A
        li $v0,4		# imrprimir char
        la $a0,B		# imrprimir B
        syscall                 # imprime B

	li $v0,8		# ler string
 	la $a0,V		# ler V
	li $a1,34		# a1=31
	syscall			# ler
	li $s0,0		# contador=0
	li $s7,10		# $s7 ---- ENTER

cont:	lb $s4,V($s0)		# 
	beq $s4,$s7,fcont	# se s4=s7 ir para fcont
	add $s0,$s0,1		# s0=s0+1
	j cont			# pular para cont

fcont:  li $s4,32		# s4=32
	beq $s0,$0,fim		# comparacao de i
	beq $s0,$s4,fim2	# se s0=s4 ir para fim2

inicio: move $s2,$s0		# j=i;
	li $s1,33		# l=33;
	sb $s7,V($s1)		# linha para 33
	sub $s1,$s1,1		# l=l-1

while1: sge $t0,$s2,0		# enquanto j>1 faca
	beq $t0,$0,fim1
	lb $s4,V($s2)		# aux= V[j]
	sb $s4,V($s1)		# V[l]=aux
	sub $s1,$s1,1		# l=l-1
	sub $s2,$s2,1		# j=j-1
	j while1

fim1:	move $s2,$s1		# j=l
	add $s1,$s1,1		# l=l+1

while2: sge $t0,$s2,0		# enquanto j>=1 faça
	beq $t0,$0,fim2		# se nao pula pra fim2
	lb $s4,V($s1)		# aux=V[l]
	sb $s4,V($s2)		# V[j]=aux
	sub $s2,$s2,1		# j=j-1
	j while2

fim2:	li $s2,1
	li $s3,'0'
	li $s4,'1'
while3:	sle $t0,$s2,31		# enquanto j<=32 faca
	beq $t0,$0,verif	# se nao pula pra fim3		
	lb $s7,V($s2)	
	bne $s7,$s4,lab1
        add $s2,$s2,1
        j while3
lab1:	bne $s7,$s3,lab2
	add $s2,$s2,1
        j while3
lab2:	li $v0,4
	la $a0,D
	syscall
	j fim

verif:	
	li $s0,0		# j=1
verif2: sle $t0,$s0,31
	beq $t0,$0,fim
	li $s1,1		# i=1
	li $s3,8		# e=8
	li $s4,0		# soma=0
while4: sle $t0,$s1,4
	beq $t0,$0,fim4
	lb $s5,V($s0)
	sub $s5,$s5,'0'
	mul $s5,$s5,$s3
	add $s4,$s4,$s5
	div $s3,$s3,2
	add $s0,$s0,1
	add $s1,$s1,1
	j while4
fim4:
	slt $t0,$s4,10
	beq $t0,$0,ifs1
	li $v0,1
	move $a0,$s4
	syscall
	j verif2
ifs1:
	slt $t0,$s4,11
	beq $t0,$0,ifs2
	li $s6,10
	beq $s4,$s6,print1
ifs2:
	slt $t0,$s4,12
	beq $t0,$0,ifs3
        li $s6,11
	beq $s4,$s6,print2
ifs3:
	slt $t0,$s4,13
	beq $t0,$0,ifs4
	li $s6,12
	beq $s4,$s6,print3
ifs4:
	slt $t0,$s4,14
	beq $t0,$0,ifs5
        li $s6,13
	beq $s4,$s6,print4
ifs5:
	slt $t0,$s4,15
	beq $t0,$0,ifs6
        li $s6,14
	beq $s4,$s6,print5
ifs6:
        li $s6,15
	beq $s4,$s6,print6

print1:	li $v0,4
	la $a0,n1010
	syscall
	j verif2

print2:	li $v0,4
	la $a0,n1011
	syscall
	j verif2

print3:	li $v0,4
	la $a0,n1100
	syscall
	j verif2

print4:	li $v0,4
	la $a0,n1101
	syscall
	j verif2

print5:	li $v0,4
	la $a0,n1110
	syscall
	j verif2

print6:	li $v0,4
	la $a0,n1111
	syscall
	j verif2	        

fim:
	li $v0,10
	syscall
