
  Exercício 03

    Escreva um programa que lê um número inteiro N e imprime N[k] para k
    variando de 1 até N.  Considere que, por definição,
    N[k]=N(N-1)(N-2)(N-3)...(N-k+1), k sendo um natural diferente de zero e N[0]=1.
    Observe que se N=k, então N[N]=N!. Exemplo: Para N=5, teremos:

    Resposta Observação
    5        = 5
    20       = 5*4
    60       = 5*4*3
    120      = 5*4*3*2
    120      = 5*4*3*2*1

  Exercício 08

    Fazer um programa que receba como parâmetro um número em binário
    (por exemplo, em um string) e retorne seu valor equivalente em
    hexadecimal. Por exemplo, se a entrada for 10001, a saída deve ser 11.
    Opcionalmente estenda para 32 bits. Neste caso, 10001 = 1111 1111 1111
    11111111 1111 1111 0001.

  Exercício 13 

    Fazer um programa que leia duas sequência de inteiros, não
    necessariamente contendo a mesma quantidade de números, e diga se a
    segunda sequência está contida na primeira.
