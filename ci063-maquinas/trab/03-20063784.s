.data 
     sequencia: .asciiz "A seq�ência fatorial�: "
     igual: .asciiz"="
     asterisco: .asciiz "*"
     linha: .asciiz "\n" 
.text
     main:
          li $v0,5     	  	# lê� numero de entrada =read numero 
          syscall        	# chamada de sistema 
          move $s0,$v0   	# coloca numero recebido por $v0 no registrador $s0  
          li $s1,1      	# mult:=1  
          move $s2,$s0       	# i:=numero
          

          la $a0,sequencia  	# chamar o rotulo 
          li $v0,4       	# comando para imprimir char  
          syscall

     loop1:
         sge $t0, $s2,1   	# compara se numero é menor igual a 1, esta é a condição para
                                # continuar o loop
         beq $t0,0,fim1    	# se o registrador $t0 receber 0 , isto quer dizer que a 
                                # afirmação é falsa, portanto acaba o comando
         mul $s1,$s1,$s2   	# equivalente mult:=mult*i
         li $v0,1         	# comando para imprimir 
         move $a0,$s1     	# imprimi conteudo de a0 , que no caso recebe $s1
         syscall

         la $a0, igual    	# chamada para impressão de mais um rotulo
         li $v0,4         	# comando para imprimir char 
         syscall

         li $v0,1      	 	# imprimir outro numero 
         move $a0,$s0    	# numero a ser imprimido é numero lido na entrada
         syscall
	 sub $s3,$s0,1     	# para que ocorra a condição numero-1, no caso seria j
     loop2:  
         
         sge $t0,$s3,$s2    	# comparar se j é maior ou igual ao i 
         beq $t0,0,fim2		# se o a condição passada tiver resultado 0 élsa
                                # portanto vai para o fim2, senão continua execução
         la $a0, asterisco	# chamada para novo rótulo a ser imprimido 
         li $v0,4		# imprimir char
         syscall

         li $v0,1		# imprimir um inteiro
         move $a0,$s3		# a := inteiro para impress�o
         syscall
         
         sub $s3,$s3,1
         j loop2
      fim2:
        
         la $a0,linha		# imprimir char
         li $v0,4
         syscall
          
         sub $s2,$s2,1		# decrementar o i
         j loop1
      fim1:
         li $v0,10
         syscall
