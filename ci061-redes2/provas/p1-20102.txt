Vinicius Massuchetto

CI061 - Redes de Computadores II
Primeira Prova do Segundo Semestre de 2010

 1. Por que se diz que a Ethernet � probabil�stica? Quais as consequ�ncias deste
 fato para a tomada de decis�o sobre a ado��o desta rede em uma determinada
 organiza��o?

    A Ethernet � uma rede probabil�stica pois a transmiss�o de dados entre as
    m�quinas que a comp�em d�-se com uma distribui��o de concorr�ncia
    diferenciada entre elas, sem que haja uma entidade central de controle de
    acessos. Ou seja, existem diferentes probabilidades para os eventos
    elencados no processo de comunica��o.

    Assim, s�o tamb�m consideradas as probabilidades de falha e de tempos
    diferentes na entrega dos pacotes, e por este motivo n�o � poss�vel garantir
    uma taxa m�nima de servi�o de transmiss�o de dados.

    Este fato � especialmente importante quando a rede possui uma funcionalidade
    cr�tica que n�o pode ser suscet�vel � falhas (tal como dispositivos de
    seguran�a), ou quando uma qualidade m�nima de transmiss�o � necess�ria (em
    servi�os de voz e v�deo, por exemplo).

    A Ethernet foi popularizada em ambientes de escrit�rio porque � capaz de,
    nestes ambientes, balancear bem os custos de instala��o e manuten��o com o
    desempenho da rede.

    Isso d�-se porque para fins de utiliza��o dos usu�rios comuns, e at� em
    certos �mbitos de utiliza��o cr�tica, a oscila��o da qualidade ou falhas de
    comunica��o posssuem aceitabilidade.

 2. O CIDR permitiu que a vers�o 4 do protocolo IP pudesse continuar sendo usado
 na maior parte da internet.

    a) Quais foram os *dois principais* problemas que esta abordagem de
    endere�amento resolveu?

    No esquema original do esquema de endere�amento IP, cada rede f�sica possui
    um endere�o de rede (NET-ID), e cada host em uma rede possui um endere�o de
    rede como prefixo de um endere�o de host (HOST-ID) individual.

    O crescimento de redes de escrit�rio causou um incha�o da capacidade de
    endere�os de rede, o que n�o era vis�vel na fase de projeto dos protocolos
    TCP/IP devido ao alto pre�o e um n�mero moderado de roteadores nas redes
    pelo mundo.

    Essa grande aloca��o de endere�os de rede causam os principais problemas:

    * Overhead administrativo de gerenciamento destes endere�os;
    * Grande tamanho das tabelas de roteamento de endere�os nos roteadores;
    * Falta de endere�os dispon�veis para serem fornecidos � novas redes.

    As solu��es propostas para estes problemas foram, basicamente:

    * Multiplexa��o dos endere�os via CIDR, que foi a solu��o tomada de imediato
      e que conteve o colapso da internet no in�cio dos anos 90;
    * Elabora��o de um novo protocolo de endere�amento, o que no caso � o IPv6.

    b) Descreva o formato do IP com o CIDR.

    A t�cnica consiste basicamente em atribuir um endere�o f�sico via
    multiplexa��o a v�rios hosts de uma rede local, como estes hosts estivessem
    conectados de forma direta � WAN. O router desta rede demultiplexa os
    quadros que nele chegam a fim de destin�-lo ao host correto.

    O CIDR apresenta-se no formato XXX.XXX.XXX.XXX/YY, em que YY � o prefixo
    CIDR, variando de 13 a 27. Este prefixo indica quantos bits est�o sendo
    usados para endere�amento de rede (NET-ID), o que tamb�m define quantos bits
    est�o sendo usados para o endere�amento local (HOST-ID), que varia de 5 a
    19.

    Assim, o n�mero de hosts poss�veis segundo o prefixo CIDR � exemplificado:

    Endere�o        Prefixo      Bits HOST-ID       N�mero de hosts
    XXX.XXX.XXX.XXX/27           5 bits             32
                   /26           6 bits             64
                   /25           7 bits             128
                   ...           ...                ...
                   /13           19 bits            524.288

 3. A sequ�ncia de palavras 1101, 1001, 0001 acaba de chegar da Internet com o
    checksum 0111. Algum bit est� incorreto? Mostre todos os seus c�lculos.

       1101
       0111
       0001
     + 0111
       ----
      11100
     +    1
       ----
       1101

 4. Considere o endere�o IP 192.155.203.201 e a m�scara de subrede
    255.255.255.224.

    a) Quantas subredes internas a organiza��o possui?

    M�scara de subrede:
        255.255.255.224
                    224 = 1110 0000
                          --- 3 bits = 8 subredes

    b) Qual host est� sendo endere�ado?

    Classe: 192.155.203.201
            1100 0000
            --- 110 = Classe C
                      NET-ID  = 192.155.203.0
                      HOST-ID =   0.  0.  0.201

    HOST-ID: 201  1100 1001
    M�scara: 224  1110 0000
                     - ---- host    = 01001 = 9
                            subrede = 192.155.203.11000000
                                    = 192.155.203.192

 5. Definimos em aula o termo "arquitetura da Internet", que est� fortemente
    correlacionado a propriedades dos protocolos da camada de rede e transporte
    ou aplica��o. Explique.

    A rede na internet � uma cole��o de roteadores que suportam um grupo de
    hosts atrav�s de enlaces nas mais diversas tecnologias. O protocolo usado
    nesta camada � o IP, que � n�o confi�vel e n�o orientado � conex�o. Isso
    quer dizer que n�o existem confirma��es de recebimento (ACK) e nem uma
    ordem determinada para recebimento dos pacotes.

    A confiabilidade da comunica��o nesta arquitetura est� implementada ou na
    camada de transporte ou na camada de aplica��o, e � onde s�o verificadas
    as condi��es necess�rias para que os pacotes sejam corretamente
    disponibilizados para os processos que precisam utiliz�-los.

 6. Um pacote IP com 5400 bytes deve passar por um enlace com MTU 1300. Em
    seguida, novo empecilho: os fragmentos resultantes devem passar por um
    enlace com MTU 500. Mostre, nos dois passos, os fragmentos que resultam.

    Enlace 1. MTU = 1300
    5400 bytes = 1300 + 1300 + 1300 + 1300 + 200 bytes
               = 5 fragmentos

    Enlace 2. MTU = 500                  Offsets
    1300 bytes = 500 + 500 + 300 bytes      0   500  1000
    1300 bytes = 500 + 500 + 300 bytes   1300  1800  2300
    1300 bytes = 500 + 500 + 300 bytes   2600  3100  3600
    1300 bytes = 500 + 500 + 300 bytes   3900  4400  4900
     200 bytes = 200 bytes               5200
               = 13 fragmentos

 7. Qual o comando dispon�vel na maioria dos SOs implementa o ICMP ECHO
    REQUEST/REPLY? Em termos de funcionamento dos protocolos das diversas
    camadas, o que significa o sucesso da execu��o deste comando?

    Trata-se do comando `ping`, que consulta a interface requerida pelo
    protocolo IP, logo n�o chega a consultar as camadas superiores.

    O sucesso na execu��o deste comando quer dizer que a interface de rede
    da m�quina em quest�o est� apropriadamente conectada e preparada para
    receber dados. Contanto, n�o quer dizer que os protocolos e aplica��es
    superiores est�o corretamente configurados e responder�o as suas
    requisi��es com sucesso.

 8. H� uma mudan�a conceitual significativa quando passados da camade de rede
    (n�vel 3) para a camada de transporte (n�vel 4). Quais entidades se
    comunicam usando o protocolo da camada de rede? Como s�o identificadas?
    Quais entidades se comunicam e s�o identificadas pela camada de transporte?
    Como s�o identificadas?

    As entidades na camada de rede s�o os diferentes hosts que integram a
    estrutura de rede, e que utilizam uma associa��o de endere�os l�gicos e
    f�sicos para se identificar.

    J� a camada de transporte � composta por processos de usu�rio que
    identificam-se atrav�s de portas de servi�o do sistema operacional.

    Refer�ncias
    -----------

    * Gu�a Did�ctica de Ethernet
      http://www.consulintel.es/Html/Tutoriales/Lantronix/tutor_lantr.htm

    * Prentice, Hall. Internetworking With TCP/IP Volume 1 Ed. 4
