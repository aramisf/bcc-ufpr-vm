Vinicius Massuchetto

CI061 - Redes de Computadores II
Lista 1 de Redes de Computadores II

  * O que é um protocolo?

    Em redes, trata-se de um conjunto de regras e mecanismos que permitem a
    comunicação entre as máquinas que a compõem.

  * Quais as principais diferenças do modelo ISO/OSI para o modelo TCP/IP da
    Internet. Faça um paralelo das camadas de cada modelo. Mostre claramente a
    correspondência de camadas.

    TCP/IP          OSI
    ------------------------------
                    Aplicação
    Aplicação       Apresentação
                    Sessão
    ------------------------------
    Transporte      Transporte
    ------------------------------
    Internet        Rede
    ------------------------------
    Acesso à Rede   Dados
                    Físico

    A noção de aplicação e orientação à processos é comum aos dos modelos.

    As funcionalidades de apresentação - tal como criptografia, compressão e
    codificação dos dados, é feita na camada de aplicação mediante programas
    específicos ou bibliotecas.

    A implementação das funções de sessão - que gerencia a abertura e
    fechamento da conexão entre dois pares, tem suas características
    implementadas em nível de transporte.

    Os requisitos da camada de transporte são atendidos em parte pelo protocolo
    TCP. O TCP inclui também o conceito de portas para entrega das diferentes
    mensagens aos processos. Também implementa a recuperação de dados e o envio
    de ACKs para assegurar que as informações são entregues corretamente.

    A camada de Internet no TCP/IP implementa um modelo não orientado à
    conexão, enquanto o modelo OSO oferece modos orientados e não orientados à
    conexão.

  * Por que as redes locais necessitam de um protocolo de Controle de Acesso ao
    Meio (MAC)?

    O IP não é um identificador de máquina, e sim de conexões de uma máquina a
    uma rede específica. O MAC é um identificador necessário para organizar as
    redes em um contexto local através da camada de enlace, e o IP neste caso
    serve para a obtenção do endereço MAC das máquinas através do protocolo
    ARP.

  * Explique o funcionamento do protocolo de Controle de Acesso ao Meio da
    Ethernet, o CSMA/CD.

    Antes de transmitir, verifica-se o meio de transmissão para saber se há
    alguma outra transmissão em curso. Se não houver, a mensagem é enviada e o
    sucesso é informado para as camadas superiores. Se houver, espera-se até
    que haja liberação para transmitir.

    Se houver colisão, a conexão é paralisada, um sinal de reforço da colisão é
    enviado, e o meio de transmissão é liberado.

    Depois da colisão executa-se o algoritmo de back-off exponencial binário,
    em que a máquina espera 2^n segundos para voltar a tentar a transferência
    novamente, com o máximo de 16 tentativas, e valor máximo de n igual a 10.

    Então, a espera mínima é de 1 segundo para a primeira tentativa, e de 1023
    segundos para a décima até a décima sexta tentativa.

    Se houverem 16 tentativas sem sucesso de transmissão, é informado um erro
    para as camadas superiores e as tentativas de transmissão são encerradas.

  * A Ethernet é uma rede probabilística, que não pode ser aplicada em
    ambientes industriais. A rede Field Bus visa este tipo de aplicação, sendo
    uma rede determinística. Descreva em linhas gerais o funcionamento desta
    rede, em particular deixe claro como o determinismo é alcançado na
    comunicação.

    Cada máquina que faz parte desta rede possui a mesma quantidade de tempo
    para realizar suas transmissões. Isso significa que há uma qualidade mínima
    de serviço garantida para as aplicações.

    Neste sentido, é preciso garantir que todas as máquinas saibam quando podem
    ou não transmitir, e para qual máquina elas devem passar a permissão de
    transmissão depois que seu tempo for esgotado.

    O determinismo é viabilizado através de quatro níveis de prioridade para os
    pacotes, e a entrada e saída da rede de novas máquinas é controlada pelas
    máquinas que já estão nela através de consulta por novas máquinas.

    Existem fatores probabilísticos para tratar colisões, mas estes eventos são
    controlados a ponto de ser possível garantir uma qualidade mínima de
    serviço.

  * Compare o desempenho da rede Ethernet com o da rede Token Ring,
    considerando as mesmas taxas de transmissão para as duas redes, e situações
    de alto e baixo tráfego na rede.

    Cada rede é mais adequada para situações diferentes.

    Para cargas elevadas a Ethernet não é boa, pois ocorrerão muitas colisões e
    as máquinas terão um alto tempo de espera para transmissão. Neste caso a
    Token Ring é mais indicada, pois estabelece o mesmo tempo de transmissão
    para as máquinas que compõem a rede.

    Uma rede Ethernet atende melhor cargas baixas e moderadas, pois as máquinas
    que mais precisam transmitir podem fazê-lo quando desejarem, sem ter de
    esperar a chegada de um bastão.

  * Qual a diferença de um código de correção de erros para um código de
    detecção de erros? Se você recebeu a palavra 0100010 codificada com o
    Código de Hamming, qual bit está incorreto?

    Códigos de detecção de erros somente informam que uma determinada mensagem
    é inválida, e como consequência disso o que geralmente ocorre é o simples
    descarte do pacote.

    Já os códigos de correção de erros procuram além de detectar um erro em um
    pacote, também efetuar a correção caso ele for passível disso.

    0 1 0 0 0 1 0
    - - - - - - -
    1 2 3 4 5 6 7

    Consideram-se os valores da seguinte forma:

    1 = 1
    2 = 2
    3 = 2 + 1
    4 = 2 + 2
    5 = 4 + 1
    6 = 4 + 2
    7 = 4 + 2 + 1

    Os bits em posição de potência de dois constituem a paridade da palavra.
    Copia-se todos os bits que não estejam nestas posições, que serão os dados
    a serem analisados.

        0   0 1 0
    - - - - - - -
    1 2 3 4 5 6 7

    Para cada posição remanescente, somar os bits que possuem o valor da
    posição na composição da soma acima demonstrada, e representar como 0 se o
    resultado for par, e 1 se for ímpar.

    0 1 0 1 0 1 0
    - - - - - - -
    1 2 3 4 5 6 7

    Posição 1 = Posições 3 + 5 + 7 = 0
    Posição 2 = Posições 3 + 6 + 7 = 1
    Posição 4 = Posições 5 + 6 + 7 = 1

    Palavra original        0100010
    Palavra resultante      0101010

    Soma dos bits diferentes = 4
    Bit errado = 4

  * Considere que a sequência de palavras 0011, 0101 e 1010 acaba de chegar da
    Internet, junto com o checksum 1100. Algum bit veio errado? Mostre
    claramente como você chegou na resposta.

    Somam-se as palavras juntamente com o checksum em complemento de 1:

      0011  11110 = 1110
      0101             1
      1010          ----
      1100          1111 -> 0000 OK
      ----
     11110

    A sequência de palavras está correta, pois a soma entre todas elas com o
    checksum em complemento de 1 dá 0.

  * Qual o formato do endereço IP versão 4? Quais classes foram definidas para
    este endereço?

    Existem quatro classes de endereço para o IPv4 de acordo com a divisão da
    palavra de 32 bits (4 bytes) do endereço.

    Endereço        ###.###.###.###         Início do endereço

    Classe A:   NET-ID | HOST-ID            0   ...
    Classe B:       NET-ID | HOST-ID        10  ...
    Classe C:           NET-ID | HOST-ID    110 ...

    Classe D: Multicast
    Classe E: Uso futuro

  * Explique o funcionamento dos protocolos ARP e RARP.

    Quando uma máquina quer comunicar-se com outra em rede local, ela faz uma
    requisição ARP a partir do IP desta máquina. Toda as máquinas recebem esta
    requisição, mas somente aquela com o IP correspondente retorna com o seu
    endereço MAC. Este retorno é feito também para todas as máquinas.

    Cada máquina mantém uma tabela de relações entre endereços IP e MAC. Quando
    uma delas recebe um retorno de requisição - mesmo que não tenha sido a
    requisitante, ela atualiza estas informações.

    O protocolo RARP funciona de maneira semelhante, mas da forma reversa,
    retornando um IP a partir de um endereço físico. Como o RARP utiliza a
    camada de enlace para comunicar, um IP - que encontra-se na camada de rede,
    não é necessário.

  * Todo host que precisa do protocolo ARP precisa de executar também o
    protocolo RARP? Por que? E o contrário: todo host que executa o protocolo
    RARP executa também o protocolo ARP? Por que?

    Nem todo host usuário do ARP precisa executar o protocolo RARP, como é o
    caso de ele já conhecer seu IP, e souber para qual IP precisa comunicar.
    Assim somente requisições ARP são necessárias para obter o endereço MAC dos
    destinatários em âmbito local.

    Porém, todo host que utiliza o RARP precisa obrigatoriamente utilizar o
    ARP, pois após obter seu IP com o RARP, para transmitir ele precisará obter
    o endereço MAC do destinatário local, e isso será feito via ARP.

  * Considere o endereço IP 202.39.10.111 e a máscara de subrede
    255.255.255.192. Responda: qual host está sendo endereçado? Quantas
    subredes internas a organização possui?

    Para saber o número de máquinas endereçáveis, examina-se a máscara:

    255.255.255.192

    192 = 11000000
          11 --> 2 bits  A organização usa dois bits para endereçar
                         suas máquinas, logo pode possuir até
                         quatro máquinas.

    Para se saber qual máquina da rede é determinado IP, determina-se a classe
    do endereço:

    202.39.10.111
    202 = 11001010
          110 --> Classe C

    Com base nesta classe pode-se saber o HOST-ID do IP.

    202.39.10.111
    --------- ---
     NET-ID   HOST-ID

    Aplica-se a máscara ao HOST-ID.

    Máscara     192 = 11 000000
    HOST-ID     111 = 01 101111
                      -- ------
                 subrede  host

    Subrede     202.39.10.01000000 = 202.39.10.64
    Host        101111 = 47

  * Explique como funciona uma Proxy ARP. Em termos de administração de rede,
    qual a diferença de usar Proxy ARP e subredes?

    É um mecanismo que permite que duas redes físicas compartilhem o mesmo
    NET-ID, com uma máquina que gerencia estes endereços e responde a
    requisições de endereço físico entre hosts destas redes.

    Quando uma máquina faz uma requisição ARP para o endereço de uma máquina em
    outra rede, a Proxy ARP responde com o seu endereço físico e passa a
    receber os pacotes para esta máquina.

    A Proxy ARP precisa saber todos os endereços físicos e IPs das máquinas de
    ambas as redes, e deve reencaminhar todos os pacotes que recebe para a
    máquina correspondente.

    Proxy ARP é uma solução pouco usada pois depende da tecnologia usada pelas
    máquinas da rede, o que torna a implementação mais cara e complicada. Já
    subredes podem ser administradas de maneira independente das redes físicas
    a partir da conexão usual por roteadores.

  * O CIDR permitiu que a versão 4 do protocolo IP pudesse continuar a
    funcionar na maior parte da Internet. Quais foram os problemas que esta
    abordagem de endereçamento resolveu? Como é a solução CIDR?

    No esquema original do IP, as redes foram divididas no endereçamento entre
    NET-ID e HOST-ID, com classes que variavam o tamanho destes campos de
    acordo com a utilização imaginada para cada caso e tamanho de rede.

    O crescimento das redes TCP/IP causou a superutilização de uma classe
    intermediária de endereços, a classe B. Os endereços não estavam em uso
    da maneira propriamente dita, mas foram adquiridos e encontravam-se
    reservados.

    Esta superutilização gerou basicamente os problemas de:

      - Overhead administrativo no gerenciamento de endereços;
      - Grande tamanho da tabela de roteamento de endereço nos roteadores;
      - Falta de endereços para ser fornecido à novas redes.

    As soluções propostas foram:

      - Multiplexar os endereços pelo compartilhamento de bits no endereço
        IP. Esta solução é o CIDR, que foi tomada de imediato e impediu o
        colapso da internet nos anos 90.
      - A elaboração de um novo protocolo que atenda esta demanda de maneira
        nativa. Este é o IPv6.

    Esta técnica visa basicamente atribuir um endereço físico via multiplexação
    a vários hosts em uma rede local, como se estes hosts estivessem diretamente
    conectados à WAN.

    O CIDR possui o formato XXX.XXX.XXX.XXX/YY, em que YY é o prefixo CIDR, que
    pode variar de 13 a 27, e indica quantos bits estão sendo usados para o
    endereçamento da rede. A definição para os bits de host é implícita, e pode
    variar de 5 a 19.

    Endereço                Bits HOST-ID    Número de Hosts
    XXX.XXX.XXX.XXX/YY
                    27      5               32
                    26      6               64
                    25      7               128
                    ..      ..              ...
                    13      19              524.288

  * O formato dos dados em uma rede pode seguir um de dois padrões: Little
    Endian e Big Endian. Explique seu funcionamento. Qual dos dois é adotado
    pela Internet?

    Trata-se da ordem em que os bits encontram-se ordenados, e os protocolos
    que tratam estes dados precisam saber em qual ordem eles estão para poder
    converter as palavras corretamente.

    Por exemplo, a sequência 1011 que representa um inteiro:

    1011 --> big endian    = 11
    1101 <-- little endian = 13

    A internet é padronizadamente big endian, mas os dados não possuem ordem
    específica e dependem de cada aplicação que estará usando as mensagem.

  * Um protocolo pode oferecer um serviço confiável, orientado à conexão, ou
    não confiável, não orientado a conexão. O que estas características
    significam em termos práticos?

    Serviços confiáveis e orientados à conexão garantem a entrega correta de
    todas as mensagens que circulam na rede, verificando-as segundo algoritmos
    de detecção e correção de erros, e requisitando-as novamente caso seja
    necessário. Este modo de conexão possui custos e overheads que podem
    tornar-se complicados para algumas utilizações, e requerem processamento e
    estruturas que podem não ser viáveis em grande escala.

    Já serviços não confiáveis e não orientados à conexão simplesmente
    transmitem as mensagens da maneira como elas são disponibilizadas. O custo
    envolvido é mais baixo e pode ser feito em camadas inferiores do modelo de
    comunicação utilizado. Consequentemente o overhead é menor e existem mais
    viabilidade em larga escala.

  * Faça um desenho do header IP, explicando o propósito de cada campo.

      * = Opcionais

      - Versão [4 bits]
        Importante para a extensibilidade do protocolo, permitindo que novas
        versões possam ser compatíveis com versões anteriores.

      - Tamanho do cabeçalho [4 bits] *
        Padrão para 20 bytes.

      - ToS [8 bits] *
        - Prioridade [3 bits] *
        - Flag D [1 bit] *
        - Flag T [1 bit] *
        - Flag R [1 bit] *
        - Reservados [2 bits]

      - Bytes totais to pacote [16 bits]
        2^16 = 65536 bytes = 64KB

      - Identificador [16 bits]
        Necessário para a fragmentação.

      - Flag DONT_FRAGMENT [1 bit]
      - Flag MORE [1 bit]
      - Flag Inutilizada [1 Bit]
      - Offset do fragmento [13 bits]

      - TTL (Time to Live) [8 bits]
        Contador a ser decrementado por cada enlace em que o pacote passa
        a fim de determinar quantas vezes o pacote pode ser repassado. Existe
        para evitar loops entre os roteadores. O pacote é descartado pelo
        roteador quando TTL = 0.

      - Protocolo [8 bits]
        Identificador do protocolo que está usando o IP.

      - Checksum [16 bits]
        Implementado em hardware. Consiste no complemento da soma das palavras
        do cabeçalho em complemento de 1. Faz com que no destinatário a soma de
        todas as palavras do cabeçalho deva ser igual a 0 para indicar ausência
        de erros.

  * Já que o protocolo IP não tem confirmações de recebimento, por que os
    datagramas tem identificadores?

    Por que o protocolo IP suporta também fragmentação. O identificador serve
    para que fragmentos de um mesmo pacote possam ser remontados no
    destinatário.

  * Explique como são calculados os campos TTL (Time To Live) e checksum do
    header IP.

    O TTL possui um valor recomendado de 64 ou 128, e quando transmitido passa
    a ser decrementado em cada enlace que o pacote passa. Isso define o número
    máximo de repasses que o pacote pode sofrer, e fará com que ele seja
    descartado em situações que repasses excessivos, como um loop.

    O checksum é o complemento da soma de todas as palavras do cabeçalho do
    pacote. Esta palavra se somada com todo o restante do cabeçalho terá
    resultado igual a 0. Estes passos são usados para determinar se um pacote
    possui erros em seu cabeçalho, o que resultaria em um resultado da soma
    diferente de 0.

  * Por que é necessário fragmentar um pacote IP? Explique onde e como um
    pacote fragmentado é reconstituído.

    Por diferentes razões, cada roteador possui um limite de transferência
    chamado MTU (Maximum Transfer Unit). Se um roteador receber um pacote maior
    do que o seu limite de repasse, ele irá fragmentar este pacote em pedaços
    menores.

    A fragmentação pode ocorrer mais de uma vez no percurso de um pacote do
    remetente até o destinatário, mas nunca reconstituído. A reconstituição
    ocorre somente no destinatário.

    Isso dá-se principalmente porque são necessários todos os fragmentos para
    reconstruir um pacote, o que não é viável para os roteadores porque isso
    ou pode demorar muito, ou porque alguns fragmentos podem ter caminhos de
    roteamento diferentes.

  * Explique o processamento genérico da entidade que implementa o protocolo
    IP: como é usada a tabela de roteamento? Qual o seu formato básico?



  * Explique como o comando ping é implementado. Qual protocolo ele usa?

    O comando `ping` faz uso da mensagem ICMP `ECHO REQUEST/REPLY`, presente no
    nível de rede do modelo de comunicação. Toda implementação do IP deve
    também acompanhar uma implementação ICMP.

    Isso quer dizer que o `ping` não acessa camada superiores à de rede ao
    verificar por um endereço IP. O sucesso de resposta do `ping` indica que a
    interface de rede da máquina de destino está apropriadamente conectada e
    configurada, mas não quer dizer que outros serviços e aplicações feitos em
    rede estão funcionando e disponíveis.

  * Explique como o comando traceroute é implementado. Qual protocolo ele usa?

    O traceroute utiliza mensagens IP com diferentes valores de TTL e recebe as
    mensagens ICMP geradas pelos roteadores para saber qual roteador é
    responsável por cada repasse de pacotes entre o caminho da origem até o
    destinatário.

  * Quais foram os tipos de mensagem ICMP vistas em sala de aula?

    O ICMP são mensagens IP de controle e funcionalidades diversas.

      - 0 ECHO REQUEST/REPLY
        Mensagem respondida imediatamente quando recebida.

      - 3 Destination unreachable
        O roteador não consegue encontrar o destino para o pacote.

        Campos:
        - 0 rede inatingível
        - 1 host inatingível
        - 2 protocolo inatingível
        - 3 porta inatingível

      - 4 Fragmentação necessária mas o a flag DONT_FRAGMENT está ligada.

      - 11 Time exceeded
        Ocorre quando TTL = 0. Usado pelo comando `traceroute`.

      - 4 Source quench
        Rede congestionada. Ocorre quando os roteadores recebem mais pacotes
        do que realmente podem transmitir.

  * O protocolo da camada de rede, IP, permite a comunicação entre máquinas. O
    protocolo da camada de transporte tem outra função, qual é ela?

    É basicamente a comunicação entre hosts. O IP encarrega-se de gerenciar o
    envio de mensagens de maneira não confiável e não orientada à conexão no
    caminho intermediário entre dois hosts. Já o TCP é acessado no rementente
    e no destinatário a fim de checar a integridade das mensagens.
