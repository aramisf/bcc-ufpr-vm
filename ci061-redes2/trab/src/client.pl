#!/usr/bin/perl

# Lançamento do modo cliente
# Arquivo em UTF-8

use IO::Socket::INET;
use Net::Ping;
use POSIX qw/strftime/;
use Sys::Hostname qw(hostname);
use Getopt::Long;

# Parse dos parâmetros

GetOptions(
    'remote-host=s' => \$remote_host,
    'remote-port=i' => \$remote_port,
    'ping-address=s' => \$ping_address
);

# Tenta determinar o próprio IP

$local_host = inet_ntoa ((gethostbyname(hostname()))[4])
    or die printf "[%s] Não foi possível determinar o próprio IP. Saindo.\n",
        strftime ('%D %T', localtime);

# Verifica validade dos parâmetros

if (!$ping_address || !$remote_host || !$remote_port) {
    printf "Modo de uso:\n" .
        "./client.pl\n" .
        "\t--remote-host <host>\n" .
        "\t--remote-port <porta>\n" .
        "\t--ping-address <endereco>\n\n";
    exit(1);
}

# Inicia socket com o servidor

printf "[%s] Iniciando cliente em %s:%s.\n",
    strftime ('%D %T', localtime), $local_host, $local_port;

printf "[%s] Iniciando conexão com %s:%s.\n",
    strftime ('%D %T', localtime), $remote_host, $remote_port;

$socket = new IO::Socket::INET (
    PeerHost => $remote_host,
    PeerPort => $remote_port,
    Proto => 'tcp',
) or die "[" . strftime ('%D %T', localtime) . "]" .
    " Erro ao conectar em $remote_host:$remote_port. Saindo.\n";

# Envia pacote com o endereço a ser verificado

printf "[%s] Requisitando ping em %s para %s:%s.\n",
    strftime ('%D %T', localtime), $ping_address, $remote_host, $remote_port;

# Aguarda resultado

$socket->send($ping_address);
$socket->recv($data, 1024);

# Exibe resultado

if ($data) {
    printf "[%s] [SUCESSO] %s está acessível.\n",
        strftime ('%D %T', localtime), $ping_address;
} else {
    printf "[%s] [FALHA] %s não está acessível.\n",
        strftime ('%D %T', localtime), $ping_address;
}

printf "[%s] Fechando conexão. \n", strftime ('%D %T', localtime);

$socket->close();
