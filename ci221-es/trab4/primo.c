#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[]) {

    int n, i;

    // node 1 --> [10, 2]
    if (argc != 2) {
        printf("Sem parametro.\n");
        exit(1);

    } else {
        n = atoi(argv[1]);

        //node 2 --> [3, 4]
        if (n % 2 == 0) {

            // node 3 --> [5, 6]
            if (n == 2 || n == -2 || n == 0) {
                // node 5 --> [10]
                printf("%d primo!\n", n);
            } else {
                // node 6 --> [10]
                printf("%d nao e primo. Numero par.\n", n);
            }

        } else {
            i = 3;

            // node 4 --> [4, 7]
            while (i <= sqrt(n) && n % i != 0 )
                i = i + 2;

            // node 7 --> [8, 9]
            if (n % i == 0) {
                // node 8 --> [10]
                printf("%d nao e primo. Divisivel por %d.\n", n, i);
            } else {
                // node 9 --> [10]
                printf("-2 > %d > 2 primo!\n", n);
            }
        }
    }

    // node 10
    return 0;
}

