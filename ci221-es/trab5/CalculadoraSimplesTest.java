import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class CalculadoraSimplesTest {

	private CalculadoraSimples minhaCalculadora;
	
	@Before
    public void CalculadoraSimples() {
        minhaCalculadora = new CalculadoraSimples();
     }

	@Test
    public void testaCalculadoraSimples() {
        //Testa metodos das Operacoes basicas
        Assert.assertEquals("Resultado Errado", 20 ,minhaCalculadora.somar(10, 10));
        
        Assert.assertEquals("Resultado Errado", 0 ,minhaCalculadora.subtrair(10, 10));
        
        Assert.assertEquals("Resultado Errado", 100 ,minhaCalculadora.multiplicar(10, 10));
        
        Assert.assertEquals("Resultado Errado", 1 ,minhaCalculadora.dividir(10, 10));
        
        Assert.assertEquals("Resultado Errado",-1 ,minhaCalculadora.dividir(10, 0));
         
        //Testa metodos de memory
        minhaCalculadora.memoryAdiciona(1000);
        Assert.assertEquals("Resultado Errado", 1000 ,minhaCalculadora.getMS());
        minhaCalculadora.limparMC();
        Assert.assertEquals("Resultado Errado", 0 ,minhaCalculadora.getMS());
                 
    } 

}
