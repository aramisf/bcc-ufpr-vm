public class Fatorial {
	
    public static long Fatorial(long num) {  
        if (num <=1 )  
        	return 1;  
        else
        	return num * Fatorial(num - 1);  
    }  

}