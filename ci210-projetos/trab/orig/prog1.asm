# 
# Autor: Fernando Gehm Moraes
# Revis�o: Ney Calazans 07/05/2008
#

        .text   
        .globl  main            
        
main:   

        la      $s0, dados      	# inicializa o ponteiro para a �rea de dados
        ori     $s3, $s3, 0x125		# carrega 125 em $s3 ($19)
        lw      $s1, 0($s0)		# carrega 00aa0101h em $s1($17)
        lw      $s2, 4($s0)		# carrega 0000bb00h em $s2($18)
 
        addu    $t0,$s1,$s3		# carrega 00aa0226h em $t0($8) = $s1+$s3

        addu    $t0,$t0,$s2		# carrega 00aabd26h em $t0($8) = $t0+$s2      
        subu    $t1,$s1,$s2		# carrega 00a94601h em $t1($9) = $s1-$s2
        and     $t2,$s1,$s2		# carrega 00000100h em $t2($10) = $t0 and $s2
        or      $t3,$s1,$s2		# carrega 00aabb01h em $t3($11) = $t0 or $s2
        xor     $t4,$s1,$s2		# carrega 00aaba01h em $t4($12) = $t0 xor $s2
        nor     $t5,$s1,$s2		# carrega ff5544feh em $t5($13) = $t0 nor $s2
        
        sw      $t0,  8($s0)		# escreve 00aabd26h ($t0=$8) nas posi��es de mem�ria 10010008h-1001000bh
        sw      $t1, 12($s0)		# escreve 00a94601h ($t1=$9) nas posi��es de mem�ria 1001000ch-1001000fh
        sw      $t2, 16($s0)		# escreve 00000100h ($t2=$10) nas posi��es de mem�ria 10010010h-10010013h 
        sw      $t3, 20($s0)		# escreve 00aabb01h ($t3=$11) nas posi��es de mem�ria 10010014h-10010017h
        sw      $t4, 24($s0)		# escreve 00aaba01h ($t4=$12) nas posi��es de mem�ria 10010018h-1001001bh 
        sw      $t5, 28($s0)		# escreve ff5544feh ($t5=$13) nas posi��es de mem�ria 1001001ch-1001001fh
               
end:    jr       $ra

        .data  
dados:  .word   0x00AA0101  0x0000BB00           
       


