\chapter{Conceitos}

\section{Software livre}

  Dentre as subdivisões dos componentes de um computador, o termo software é
  constantemente dissociado em conceitos menores e que são usados pelos autores
  para atender seus respectivos contextos. Para este trabalho, no entanto, uma
  definição convencional é:

  \begin{citacao}

    [\ldots] todos os componentes funcionais não físicos de um computador, e
    portanto, não somente os programas em si, mas também os dados a serem
    processados por eles. \cite[p. 1, tradução e grifo
    nosso]{Engelhardt2008}.

  \end{citacao}

  \label{Text:QuatroLiberdades}

  Esta definição é especialmente útil para fazer a diferenciação entre software
  livre e {proprietário. A Fundação do Software Livre -- uma das principais
  entidade em defesa da liberdade digital, definiu as quatro liberdades do
  software livre \cite{Stallman2007}.

  \begin{itemize}

    \item{Liberdade 0: A liberdade de executar um programa para qualquer que
    seja a finalidade e em qualquer condição;}

    \item{Liberdade 1: A liberdade de estudar um programa e de modificá-lo como
    for desejado -- ter acesso ao código-fonte é uma condição para esta
    liberdade;}

    \item{Liberdade 2: A liberdade de redistribuir cópias de um programa, e
    assim ajudar outras pessoas a ter acesso a este programa;}

    \item{Liberdade 3: A liberdade de melhorar um programa e de distribuir
    novas versões para o público, e assim beneficiar toda a comunidade.}

  \end{itemize}

  Também é necessário fazer a distinção entre software livre e gratuito, já que
  a tradução em inglês para ambas as palavras é free, e isso acaba gerando
  bastante confusão no entendimento do conceito. Embora a maior parte dos
  softwares livres sejam gratuitos, temos que boa parte dos softwares gratuitos
  não são livres segundo as quatro liberdades enunciadas acima. Nestes
  softwares -- os chamados \emph{freewares}, existe uma liberdade muitas vezes
  restrita para executar o programa, mas não podemos ver seu código fonte e
  muito menos modificar e redistribuir cópias personalizadas dele.

  Muitas pessoas não familiarizadas com o conceito e suas interações de mercado
  perguntam-se qual a vantagem de se desenvolver produtos em software livre já
  que não se pode vendê-los. Esta visão -- que será detalhada mais à frente --
  é a própria interpretação do sistema em que vivemos sobre os conjuntos de
  conhecimento gerados pela humanidade. Embora o produto de software possa ser
  distribuído gratuitamente, o que está sendo comprado na maioria das vezes é o
  conhecimento técnico através do trabalho humano em instalar, modificar,
  melhorar, configurar e promover treinamento sobre um determinado produto.

  Neste sentido, como o trabalho é colaborativo e transparente para os
  desenvolvedores, o custo é distribuído entre as empresas interessadas em
  utilizar estes softwares. Novas funcionalidades são introduzidas à medida que
  demandas são criadas e escolhidas para implementação, e versões
  personalizadas de um produto principal podem ser criadas para atender
  requisitos específicos.

  Porém, não somente por estas questões é que existe a preferência pelo
  software livre. Para o mercado de software de uma maneira mais específica,
  vemos a expressão destas vantagens sendo frequentemente enunciadas pelos
  autores da área. \citeonline{Kaminsky2009} lista uma série delas, sendo as
  principais:

  \begin{itemize}

    \item{uma maior participação de desenvolvedores pelo mundo todo, o que
    resulta em um software mais revisado e seguro, já que mais pessoas têm
    acesso ao código-fonte e são capazes de identificar suas vulnerabilidades;}

    \item{o aumento da dificuldade para formação de monopólios de prestação de
    serviços, pois as soluções estão disponíveis também para outras empresas
    que podem a qualquer momento obter uma cópia do produto em questão e
    prestar os mesmos serviços em relação a ele;}

    \item{é possível que os recrutadores tenham um melhor conhecimento das
    habilidades dos desenvolvedores ao selecioná-los para um determinado cargo,
    já é possível também ter acesso às contribuições dadas por eles aos
    projetos de software livre.}

  \end{itemize}

  A fim de facilitar a compreensão das dinâmicas da cultura livre,
  \citeonline{Shaver2008} propõe um diagrama de termos e ideias que, a partir
  do software livre deriva outras definições presentes no mercado, resultando
  na ênfase de algumas de suas qualidades.

  \begin{figure}[H]

    \centering
    \frame{\includegraphics[width=0.9\textwidth]{./img/free-software-map.jpg}}

    \caption{Mapa histórico-generativo de termos e ideias do software livre}

    Fonte: \citeonline{Shaver2008}

  \end{figure}

  Para este trabalho, no entanto, o valor fornecido pelo software livre a ser
  mais utilizado é elementar, e tem a ver com a ideia de liberdade tão presente
  na atividade de desenvolvimento de software. Trata-se da propriedade
  intelectual, ou ainda de forma mais detalhada, dos direitos sobre os
  diferentes tipos de usufruto das invenções em todos os domínios da atividade
  humana.

  Um modo interessante de definir estes usufrutos é pela análise da definição
  das próprias liberdades do software livre. Para qualquer invenção ou
  conhecimento que exista, podemos identificar quem legalmente pode utilizá-la,
  redistribuí-la -- com e sem objetivos comerciais -- ou modificá-la. No modelo
  proprietário do uso de bens, estas responsabilidades geralmente caem sobre
  atores diferentes, cada qual com seu exercícios sobre direitos formalmente
  adquiridos.

  Para entender este conceito de liberdade, é preciso expandir a aplicação do
  significado e não focar somente nos meios de acesso a bens materiais. É
  justamente este aprisionamento que tem levado o conceito de software livre a
  ser ainda muito ligado a justificativas que no final das contas sempre levam
  a argumentos de cunho financeiro.

  \label{ConceitoLiberdade}

  \begin{citacao}

    O termo ``software livre'' é suscetível à muitas interpretações errôneas: a
    um argumento não intencional como ``software que pode ser adquirido sem
    custo'' se encaixa tão bem quanto ``software que dá certas liberdades ao
    usuário''. Resolvemos este problema publicando a definição de software
    livre, e por dizer ``pense em `liberdade de expressão', não em `cerveja
    grátis'''. Esta não é a solução perfeita e não vai eliminar o problema. Uma
    termo correto e sem ambiguidade seria melhor, se também não tivesse
    outros problemas... Toda substituição proposta para o termo ``software
    livre'' tem algum tipo de problema semântico -- e isso inclui o ``software
    de código aberto''. \cite[tradução nossa]{Stallman2007}.

  \end{citacao}

  Sem deslegitimar a importância da viabilidade econômica da implantação de
  software livre nas organizações, as seções seguintes procuram discutir os
  programas de computador em outros níveis de abstração, e consequentemente
  requerem o abandono de impressões exclusivamente estigmatizadas não só em
  relação ao software livre, mas também em relação aos modelos teóricos a serem
  tratados.

\section{Sociedade da informação}

  É evidente que nossa sociedade passa por uma profunda transformação ditada
  pelo desenvolvimento tecnológico, e que isto traz um impacto substancial no
  modo de vida que temos. Embora não seja consensual entre os estudiosos da
  área de que já ultrapassamos um marco que torna possível a definição de um
  novo modelo de sociedade, é necessário para os objetivos deste trabalho
  adotar um termo de tratamento à permeação da tecnologia na sociedade.

  Segundo \citeonline{Castells1999}, as diversas revoluções tecnológicas
  ocorridas ao longo da história são marcadas, basicamente, por sua
  penetrabilidade em todos os domínios da atividade humana. Em outras palavras,
  define-se uma nova era quando as inovações tecnológicas induzem uma profunda
  modificação nas relações entre as pessoas, o que envolve inclusive as
  relações de poder e produção em uma sistemática econômica. Para
  \citeonline{Masuda1980}, a magnitude destas transformações é equiparável à
  outros marcos singulares da história.

  \begin{citacao}

    Na história documentada, existem três impulsos de mudança fortes o
    suficiente para alterar o homem em sua essência. A introdução da
    agricultura [\ldots] a revolução industrial [\ldots] [e] a revolução
    tecnológica de processamento da informação [ou revolução da informação].
    \cite[grifo nosso]{Masuda1980}.

  \end{citacao}

  Ao longo deste trabalho, os termos sociedade da informação ou era da
  informação são frequentemente usados, e por tratar-se de um termo bastante
  recente -- alvo dos estudos antropológicos que tomam por base o impacto da
  tecnologia na sociedade, não existe uma definição universalmente aceita e
  muito menos um período consensualmente estabelecido para apontar marcos e
  eventos determinantes em termos de contextualização histórica
  \cite{Karvalics2007}.

  Assim, o uso do termo é colocado de modo a remeter ao novo paradigma de
  sociedade em que atualmente nos encontramos, no qual a dinâmica dada à
  informação e seus sistemas não define somente o modo de funcionamento dos
  mercados, mas também orienta transformações sociais, econômicas e culturais.

  Há ainda, dentre os estudos sobre o impacto da tecnologia na sociedade, o
  surgimento de outro termo e conceito chamado de sociedade do conhecimento. As
  interpretações apresentadas são variáveis, tais como:

  \begin{itemize}

    \item{uma forma mais progressista de se referir à sociedade da
    informação;}

    \item{uma forma mais abrangente de se referir à sociedade da informação,
    sendo esta um dos componentes da sociedade do conhecimento;}

    \item{Uma forma mais atual e que será vez cada mais utilizada, com a
    gradativa substituição de sociedade da informação por sociedade do
    conhecimento.}

  \end{itemize}

  A partir desta confusão de atribuições, \citeonline{Karvalics2007} defende
  uma separação artificial a fim de facilitar o estudo, evitar ambiguidades e
  favorecer os aprofundamentos das pesquisas em tecnologia e sociedade. Esta
  separação nos é especialmente útil, visto que ambas as terminologias --
  informação e conhecimento -- são frequentemente usadas no estudo do
  desenvolvimento livre de software, e que podem ser apropriadamente
  distinguidas. Para isso, nos pautamos em três principais tópicos.

  \begin{itemize}

    \item{Os processos de produção da informação se dão na mente dos
    indivíduos, não em ambientes naturais ou artificialmente controlados, por
    mais que sua expressão possa ocorrer em meio material.}

    \item{Os sistemas de tecnologia da informação operam com a informação
    convertida em símbolos, pois computadores processam nada mais do que
    símbolos. Mentes e intelectos, por sua vez, processam informação.}

    \item{Conhecimento pode ser definido como informação organizada,
    transformada e contextualizada. Desse modo, ao falar sobre conhecimento e
    informação, falamos de dois componentes indivisíveis de um único universo
    cognitivo.}

  \end{itemize}

  Dentre as diversas definições nos campos social, econômico, cultural,
  filosófico, técnico e educacional, existe aquela impressão popular com um tom
  predominantemente liberal e progressista em relação à aplicação da tecnologia
  na sociedade, apresentando basicamente um instrumento de melhoria da
  qualidade de vida, e que está disponível principalmente para oferecer
  agilidade, comodidade, melhores condições de trabalho e mais opções de
  entretenimento.

  Esta visão é, de fato, como a maior parte da população brasileira tem
  enxergado a evolução tecnológica. Em 2010, cerca de 95\% das pessoas
  acreditava que o impacto que a tecnologia causa na sociedade possui
  benefícios consideravelmente maiores do que os possíveis malefícios, ou ao
  menos o mesmo grau entre benefício e malefício \cite{CienciaeTecnologia2010}.

  Uma crescente esperança de que estas mudanças irão cada vez mais trazer
  melhorias para o nosso modo de vida podem ser notadas através dos mesmos
  dados obtidos em 2006, quando 28\% das pessoas acreditava que a tecnologia
  trazia somente benefícios. Em 2010, temos que 38,9\% das pessoas possuíam
  esta mesma opinião \cite{CienciaeTecnologia2010}.

  Por outro lado, uma visão científica um tanto menos romântica -- para não
  dizer pessimista, apresenta a sociedade do conhecimento como uma
  reestruturação do sistema capitalista que teve por finalidade principal a
  ampliação do poderio econômico dos proprietários dos meios de produção, e que
  muito pouco tem a ver com os interesses das massas populacionais no que se
  refere à melhoria da qualidade de vida \cite{Soderberg2002}.

  Charles Babbage, contemporâneo de Marx e considerado o pai da computação --
  inventor da máquina analítica, um sistema mecânico que mais tarde viria a
  basear a construção dos primeiros computadores -- produz trabalhos em
  economia política sobre o `gerenciamento científico' dos processos
  industriais -- ou a busca da produção por meios exclusivamente mecânicos
  \cite{DyerWitheford1999}. \citeonline{Marx1939} identifica várias de suas
  constatações nestes trabalhos, pois não enxerga ali somente o próprio caráter
  tecnocrático do capitalismo, mas também um estudo estratégico para a lutas de
  classes. Uma crítica a respeito dos discursos vangloriantes da tecnologia é
  colocada nas anotações finais de O Capital -- os \emph{Grundrisse}, mostrando
  de maneira bem clara a sua visão sobre o progresso da humanidade nos
  diferentes campos do conhecimento:

  \begin{citacao}

    `O progresso contínuo de sabedoria e experiência', diz Babbage, `é o nosso
    grande poder'. Esta progressão, este progresso social pertence e é
    explorado pelo capital. Todas as formas anteriores de propriedade condenam
    grande parte da humanidade, os escravos, a serem puros instrumentos de
    trabalho. O desenvolvimento histórico, político, artístico, científico,
    etc. acontece em privilegiados círculos sobre suas cabeças. Mas somente o
    capital subjugou o progresso histórico em detrimento do seu
    enriquecimento. \cite[tradução nossa]{Marx1939}.

  \end{citacao}

  A visão popular, logicamente, é bastante diferente. Ela reflete, antes de
  mais nada, a falta de formação e questionamento em relação à promoção das
  mudanças tecnológicas, e sequer coloca os interesses dos seus principais
  promotores sob suspeita. Parece existir uma aceitação fácil, generalizada e
  sem resistências do discurso daqueles que propagandeiam a tecnologia como
  fornecedora de soluções presentes e amplamente disponíveis, como se a
  configuração do desenvolvimento científico atual não estivesse longe de ser
  voltado ao desenvolvimento econômico das minorias sociais.

  \label{Text:DefinicaoCiberespaco}

  Buscando descrever as peculiaridades da sociedade da informação, alguns
  autores introduziram desde a década de 90 o conceito de ciberespaço na teoria
  da comunicação. Este conceito possui origem na ficção científica das
  fantásticas novelas que se passam em mundos e circunstâncias futurísticas --
  literatura que ficou famosa a partir da década de 30 e que mais tarde deu
  origem à cultura \emph{cyberpunk} \cite{Stallabras1996}, cujo tom lírico e
  poético são marcantes na própria literatura acadêmica.

  \begin{citacao}

    Assim como se diz ``tem areia'', ``tem água'' se diz ``tem textos'', ``tem
    mensagens'' pois eles se tornam matérias como se fossem fluxos justamente
    porque o suporte deles não é fixo, porque no seio do espaço cibernético
    qualquer elemento tem a possibilidade de interação com qualquer outro
    elemento presente. Então, isso não é uma utopia daqueles que
    experimentaram, conhecem e participam da Internet. É como se todos os
    textos fizessem parte de um texto, só que é o hipertexto, um autor
    coletivo e que está em transformação permanente. É como se todas as músicas
    passassem a fazer parte de uma mesma polifonia virtual e potencial, como se
    todas as músicas fizessem parte de uma só música, também ela virtual e
    potencial. \cite[p. 3]{Levy1994}.

  \end{citacao}

  \citeonline{Levy1998} define ainda de forma bastante conveniente -- assim
  como outros autores, o ciberespaço em um nível computacional como o ``espaço
  de interconexão entre os computadores e as memórias dos computadores'', uma
  rede vasta, complexa e estruturada de comunicação.

  Em relação à contextualização histórica do conceito de sociedade da
  informação, embora alguns autores utilizem-se dos acontecimentos ao redor das
  políticas de propriedade intelectual a partir da década de 1950,
  \citeonline{Castells1999} atribui esta reestruturação da sociedade a um
  momento um pouco mais tardio, e introduz o assunto da seguinte maneira:

  \begin{citacao}

    A revolução da tecnologia da informação foi essencial para a implementação
    de um importante processo de reestruturação do sistema capitalista a partir
    da década de 1980. No processo, o desenvolvimento e as manifestações dessa
    revolução tecnológica foram moldados pelas lógicas e interesses do
    capitalismo avançado, sem se limitarem às expressões desses interesses.

  \end{citacao}

  \citeonline{Masuda1980} esquematiza ainda, uma tabela comparativa entre a
  revolução industrial e a revolução da informação, e que demonstra como se deu
  a transformação dos padrões de desenvolvimento de uma revolução para a outra.
  Os itens a seguir apresentam as principais características destes padrões
  para a sociedade industrial e da informação, respectivamente.

  \begin{itemize}

    \item{Inovações tecnológicas: Motor a vapor, amplificação do trabalho
    manual; Computadores, amplificação do trabalho mental (conhecimento);}

    \item{Estrutura sócio-econômica: Produtos e serviços, indústria da
    manufatura, fábrica como o centro da produção; Informação, tecnologia,
    indústria intelectual, distribuição da informação;}

    \item{Estrutura econômica: Divisão do trabalho, divisão da produção;
    Sinergia, processos de produção integrados;}

    \item{Valores sociais: Materiais, direitos humanos e libertação;
    Eficiência, contribuição social e globalismo.}

  \end{itemize}

  Os estudos sobre tecnologia e sociedade estendem-se à uma diversidade de
  campos cujos acompanhamentos não nos cabem aprofundar neste momento, já que
  esta seção visa atender somente à uma necessidade de introdução da
  complexidade que é a discussão sobre o termo sociedade da informação, e
  esclarecer como se deu o processo histórico de formação do contexto
  tecnológico atual. Em todo este contexto, uma definição bastante razoável é
  dada por \citeonline{Matos2002}, e que é o suficiente para a tomarmos por
  base e evitar anacronismos quando o termo for encontrado ao longo deste
  trabalho.

  \begin{citacao}

    A sociedade da informação é uma expressão comumente usada para designar uma
    forma de organização social, econômica e cultural que tem como base, tanto
    material como simbólica, a informação. Esta sociedade assim organizada
    seria aquela em que vivemos. [\ldots] \cite[p. 12, grifo
    nosso]{Matos2002}.

  \end{citacao}

\section{Marxismo}

  Variantes do termo marxismo têm aparecido com frequência para designar as
  diferentes vertentes ideológicas que surgiram em torno dos conceitos centrais
  deixados pelos estudos de Karl Marx, e acabam por causar divergências entre
  os diferentes grupos que se dizem socialistas.

  \begin{citacao}

    Por marxismo entende-se o conjunto de teorias filosóficas, econômicas,
    sociológicas e politicas, elaboradas por Karl Marx com a colaboração de
    Friedrich Engels e desenvolvidas por seus adeptos, em parte ortodoxos e em
    partes dissidentes. Como as três fontes principais do marxismo indicam-se:
    a filosofia idealista alemã (Hegel), o materialismo filosófico francês do
    século XVIII, e na economia politica inglesa do começo do século XIX
    \cite{Internacional1993}.

  \end{citacao}

  O método Marxista foi concebido através da necessidade de análise de uma
  sociedade que difere substancialmente daquela em que vivemos. Aplicar este
  mesmo método novamente requer uma aproximação cautelosa e pragmática dos
  conceitos já estabelecidos, e que tenha como objetivo preservar as linhas
  científicas que levaram à sua renomada consolidação. A definição pelo que se
  conhece por marxismo ortodoxo é dada por \citeonline{Lukacs1919} -- um dos
  precursores do conceito -- em um artigo clássico sobre o assunto.

  \begin{citacao}

    Marxismo ortodoxo, no entanto, não implica na aceitação estática e sem
    críticas das investigações de Marx. Não é a `crença' nesta ou naquela tese,
    nem a exegese de um `livro sagrado'. Ao contrário, ortodoxia se refere
    exclusivamente a método. É a convicção científica de que o materialismo
    dialético é a estrada para a verdade e que seus métodos podem ser
    desenvolvidos, expandidos e aprofundados somente através das linhas
    traçadas por seus fundadores. É a convicção, ainda, de que todas as
    tentativas de ultrapassar ou `melhorar' estas metodologias levaram e devem
    levar sempre à uma maior simplicidade, trivialidade e ecleticismo.
    \cite[tradução nossa]{Lukacs1919}.

  \end{citacao}

  Desta forma, uma análise que se proponha a um método ortodoxo sobre qualquer
  estrutura social deve ter como alicerce o conceito central de materialismo
  dialético, observando sempre as transformações dos modos de produção com o
  entendimento de que elas acarretam novas mudanças também nas relações
  sociais.

  Em uma tentativa de entender melhor as crescentes e intensas transformações
  sociais vivenciadas pela humanidade desde a época de Marx, e como forma de
  adaptação e suprimento de deficiências da teoria inicial à novas análises,
  diversas vertentes marxistas foram iniciadas, cada qual atendendo sua demanda
  e concepção.

  Uma destas vertentes resulta de estudos realizados no século XX denominado de
  neo-marxismo, e que buscam combinar elementos de outras linhagens
  intelectuais com aquelas solidificadas no marxismo ortodoxo, introduzindo
  conceitos do existencialismo e gerando novas metodologias como o marxismo
  analítico e o marxismo estrutural, e também influenciando novos movimentos
  sociais, como a nova esquerda, o movimento \emph{hippie} e o estudantil.

  O neo-marxismo pode ser considerado como uma resposta dos estudiosos às
  estratégias de globalização do capitalismo, já que muitos deles descrevem
  teorias de economia global e buscam explicar os processos de monopolização
  dos mercados e concentração das riquezas, frequentemente inserindo novos
  termos e conceitos.

  Porém, existem pertinentes discussões acerca da preservação do marxismo
  convencional nestes estudos, e o quão eles estariam empregando os métodos
  ortodoxos adequadamente. Neste sentido, sua eficácia no subsídio das
  revoluções também acabam sendo questionadas.

  A temática marxista para o tratamento do software livre -- ou em outras
  palavras, o propósito de aplicação deste modelo teórico em um processo mais
  específico da sociedade da informação -- é tratado a seguir por uma série de
  seções que buscam justificar a escolha deste modelo.
