\chapter{Marxismo como modelo teórico}

\section{Uma alternativa de análise}

  \label{Text:AlternativaAnalise}

  Já antes dos primeiros computadores, com a vertente iluminista da filosofia
  da liberdade no século 18, este tema em concepções mais modernas já era
  centro de debates em busca da compreensão da complexidade humana
  \cite{Bristow2010}.

  Em tempos em que as monarquias possuíam alto sigilo sobre o progresso
  tecnológico, suas políticas e informações \cite{Dorigo2005}, o início do
  pensamento sobre o quê de fato seria a liberdade do homem toma grande
  importância no questionamento das lógicas protecionistas. Friedrich Schiller,
  por exemplo, cita entre sua coleção de cartas a seguinte passagem:

  \begin{citacao}

    A cultura, longe de nos dar liberdade, somente desenvolve novas
    necessidades ao longo do tempo. As correntes do mundo físico nos apertam
    cada vez mais de modo que o medo da perda extingue até o mais ardente
    impulso em direção à inovação, enquanto a obediência passiva é mantida como
    a maior das sabedorias da vida. \cite[tradução nossa]{Schiller1909}.

  \end{citacao}

  Inúmeros filósofos, matemáticos e outros cientistas -- dentre eles os
  precursores da revolução francesa -- viriam a trabalhar sobre as primeiras
  perspectivas do que se conhece sobre direitos humanos. Os trabalhos feitos
  por eles foram importantes para a inserção do uso da razão em temáticas
  relacionadas à livre religião, anti-escravismo, anti-colonialismo e liberdade
  de expressão. Formava-se uma nova concepção do que se entendia pelo papel de
  cada indivíduo na sociedade, e quebrava-se a lógica medieval do pensar
  \cite{Verhoeven2011}.

  É evidente que dentre essa movimentação ideológica estava presente uma
  intensa preocupação intelectual com o que se entende pela liberdade do homem,
  e como ela se expressa e é tratada pela sociedade juntamente com os demais
  aspectos culturais \cite{Bristow2010}.

  Estas concepções diferenciadas culminaram em mobilizações populares bastante
  expressivas politicamente e que vieram a moldar todo o pensar ocidental até
  os dias de hoje. Tem-se que o trabalho destas pessoas é atualmente o ponto de
  partida implicitamente presente em qualquer análise da sociedade que se
  queira realizar \cite{Bristow2010}.

  O amadurecimento das ideias iluministas possibilitou a expansão da aplicação
  de conceitos filosóficos liberais à praticamente todas as áreas de interação
  social existentes na época. Dentre os estudos resultantes está a importante
  obra de Georg Hegel, e embora sua integração formal à vertente iluminista não
  seja um consenso entre os estudiosos da área, existem fortes relações entre
  os conceitos por ele colocados e a herança dos pensadores iluministas
  \cite{Lessa2007}.

  Apesar das críticas posteriores ao seu trabalho por outros filósofos que
  ganharam notoriedade por suas ideias, um aspecto a ser relevado da obra de
  Hegel são as suas relações entre indivíduo e mundo, até porque mais tarde
  surgiriam interpretações de sentido revolucionário destas relações, e que
  levariam à questionamentos fundamentais no campo político quanto à percepção
  do estado, do trabalho e da produção de bens \cite{Lessa2007}.

  Independentemente, o que ocorreu foi que, com justificativas humanistas à
  mão, o modelo vigente de exploração dos trabalhadores viria a ser questionado
  pelos pensadores, e que estes acabariam por iniciar a formulação de toda uma
  nova gama de interpretações voltadas especificadamente às relações de mercado
  e de trabalho.

  Neste contexto histórico, Karl Marx elaborou trabalhos a partir de uma
  abordagem metodológica que, quando tem seus conceitos colocados pela
  dialética, possuem boa aceitação em relação ao esclarecimento das lógicas de
  mercado do sistema capitalista e acabam por não só subsidiar ideologicamente
  as organizações sociais segundo um método científico, mas também por sugerir
  uma agenda política para a implantação de sistemas de poder diferenciados que
  venham a dividir de maneira justa os bens produzidos por uma sociedade
  \cite{Giannotti1996}.

  Dada a linha cronológica, é especialmente importante afirmar que Marx não é
  só herdeiro da filosofia de Hegel e dos socialistas utópicos que o
  antecederam, mas sim de toda a tradição humanista e racionalista do século
  XVIII \cite{Netto2002}.

  Em vida, Marx foi muito mais conhecido como militante do que como filósofo.
  Sua obra foi citada por estudiosos e influenciou movimentos operários de
  maneira substancial somente a partir de sua morte. Embora existam
  argumentações que afirmam não ser mais possível fazer interpretações de fatos
  atuais através dos conceitos de sua obra -- já que a sociedade passou por
  numerosas e intensas mudanças desde sua época, não é difícil identificar as
  correspondências do mundo de hoje com os conceitos centrais deixados por Marx
  \cite{Meksenas2008}.

  Muito discute-se sobre a crise da teoria marxista. Diversos governos que
  atribuíram os nomes ``marxismo'' e ``comunismo'' às suas políticas mostraram
  de diferentes formas e por diferentes razões -- assim como os governos
  capitalistas -- uma profunda incompatibilidade à manutenção de uma ordem
  social próspera.  Encaixam-se aí a falência da União Soviética, a abertura
  econômica de forma liberal pela China, os governos comunistas da Ásia e da
  África \cite{Giannotti2011}, e até Cuba -- cujo embargo econômico projetado
  pelos Estados Unidos é sistematicamente ignorado ao direcionar críticas às
  suas fragilidades econômicas.

  \citeonline{Giannotti2011} justifica ainda, como situar a validade deste tipo
  de estudo em contextos atuais, apresentando a ideia de que uma teoria não se
  invalida a partir dos fracassos históricos daqueles que se utilizaram de seu
  nome ou de seus conceitos -- muito embora por vezes possa ter sido somente o
  nome.

  \begin{citacao}

    O colapso [dos países socialistas] evidenciaram que a luta contra as
    misérias, instaladas pelo sistema capitalista, não implica qualquer
    compromisso com partidos comunistas de cunho leninista. Esse colapso reduz
    a pó a vulgata marxista, mas não impede que se continue a estudar as
    representações e as relações sociais da ótica do metabolismo que o homem
    mantém com a natureza, em suma, daquela que vê as relações sociais de
    produção imbricadas com o desenvolvimento das forças produtivas. [\ldots]
    a obra escrita ilumina-se a partir de certas perspectivas históricas, de
    certos vieses que alimentam modos de pensar e de ver, inscritos em nosso
    cotidiano. \cite[p. 17, grifo nosso]{Giannotti2011}.

  \end{citacao}

  \citeonline{Meszaros2010} também discorre sobre a adaptabilidade da teoria
  base para com novos conceitos:

  \begin{citacao}

    [\ldots] a transformação social prevista pela visão marxista deve ser capaz
    de avaliar as dificuldades inerentes à própria magnitude das tarefas a
    serem realizadas, como também enfrentar as contingências sócio-históricas
    mutáveis e inevitáveis, reexaminando as proposições básicas da teoria
    original e, se necessário, adaptando às novas circunstâncias. \cite[grifo
    nosso]{Meszaros2010}.

  \end{citacao}

  Não só pela validade da elucidação teórica se justifica uma análise marxista.
  Embora o sistema capitalista tenha se reformulado, se globalizado e adquirido
  novas dinâmicas de exercício de poder devido ao avanço tecnológico e à
  sociedade da informação, temos os mesmos elementos conceituais mantendo os
  mesmos padrões: o trabalhador explorado, a forte orientação à propriedade, a
  centralização do lucro privado, a mais-valia e a grande desigualdade nas
  relações de produção \cite{Meksenas2008}.

  O grande valor de Marx, no entanto, vem com seu método. As constatações por
  ele deixadas podem vir a mostrar interpretações úteis no entendimento das
  mudanças sociais promovidas pela era da informação a partir do momento que a
  manutenção do sistema capitalista encontra-se ainda sobre as mesmas relações
  de poder que ele identificou.

  \begin{citacao}

    [\ldots] Marx foi capaz de apreender as tendências estruturais da ordem
    burguesa [\ldots] porque encontrou a perspectiva teórico-metodológica capaz
    de lhe permitir a apreensão do movimento deste objeto [\ldots] \cite[grifo
    nosso]{Netto2002}

  \end{citacao}

  A principal suspeita a respeito da aplicabilidade de seu método na atualidade
  dá-se pela percepção do imaginário coletivo de que os sistemas de informação
  aos poucos substituirão os trabalhadores, e que tão logo não haverá campo
  para aplicação de uma análise marxista. Uma interpretação desta natureza peca
  em essência por deixar de analisar um pouco mais profundamente sobre o que é
  de fato a informação, e quais suas demandas de poder, produção e consumo em
  relação à sociedade \cite{Soderberg2002}.

  Uma crítica irônica é feita por \citeonline{DyerWitheford1999} em relação à
  este tipo de posicionamento. O autor dedica todo um capítulo para responder
  argumentos que negam a compatibilidade da teoria marxista com a sociedade
  contemporânea:

  \begin{citacao}

    [\ldots] se o marxismo é tido como obsoleto pela era da informação, é
    somente pela luz de um certo desenvolvimento `informacional' --
    globalização, pré eminência da mídia, tele-trabalho -- é que podemos ver a
    completa importância de alguns temas presentes nos textos de Marx -- por
    exemplo, a ênfase [dos que pregam o fim do marxismo] na internacionalização
    e automação da produção. [\ldots] o marxismo sempre manifestará uma
    contínua ``espectrabilidade'', uma estranha negação em morrer e ser
    enterrado, e que está profundamente conectada à natureza ``espectral'' e
    ``imaterial'' do tecnocapitalismo contemporâneo. \cite[p. 8, tradução e
    grifo nosso]{DyerWitheford1999}.

  \end{citacao}

  \citeonline{Soderberg2002}, por sua vez, é ainda mais incisivo na temática do
  software livre, sendo um dos primeiros autores a produzir conteúdo dedicado e
  significativo na relação da liberdade de software com o marxismo:

  \begin{citacao}

    O marxismo oferece um bom modelo teórico para análise das contradições
    inerentes do regime de propriedade intelectual. O sucesso do software livre
    em trabalhar fora do sistema comercial de software é uma amostra do que foi
    descrito por Marx há mais de 150 anos sob as formalizações de força
    produtiva e de intelecto geral. [\ldots] a história não se resume ao
    levante das forças produtivas que foram convenientemente mapeadas pelos
    exemplos do materialismo histórico, mas são conflitos protagonizados por
    atores sociais, dentre eles o movimento do software livre e sua
    característica especial de desafiar a dominação do capital sobre o
    desenvolvimento tecnológico. \cite[tradução e grifo
    nosso]{Soderberg2002}.

  \end{citacao}

  Para Marx, a realidade nada mais é do que um sistema de relações de modo que
  as instâncias que compõem a sociedade são sempre complexas e constituem uma
  totalidade de totalidades articuladas, estas por sua vez formadas de
  categorias e relações simples que devem ser descortinadas para uma
  reconstituição abstrata do todo \cite{Carvalho2007}.

  \begin{citacao}

    A categoria de totalidade significa (...), de um lado, que a realidade
    objetiva é um todo coerente em que cada elemento está, de uma maneira ou de
    outra, em relação com cada elemento e, de outro lado, que essas relações
    formam, na própria realidade objetiva, correlações concretas, conjuntos,
    unidades, ligados entre si de maneiras completamente diversas, mas sempre
    determinadas \cite[p. 240]{Lukacs1967}.

  \end{citacao}
  
  O estudo da sociedade da informação através da ótica marxista busca, acima de
  tudo, a identificação, especificação e análise destas articulações.
  Obviamente, não é possível para este estudo abranger toda uma análise desta
  natureza, já que o momento de nossa sociedade abrange relações
  suficientemente complexas de totalidades, e que não são passíveis de um
  estudo breve. Se quer, somente, identificar algumas particularidades das
  articulações destas totalidades sem desconsiderar uma implicação completa e
  mais abrangente.

  Isto quer dizer -- em caráter essencial para a análise marxista -- que é
  importante considerar que a totalidade em que o software livre está inserido
  não pode ser tomado com o mesmo peso das outras totalidades que compõem as
  relações da sociedade.

  Assim, entende-se que as perspectivas marxistas são de razoável utilidade
  para a análise da estrutura econômica e social proposta pelo software livre,
  e que a correlação desta teoria com a prática podem vir a mostrar visões
  esclarecedoras sobre o que se entende pela liberdade de conhecimento, e quais
  as mudanças possíveis na sociedade a partir da implementação de políticas que
  valorizem iniciativas relacionadas à cultura da livre informação.

\section{O marxismo na era da informação}

  Quando adentramos em discussões sobre tecnologia e sociedade, é comum
  depararmo-nos com discussões que decretam a decadência do valor teórico
  deixado por Karl Marx perante a vitória do capitalismo com sua nova estrutura
  tecnológica. Nos dizem que a alta tecnologia está levando o planeta a um novo
  estágio civilizatório caracterizado pela onipresença de computadores e
  sistemas de comunicação em uma economia baseada em conhecimento e suportada
  pela crescente inovação científica. Em meio a tudo isso, os socialistas são
  tidos como ultrapassados, ingênuos e utopistas, e esta é de fato a impressão
  geral que se constrói no imaginário coletivo quando tenta-se introduzir
  conceitos como mais-valia em meio à máquinas inteligentes, superestrutura
  quando boa parte do que se é construído e trabalhado encontra-se somente em
  estado simbólico, ou mesmo em uma ditadura do proletariado sobre todo o
  mercado de mídia eletrônica que nunca cessa seu crescimento
  \cite{DyerWitheford2004}.

  Estas características em relação aos socialistas não são casuais. Os devidos
  comunicadores encarregaram-se de formar esta imagem, encontrando na história
  os elementos apropriados que demonstrassem a falência socialista e o
  sucesso capitalista. Assistimos os Estados Unidos da América, Japão e Europa
  tornarem-se grandes potências mundiais enquanto defendiam seu sistema
  econômico aberto e `democrático'. Por outro lado, `ditaduras' socialistas
  como Rússia e Cuba, ao mesmo tempo que bradavam o marxismo como o modelo
  teórico de governo, também sucumbiam à falência econômica e à miséria. Esta
  ideia por si só já estabelece o `preconceito social' sobre a ideia do
  comunismo, socialismo e marxismo, muitas vezes rechaçado sistematicamente e
  de forma organizada por governos neoliberais. O que se acaba tendo, de uma
  maneira geral, é o impedimento da popularização de conceitos econômicos
  antagônicos aos que estão impostos em favor da alienação e do consumo
  \cite{Giannotti2011}, distanciando cada vez mais um horizonte que quer
  afastar esta espécie de realidade o máximo possível.

  \begin{citacao}

    [\ldots] é preciso continuar ressaltando que [as teses de Marx] não foram
    divulgados pelos canais que nossa Ciência ou nossa Filosofia empregam
    normalmente: de um lado, as universidades, os institutos de pesquisa e as
    publicações especializadas, de outro, a mídia e a moda. \cite[p. 15, grifo
    nosso]{Giannotti2011}.

  \end{citacao}

  O pós-guerra sem grandes crises econômicas dos anos 60 e 70 apresentaram os
  primeiros estudiosos de maneira específica da tecnologia na sociedade, e que
  acabaram por prever um progresso pacífico e sem precedentes de uma nova
  tendência do mundo em ser cada vez mais baseado em conhecimento. A linhagem
  destes autores é tida basicamente como autonomista, e por vezes expressa uma
  oposição explícita ao marxismo, ignorando completamente a tese de
  insustentabilidade do sistema capitalista -- por tantas vezes comprovada nas
  crises financeiras \cite{DyerWitheford2004}.

  O ``espectro que paira sobre o capitalismo'' foi poeticamente descrito por
  Marx como a propriedade autodestrutiva e conflituosa deste sistema econômico
  que paradoxalmente passa a comprometer sua própria estrutura de distribuição
  de produtos e serviços, tendo no livre mercado os ingredientes motivadores
  para que não exista retrocedência de endividamento \cite{Marx1887}, seja ele
  econômico, ambiental ou social.

  A configuração neoliberal de um sistema socioeconômico facilita os processos
  de apropriação das instituições reguladoras do estado pela iniciativa
  privada, caracterizando uma sistemática de apropriação de recursos que
  poderiam ser utilizados de maneira mais ampla frente à sociedade. Tem-se um
  contínuo processo de externalização de financiamentos e custeios que,
  invariavelmente, contribuem para a fragilização do estado e geração de crises
  econômicas quando as possibilidade de socorro aos diferentes setores
  produtivos estão esgotadas, resultando em um comprometimento dos demais
  setores em uma cascata interminável de falências.

  Em um dos exemplares mais recentes deste fenômeno temos as deficiências
  regulatórios do mercado imobiliário nos Estados Unidos. O grande número de
  investidores com aplicações irresponsáveis para com o país gerou uma grande
  bolha especulativa que tornou-se insustentável a partir do momento que atores
  importantes deste cenário deixaram de aplicar seus recursos nas bolsas de
  valores \cite{Wolff2008}, sendo uma espécie de estopim inicial para a crise
  econômica que já se arrasta há alguns anos pelos Estados Unidos e pela
  Europa.

  Interpretações marxistas tendem a ser mais amplas na interpretação das crises
  a ponto de considerar um estopim de crise o mero resultado do sistema em que
  ele opera.  Assume-se que o capitalismo está sempre em um estado estrutural
  extremamente tensionado economicamente, o que tende também a agravar outras
  áreas que comprometem a saúde econômica da sociedade, tal como a ecologia e
  os direitos humanos \cite{Meszaros2010}.

  \begin{citacao}

    A dramática crise financeira que experimentamos nos últimos três anos é
    apenas um aspecto da trifurcada destrutibilidade do sistema de capital. Em
    primeiro lugar na esfera militar, com as intermináveis guerras do capital
    desde o começo do imperialismo monopolista nas décadas finais do século
    XIX, e suas mais devastadoras armas de destruição em massa nos últimos 60
    anos; em segundo na intensificação, pelo óbvio impacto destrutivo do
    capital na ecologia, afetando diretamente e já colocando em risco o
    fundamento natural elementar da própria existência humana; e, em terceiro,
    no domínio da produção material e do desperdício cada vez maior, devido ao
    avanço da ``produção destrutiva'', em lugar da outrora louvada ``destruição
    criativa'' ou ``produtiva''. Esses são os graves problemas sistêmicos de
    nossa crise que só podem ser solucionados por uma completa mudança
    estrutural. \cite{Meszaros2010}.

  \end{citacao}

  Apesar de algumas crises, os anos 60 até os anos 2000 foram uma espécie de
  consolidação estável do capitalismo com sua nova sistemática globalizada de
  poder e processos fordistas. Muitos autores chegaram a acreditar que sua
  estrondosa magnitude representava também sua forma definitiva
  \cite{Dupas2001}, já que sua configuração acabou por provar-se extremamente
  eficaz.

  Em um momento passageiro, quando eclodiu a inesperada crise do petróleo na
  década de 70, houve ânimo para a formulação de mais interpretações
  antagônicas àquelas futuristicamente tecnocráticas que estavam postas, e
  ajudaram a reforçar impressões diferenciadas em relação ao desenvolvimento
  tecnológico.  Porém, seguida a estabilização econômica, pareceu existir uma
  dificuldade do encontro de evidências de que haveria mais cedo ou mais tarde
  uma completa derrocada deste sistema, o que acabou por reforçar as teses de
  autores autonomistas e pós-industrialistas que vieram a estabelecer
  novamente, com o dobro de intensidade, descrições de uma futura sociedade
  tecnocrática bem sucedida \cite{DyerWitheford2004}.

  Embora produções acadêmicas importantes de linhagem marxista tenham ocorrido
  desde a década de 60, as análises mais `pessimistas' reduziam-se somente à
  premonições e mapeamentos de tendências em relação às grandes economias. O
  vigor do capitalismo respaldava também um sentimento de remota possibilidade
  de que crises capazes de afetar o núcleo econômico do mundo ocorreriam,
  tornando baixo o interesse em teorias acerca disso.
  \citeonline{Dupas2001}, por exemplo, discorre em tom incerto por todo um
  capítulo a respeito da tecnologia da informação e a hegemonia dos Estados
  Unidos, sem ainda que houvessem indicativos claros de uma grave crise
  econômica naquele país:

  \begin{citacao}

    Estamos diante do mais longo ciclo de crescimento econômico dos Estados
    Unidos, [\ldots] a questão sobre quando virá o declínio persegue o mundo
    todo e exige novas explicações. [\ldots] Ainda que várias opiniões apontem
    para um ajuste futuro por conta dos desequilíbrios da chamada ``nova
    economia'', a consolidação da hegemonia é tão impressionante que permite a
    metáfora de um enorme e competente polvo, com seus tentáculos fortemente
    agarrados na tecnologia da informação, a alimentar-se dos mercados globais.
    \cite[p. 45, grifo nosso]{Dupas2001}.

  \end{citacao}

  E é neste contexto de tensão e questionamentos que uma forte crise eclode em
  2007, e que mais tarde acomete diferentes países da Europa sucessivamente.
  Novamente estava colocada a evidência da visão marxista sobre a economia, e
  de como se comportava a insustentabilidade do capitalismo \cite{Wolff2008},
  iconizando a figura de Marx mais do que nunca nos tempos modernos.

  \begin{figure}[H]

    \centering
    \frame{\includegraphics[width=0.5\textwidth]{./img/marx-right.jpg}}

    \caption{Marx na Internet: ``Eu avisei que estava certo sobre o
    capitalismo''}

    Ilustração do artista plástico Azlan McLennan proliferada pela Internet
    satirizando as representações do personagem estadunidense Tio Sam, em favor
    da legitimidade da teoria marxista que teria sido reforçada e trazida à
    pauta pela então recente crise dos Estados Unidos em 2007. O reaquecimento
    das discussões sobre as teorias Marxistas influenciaram tanto a produção de
    material acadêmico quanto artístico.

    Fonte: \citeonline[tradução nossa]{McLennan2009}

  \end{figure}

  Analisadas estas perspectivas, e também observando o reforço das discussões
  causadas por estas últimas crises, é interessante observar a sazonalidade
  histórica com que o discurso marxista se intensifica nos debates sobre
  economia política. E nesta lógica a teoria marxista parece ter-se renovado
  e amadurecido aos altos e baixos desde a década de 60, revisando concepções a
  cada nova crise econômica, e obtendo a colaboração de diversas vertentes e
  interpretações políticas à medida que o debate tornava-se mais relevante e
  aumentava a necessidade de entendimento dos diferentes aspectos da nova
  sociedade \cite{DyerWitheford2004}.

  Todas as correntes de estudos que se formaram acerca deste tema construíram
  divergências significativas entre si, especialmente quando adentramos no
  campo da tecnologia. No discurso, uma pauta relevante é o distanciamento dos
  aspectos ortodoxos da teoria para a construção de novos conceitos, e este
  fato talvez seja uma das mais enriquecedoras contribuições para o
  amadurecimento das discussões e o desenvolvimento de uma adaptabilidade dos
  conceitos clássicos à nossa nova sociedade.

  \label{Text:VisaoAutonomista}

  Dentre toda essa discussão desenvolveu-se o intenso discurso autonomista, ou
  ainda o chamado marxismo autonomista. Esta linhagem de pensamento sobre o
  homem remete à sua autonomia enquanto ser e agir, e apesar das lutas de
  classes continuarem a ser um elemento central, o foco não é mais sobre o
  avanço das forças produtivas, mas sim no conflito entre aqueles que criam e
  aqueles que se apropriam. As organizações e programas revolucionários
  tornam-se cada vez mais distantes do estado e dos partidos políticos, e
  passam a ser focadas na mudança que a própria classe operária é capaz de
  promover de maneira independente e incisiva.

  O autonomismo também veio em resposta às novas formas de lutas em que, por
  seus integrantes não possuírem raízes e vivências operárias, possuíam uma
  deficiência -- ou resistência -- no entendimento pleno de uma postura
  anti-capitalista. Estariam amparados aí os movimentos feministas,
  anti-racistas, ambientalistas e grupos reivindicantes de políticas públicas
  urbanas. Trata-se, em outros termos, de uma busca pelo sujeito revolucionário
  de nosso tempo \cite{Gurgel2010} através de uma análise dentre os
  movimentos que demonstram uma profunda insatisfação com a atual estrutura da
  sociedade, mas que também muitas vezes não estão inseridos no sistema
  produtivo sob a plena condição de explorados.

  Embora a visão autonomista possa ser caracterizada pelo abandono de alguns
  fortes conceitos da estrutura do raciocínio marxista
  \cite{DyerWitheford2004}, ela acaba construindo subsídios interessantes para
  analisar a estrutura da comunidade do software livre. As visões
  autonomistas trabalham, por exemplo, sobre fenômenos em que um processo de
  aprendizado coletivo torna-se uma entidade produtiva por si própria
  \cite{Soderberg2002}, e esta é exatamente uma das características mais fortes
  do software livre.  Entende-se que ignorar completamente a importância que a
  informação obteve na sociedade de hoje não é algo prudente a ser feito para
  uma análise que busca abranger as mudanças nas relações sociais em um
  contexto atualizado.  Por este motivo, este trabalho tenta relacionar alguns
  aspectos do movimento autonomista com as situações criadas pela sociedade da
  informação.
