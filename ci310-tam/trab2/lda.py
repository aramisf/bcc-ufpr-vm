#!/usr/bin/python
# coding=utf-8

from __future__ import division
import optparse
import numpy as np

class Lda:

    def parse_file(self, datafile):

        import re

        self.basename = re.sub('\..*$', '', datafile)
        f = open(datafile, 'r')
        m1 = list()
        m2 = list()

        for line in f:
            l = re.sub('^\ *', '', line).split('  ')
            vals = [float(l[0]), float(l[1])]
            if float(l[2]) == 0:
                m1.append(vals)
            else:
                m2.append(vals)

        self.m1 = np.matrix(m1)
        self.m2 = np.matrix(m2)

    def calc_matrix(self):

        m1_sum = np.sum(self.m1, axis = 0)
        m2_sum = np.sum(self.m2, axis = 0)

        self.m1_p = len(self.m1) / (len(self.m1) + len(self.m2))
        self.m2_p = len(self.m2) / (len(self.m2) + len(self.m2))

        self.m1_avg = m1_sum / len(self.m1)
        self.m2_avg = m2_sum / len(self.m2)
        self.mc_avg = (self.m1_avg + self.m2_avg) / 2

        self.m1_norm = self.m1 - self.m1_avg
        self.m2_norm = self.m2 - self.m2_avg

        self.m1_cov = np.cov(self.m1_norm, rowvar = 0)
        self.m2_cov = np.cov(self.m2_norm, rowvar = 0)

        mc_cov = len(self.m1_cov) * [None]
        for i in range(0, len(mc_cov)):
            mc_cov[i] = len(self.m1_cov[0]) * [None]

        for i in range(0, len(self.m1_cov)):
            for j in range(0, len(self.m1_cov[0])):
                mc_cov[i][j] = self.m1_p * self.m1_cov[i][j] \
                    + self.m2_p * self.m2_cov[i][j]

        mc_cov = np.matrix(mc_cov)
        self.mc_inv = mc_cov.getI()

    def calc_fn(self, m):

        m1_trans = np.transpose(self.m1_avg)
        m2_trans = np.transpose(self.m2_avg)

        self.fn = list()
        self.fn_count = 0

        for x in m:
            x = np.matrix(x)
            x = np.transpose(x)

            fn1 = self.m1_avg * self.mc_inv * x \
                - 0.5 * self.m1_avg * m1_trans

            fn2 = self.m2_avg * self.mc_inv * x \
                - 0.5 * self.m1_avg * m2_trans

            f1 = fn1.flat[0]
            f2 = fn2.flat[0]

            c = 0 if f1 >= f2 else 1
            self.fn.append([f1, f2, c])
            self.fn_count += c

        print self.fn_count / len(m)

    def plot_data(self):

        try:
            import matplotlib.pyplot as plt
        except:
            return False

        for m in self.m1:
            plt.plot(m.flat[0], m.flat[1], 'rx')
        for m in self.m2:
            plt.plot(m.flat[0], m.flat[1], 'bx')

        plt.plot(self.m1_avg.flat[0], self.m1_avg.flat[1], 'ro')
        plt.plot(self.m2_avg.flat[0], self.m2_avg.flat[1], 'bo')
        plt.plot(self.mc_avg.flat[0], self.mc_avg.flat[1], 'ko')

        plt.savefig(self.basename + '-avg.png')
        plt.clf()

    def plot_fn(self):

        try:
            import matplotlib.pyplot as plt
        except:
            return False

        for f in lda.fn:
            color = 'r' if f[2] else 'b'
            plt.plot(f[0], f[1], color + 'x')

        plt.savefig(self.basename + '-fn.png')
        plt.clf()

if __name__ == '__main__':

    parser = optparse.OptionParser()
    parser.add_option('--file', dest = '_file', metavar = 'file',
        help = 'arquivo de dados')
    (options, args) = parser.parse_args()

    if not options._file:
        parser.print_help()
        exit(0)

    lda = Lda()
    lda.parse_file(options._file)
    lda.calc_matrix()

    lda.plot_data()

    lda.calc_fn(lda.m1)
    lda.plot_fn()

    lda.calc_fn(lda.m2)
    lda.plot_fn()
