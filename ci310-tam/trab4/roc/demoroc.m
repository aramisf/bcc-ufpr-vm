function [auc, aucch, tpch, fpch, recrate, thres, alltp, allfp, allT] = demoroc(filename, pos, neg)

t = [];
y = [] ;
res = load(filename) ;%

lin = size(res,1) ;

for i = 1 : lin
    if(res(i,1) == -1)
        res(i,1)=0;
    end
end

[probsort, ind] = sort(res(:,2),'descend') ;%

for i = 1:lin
  y(i) = probsort(i);
  t(i) = res(ind(i),1) ;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Old ROC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 ttp = 0;
 ffp = 0;
 xx = [] ;
 yy = [] ;
 previous_y = y(1) ;
 bestrec = 0. ;
 bestT = 0.;  %% melhor T -> fornece a melhor taxa de reconhecimento
 bestX = 0.;  %% melhor X -> usado para plotar o valor no grafo
 bestY = 0. ;
 rec = 0 ;

 for i = 1:lin
     if(t(i)>0)
         ttp = ttp+1 ;
         %fprintf(1, 'POS %d\n', i) ;
     else
         ffp = ffp+1 ;
         %fprintf(1, '--NEG %d\n', i) ;
     end

     xx(i) =  ffp/neg ;
     yy(i) = ttp/pos ;

     if(previous_y ~= y(i))
         rec = ((yy(i) * pos) + (neg -(xx(i)*neg)))/lin;
         %fprintf(1, 'T = %d, Rate %f (%.2f %.2f)\n', previous_y, rec, xx(i), yy(i) ) ;
         previous_y = y(i) ;
     end

     if( bestrec < rec)
         bestrec = rec ;
         bestT = previous_y ;
         bestY = yy(i) ;
         bestX = xx(i) ;
     end

 end

recrate = bestrec ;
thres = bestT ;
alltp = yy ;
allfp = xx ;
allT = y ;

y = y' ;
t = t' ;

% generate test data from Fawcett [1] (fig 3)

%fprintf(1, 'generating test data...\n');

%t = [1 1 0 1 1 1 0 0 1 0 1 0 1 0 0 0 1 0 1 0]';
%y = [.9  .8  .7  .6  .55 .54 .53 .52 .51 .505 ...
%     .4  .39 .38 .37 .36 .35 .34 .33 .3 .1]';





% generate an ROC curve and plot it

%fprintf(1, 'plotting ROC curve...\n');

[tp,fp] = roc2(t,y);

%figure(1);
hold on;
plot(fp,tp, 'b');

%% imprime o ponto operacional que da a melhor taxa de rec
plot( bestX, bestY, 'ro') ;

% compute the area under the ROC
auc = auroc(tp,fp) ;
%fprintf(1, 'AUROC   = %f\n', auc);

% compute the ROC convex hull (ROCCH) curve

if 1

   %fprintf(1, 'plotting ROCCH curve...\n');%

   [tp,fp] = rocch(t,y);

   %plot(fp, tp, 'k-');
   %xlabel('false positive rate');
   %ylabel('true positive rate');
   %title('ROC and ROCCH curve');
   %compute the area under the ROCCH

   tpch = tp ;
   fpch = fp ;
   aucch =  auroc(tp,fp) ;
   %fprintf(1, 'AUROCCH = %f\n', aucch);

end
% bye
