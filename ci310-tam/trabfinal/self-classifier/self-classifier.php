<?php
/*
 * Plugin Name: Self Classifier
 * Plugin URI: http://vinicius.soylocoporti.org/self-classifier-wordpress-plugin/
 * Description: Nothing yet.
 * Version: 0.1
 * Author: Vinicius Massuchetto
 * Author URI: http://vinicius.soylocoporti.org.br
 * License: GPL2
 */

/* Debugging */
ini_set('display_errors', 1);

define ('SC_SLUG', 'self-classifier');
define ('SC_URL', plugins_url() .'/'. SC_SLUG .'/');
define ('SC_TRAINING_CAT', 1);
define ('SC_TESTING_CAT', 4);
define ('SC_DB_OVERHEAD', true);

$sc_cats_opts = array(
    'type' => 'post',
    'taxonomy' => 'category',
    'exclude' => (SC_TESTING_CAT ? SC_TESTING_CAT : 0));

$sc_opts = array(
    'max_word_hist' => 100,
    'max_verb_hist' => 80,
    'max_pron_hist' => 60,
    'min_words' => 1);

add_action('admin_init', 'sc_init');
function sc_init() {
    $f = '/' . dirname(plugin_basename(__FILE__)) . '/lang/' . WPLANG . '.mo';
    if (is_file($f))
        load_textdomain('sc', $f);
    wp_enqueue_script('jquery');
}

add_action('admin_menu', 'sc_add_admin_menu');
function sc_add_admin_menu() {
    add_options_page(
        __('Self Classifier', 'sc'),
        __('Self Classifier', 'sc'),
        'administrator',
        'sc_panel_config',
        'sc_panel_config');
}

function sc_panel_config(){
    global $sc_cats_opts;
    $cats = get_categories();
    ?>
    <script src="<?php echo SC_URL . SC_SLUG; ?>.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo SC_URL . SC_SLUG; ?>.css" />
    <div class="wrap">
        <div class="icon32" id="icon-options-general"></div>
        <h2><?php _e('Self Classifier', 'sc'); ?></h2>
        <p>
            <input type="button" class="button" id="sc_classify_all" value="<?php _e('Check Success Rate', 'sc'); ?>" />
            <input type="button" class="button" id="sc_build_all" value="<?php _e('Build all indexes', 'sc'); ?>" />
            <input type="button" class="button" id="sc_delete_all" value="<?php _e('Delete all indexes', 'sc'); ?>" />
        </p>
        <div id="categories">
            <p><b><?php _e('Click to classify', 'sc'); ?>:</b>&nbsp;
            <?php foreach ($cats as $c) : ?>
                <a href="javascript:void(0);" class="category_classify" id="<?php echo $c->term_id; ?>">
                    <?php echo $c->name; ?>
                </a>
            <?php endforeach; ?>
            </p>
        </div>
        <p id="sc_result"></p>
    </div>
    <?php
}

add_action('add_meta_boxes', 'sc_add_metaboxes');
function sc_add_metaboxes() {
    add_meta_box('sc_panel_post', __('Self Classifier', 'sc' ), 'sc_panel_post', 'post');
}

function sc_panel_post() {
    ?>
    <script src="<?php echo SC_URL . SC_SLUG; ?>.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo SC_URL . SC_SLUG; ?>.css" />
    <input class="button" id="sc_classify" type="button" name="sc_classify" value="<?php _e('Classify', 'sc'); ?>" />
    <div id="sc_result"></div>
    <?php
}

add_action('admin_init', 'sc_catch_ajax');
function sc_catch_ajax() {

    global $sc_opts;

    if (!isset($_POST['sc_action']))
        return;

    if ($_POST['sc_action'] == 'build_all') {
        sc_build_all();
        exit(0);
    }

    if ($_POST['sc_action'] == 'delete_all') {
        sc_delete_all();
        exit(0);
    }

    if ($_POST['sc_action'] == 'category_classify' && $_POST['sc_category']) {
        if ($min_words = intval($_POST['sc_min_words']))
            $sc_opts['min_words'] = $min_words;
        sc_classify_category($_POST['sc_category']);
        exit(0);
    }

    if ($_POST['sc_action'] == 'classify' && strlen($_POST['sc_content']) > 0) {
        sc_classify_post($_POST['sc_content']);
        exit(0);
    }

    exit(1);
}

function sc_delete_all() {
    global $wpdb;
    $qs = array("
        DELETE
        FROM `$wpdb->postmeta`
        WHERE
            meta_key = '_sc_post_word_histogram' OR
            meta_key = '_sc_post_verb_histogram' OR
            meta_key = '_sc_post_pron_histogram'","
        DELETE
        FROM `$wpdb->options`
        WHERE
            option_name LIKE '_sc_cat_word_histogram_%' OR
            option_name LIKE '_sc_cat_verb_histogram_%' OR
            option_name LIKE '_sc_cat_pron_histogram_%'");
    $total = 0;
    foreach ($qs as $q)
        $total += $wpdb->query($q);
    _e('Deleted all metadata in the database. You can build everything
        again by clicking on "Build all indexes"', 'sc');
}

function sc_normalize_text($text) {
    $text = strip_tags($text);
    $text = remove_accents($text);
    $text = strtolower($text);
    $text = preg_replace('/[^a-z ]/', ' ', $text);
    return $text;
}

function sc_get_word_list($type, $nopreprocessing = false) {
    $file = ABSPATH . 'wp-content/plugins/' . SC_SLUG . '/wordlists/' . WPLANG . '.' . $type;
    if (!file_exists($file))
        return false;

    $file = file_get_contents($file);
    preg_match_all('/^[^#\n]+$/m', $file, $matches);

    if ($nopreprocessing)
        return $matches[0];

    foreach ($matches[0] as $w)
        $words[] = sc_normalize_text($w);
    return $words;
}

function sc_exclude_words($text) {
    $words = sc_get_word_list('exclude');
    foreach ($words as $w)
        $text = preg_replace('/ '.$w.' /', ' ', $text);
    return preg_replace('/\ +/', ' ', $text);
}

function sc_get_verb_list() {
    $words = sc_get_word_list('verbs', $nopreprocessing = true);
    $verbs = array();
    $pos = -1;
    foreach ($words as $w) {
        if ($w[0] == "@") {
            $pos++;
            $w = str_replace('@ ', '', $w);
        }
        $verbs[$pos][] = $w;
    }
    return $verbs;
}

function sc_get_pron_list() {
    return sc_get_word_list('prons', $nopreprocessing = true);
}

function sc_get_text_pron_histogram($text) {

    $words = sc_get_text_word_histogram($text,
        $normalize = true, $exclude = false);

    global $prons;
    if (!count($prons))
        $prons = sc_get_pron_list();

    $prons_histogram = array();
    foreach ($words as $wk => $wv) {
        if (in_array($wk, $prons))
            $prons_histogram[$wk] = $wv;
    }

    return $prons_histogram;
}

function sc_get_text_verb_histogram($text) {

    $histogram = sc_get_text_word_histogram($text,
        $normalize = false, $exclude = false);

    global $verbs;
    if (!count($verbs))
        $verbs = sc_get_verb_list();

    $verbs_histogram = array();
    foreach ($histogram as $k => $v) {
        foreach ($verbs as $v) {
            if (in_array($k, $v)) {
                $verbs_histogram[$v[0]] =
                    $verbs_histogram[$v[0]] + $histogram[$k];
            }
        }
    }

    return $verbs_histogram;
}

function sc_get_text_word_histogram(
    $text, $normalize = true, $exclude = true) {

    if ($normalize)
        $text = sc_normalize_text($text);
    else
        $text = strtolower($text);

    if ($exclude)
        $text = sc_exclude_words($text);

    $text = explode(' ', $text);

    $histogram = array();
    foreach ($text as $word)
        $histogram[$word]++;

    $histogram = sc_histogram_normalize($histogram, 'word');
    return $histogram;
}

function sc_get_post_word_histogram($post_id, $force_update = false) {
    if (!$force_update &&
        $hist = get_post_meta($post_id, '_sc_post_word_histogram', 1))
        return $hist;

    $p = get_post($i = $post_id);
    $hist = sc_get_text_word_histogram(strip_tags($p->post_content));
    $hist = sc_histogram_normalize($hist, 'word');
    update_post_meta($post_id, '_sc_post_word_histogram', $hist);
    return $hist;
}

function sc_get_post_verb_histogram($post_id, $force_update = false) {
    if (!$force_update &&
        $hist = get_post_meta($post_id, '_sc_post_verb_histogram', 1))
        return $hist;

    $p = get_post($i = $post_id);
    $hist = sc_get_text_verb_histogram($p->post_content);
    $hist = sc_histogram_normalize($hist, 'verb');
    update_post_meta($post_id, '_sc_post_verb_histogram', $hist);
    return $hist;
}

function sc_get_post_pron_histogram($post_id, $force_update = false) {
    if (!$force_update &&
        $hist = get_post_meta($post_id, '_sc_post_pron_histogram', 1))
        return $hist;

    $p = get_post($i = $post_id);
    $hist = sc_get_text_pron_histogram($p->post_content);
    $hist = sc_histogram_normalize($hist, 'pron');
    update_post_meta($post_id, '_sc_post_pron_histogram', $hist);
    return $hist;
}

add_action('save_post', 'sc_save_post');
function sc_save_post($post_id) {
    sc_get_post_word_histogram($post_id, true);
    sc_get_post_verb_histogram($post_id, true);
    sc_get_post_pron_histogram($post_id, true);
}

function sc_histogram_normalize($hist, $mode = 'word') {
    global $sc_opts;
    $max = max($hist);
    foreach ($hist as $k => $v)
        $hist[$k] = ($max > 0) ?
            intval($sc_opts['max_' . $mode . '_hist'] * $v / $max)
            : 0;
    return $hist;
}

function sc_histogram_sum($hist1, $hist2) {
    foreach ($hist2 as $k => $v)
        $hist1[$k] += $v;
    return $hist1;
}

function sc_get_category_word_histogram($cat_id, $force_update = false) {
    if (!$force_update &&
        $hist = get_option('_sc_cat_word_histogram_' . $cat_id))
        return $hist;

    $posts_opts = array(
        'numberposts' => -1,
        'category__in' => $cat_id,
        'category__not_in' => (SC_TESTING_CAT ? SC_TESTING_CAT : 0));
    $posts = get_posts($posts_opts);

    $hist = array();
    foreach($posts as $p) {
        $h = sc_get_post_word_histogram($p->ID);
        $hist = sc_histogram_sum($hist, $h);
    }

    $hist = sc_histogram_normalize($hist, 'word');
    update_option('_sc_cat_word_histogram_' . $cat_id, $hist);
    return $hist;
}

function sc_get_category_pron_histogram($cat_id, $force_update = false) {
    if (!$force_update &&
        $hist = get_option('_sc_cat_pron_histogram_' . $cat_id))
        return $hist;

    $posts_opts = array(
        'numberposts' => -1,
        'category__in' => $cat_id,
        'category__not_in' => (SC_TESTING_CAT ? SC_TESTING_CAT : 0));
    $posts = get_posts($posts_opts);

    $hist = array();
    foreach($posts as $p) {
        $h = sc_get_post_pron_histogram($p->ID);
        $hist = sc_histogram_sum($hist, $h);
    }

    $hist = sc_histogram_normalize($hist, 'pron');
    update_option('_sc_cat_pron_histogram_' . $cat_id, $hist);
    return $hist;
}

function sc_get_category_verb_histogram($cat_id, $force_update = false) {
    if (!force_update &&
        $hist = get_option('_sc_cat_verb_histogram_' . $cat_id))
        return $hist;

    $posts_opts = array(
        'numberposts' => -1,
        'category__in' => $cat_id,
        'category__not_in' => (SC_TESTING_CAT ? SC_TESTING_CAT : 0));
    $posts = get_posts($posts_opts);

    $hist = array();
    foreach($posts as $p) {
        $h = sc_get_post_verb_histogram($p->ID);
        $hist = sc_histogram_sum($hist, $h);
    }

    $hist = sc_histogram_normalize($hist, 'verb');
    update_option('_sc_cat_verb_histogram_' . $cat_id, $hist);
    return $hist;
}

function sc_build_all() {
    global $sc_cats_opts;

    $total_size = 0;
    $total_words = 0;

    $cats = get_categories($sc_cats_opts);
    ?>
    <table>
    <tr>
        <td><?php _e('Category', 'sc'); ?></td>
        <td align="right"><?php _e('Words', 'sc'); ?></td>
        <td align="right"><?php _e('Size', 'sc'); ?></td>
    </tr>
    <?php
    foreach ($cats as $c) {
        if (!$c->count)
            continue;

        $hist_word = sc_get_category_word_histogram(
            $c->term_id, $force_update = true);
        $hist_verb = sc_get_category_verb_histogram(
            $c->term_id, $force_update = true);
        $hist_pron = sc_get_category_pron_histogram(
            $c->term_id, $force_update = true);

        $size =
            strlen(serialize($hist_word)) +
            strlen(serialize($hist_verb)) +
            strlen(serialize($hist_pron));
        $total_size += $size;

        $words =
            count($hist_word) +
            count($hist_verb) +
            count($hist_pron);
        $total_words += $words;

        ?>
        <tr>
        <td><b><?php echo $c->name; ?></b></td>&nbsp;
        <td align="right"><?php echo $words; ?></td>
        <td align="right"><?php echo $size/1000 . 'k'; ?></td>
        </tr>
        <?php
    }
    ?>
    <tr>
    <td><b><?php _e('Total', 'sc'); ?></b></td>
    <td align="right"><?php echo $total_words; ?></td>
    <td align="right"><?php echo $total_size/1000 . 'k'; ?></td>
    </tr>
    </table>
    <?php
}

function sc_histogram_word_distance($hist1, $hist2) {
    global $sc_opts;
    $min = $sc_opts['min_words'];

    $r = 0;
    foreach ($hist1 as $k1 => $v1) {
        foreach ($hist2 as $k2 => $v2) {

            if ($k1 != $k2 || $v1 < $min || $v2 < $min)
                continue;

            $v = $v1 + $v2;
            $r += $v;

        }
    }
    return $r;
}

function sc_cat_usort($a, $b) {
    if ($a->histogram_word_distance == $b->histogram_word_distance)
        return 0;
    return ($a->histogram_word_distance < $b->histogram_word_distance)
        ? 1 : -1;
}

function sc_classify_post_get($post) {

    if (is_object($post)) {
        $hist_word = sc_get_post_word_histogram($post->ID);
        $hist_verb = sc_get_post_verb_histogram($post->ID);
        $hist_pron = sc_get_post_pron_histogram($post->ID);

    } elseif (is_string($post)) {
        $hist_word = sc_get_text_word_histogram($post);
        $hist_verb = sc_get_text_verb_histogram($post);
        $hist_pron = sc_get_text_pron_histogram($post);
    }

    $cats_result = array();
    $cats = get_categories(array(
        'exclude' => array(SC_TRAINING_CAT, SC_TESTING_CAT)));
    foreach ($cats as $c) {

        $h = false;
        if ($h = get_option('_sc_cat_word_histogram_' . $c->term_id, 0))
            $c->histogram_word_distance =
                sc_histogram_word_distance($hist_word, $h);

        $h = false;
        if ($h = get_option('_sc_cat_verb_histogram_' . $c->term_id, 0))
            $c->histogram_verb_distance =
                sc_histogram_word_distance($hist_verb, $h);

        $h = false;
        if ($h = get_option('_sc_cat_pron_histogram_' . $c->term_id, 0))
            $c->histogram_pron_distance =
                sc_histogram_word_distance($hist_pron, $h);

        $cats_result[] = $c;

    }



    $cats_result = sc_classify_post_result ($cats_result);
    return $cats_result;
}

function sc_classify_post_result ($c) {

    // Ordena pelo score de palavras gerais.

    usort($c, 'sc_cat_usort');

    // Calcula certeza da decisão

    foreach ($c as $e) $e->certainty = '';
    $d1 = ($c[0]->histogram_word_distance - $c[1]->histogram_word_distance);
    $d2 = ($c[0]->histogram_word_distance - $c[4]->histogram_word_distance);
    $c[0]->certainty = intval($d1 * 100 / $d2);

    // Se o resultado geral de TOP1 tem certeza menor do que 10%,
    // então considera-se o resultado dos verbos para TOP2.

    if ($c[0]->certainty <= 10)
        if ($c[1]->histogram_verb_distance < $c[0]->histogram_verb_distance)
            list ($c[0], $c[1]) = array ($c[1], $c[0]);

    return $c;

}

function sc_classify_post($content) {
    $cats = sc_classify_post_get($content);
    ?>
    <p><b><?php _e('Similarity order', 'sc'); ?></b></p>
    <table>

        <thead>
            <tr>
                <td><?php _e('Category', 'sc'); ?></td>
                <td><?php _e('General Words', 'sc'); ?></td>
                <td><?php _e('Popular Verbs', 'sc'); ?></td>
                <td><?php _e('Pronouns', 'sc'); ?></td>
                <td><?php _e('Certainty', 'sc'); ?></td>
            </tr>
        </thead>

        <?php foreach ($cats as $c) : ?>
            <?php if (!$c->histogram_word_distance) continue; ?>
            <tr>
                <td><?php echo $c->name; ?></td>
                <td align="right"><?php echo $c->histogram_word_distance; ?></td>
                <td align="right"><?php echo $c->histogram_verb_distance; ?></td>
                <td align="right"><?php echo $c->histogram_pron_distance; ?></td>
                <td align="right"><?php echo $c->certainty; ?></td>
            </tr>
        <?php endforeach; ?>

    </table>
    <?php
}

function sc_classify_category($cat_id) {
    $posts = get_posts(array(
        'numberposts' => -1,
        'category__and' => array($cat_id, SC_TESTING_CAT)));

    $match = 0;
    $total = 0;

    ?>
    <table>
    <?php

    foreach ($posts as $p) {
        unset ($cats);
        $cats = sc_classify_post_get($p);
        $ok = false;

        $total++;
        if (in_category($cats[0]->term_id, $p->ID)) {
            $match++;
            $status = 'OK';
        } else {
            $status = 'FAIL';
        }

        $real_cats = array();
        $real_cat_ids = wp_get_post_categories($p->ID);
        foreach ($real_cat_ids as $id) {
            $c = get_category($id);
            $real_cats[] = $c->name;
        }
        ?>
        <tr>
            <td class="<?php echo strtolower($status); ?>"><?php echo $status; ?></td>
            <td><a href="<?php echo get_permalink($p->ID); ?>"><?php echo $p->post_title; ?></a></td>
            <td>
                <a href="javascript:void(0);" class="view_scores"><?php _e('Scores', 'sc'); ?></a>
                <table class="scores">
                    <thead>
                        <tr>
                            <td><?php _e('Category', 'sc'); ?></td>
                            <td><?php _e('Words', 'sc'); ?></td>
                            <td><?php _e('Verbs', 'sc'); ?></td>
                            <td><?php _e('Pronouns', 'sc'); ?></td>
                            <td><?php _e('Certainty', 'sc'); ?></td>
                        </tr>
                    </thead>
                    <?php foreach ($cats as $c) : ?>
                        <?php if (!$c->histogram_word_distance) continue; ?>
                        <tr>
                            <td><?php echo $c->name;?></td>
                            <td><?php echo $c->histogram_word_distance; ?></td>
                            <td><?php echo $c->histogram_verb_distance; ?></td>
                            <td><?php echo $c->histogram_pron_distance; ?></td>
                            <td><?php echo $c->certainty; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </td>
            <td><small><?php echo implode(', ', $real_cats); ?></small></td>
        </tr>
        <?php
    }
    ?>
    <tr class="total">
        <td colspan="4">
            <?php _e('Success rate:', 'sc'); ?>&nbsp;
            <?php echo ($total > 0) ?
                floor(($match/$total * 100)) . '%' :
                __('None', 'sc'); ?>
        </td>
    </tr>
    </table>
    <?php
}
?>
