
if (!$)
    $ = jQuery.noConflict();

function loading_start(element) {
    if (element.parent().find('.sc_loading').length > 0)
        return;
    var loading = '<div class="sc_loading"></div>';
    element.parent().append(loading);
}

function loading_stop(element) {
    $('.sc_loading').remove();
}

$(document).ready(function(){

    $('#sc_classify').click(function(){
        var content = $('#editorcontainer textarea').val();
        $.post(location.href, {
            'sc_action': 'classify',
            'sc_content': content
        }, function(data) {
            $('#sc_result').html(data);
        });
    });

    $('#sc_build_all').click(function(){
        loading_start($(this));
        $.post(location.href, {
            'sc_action': 'build_all'
        }, function(data) {
            loading_stop($(this));
            $('#sc_result').html(data);
        });
    });

    $('#sc_delete_all').click(function(){
        loading_start($(this));
        $.post(location.href, {
            'sc_action': 'delete_all'
        }, function(data) {
            loading_stop($(this));
            $('#sc_result').html(data);
        });
    });

    $('#sc_classify_all').click(function(){
        $('#categories').slideDown();
    });

    $('.category_classify').click(function(){
        loading_start($(this));
        var cat_id = $(this).attr('id');
        var min_words = $('#sc_min_words').val();
        $.post(location.href, {
            'sc_action': 'category_classify',
            'sc_min_words': min_words,
            'sc_category': cat_id
        }, function(data) {
            loading_stop($(this));
            $('#sc_result').html(data);
        });
    });

    $('.view_scores').live('click', function(){
        $(this).parent().find('table.scores').slideDown();
    });

});
