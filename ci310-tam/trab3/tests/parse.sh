#!/bin/bash

if [ ! $1 ] ; then
    exit 0
fi

PREDICT_FILE=$1
TEST_FILE="../data/teste.vet"

COUNT=0

VAL_m1m1=0
VAL_m1M1=0
VAL_M1m1=0
VAL_M1M1=0

for K in `cat $PREDICT_FILE | cut -d' ' -f1`; do
    let COUNT++

    VAL=`sed -n ${COUNT}p ${TEST_FILE} \
        | cut -d' ' -f1`

    I=$((VAL+0))
    J=$((K+0))

    if [ $I == "-1" -a $J == "-1" ] ; then
        let VAL_m1m1++
        continue
    fi

    if [ $I == "-1" -a $J == "1" ] ; then
        let VAL_m1M1++
        continue
    fi

    if [ $I == "1" -a $J == "-1" ] ; then
        let VAL_M1m1++
        continue
    fi

    if [ $I == "1" -a $J == "1" ] ; then
        let VAL_M1M1++
        continue
    fi

done

echo "    -1    1"
echo "-1  $VAL_m1m1  $VAL_m1M1"
echo " 1  $VAL_M1m1  $VAL_M1M1"
