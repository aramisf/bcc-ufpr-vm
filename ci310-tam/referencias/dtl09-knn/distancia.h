//
//  distancia.h
//  kNN
//
//  Created by Diego Trevisan Lara on 08/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface distancia : NSObject

@property NSUInteger classe;
@property float distancia;

+ (distancia*)distanciaDe:(float)dist eClasse:(NSUInteger)classe;

@end
