//
//  kNNViewController.h
//  kNN
//
//  Created by Diego Trevisan Lara on 08/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "matriz.h"
#import "distancia.h"

@interface kNNViewController : UIViewController
{
    matriz *m1, *m2;
    NSUInteger altura, largura, valorK;
    NSMutableArray *vetorDistancias;
    
    IBOutlet UILabel *lblAcertos, *lblK;
    IBOutlet UIActivityIndicatorView *actV;
    IBOutlet UIProgressView *progV;
}

- (IBAction)verificaIgualdades;
- (IBAction)alteraK:(id)sender;

@end
