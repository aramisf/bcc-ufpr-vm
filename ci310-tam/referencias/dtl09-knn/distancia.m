//
//  distancia.m
//  kNN
//
//  Created by Diego Trevisan Lara on 08/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "distancia.h"

@implementation distancia
@synthesize distancia, classe;

+ (distancia*)distanciaDe:(float)dist eClasse:(NSUInteger)classe
{
    distancia *d = [[distancia alloc] autorelease];
    [d setDistancia:dist];
    [d setClasse:classe];
    return d;
}

@end
