//
//  matriz.m
//  kNN
//
//  Created by Diego Trevisan Lara on 08/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "matriz.h"

@implementation matriz
@synthesize matrix, largura, altura;

- (void)inicializaComLargura:(NSUInteger)width eAltura:(NSUInteger)height
{
    largura = width;
    altura = height;
    matrix = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (int i = 0; i < height; i++)
    {
        NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
        
        for (int j = 0; j < width; j++)
            [array addObject:[NSNumber numberWithFloat:0]];
        
        [matrix addObject:array];
        [array release];
    }
}

- (void)setaValor:(float)value naColuna:(NSUInteger)col eLinha:(NSUInteger)lin
{
    NSMutableArray *arr = [matrix objectAtIndex:lin];
    [arr replaceObjectAtIndex:col withObject:[NSNumber numberWithFloat:value]];
}

- (float)valorDaColuna:(NSUInteger)col eLinha:(NSUInteger)lin
{
    NSMutableArray *arr = [matrix objectAtIndex:lin];
    return [[arr objectAtIndex:col] floatValue];
}

- (void)imprime
{
    NSString *linha = @"";

    for (int i = 0; i < altura; i++)
    {
        for (int j = 0; j < largura; j++)
        {
            NSArray *arr = [matrix objectAtIndex:i];
            NSNumber *num = [arr objectAtIndex:j];
            //NSLog(@"%f ", [num floatValue]);
            linha = [NSString stringWithFormat:@"%@ %f", linha, [num floatValue]];
        }
        NSLog(@"%@", linha);
        linha = @"";
    }
}

- (void)meMata
{
    [matrix removeAllObjects];
    [matrix release];
}

@end
