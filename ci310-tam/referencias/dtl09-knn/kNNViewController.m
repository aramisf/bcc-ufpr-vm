//
//  kNNViewController.m
//  kNN
//
//  Created by Diego Trevisan Lara on 08/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "kNNViewController.h"

@implementation kNNViewController

#pragma mark - Principal

- (void)carregaTraining
{
    NSString *strTraining = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"training" ofType:nil] usedEncoding:nil error:nil];
    
    altura = [[strTraining componentsSeparatedByString:@"\n"] count];
    largura = [[[[strTraining componentsSeparatedByString:@"\n"] objectAtIndex:0] componentsSeparatedByString:@" "] count];
    
    m1 = [[matriz alloc] init];
    [m1 inicializaComLargura:largura eAltura:altura];
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[strTraining componentsSeparatedByString:@"\n"]];
    
    for (int i = 0; i < altura; i++)
    {
        NSArray *array2 = [[array objectAtIndex:i] componentsSeparatedByString:@" "];
        for (int j = 0; j < largura; j++)
        {
            float num = [[array2 objectAtIndex:j] floatValue];
            [m1 setaValor:num naColuna:j eLinha:i];
        }
    }
    
    [array release];
}

- (void)carregaTesting
{
    NSString *strTraining = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"testing" ofType:nil] usedEncoding:nil error:nil];
    
    m2 = [[matriz alloc] init];
    [m2 inicializaComLargura:largura eAltura:altura];
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[strTraining componentsSeparatedByString:@"\n"]];
    
    for (int i = 0; i < altura; i++)
    {
        NSArray *array2 = [[array objectAtIndex:i] componentsSeparatedByString:@" "];
        for (int j = 0; j < largura; j++)
        {
            float num = [[array2 objectAtIndex:j] floatValue];
            [m2 setaValor:num naColuna:j eLinha:i];
        }
    }
    
    [array release];
}

- (float)distanciaDaLinha:(NSUInteger)lm1 comLinha:(NSUInteger)lm2
{
    float soma = 0;
    
    for (int i = 0; i < largura - 1; i++)
    {
        float valM1 = [m1 valorDaColuna:i eLinha:lm1];
        float valM2 = [m2 valorDaColuna:i eLinha:lm2];
        soma = soma + (float)pow(valM1 - valM2, 2);
    }
    
    return soma;
}

- (void)imprimeVetorDeDistancias:(NSArray*)vetor
{
    for (int i = 0; i < [vetor count]; i++)
    {
        NSLog(@"distancia: %f", [[vetor objectAtIndex:i] distancia]);
    }
    NSLog(@"fim");
}

- (void)vetor:(NSMutableArray*)arr addOrdenado:(distancia*)dist
{
    if ([arr count] == 0)
        [arr addObject:dist];
    
    else
    {
        float distancia = [dist distancia];
        for (int i = 0; i < [arr count]; i++)
        {
            float d = [[arr objectAtIndex:i] distancia];
            if (distancia < d)
            {
                [arr insertObject:dist atIndex:i];
                return;
            }
        }
        [arr addObject:dist];
    }
}

- (BOOL)classeDaLinha:(NSUInteger)linha eK:(NSUInteger)k
{
    NSUInteger classeQueDeveSer = (NSUInteger)[m2 valorDaColuna:largura-1 eLinha:linha];
    
    NSArray *vetorDistanciaLinha = [vetorDistancias objectAtIndex:linha];
    int vetorOcorrencias[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    for (int i = 0; i < k; i++)
    {
        NSUInteger classe = [[vetorDistanciaLinha objectAtIndex:i] classe];
        vetorOcorrencias[classe]++;
    }
    
    NSInteger maior = -1;
    NSInteger indiceDoMaior = -1;
    for (int i = 0; i < 10; i++)
    {
        if (vetorOcorrencias[i] > maior)
        {
            maior = vetorOcorrencias[i];
            indiceDoMaior = i;
        }
    }
    
    if (indiceDoMaior == classeQueDeveSer)
        return YES;
    else
        return NO;
}

- (IBAction)verificaIgualdades
{    
    NSUInteger acertos = 0;
    
    for (int i = 0; i < altura; i++)
        if ([self classeDaLinha:i eK:valorK])
            acertos++;
    
    [lblAcertos setText:[NSString stringWithFormat:@"Acertos: %i/%i", acertos, altura]];
}

- (void)calculaDistancias
{
    vetorDistancias = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (int linha2 = 0; linha2 < altura; linha2++)
    {
        NSMutableArray *vetor = [[NSMutableArray alloc] initWithCapacity:0];
        NSLog(@"Processando %i", linha2);
        [progV setProgress:(float)linha2/altura];
        for (int linha1 = 0; linha1 < altura; linha1++)
        {
            NSAutoreleasePool *p = [[NSAutoreleasePool alloc] init];
            
            [self vetor:vetor addOrdenado:[distancia distanciaDe:[self distanciaDaLinha:linha1 comLinha:linha2] eClasse:[m1 valorDaColuna:largura-1 eLinha:linha1]]];
            
            [p drain]; //Ufa
        }
        
        [vetorDistancias addObject:vetor];
        [vetor release];
    }
}

- (void)dispara
{
    [self carregaTraining];
    [self carregaTesting];
    [self calculaDistancias];
    [self verificaIgualdades];
    [actV stopAnimating]; //Faz a bolinha de "carregando" parar de girar
}

- (void)viewDidLoad
{
    valorK = 3;
    [self performSelector:@selector(dispara) withObject:nil afterDelay:0.1];
    [super viewDidLoad];
}

#pragma mark - Elementos Visuais

- (IBAction)alteraK:(id)sender
{
    if (![sender tag])
    {
        if (valorK > 1)
            valorK--;
    }
    else
        valorK++;
    
    [lblK setText:[NSString stringWithFormat:@"K = %i", valorK]];
}

#pragma mark - Memória

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

#pragma mark - Orientação

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
