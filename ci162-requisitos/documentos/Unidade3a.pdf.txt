Unidade 3: Especificação de Requisitos (Parte a)

1

3.1. Casos de uso
3.1.1. Conceitos básicos e parâmetros de descrição
Os casos de uso dão conta da maioria dos requisitos de um sistema computacional.
Definição: Um caso de uso é a especificação de uma sequência completa de interações entre um
sistema e um ou mais agentes externos.
A representação gráfica de um caso de uso é uma elipse rotulada pelo verbo que melhor o
representa, no infinitivo.

Ativar luz

Contra-exemplo: “entrar no sistema” não é um caso de uso, pois espera-se que essa entrada seja
para realizar uma sequência de ações com um objetivo específico associados aos requisitos.
Motivação: Um caso de uso consiste em um relato do uso (previsto) de certa funcionalidade
requerida do sistema, sem revelar a estrutura e o comportamento internos do mesmo. A partir do
exame de um caso de uso, um observador externo sabe quais as funcionalidades oferecidas
pelo sistema, quais os dados exigidos do usuário final e quais as saídas produzidas.
Cada caso de uso é definido pela narrativa textual das interações que ocorrem entre os elementos
externos e o sistema. Como a UML não define uma estrutura padrão, há várias formas de
descrever essa interação completa.
O estilo de descrição de um caso de uso pode variar em três dimensões: o formato, o grau de
detalhamento e o grau de abstração.
Conceito: O formato consiste na estrutura utilizada para organizar a narrativa textual. Os mais
comuns são: contínuo, numerado e tabular.
Exemplos de um caso de uso nos diferentes formatos:
Caso de uso: Retirada de uma certa quantia de dinheiro de um caixa eletrônico bancário
Descrição continuada:
Este caso de uso se inicia quando o cliente chega ao caixa eletrônico e insere seu cartão.
O sistema requisita a senha do cliente. Após o cliente fornecer a senha e esta ser validada,
o sistema exibe as opções de operações possíveis. O cliente opta por realizar um saque.
Então o sistema requisita o total a ser sacado. O cliente fornece o valor eu deseja sacar. O
sistema fornece a quantia desejada e imprime o recibo para o cliente. O Cliente retira a
quantia e o recibo, e o caso de uso termina.
Descrição numerada:
1 Cliente insere seu cartão no caixa eletrônico;
2 Sistema apresenta solicitação de senha;
3 Cliente digita senha;

CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 3: Especificação de Requisitos (Parte a)

2

4 Sistema valida senha e exibe menu de operações disponíveis;
5 Cliente indica que deseja realizar um saque;
6 Sistema requisita o valor eu o cliente deseja sacar;
7 Cliente fornece o valor que deseja sacar;
8 Sistema fornece a quantia desejada e imprime o recibo para o cliente;
Cliente retira a quantia e o recibo, e o caso de uso termina.
(Erro?)
Descrição tabelada:
Cliente

Sistema

insere seu cartão no caixa eletrônico;
apresenta solicitação de senha;
digita senha;
valida senha e exibe menu de operações
disponíveis;
indica que deseja realizar um saque;
requisita o valor eu o cliente deseja sacar;
fornece o valor que deseja sacar;
fornece a quantia desejada e imprime o recibo
para o cliente;
retira a quantia e o recibo, e o caso de uso
termina.
Observação: Há autores que preconizam o uso de Português estruturado, o que inclui construções
parecidas àquelas das linguagens de programação estruturadas. Embora isto possa ser
interessante, é importante notar que os casos de uso devem ser facilmente entendidos pelo
cliente/usuário.
Conceito: O grau de detalhamento utilizado na descrição de um caso de uso pode variar desde o
mais sucinto até a descrição mais detalhada. Um caso de uso sucinto descreve as interações
entre ator e sistema sem muito detalhamento. Uma descrição expandida descreve as interações
em detalhes.
Conceitos: O grau de abstração de um caso de uso diz respeito à existência ou não de menção a
aspectos relativos à tecnologia na descrição. Um caso de uso que não faz menção a aspectos
tecnológicos é chamado essencial. O que inclui esse tipo de consideração é dito real.
O caso de uso usado no exemplo dos diferentes formatos possíveis é de tipo real, pois considera
a solução tecnológica do caixa automático (solução de leitora de cartões).
Dica: Duas perguntas eficientes para verificar se uma descrição de cenário é real ou essencial
consistem em: “ Esta descrição seria válida há 100 anos?” “Esta descrição será válida daqui a
CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 3: Especificação de Requisitos (Parte a)

3

cem anos?” Se a resposta a alguma delas for negativa, a descrição é real.
Conceito: Um cenário ou cenário de uso é a descrição de uma das maneiras pelas quais um
caso de uso pode ser executado. Um cenário é também chamado de instância de um caso de
uso.
Podemos dizer que um cenário de uso corresponde a um dos caminhos de execução do
procedimento associado ao caso de uso em questão. Estes caminhos, por sua vez, estão
associados às diversas situações reais passíveis de ocorrência no uso do sistema para o caso de
uso em questão.
Estas situações se referem, além de aos possíveis caminhos de execução do sistema, aos
problemas passíveis de ocorrência no mundo real, tanto com relação à tecnologia envolvida
quanto em relação ao usuário em questão. (breakdowns)
Normalmente, um caso de uso corresponde a vários cenários.
Exemplo: Caso de uso: pedido de compra pela Internet
a) O cliente seleciona um conjunto de produtos do catálogo da loja;
b) O cliente indica o desejo de realizar o pagamento por cartão de crédito;
c) O sistema informa que o último produto escolhido está fora de estoque;
d) O cliente pede para que p sistema feche o pedido sem o item que está em falta;
e) O sistema solicita os dados do cliente para a realização do pagamento;
f)

O cliente fornece o número do cartão, a data de expiração, e o endereço de entrega do
pedido;

g) O sistema apresenta o valor total, a data de entrega e uma identificação do pedido para
futuro rastreamento;
h) O sistema envia uma mensagem por correio eletrônico ao cliente como confirmação do
pedido de compra;
i)

O sistema envia os dados para o sistema de logística da empresa.

Observação: Conforme os aspectos dos descritos relativos aos cenários, fica claro, então, que,
com o intuito de identificar todos os possíveis cenários associados a um caso de uso, é
necessário considerar:
•

as possibilidades oferecidas pelo sistema em cada momento de interação;

•

os possíveis erros do usuário;

•

os dois tipos de breakdown (humano e tecnológico) passíveis de ocorrência no uso situado
da aplicação.

Todas estas situações identificadas na descrição dos cenários auxiliam na construção dos casos
de uso.
3.1.2. Benefícios dos casos de uso
Embora não todos os requisitos de um sistema computacional qualquer possam ser
CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 3: Especificação de Requisitos (Parte a)

4

representados por casos de uso, e mesmo que a criação de casos de uso não seja necessária
(uma especificação precisa de requisitos resolve este problema), os benefícios dos casos de uso
são inúmeros:
•

São fáceis de criar e de entender;

•

Forçam os desenvolvedores a pensar no design do sistema pela ótica dos usuários;

•

Auxiliam à participação ativa dos usuários no processo de desenvolvimento;

•

Os casos de uso proporcionam contexto aos requisitos do sistema. É mais fácil entender
um requisito ao enxergar a forma como ele é incorporado à solução (sistema);

•

Os casos de uso determinam uma ordem nos requisitos. Isso permite enxergar o que que
precisa acontecer antes para que a próxima coisa aconteça, e assim por diante;

•

São criados pelos desenvolvedores, o que implica eles terem entendido os requisitos;

•

São um fator crítico no processo de análise, ajudando a compreender o que o sistema
necessita fazer e se ele está indo nesta direção;

•

Os casos de uso minimizam os riscos na transição entre a análise (requisitos) e a
implementação;

•

Conduzem de forma direta processo de teste, ajudando a assegurar que o sistema
realmente faça o que ele deve fazer;

•

Os casos de uso servem como entradas para a documentação para o usuário.

3.1.3. Características principais dos casos de uso
Conceito: Retomando o conceito visto, um caso de uso descreve uma sequência de ações de um
sitema que tem um resultado passível de observação por um ator em particular.
Diversas características podem ser associadas a um caso de uso:
Sequências de ações: A sequência de ações descreve um conjunto de funções
executadas, um procedimento algorítmico ou outro processo interno que produz o
resultado esperado.
O conjunto começa a ser construído quando ocorre uma entrada no sistema. Uma ação
que faz parte de um caso de uso é atômica, isto é, ou ela é executada por inteiro ou não é
executada. A propósito, a exigência da atomicidade é um forte determinante na seleção do
grau de granularidade do caso de uso. O caso de uso proposto deve ser examinado e, se
suas ações não forem atômicas, ele deverá ser detalhado até o nível em que as ações
tenham esta característica.
Comportamento do sistema: O sistema trabalha para os atores. Ele exibe a
funcionalidade descrita no caso de uso e recebe ordens dos atores relativas a quando
fazer o que;
Um resultado útil passível de observação: Um caso de uso deve ser útil para um perfil
de usuário (ator);
Exemplo: “O morador pressiona o botão da luz.” não tem utilidade para o perfil associado,
pois o sistema não parece fazer nada para ele. “O morador pressiona o botão da luz e o
sistema acende a luz.” seria a descrição correta.
Um ator em particular: O ator (o morador ou ou o sinal do botão de emergência) é o
indivíduo ou o dispositivo que inicia (“dispara”) a ação (respectivamente “Acender a luz” ou
“Ativar o sistema de alarme”).

CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 3: Especificação de Requisitos (Parte a)

5

3.1.4. Atores
Conceito: Na terminologia da UML, qualquer elemento externo ao sistema que interage com ele é
considerado um ator.
Na UML, um ator é representado graficamente por um desenho simplificado (sticky) de uma
pessoa (“homem palito”).

Os atores podem ser documentados de forma simples, por meio de uma breve caracterização (de
uma ou duas sentenças), associada ao nome que descreva o papel e não um indivíduo que o
exerce momentaneamente.
Exemplos: cliente, vendedor, fornecedor, professor, aluno,... e não: João, Pedro, Maria, José.
Embora o termo “ator” remeta ao conceito de “pessoa”, atores de um sistema podem ser
agrupados em três categorias principais:
Cargos, organizações ou divisões de uma organização : empregado, gerente,
vendedor, cliente, ...; empresa fornecedora, agência de impostos, administradora de
cartões, almoxarifado,...;
Outros sistemas (software): sistema de cobrança, sistema de estoque, ...;
Equipamentos (hardware): leitora de código de barras, leitora de cartões, sensor,...
Conceito: Um ator corresponde a um papel no uso do sistema.
Observação (1): Uma mesma pessoa pode atuar como dois atores diferentes no sistema em
momentos distintos.
Exemplo: O gerente de uma organização pode atuar no sistema no seu papel de gestão mas,
também, como funcionário comum em outra situação de uso.
Observações (2): Um ator pode participar de vários casos de uso. Um caso de uso pode envolver
vários atores.
3.1.5. Anatomia (estrutura completa da representação textual) de um caso de uso
Até o momento foram vistas a representação visual (Diagrama de Casos de Uso) e a textual. No
entanto, a descrição textual faz parte de uma estrutura que contém, além da própria descrição
textual, outros campos igualmente relevantes.
Nome: O nome deve ser o mesmo utilizado no DCU e único na modelagem completa do
sitema;
Identificador: Código único para cada caso de uso, que permite a realização de
referências cruzadas. Uma sugestão de nomenclatura para este código é: CSU01,
CSU02,...;
Importância: Relevância relativa no conjunto completo dos casos de uso do sistema;

CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 3: Especificação de Requisitos (Parte a)

6

Sumário: declaração do objetivo do ator ao utilizar o caso de uso (no máximo duas
sentenças);
Ator primário: Nome (papel) do ator que inicia o caso de uso ou é o sue principal
beneficiário. Um caso de uso tem somente um ator priário;
Atores secundários: Nomes (papéis) dos restantes atores envolvidos no caso de uso em
descrição;
Precondições: Uma precondição de um caso de uso consiste no conjunto de hipóteses
que determinam a execução do caso de uso. Um caso de uso pode não ter precondições;
Fluxo principal: Também chamado de “fluxo básico”, ele corresponde à descrição da
seqüência de interações usuário-sistema que normalmente acontece quando o caso de
uso é executado. Este fluxo deve ser claro e conciso (representado em uma das três
formas vistas);
Fluxos alternativos: Descrevem os diversos cenários eventuais de execução do caso de
uso. Dentre os fluxos alternativos podem ser citados, entre outros: fluxo principal sem
alternativas de execução; fluxo principal com dois trechos independentes entre si e
passíveis de execução por respectivos caminhos alternativos; fluxo principal com um
trecho de alternativas mutuamente exclusivas;
Fluxos de exceção: Descrevem caminhos de execução em situações de erro ou
breakdowns. São exemplos de fluxos de exceção, num sistema de vendas: cartão de
crédito excedendo o limite; falta, no estoque, do item solicitado; débito anterior do cliente
registrado na loja;
Pós-condições: Descrição do estado do sistema após a execução do caso de uso (Note
que a execução de um caso de uso pode não ter resultado passível de observação
externa). Pós-condições devem declarar o estado e não a forma em que ele foi alcançado.
Exemplos de pós-condições são: “A informação X foi criada / atualizada / removida”;
Notas de implementação: Servem para capturar idéias que passam pela cabeça do
modelador, para que elas não sejam perdidas;
Documentação suplementar
Os casos de uso tem uma documentação suplementar de particular importância no
desenvolvimento dos sistemas. Ela é constituída dos seguintes itens informacionais: Regras do
negócio; Requisitos da interface-usuário; e Requisitos de desempenho.
3.1.6. Identificação dos elementos do UCM
Identificação de atores
No início da construção do UCM, os atores do sistema devem ser identificados. A identificação dos
atores é feita por meio da procura de todas as fontes de informação a serem processadas pelo
sistema e de todos os destinos da informação a ser gerada por ele.
O analista deve identificar quais as áreas da organização que utilizarão e/ou serão afetadas pelo
sistema em desenvolvimento.
No intuito de identificar os atores do sistema, pode e deve ser usada a classificação vista para os
stakeholders, já que estes termos correspondem a um único conceito.
Na identificação dos casos de uso, é comum surgirem novos atores não identificados
CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 3: Especificação de Requisitos (Parte a)

7

previamente.
Identificação de casos de uso
A partir da lista de atores, deve-se passar à identificação dos casos de uso. Na procura dos casos
de uso, é necessário distinguir entre os casos de uso primários e os caso de uso secundários.
Conceito: Casos de uso primários são aqueles que correspondem aos objetivos dos atores. Eles
representam os procedimentos da organização que estão sendo resolvidos pelo software em
desenvolvimento.
Perguntas úteis para a identificação dos casos de uso primários:
•

Quais são as necessidades e os objetivos de cada ator em relação ao sistema?

•

Que informações o sistema deve produzir?

•

O sistema deve realizar alguma ação que ocorre regularmente no tempo?

•

Para cada requisitos funcional, existe(m) um ou mais caso(s) de uso que os atenda?

Estratégias adicionais para a identificação de caso de uso primários:
•

Caso de uso “oposto”: Chama-se assim aquele caso de uso cuja realização desfaz o
resultado do caso de uso associado.
Exemplo: “Fazer pedido de compra” e “Cancelar pedido”. Este tipo de caso de uso deve
ser efetivado como tal sempre que um caso de uso representar uma ação que deva poder
ser desfeita;

•

Caso de uso que precede outro caso de uso: Algumas vezes, certas condições devem
ser verdadeiras para que um dado caso de uso seja executado.
Exemplo: “Fazer pedido de compra” e “Cadastrar-se na loja virtual”. Com o intuito de achar
este tipo de caso de uso, o analista deve se questionar acerca da existência de ações
necessariamente anteriores ao caso de uso em questão;

•

Caso de uso que sucede outro caso de uso: Este tipo de caso de uso surge frente à
pergunta de que ação sucede a execução do caso de uso em questão.
Exemplo: “Fazer pedido de compra” e “agendar entrega”;

•

Caso e uso temporal: Pode haver funcionalidades de um sistema que não sejam iniciadas
por um ator. A pergunta que encontra este tipo de caso de uso é se há alguma tarefa que o
sistema deva realizar de forma automática. Neste tipo de caso o ator é rotulado como
“Tempo” ou como o agente que recebe a informação de saída do sistema.
Exemplos: “O sistema deve fazer relatório de vendas todo último dia de mês.” ou “Obter
relatório mensal de vendas“, com o ator “Tempo” ou “GRU”

•

Caso de uso relacionado a alguma condição interna: Neste tipo de caso de uso também
não há um ator diretamente envolvido. Um evento interno “dispara” a execução do caso de
uso.
Exemplos: “O sistema deve notificar o usuário da existência de novas mensagens de
correio eletrônico.” e “O sistema deve avisar o gerente de estoque de que um determinado
produto chegou ao nível mínimo.”

Conceito: Casos de uso secundários são aqueles que não trazem benefício direto para os
atores, mas são necessários para que o sistema funcione corretamente. Eles se encaixam nas
categorias a seguir.
CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 3: Especificação de Requisitos (Parte a)
•

8

Manutenção de cadastros: Inclusão, alteração ou exclusão de dados cadastrais.
Normalmente, o mais adequado é criar um único caso de uso que resolva as quatro
operações cadastrais (criar, consultar, atualizar e excluir – Create-Read-Update-Delete –
CRUD), mas quando as operações cadastrais são realizadas por atores diferentes, é mais
aconselhável criar casos de uso separados. (Isto vale como regra geral: a criação de casos
de uso deve ser norteada por ator e não por itens cadastrados!). A criação explícita de
caso(s) de uso para manutenção de cadastros é recomendada, além de pelo fato de este
comportamento se encaixa nas definições do comportamento de um caso de uso, mas,
também e principalmente, para evitar que o UCM seja incompleto e, portanto, que haja a
dúvida de se o cadastramento será implementado apesar de não ser representado no
UCM.
Exemplo: Num sistema de folha de pagamento, deve haver cadastros de funcionários e de
cargos.

•

Manutenção de usuários e de seus perfis (visões do sistema e permissões sobre
ele): Este caso de uso é típico em sistemas computacionais.

•

Tratamento de informação proveniente de outros sistemas: Nos casos em que o
sistema em desenvolvimento precise se comunicar com outros sistemas, deve haver um
caso de uso responsável pela comunicação e pela sincronização entre os dois sistemas.
Exemplo: Em um sistema de venda de produtos, pode ser necessário se comunicar com o
sistema de controle de estoque para verificar se existem itens (instâncias) disponíveis.

Observação: Embora os casos de uso secundários devam, sim, ser considerados e representados
no UCM, o modelador deve começar pelos casos de uso primários, que representam os
processos do negócio da organização, os aspectos nos quais o sistema agregará valor ao negócio
da organização.
3.1.7. Construção do Diagrama de Casos de Uso
Conceito: O UCM (Use Case Model) é um modelo de análise e representa o refinamento dos
requisitos. O diagrama utilizado para o UCM é o Diagrama de Casos de uso (ou UCD)
A notação gráfica simples aliada às descrições em linguagem natural tornam este modelo
extremamente popular no desenvolvimento de software.
O UCM, por um lado, direciona as atividades posteriores da equipe de desenvolvimento e, por
outro, força os projetistas a modelarem o sistema de forma que ele atenda às necessidades reais
do usuário.
Conceito da IHC: Este modelo é o modelo da Engenharia de Software que chega mais perto dos
cenários de uso, que são descrições dos usos situados de um sistema pela ótica dos seus
diferentes perfis de usuários potenciais.
Objetivos do UCM: Os principais objetivos do UCM consistem, por um lado, em prover aos
especialistas um veículo que permita discutir e revisar a funcionalidade e o comportamento do
sistema e, por outro, dar suporte à parte escrita do Modelo, fornecendo uma visão de alto nível do
sistema.
Alocação de casos de uso por diagrama: É possível representar, num único UCM:
•

um caso de uso e seus relacionamentos;

•

todos os casos de uso associados a um ator;

•

todos os casos de uso a serem tratados em uma iteração;

CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 3: Especificação de Requisitos (Parte a)
•

todos os casos de uso de uma divisão da organização em questão;

•

todos os caso de uso de um pacote. Nesta opção, deve-se representar, num nível mais
abstrato, o UCM considerando cada pacote como um conjunto deles com completude e
unidade, isto é, com relevante correlação entre seus elementos constituintes.

3.1.8. Casos de uso e Storyboarding
Como foi visto, a técnica do storyboarding consiste na definição de quem faz o que e como.
Quando o ator de um caso de uso é humano e a interação é entre o usuário e o sistema, o
storyboarding pode ajudar na descrição da interação e, utilizando a abordagem de iterações
incrementais, é possível convergir para a criação do caso de uso e da interface gráfica
simultaneamente.

CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

9

