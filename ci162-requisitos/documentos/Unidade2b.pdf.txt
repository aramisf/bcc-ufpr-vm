Unidade 2: Elicitação de Requisitos (Parte b)
2.2. Métodos e técnicas de extração de requisitos
2.2.1. Entrevistas e questionários
Uma das técnicas mais importantes de elicitação de requisitos é a entrevista. A entrevista é uma
técnica simples e direta que tem aplicação virtualmente em qualquer contexto.
A Sociologia nos ensina que é extremamente difícil entender genuinamente os outros, pois este
processo é filtrado pelo nosso próprio tecido conceitual individual resultante de nosso ambiente e
das nossas experiências.
Adicionalmente, os desenvolvedores temos uma excessiva autoconfiança pois, em geral, a gente
atua em situações repetidas nas quais certos elementos da solução são óbvios. Em outras
palavras, a gente já atuou em problemas semelhantes e acredita que essa experiência vai nos
servir para resolver o problema que a gente tem em mãos. A questão é, então, que a gente deve
cuidar para que o conhecimento não interfira na compreensão da especificidade do problema.
Questões independentes de contexto
Existem questões que se aplicam a todo e qualquer contexto. Entre elas podem ser citadas as
seguintes:
Quem são os usuários (ou perfis de usuário)?
Quem é o cliente?
Eles têm necessidades diferentes em relação ao sistema?
Onde mais pode ser encontrada uma solução para este problema?
Estas questões nos forçam a ouvir antes de sair propondo uma solução para o problema.
Questões associadas à solução
A adição de questões relacionadas com a solução potencial auxiliam o usuário ao prover novos
insights e talvez até uma visão diferente sobre o problema. É claro que os usuários também
dependem do projetista para terem contexto.
Estas duas classes de questões dão lugar a um template de “Entrevista genérica, quase
independente de contexto”, uma entrevista que estrutura as questões a serem apresentadas e
pode ser usada na maioria de contextos de aplicação.
Este template de entrevista proporciona:

•

questões independentes de contexto;

•

questões dependentes de contexto;

•

questões que asseguram que os aspectos mais críticos tenham sido explorados
devidamente.

CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

1

Unidade 2: Elicitação de Requisitos (Parte b)

2

Estratégias para entrevistas produtivas
Algumas dicas ajudam para que a entrevista seja produtiva.
•

Prepare questões independentes de contexto e registre-as num caderno de notas para
referência durante a entrevista;

•

Antes da entrevista, pesquise o background do stakeholder e da organização a ser
entrevistada. Não canse o entrevistado com perguntas cuja resposta você já conhece. Por
outro lado, verifique rapidamente se sua compreensão em relação a elas está correta;

•

Registre todas as respostas no seu caderno durante a entrevista. Algumas colocações
podem vir a se mostrar úteis posteriormente, quando você estiver organizando a
informação obtida, ou mesmo depois. Normalmente, é mais rápido fazer anotações no
papel do que em meio eletrônico neste momento;

•

Faça referência ao template durante a entrevista para assegurar que todas as questões
relevantes tenham sido feitas.

•

É bastante comum a situação em que o usuário se envolve, durante a resposta a alguma
pergunta, num monólogo de desabafo em relação à situação crítica ou caótica que ele está
vivendo. Nesse caso, não interrompa o fluxo do relato do usuário. Deixe o usuário ir até o
final, tomando nota de tudo o que ele diz e, inclusive, fazendo, na sequência, perguntas
associadas ao que ele dizer. Somente após exaurir o tema em foco, volte à pergunta
imediatamente seguinte à corrente.

Após identificar os principais perfis de usuário diretos e stakeholders, pode ser necessário realizar
algumas entrevistas adicionais para identificar as necessidades principais destes atores.
Entrevistas versus questionários
Muitas vezes os desenvolvedores se questionam em relação à real necessidade de fazer as
entrevistas, quando, supostamente, um questionário com os mesmos itens poderia ser enviado a
100 pessoas no mesmo período de tempo em que uma entrevista é feita.
Não há substituto possível para o contato pessoal, nem para a maneira informal em que o
desenvolvedor pode se comportar durante alguns momentos na entrevista.
Embora a aplicação de um questionário possa permitir tratamento estatístico dos resultados
quantitativos, o questionário não substitui a entrevista em termos do seu potencial de
contribuição.
Limitações dos questionários: Na busca de requisitos, o questionário tem uma série de problemas,
a saber:

•

A adição de perguntas determinadas ao longo da atividade (entrevista) não pode ser feita
de maneira adiantada, já que não há como prever as respostas do entrevistado;

•

Os pressupostos subjacentes às perguntas acabam comprometendo as respostas.
Exemplo: A disciplina atendeu às sua expectativas? Pressuposto: O aluno tinha
expectativas. Durante a entrevista este problema pode ser contornado;

•

É difícil explorar novos domínios (Numa entrevista, o usuário acaba sugerindo a pergunta
certa se notar que o entrevistador não está indo ao foco: “O que você realmente deveria
perguntar é...”) e não há interação para explorar os assuntos a serem explorados;

•

É difícil dar continuidade a respostas vagas ou ambíguas.

CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 2: Elicitação de Requisitos (Parte b)

3

No entanto, a aplicação de questionários pode ser útil para confirmar resultados de entrevistas e,
também, para levantar respostas a conjuntos numerosos de itens, tais como preferências em
relação a diversas situações de interação dos diferentes perfis de usuários.
Em resumo, se fazemos as perguntas certas às pessoas certas na forma certa, a gente constrói a
compreensão necessária do domínio de aplicação.

CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

