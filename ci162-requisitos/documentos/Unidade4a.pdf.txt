Unidade 4: Especificação do comportamento do sistema (parte a)

1

4.1. Máquinas de estado finitos
Quando a descrição dos requisitos é excessivamente complexa para ser feita em linguagem
natural e não se quer correr o risco de eles serem mal interpretados, podem ser utilizados
métodos alternativos.
Um exemplo disso são os sistemas ditos “life-critical”.
Um desses métodos é a Máquina de estados finitos.
Conceito: Uma Máquina de estados finitos é uma máquina hipotética que pode estar somente
num dentre um dado número de estados num momento exato do tempo.
Em resposta a uma entrada de um usuário ou dispositivo externo, a máquina muda de estado e
gera uma saída ou executa alguma outra ação.
Tanto a saída como o estado seguinte podem ser determinados com base na compreensão do
estado corrente e do evento que causou a transição.
Neste sentido, é possível dizer que o comportamento do sistema é determinístico. Isto é o
mesmo que dizer que pode-se determinar, matematicamente, cada estado possível e,
portanto, as saídas do sistema, a partir de qualquer conjunto de entradas proporcionadas.
A natureza matemática das Máquinas de estados finitos fazem com que elas sejam úteis em
procedimentos que necessitam ser descritos com rigor, dentre os quais a verificação de requisitos.
Problemas de (falta de) consistência, completude e ambiguidade podem ser evitados por meio
deste método.
Notação: Uma das representações mais populares das Máquinas de estados finitos é o Diagrama
de transições entre estados. Nestes Diagramas, elipses representam os estados alternativos e
as setas as ações que levam às transições entre os estados.
4.2. Conceitos de Diagramas de estados
A UML inclui Diagramas de estados para ilustrar os eventos e os estados dos objetos dos
sistemas.
Diagramas de estados devem ser construídos para casos de uso mas podem, também, servir para
mostrar conceitos de outros tipos.
Conceito: Um evento é uma ocorrência significativa.
Exemplo: O telefone sem fio é retirado do suporte.
Conceito: Um estado é a condição de um objeto num certo momento de tempo – o tempo entre os
eventos.
Exemplo: Um telefone no estado “idle” depois de ter sido colocado novamente no suporte e antes
de ser retirado para atender a uma nova ligação
Conceito: Uma transição é um relacionamento entre dois estados que indica que quando um
evento ocorre o objeto muda do estado anterior para o estado seguinte.
Exemplo: Quando o evento “retirar do suporte” ocorre, o telefone passa por uma transição: do
estado “idle” para o estado “ativo”.
4.3. Diagramas de estados
Um Diagrama de estados mostra o ciclo de vida de um objeto.
Conceito: Um Diagrama de estado representa os eventos e os estados relevantes de um objeto
(Relevantes em relação ao que?) assim como o comportamento do objeto em reação ao evento.
É comum representar um pseudo-estado inicial que muda automaticamente para outro estado
CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 4: Especificação do comportamento do sistema (parte a)

2

quando a instância do objeto é criada.
Exemplo:
●
Idle

fora do gancho

Ativo

no gancho
evento
transição
O Diagrama de estados não necessita mostrar todos os estados pelos quais o objeto passa.
Em outras palavras, é possível se representar o ciclo de vida de um objeto de forma genérica
como detalhada, dependendo das necessidades do desenvolvedor em relação a ele.
Um Diagrama de estados pode ser aplicado a inúmeros elementos da UML, dentre os quais:
•

programas;

•

tipos (conceitos);

•

casos de uso;

•

realização da interação usuário-sistema (widgets).

Considerando que o próprio sistema como um todo pode ser visto como um conceito, ele pode ter
também um Diagrama de estados associado.
4.4. Usos úteis de um Diagrama de estados
Um uso útil para o Diagrama de estados consiste na descrição da sequência permitida de
eventos externos ao sistema que são reconhecidos e tratados pelo sistema no contexto de
um caso de uso.
Exemplo1:
Durante o caso de uso “Comprar Itens” na aplicação do ponto de venda não é permitido fazer a
operação de fazer o pagamento com cartão antes de que o processo de venda tenha sido
fechado.
●
entrarItem
Aguardando Venda

Entrando Item

entrarItem

fecharVenda
fazerPagamento
Aguardando pagamento
Exemplo2:
Durante o caso de uso “Processar Documento” no contexto de um editor de textos, não é
permitido executar a operação “Salvar Arquivo” antes de que ou a operação “Arquivo Novo” ou a
operação “Abrir Arquivo” tenham sido executadas.

CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 4: Especificação do comportamento do sistema (parte a)

3

Outra situação em que os Diagramas de estado se mostram úteis é na descrição e casos de uso.
Exemplo: Diagrama de estados para o caso de uso “Comprar Itens”
●

AguardandoVenda

entrarItem

EntrandoItens entrarItem
fecharVenda

pagarEmDinheiro

AguardandoPagamento

PagarComCartão
AutorizandoPagamento

PagarComCheque

4.5. Tipos dependentes e independentes de estados
Diagramas de estados devem ser usados para tipos dependentes de estados.
Conceito: Um tipo (conceito, objeto,...) é dependente se ele tiver comportamento diferente
dependendo do estado em que e encontra.
Conceito: Um objeto é independente de evento se, para todo evento de interesse o objeto
sempre interage da mesma forma.
Exemplo1: Na edição de textos no paradigma WIMP (Windows, Icons, Menus and Pointing
devices) não é possível “colar” algum dado que não tiver sido antes “cortado” ou “copiado”, ou, em
outras palavras, que não estiver na área de transferência.
Exemplo2: Transações compostas, por exemplo em ambiente bancário.
Exemplo3: Dispositivos físicos se comportam de forma diferente dependendo do estado corrente.
4.6. Diagramas de estados para os diferentes tipos de evento
Conceito: Evento externo, também chamado “evento do sistema”, é causado por alguma coisa
(por exemplo, um ator) externa à fronteira do sistema.
Os Diagramas de sequência ilustram os eventos externos.
Exemplo: Quando um caixa pressiona o botão “Entrar Item” num terminal de ponto de venda,
ocorre um evento externo.
Conceito: Evento interno, chamado de “operação do sistema”, é causado por alguma coisa
interna à fronteira do sistema. Em termos de software, um evento interno emerge quando uma
operação é invocada num objeto, ou por um ator ou por outro objeto interno por meio de uma
mensagem.
Exemplo: Quando um objeto Venda recebe uma mensagem fazerLinhaDeItem, um evento interno
ocorre.
Conceito: Evento temporal é causado pela ocorrência de uma data, horário ou intervalo de tempo
específicos. Em termos de software, um evento temporal é determinado por um relógio em tempo
CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 4: Especificação do comportamento do sistema (parte a)

4

real ou por um relógio simulador de tempo.
Exemplos: Emissão de relatórios mensais; logout automático após 10 minutos sem ação do
usuário.
O Paradigma de Orientação a Objetos envolve colaboração entre objetos por meio de mensagens
para a execução das tarefas. O Diagrama de Colaboração enter classes descreve esta
característica.
Portanto, não faz muito sentido usar Diagramas de estados para representá-la.
Assim, Diagramas de estados devem ser usados preferencialmente para ilustrar eventos externos
e temporais, assim como a reação respectiva a cada um deles.
4.7. Notação adicional em Diagramas de estados
A UML contém um rico repertório de aspectos dentre os quais podemos citar três especialmente
úteis:

•

ações de transição;

•

condições de guarda (ou testes booleanos) de transições;

•

estados aninhados.
Ação de transição

Idle

Fora do gancho / dando sinal de discagem
[cliente em dia]

Ativo

no gancho

condição de guarda ou booleana
Exemplo de estados aninhados:
fora do gancho / dandoSinalDeDiscagem
Idle

no gancho

●

Ativo

DandoSinalDeDiscagem
número
número

Discando

Falando
conectado

completo

Conectando

Um estado permite aninhamento para conter sub-estados.
Um sub-estado herda as transações do seu super-estado (o estado que o engloba).
Esta é uma característica crucial que leva a Diagramas de estados sucintos. Sub-estados podem
ser graficamente exibidos de maneira aninhada dentro de um super-estado.
Por exemplo, quando ocorre uma transição para o estado “Ativo”, ocorrem a criação e a transição
CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

Unidade 4: Especificação do comportamento do sistema (parte a)

5

para o sub-estado “DandoSinalDeDiscagem”. Independentemente de em qual sub-estado esteja o
objeto, se o evento “no gancho” relativo ao super-estado “Ativo” ocorrer, a transição para o estado
Idle” ocorre.
4.8. Diagramas de atividades
Conceito: Os fluxogramas têm uma “encarnação” na UML nos Diagramas de Atividades.
Eles têm uma vantagem que os torna representações fáceis até para atores não familiarizados
com a Computação.
A desvantagem deste novo artefato é a mesma dos demais diagramas da UML: a dificuldade de
manutenção. Ninguém gosta de ter de atualizar diagramas, pois a edição é trabalhosa (...)
Os fluxogramas têm os seguintes componentes:
atividade

decisão

início (e fim, com uma circunferência envolta do círculo)
Diagramas de atividades podem ser preferíveis na descrição de processos (fluxos principal,
alternativos e de exceção) sempre que a descrição em língua natural não for suficientemente
precisa.

CI162: Engenharia de Requisitos – 1o semestre de 2011 - Profa. Laura Sánchez García

