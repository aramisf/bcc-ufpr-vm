Proposta de Entrevista

Desenvolvimento de um sistema para gerenciamento de vendas por telefone

Perfis de usuário:
 * Operadores de telefonia
 * Supervisores


Perfil do Usuário:
------------------

1.  Nome

2.  Empresa

3.  Ramo de Atuação da Empresa

4.  Cargo

5.  Quais são suas responsabilidades chave?

6.  Quais resultados você produz?

7.  Para quem?

8.  Como seu desempenho é medido?

9.  Quais problemas interferem no seu desempenho?

10. Quais pontos fazem seu trabalho mais fácil e mais difícil?


Entendendo o Problema
---------------------

11. Quais os problemas do seu dia a dia sem boas soluções?
    a) Por que esse problema existe?
    b) Como isto é resolvido hoje?
    c) Como você gostaria que fosse resolvido?


Entendendo o Ambiente do Usuário
--------------------------------

12. Quem utiliza e quem deverá ter acesso ao sistema?

13. Quais os níveis de escolaridade de cada um deles?

14. Qual a familiarização com computadores deles?

15. Os usuários estão acostumados a lidar com sistemas?

16. Quais plataformas estão em uso?

17. Há planos de troca de plataformas?

18. Há aplicativos adicionais em uso com esta plataforma?

19. Quanto tempo você imagina que um treinamento dos funcionários
    deva durar?

20. Quais os tipos de ajuda que você imagina que sejam necessárias no
    processo de implementação?


Recapitulando Para Entendimento
-------------------------------

21. As questões acima representam adequadamente os seus problemas com
    a solução atual?

22. Há outros problemas que você esteja enfrentando?


Comentários Sobre os Problemas do Usuário
-----------------------------------------

23. Quais problemas estão associados com os aspectos identificados
    na entrevista até agora?
    
    23.1 É um problema verdadeiro?
    
    23.2 Quais as razões deste problema?
    
    23.3 Como ele é resolvido?
    
    23.4 Como você gostaria de resolver o problema?
    
    23.5 Qual a prioridade de resolução deste problema?


Moldando a Solução
------------------

24. E se o cliente pudesse...

    24.1 


Conhecendo as Oportunidades
---------------------------

25. Quem na empresa precisa destas soluções?

26. Quantos usuários destes usariam o sistema?

27. Como você valorizaria uma solução eficaz?


Conhecendo a Confiabilidade, Desempenho e Necessidade de Suporte
----------------------------------------------------------------

28. Quais as expectativas de confiabilidade?

29. Quais as expectativas de desempenho?

30. Quem manterá o produto?

31. Há alguma necessidade especial para suporte?

32. Quais as necessidades de manutenção?

33. Quais as necessidades de segurança?

34. Quais as necessidades de instalação e configuração?

35. Quais as necessidades de licenciamento?

36. Como o software será distribuído?

37. Há necessidade de criação de marca e preparação do produto?


Outras Necessidades
-------------------

38. Existem outras necessidades legais, ambientais, ou de padrões
    necessárias para o desenvolvimento do software?

39. Você consegue imaginar alguma outra necessidade?


Encerramento
------------

40. Há outras perguntas que eu deveria fazer?

41. Se eu tiver mais perguntas, posso lhe procurar? Você gostaria de
    fazer uma revisão destes requisitos depois?


Resumo
------

42. Enquanto a entrevista ainda é recente, resuma os três itens mais
    importantes que foram identificados.
