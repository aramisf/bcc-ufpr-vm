.section .data
	inicio:			 .long 0
	fim:				 .long 0
	str_ini:        .string "\nSlot\tStatus\t\tEndereço\tTamanho\nHeap\t0x%X\n"
	str_ocu:        .string "%d\tO\t\t-\t\t%d\n"
	str_liv:        .string "%d\tL\t\t-\t\t%d\n"
	str_total_ocu:  .string "\nOcupados: %d slots / %d bytes\n"
	str_total_liv:  .string "Livres: %d slots / %d bytes\n\n"

	.equ LIVRE,1
	.equ OCUPADO,0
	.equ BRK,45
	.equ SYSCALL,0x80
	.equ INFO,8

.section .text


##################################################################
###  Aloca um espaço na heap de tamanho passado por parametro  ###
##################################################################
.globl meuAlocaMem
.type meuAlocaMem,@function
meuAlocaMem:
	pushl %ebp
	movl %esp,%ebp
	pushl %ebx
if_mam:
	cmpl $0,inicio			#inicializa heap
	jne fim_if_mam
	movl $BRK,%eax
	movl $0,%ebx
	int $SYSCALL
	movl %eax,inicio		#inicializando as variáveis globais
	movl %eax,fim			#
fim_if_mam:
	movl inicio,%eax
	movl fim,%ebx
	movl 8(%ebp),%ecx
while_mam:
	cmpl %ebx,%eax			#verifica se o ponteiro para atual posicao chegou ao fim da heap
	je aumenta_heap
	cmpl $LIVRE,(%eax)		#verifica se esta livre
	jne proximo_mam
	cmpl 4(%eax),%ecx		#verifica se tem espaco suficiente
	jle aloca
proximo_mam:
	addl 4(%eax),%eax
	addl $INFO,%eax
	jmp while_mam
aloca:
	movl $OCUPADO,(%eax)
	movl 4(%eax),%edx
	addl $INFO,%eax
	subl $INFO,%edx
	cmpl %edx,%ecx			#verifica se o espaco livre é maior* ou igual ao espaco desejado + INFO(da memoria livre restante)
	jge fim_while_mam		#se for, acabou
	movl %ecx,-4(%eax)		#senao, grava o espaco da memoria ocupada e tambem as informacoes da memoria livre restante
	addl %ecx,%eax
	movl $LIVRE,(%eax)
	#movl %ecx,%ebx			#salvando %ecx(tamanho desejado para alocaçao)
	subl %ecx,%edx
	movl %edx,4(%eax)
	subl %ecx,%eax
	jmp fim_while_mam
aumenta_heap:
	#movl %eax,%ebx
	addl $INFO,%ebx
	addl %ecx,%ebx
	movl %ebx,fim
	movl $BRK,%eax
	int $SYSCALL
	cmpl $0,%eax			#verifica se ocorreu erro
	je if_error
	subl %ecx,%eax			#grava informacoes da memoria alocada
	movl $OCUPADO,-8(%eax)	#
	movl %ecx,-4(%eax)		#
fim_while_mam:
	popl %ebx
	popl %ebp
	ret
if_error:
	movl $0,%eax
	popl %ebx
	popl %ebp
	ret
### end ( meuAlocaMem )


###########################################################
###  Libera o espaço do ponteiro passado por parametro  ###
###########################################################
.globl meuLiberaMem
.type meuLiberaMem,@function
meuLiberaMem:
	pushl %ebp
	movl %esp,%ebp
	pushl %ebx
	movl 8(%ebp),%ecx
	movl $LIVRE,-8(%ecx)
	movl inicio,%eax
	movl fim,%ebx
while_eax:
	cmpl %ebx,%eax
	je fim_while_eax
	cmpl $OCUPADO,(%eax)
	je proximo_mlm
while_ecx:
	movl %eax,%ecx
	addl 4(%ecx),%ecx
	addl $INFO,%ecx
	cmpl %ebx,%ecx
	je fim_while_ecx
	cmpl $OCUPADO,(%ecx)
	je proximo_mlm
	movl 4(%ecx),%edx
	addl $INFO,%edx
	addl 4(%eax),%edx
	movl %edx,4(%eax)
	addl 4(%ecx),%ecx
	addl $INFO,%ecx
	jmp while_ecx
fim_while_ecx:
	movl %eax,%ebx
	movl %ebx,fim
	movl $BRK,%eax
	int $SYSCALL
	jmp fim_while_eax
proximo_mlm:
	addl 4(%eax),%eax
	addl $INFO,%eax
	jmp while_eax
fim_while_eax:
	popl %ebx
	popl %ebp
	ret
### end ( meuLiberaMem )


###################################
###  Imprime o mapa da memória  ###
###################################
.globl imprMapa
.type imprMapa,@function
imprMapa:
	pushl %ebp
	movl %esp,%ebp
	subl $20,%esp
	movl $0,-4(%ebp)   #quantidade de espaços livres
	movl $0,-8(%ebp)   #soma dos bytes livres
	movl $0,-12(%ebp)  #quantidade de espaços ocupados
	movl $0,-16(%ebp)  #soma dos bytes ocupados
	movl inicio,%eax
	cmpl $0,%eax
	jne if_nomalloc
	movl $BRK,%eax
	movl $0,%ebx
	int $SYSCALL
if_nomalloc:
	pushl %eax
	pushl $str_ini
	call printf
	addl $8,%esp
	movl inicio,%eax
	movl $1,%edi
while_im:
	movl fim,%ebx
	cmpl %ebx,%eax
	je fim_while_im
	pushl 4(%eax)
	pushl %edi
if_im:
	cmpl $LIVRE,(%eax)
	jne else_im
	pushl $str_liv
	movl 4(%eax),%ecx
	movl -8(%ebp),%edx
	addl %ecx,%edx
	movl %edx,-8(%ebp)
	movl -4(%ebp),%ecx
	addl $1,%ecx
	movl %ecx,-4(%ebp)
	jmp fim_if_im
else_im:
	pushl $str_ocu
	movl 4(%eax),%ecx
	movl -16(%ebp),%edx
	addl %ecx,%edx
	movl %edx,-16(%ebp)
	movl -12(%ebp),%ecx
	addl $1,%ecx
	movl %ecx,-12(%ebp)
fim_if_im:
	movl %eax,-20(%ebp)
	call printf
	addl $12,%esp
	movl -20(%ebp),%eax	#restaura valor de %eax
	addl 4(%eax),%eax
	addl $INFO,%eax
	addl $1,%edi
	jmp while_im
fim_while_im:
	pushl -16(%ebp)
	pushl -12(%ebp)
	pushl $str_total_ocu
	call printf
	addl $12,%esp
	pushl -8(%ebp)
	pushl -4(%ebp)
	pushl $str_total_liv
	call printf
	addl $12,%esp
	addl $20,%esp
	popl %ebp
	ret
### end ( imprMapa )
