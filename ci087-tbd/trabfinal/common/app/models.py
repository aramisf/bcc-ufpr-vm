from django.db import models

class Country(models.Model):
    name = models.CharField('Name', max_length = 255)

class State(models.Model):
    name = models.CharField('Name', max_length = 255)
    country = models.ForeignKey('Country')

class City(models.Model):
    name = models.CharField('Name', max_length = 255)
    state = models.ForeignKey(State)

class CompanyType(models.Model):
    type = models.CharField('Type', max_length = 255)

class Company(models.Model):
    name = models.CharField('Name', max_length = 255)
    type = models.ForeignKey(CompanyType)
    address = models.CharField('Address', max_length = 255)
    city = models.ForeignKey(City)

class ProductType(models.Model):
    name = models.CharField('Name', max_length = 255)

class ProductCurrency(models.Model):
    name = models.CharField('Name', max_length = 255)
    country = models.ForeignKey(Country)

class Product(models.Model):
    name = models.CharField('Name', max_length = 255)
    type = models.ForeignKey(ProductType)
    price = models.FloatField('Price')
    currency = models.ForeignKey(ProductCurrency)

class Person(models.Model):
    name = models.CharField('Name', max_length = 255)
    phone = models.CharField('Phone', max_length = 255)
    address = models.CharField('Address', max_length = 255)
    city = models.ForeignKey(City)
    state = models.ForeignKey(State)

class Sale(models.Model):
    person = models.ForeignKey(Person)
    product = models.ForeignKey(Product)
    date = models.DateTimeField('Date', auto_now = True)

