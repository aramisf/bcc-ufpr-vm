#!/usr/bin/python

#raw_input()

import sys
import os
sys.path.insert(0, os.path.abspath('../' + sys.argv[1]))

import random

from django.core.management import setup_environ
from project import settings
setup_environ(settings)

from project.app.models import Country, State, City, \
    Company, CompanyType, ProductType, ProductCurrency, \
    Product, Person, Sale

from tests_params import n_country, n_state, n_city, n_company_type, \
    n_company, n_product_type, n_product_currency, n_product, \
    n_person, n_sale

i = 0
while i < n_country:
    Country(name = i).save()
    #print "wrote Country %d" % i
    i += 1

rand_country = Country.objects.filter(name = random.randint(0, n_country))[0]
#print "got rand_country %s" % rand_country.name

i = 0
while i < n_state:
    State(
        country = rand_country,
        name = i
    ).save()
    #print "wrote State %d" % i
    i += 1

rand_state = State.objects.filter(name = random.randint(0, n_state-1))[0]
#print "got rand_state %s" % rand_state.name

i = 0
while i < n_city:
    City(
        state = rand_state,
        name = i
    ).save()
    #print "wrote City %d" % i
    i += 1

rand_city = City.objects.filter(name = random.randint(0, n_city-1))[0]
#print "got rand_city %s" % rand_city

i = 0
while i < n_company_type:
    CompanyType(type = i).save()
    #print "write company type %d" % i
    i += 1

rand_company_type = CompanyType.objects.filter(type = random.randint(0, n_company_type-1))[0]
#print "got rand_company_type %s" % rand_company_type.type

i = 0
while i < n_company:
    Company(
        type = rand_company_type,
        city = rand_city,
        address = random.randint(0, 500),
        name = i
    ).save()
    #print "wrote Company %d" % i
    i += 1

i = 0
while i < n_product_type:
    ProductType(name = i).save()
    #print "wrote ProductType %d" % i
    i += 1

rand_product_type = ProductType.objects.filter(name = random.randint(0, n_product_type-1))[0]
#print "got rand_product_type %s" % rand_product_type.name

i = 0
while i < n_product_currency:
    ProductCurrency(country = rand_country, name = i).save()
    #print "wrote ProductCurrency %d" % i
    i += 1

rand_product_currency = ProductCurrency.objects.filter(name = random.randint(0, n_product_currency-1))[0]
#print "got rand_product_currency %d" % i

i = 0
while i < n_product:
    Product(
        type = rand_product_type,
        price = random.random() * random.randint(0, i),
        currency = rand_product_currency,
        name = i
    ).save()
    #print "wrote Product %d" % i
    i += 1

rand_product = Product.objects.filter(name = random.randint(0, n_product-1))[0]
#print "got rand_product %s" % rand_product.name

i = 0
while i < n_person:
    Person(
        phone = random.randint(0, 50000),
        address = random.randint(0, 50000),
        city = rand_city,
        state = rand_state,
        name = i
    ).save()
    #print "wrote Person %d" % i
    i += 1

rand_person = Person.objects.filter(name = random.randint(0, n_person-1))[0]
#print "got rand_person %s" % rand_person.name

i = 0
while i < n_sale:
    Sale(
        person = rand_person,
        product = rand_product
    ).save()
    #print "wrote Sale %d" % i
    i += 1


