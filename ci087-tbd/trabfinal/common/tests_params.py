#!/usr/bin/python

# Write

'''n_country = 2
n_state = 50
n_city = 2000

n_company_type = 3
n_company = 100

n_product_type = 3
n_product_currency = 3
n_product = 500

n_person = 4000
n_sale = 10000'''

# Read

n_country = 2
n_state = 5
n_city = 10

n_company_type = 3
n_company = 10

n_product_type = 3
n_product_currency = 3
n_product = 10

n_person = 10
n_sale = 20
