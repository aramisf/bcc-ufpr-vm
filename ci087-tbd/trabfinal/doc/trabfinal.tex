\documentclass[11pt]{article}

\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[hmargin=2cm,vmargin=3cm]{geometry}
\usepackage[none]{hyphenat}
\usepackage{multicol}
\usepackage{caption}
\usepackage{graphicx}
\usepackage{listings}

\newenvironment{mfigure}
  {\par\medskip\noindent\minipage{\linewidth}}
  {\endminipage\par\medskip}

\lstset{%
    basicstyle=\footnotesize\ttfamily,
    frame=lines,
    tabsize=2,
    commentstyle=\color{black!60},
    lineskip={-1pt}}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0.4cm}
\setlength{\columnsep}{20pt}
\setlength{\footnotesep}{0.3cm}
\setlength{\skip\footins}{1cm}

\newlength \figwidth
\if twocolumn
  \setlength \figwidth {0.9\columnwidth}
\else
  \setlength \figwidth {0.5\textwidth}
\fi


\title{Tópicos em Banco de Dados \\
    Trabalho Final \\
    Desempenho do Django Framework \\
    com Cassandra e MySQL}
\author{Vinicius André Massuchetto \\{\em vam06@inf.ufpr.br}}
\date{Maio de 2012}

\begin{document}

  \maketitle

  \begin{multicols}{2}

  \section{Introdução}

    \texttt{Django}\footnote{Site do Projeto Django: http://djangoproject.com}
    é um framework de alto nível escrito em \texttt{Python} para
    desenvolvimento web, e encoraja o desenvolvimento rápido, limpo e
    pragmático de aplicações.

    Um de seus principais recursos é o uso independente de bancos de dados
    diferenciados. Um mesmo projeto pode, sem a necessidade de nenhum ajuste,
    funcionar com o \texttt{PostgreSQL}, \texttt{MySQL}, \texttt{SQLite} e
    \texttt{Oracle}. Backends para outros SGBDs também estão disponíveis, mas
    não acompanham a ferramenta por padrão.

    O sistema de banco de dados \texttt{Cassandra}\footnote{Site do projeto
    \texttt{Cassandra}: http://cassandra.apache.org} faz parte do projeto
    \texttt{Apache}\footnote{Site do projeto \texttt{Apache}:
    http://apache.org}, e é voltado principalmente para a utilização em
    ambientes críticos de produção. Este sistema tem se tornado conhecido por
    ter sido empregado em grandes projetos que demandam bastante
    escalabilidade\footnote{Principais empresas usuárias do \texttt{Cassandra}:
    http://datastax.com/cassandrausers}, tais como redes sociais e aplicações
    em nuvem.

    Tanto o \texttt{Django} quanto o \texttt{Cassandra} são sistemas em
    evidência no cenário de desenvolvimento web atual. O \texttt{Django} por
    permitir o desenvolvimento rápido e pragmático de aplicações -- implicando
    diretamente em rapidez na entrega e melhor manutenção -- sendo conhecido
    também pelo seu alto desempenho, e o \texttt{Cassandra} pela ideia de
    armazenamento de dados contínuo e distribuído. Ambas tem sido também
    constantemente aprimoradas em um ativo ambiente \texttt{Open Source} de
    desenvolvimento.

    Porém, não existe uma integração estável e amplamente utilizada entre ambos
    os sistemas. Projetos \texttt{Django} são comumente feitos com
    \texttt{MySQL} ou \texttt{PostgreSQL}, e se houver a necessidade de
    integração com o \texttt{Cassandra}, é necessário que sejam feitas grandes
    implementações à parte.

    Esta integração é dificultada principalmente pelo paradigma relacional.
    Como todos os backends `oficiais' do \texttt{Django} são voltados para
    SGBDs baseados em \texttt{SQL}, a implementação das metodologias de
    consulta comum que consiste na conversão de objetos \texttt{Python} em
    consultas \texttt{SQL} -- as chamadas \texttt{QuerySets} do \texttt{Django}
    -- são também pautada neste paradigma.  Logicamente, os bancos \texttt{SQL}
    são muito diferentes dos \texttt{NoSQL}, e a distribuição de um sistema que
    suporte ambos os tipos é bastante trabalhosa.

    Tendo em vista esta escassez de soluções, foi feito um fork independente do
    \texttt{Django} somente para o atendimento a SGBDs não relacionais, o
    \texttt{Django-nonrel}\footnote{Página do
    projeto\texttt{Django-nonrel}:http://www.allbuttonspressed.com/projects/django-nonrel},
    e que torna possível o uso de backends de conexão para alguns bancos
    \texttt{NoSQL}, tal como o
    \texttt{django\_cassandra\_backend}\footnote{Site do projeto
    \texttt{django\_cassandra\_backend}:
    http://github.com/vaterlaus/django\_cassandra\_backend} e o
    \texttt{alexandra}\footnote{Site do projeto \texttt{alexandra}:
    http://github.com/dziegler/alexandra} através do uso de clientes
    \texttt{Python} de conexão como o \texttt{Pycassa}\footnote{Site do projeto
    \texttt{Pycassa}: http://github.com/pycassa/pycassa} e o
    \texttt{Telephus}\footnote{Site do projeto \texttt{Telephus}:
    http://github.com/driftx/Telephus}.

    Porém, estas soluções foram desenvolvidas em caráter experimental e têm
    funcionalidade limitada, embora tornem possível a demonstração de
    funcionamento do \texttt{Django} com bancos \texttt{NoSQL}. O interesse
    deste trabalho é justamente explorar este experimentalismo e analisar o
    desempenho que as aplicações possuem quando construídas com o
    \texttt{Django} em bancos \texttt{MySQL} -- um caso comum de mercado, e
    quando construídas com o \texttt{Django-nonrel} e o \texttt{Cassandra} --
    uma novidade em implementação de sistemas de modo geral.

  \section{Objetivo}

    O objetivo desde projeto é analisar as soluções independentes existentes
    para integração entre \texttt{Django} e \texttt{Cassandra} utilizando o
    aplicativo \texttt{django\_cassandra\_backend} e o \texttt{Django-nonrel},
    e compará-lo com o \texttt{backend} padrão disponível no projeto original
    para \texttt{MySQL}. As comparações de desempenho devem basear-se em tempo
    de execução para operações sucessivas de leitura e escrita da mesma
    configuração de dados.

  \section{Desenvolvimento}

  \subsection{Arquitetura}

    O \texttt{Django}, assim como o \texttt{Python}, possui uma interface em
    linha de comando que pode ser utilizada para executar os comandos
    suportados pela ferramenta da mesma forma como é feito em código. Este
    recurso é especialmente prático para a execução de scripts e de testes
    automatizados.

    Foram configuradas duas instalações diferentes, uma com o \texttt{Django}
    em \texttt{backend} de banco de dados para \texttt{MySQL}, e outra com o
    fork \texttt{Django-nonrel} conectando através da \texttt{Thrift
    API}\footnote{Página da Thrift API: http://thrift.apache.org/} em um banco
    \texttt{Cassandra}. Ambas as instalações possuem um link simbólico para uma
    aplicação comum que declara o esquema de dados utilizado e possui os testes
    de escrita a serem executados.

    Os principais arquivos são:

  \end{multicols}

    \lstinputlisting{./code/arquitetura.txt}

  \begin{multicols}{2}

    Todos os testes foram efetuados em um processador \texttt{Intel Pentium CPU
    P6200 @ 2.13GHz} com 3GB de memória RAM; disco \texttt{Samsung HM321HI}, em
    somente um servidor.

    Os sistemas utilizados e que não se encontram nos arquivos fonte: Sistema
    operacional \texttt{Debian GNU/Linux} com kernel versão
    \texttt{3.2.0-3-amd64}; \texttt{MySQL} versão \texttt{14.14 Distrib 5.5.24
    x86\_64} distribuído com o pacote \texttt{mysql-server 5.5.24+dfsg-8};
    \texttt{Cassandra} versão \texttt{1.1.4} distribuído com o pacote
    \texttt{cassandra 1.1.4}; \texttt{Python} versão \texttt{2.7.3rc2}
    distribuído com o pacote \texttt{python 2.7.3~rc2-1}; \texttt{Python
    MySQLdb} distribuída no pacote versão \texttt{python-mysqldb 1.2.2-10+b1};
    \texttt{Python Thrift API} distribuído no pacote versão
    \texttt{python-thrift 0.8.0-2}.

  \subsection{Esquema de Dados}

    Como colocado na introdução, as \texttt{QuerySets} -- interface de
    consultas do \texttt{Django} aos SGBDs, torna possível que uma mesma
    aplicação funcione em diferentes SGBDs sem qualquer modificação de código.

    Outra vantagem é que a descrição do esquema de dados é feito diretamente em
    código, e com isso pode-se gerar comandos \texttt{SQL} para criação de
    tabelas ou requisições ao \texttt{Cassandra} para criação de famílias de
    colunas a partir da mesma descrição de esquema de dados.

    Este esquema está descrito no arquivo \texttt{common/app/models.py}. Por
    exemplo, temos aqui definição das entidades \texttt{Cidade}, \texttt{Tipo de
    Empresa}, e \texttt{Empresa}:

    \lstinputlisting{./code/models.txt}

    A entidade \texttt{Cidade} possui um nome e uma referência para outra
    entidade \texttt{Estado}, o \texttt{Tipo de Empresa} possui somente um
    nome, e é referenciado pela entidade \texttt{Empresa}, que possui um nome,
    endereço, e outra referência para \texttt{Cidade}.

    Em um arquivo \texttt{models.py} apropriadamente validado pelo
    interpretador do \texttt{Django}, o seguinte comando realiza a introspecção
    do esquema de dados no banco de dados que estiver configurado:

    \lstinputlisting{./code/syncdb.txt}

    No caso do \texttt{MySQL}, isto implica na criação das tabelas com as
    correspondentes dendências entre colunas, e no caso do \texttt{Cassandra},
    nas famílias de colunas, tudo sem nenhum tipo de configuração adicional no
    projeto \texttt{Django} em questão.

    O esquema completo montado pelo projeto conforme os padrões do
    \texttt{Django} tem sua representação gráfica a seguir:

  \end{multicols}

    \begin{center}
      \includegraphics[width=0.65\textwidth]{./img/models.png}
    \end{center}

  \begin{multicols}{2}

  \subsection{Testes de Escrita}

    A vantagem de uso de um framework com tanta flexibilidade para a escolha do
    sistema de banco de dados para estes testes está no fato de que a mesma
    descrição do esquema de dados permite a criação de estruturas para
    armazenamento em todos os tipos de banco de dados suportados, o que acaba
    por facilitar consideravelmente os testes -- já que os dados possuem a
    mesma complexidade de relacionamento entre suas entidades mapeadas -- e
    também permite que o mesmo script de testes seja executado modularmente
    para ambos os bancos de dados sem nenhum tipo de modificação, já que a
    tradução para as interfaces de consulta é feito diretamente pelo
    \texttt{Django}.

    As tabelas abaixo possuem o indicativo \texttt{W} (write) e \texttt{R}
    (read) para a operação realizada, a entidade que está sendo tratada por
    esta operação, e a quantidade de operações efetuadas. Para cada operação,
    um inteiro aleatório é gerado e com ele é feita a busca ou a escrita no
    sistema em questão. As rotinas executadas estão presentes no arquivo
    \texttt{common/tests\_write.py}, e os parâmetros em
    \texttt{common/tests\_params.py}.

    \begin{tabular}{ l | l | l }
        Entidade           & Op. & Quant.\\
        \hline
        Países             & W & 2       \\
        Países             & R & 1       \\
        Estados            & W & 50      \\
        Estados            & R & 1       \\
        Cidades            & W & 2.000   \\
        Cidades            & R & 1       \\
        Tipos de Empresa   & W & 3       \\
        Tipos de Empresa   & R & 1       \\
        Empresas           & W & 100     \\
        Tipos de Produtos  & W & 3       \\
        Tipos de Produtos  & R & 1       \\
        Moedas             & W & 3       \\
        Moedas             & R & 1       \\
        Produtos           & W & 500     \\
        Produtos           & R & 1       \\
        Pessoas            & W & 4.000   \\
        Pessoas            & R & 1       \\
        Vendas             & W & 10.000
    \end{tabular}

    \subsubsection{Tempo}

    O \texttt{Cassandra} processa muito mais rapidamente com uma alta de carga
    de escritas do que o \texttt{MySQL}. As figuras \ref{write-mysql} e
    \ref{write-cassandra} mostram os resultados do comando \texttt{time} na
    execução dos testes de escrita.

    \begin{mfigure}
      \lstinputlisting{./code/write-mysql.txt}
      \captionof{figure}{Tempo de execução dos testes de escrita para o
      \texttt{MySQL}}
      \label{write-mysql}
    \end{mfigure}

    \begin{mfigure}
      \lstinputlisting{./code/write-cassandra.txt}
      \captionof{figure}{Tempo de execução dos testes de escrita para o
      \texttt{Cassandra}}
      \label{write-cassandra}
    \end{mfigure}

    \subsubsection{CPU}

    A figura \ref{write-cpu} descreve o uso de CPU dos daemons destes sistemas
    de bancos de dados, percebemos um rápido pico de CPU para o
    \texttt{jsvc.exe} do \texttt{Cassandra}, enquanto o \texttt{mysqld}
    permanece estável por mais tempo.

    \begin{mfigure}
      \includegraphics[width=\linewidth]{./log/write-cpu.png}
      \captionof{figure}{Uso de CPU pelo daemon \texttt{mysqld} do
      \texttt{MySQL} e \texttt{jsvc.exec} do \texttt{Cassandra}}
      \label{write-cpu}
    \end{mfigure}

    Observamos um comportamento semelhante para o script \texttt{Python} que
    roda faz as interfaces de conexão com estes daemons na figura
    \ref{write-cpu-py}.

    \begin{mfigure}
      \includegraphics[width=\textwidth]{./log/write-cpu-py.png}
      \captionof{figure}{Uso de CPU pelo script \texttt{Python} que roda os
      testes em \texttt{Cassandra} e \texttt{MySQL}}
      \label{write-cpu-py}
    \end{mfigure}

    \subsubsection{Memória}

    Já o uso de memória é estável por ambos os daemons dos SGBDs em questão,
    sendo somente que a quantidade alocada para o \texttt{Cassandra} -- por
    previsão de seu projeto de funcionamento -- é bem maior. As quantidades
    alocadas de acordo com o tempo de execução das rotinas está relatada na
    figura \ref{write-mem}.

    \begin{mfigure}
      \includegraphics[width=\textwidth]{./log/write-mem.png}
      \captionof{figure}{Uso de memória pelo daemon \texttt{mysqld} do
      \texttt{MySQL} e \texttt{jsvc.exec} do \texttt{Cassandra}}
      \label{write-mem}
    \end{mfigure}

    O uso de memória do script \texttt{Python} tem um comportamente parecido
    com o seu uso de memória, com um pico de alocação inicial do
    \texttt{Cassandra} consideravelmente grande em relação ao \texttt{MySQL},
    conforme mostrado na figura \ref{write-mem-py}.

    \begin{mfigure}
      \includegraphics[width=\textwidth]{./log/write-mem-py.png}
      \captionof{figure}{Uso de memória pelo script \texttt{Python} que roda os
      testes em \texttt{Cassandra} e \texttt{MySQL}}
      \label{write-mem-py}
    \end{mfigure}

    É necessário observar que para os testes de escrita são necessárias também
    algumas poucas leituras para obtenção das chaves no \texttt{Cassandra}, o
    que certamente interefere no desempenho entre as escritas de uma entidade e
    outra.

  \subsection{Testes de Leitura}

    Embora não seja uma finalidade do \texttt{Cassandra}, é importante também
    dimensionar o custo de leitura de dados nestes SGBDs. Como a busca e
    leitura de dados no \texttt{Cassandra} é extramente demorada, o conjunto de
    dados utilizados foi bem mais reduzido do que aqueles utilizados para
    efetuar as escritas.

    As rotinas executadas estão presentes no arquivo
    \texttt{common/tests\_read.py}, e os parâmetros em
    \texttt{common/tests\_params.py}.

    \begin{tabular}{ l | l | l }
        Entidade           & Op. & Quant.\\
        \hline
        Países             & R & 2    \\
        Estados            & R & 5    \\
        Cidades            & R & 10   \\
        Tipos de Empresa   & R & 3    \\
        Empresas           & R & 10   \\
        Tipos de Produtos  & R & 3    \\
        Moedas             & R & 3    \\
        Produtos           & R & 10   \\
        Pessoas            & R & 10   \\
        Vendas             & R & 20
    \end{tabular}

    O resultado do comando \texttt{time} está colocado nas figuras
    \ref{read-mysql} e \ref{read-cassandra}, e sua considerável discrepância --
    mesmo para um conjunto reduzido de dados -- é discutido mais adiante.

    \begin{mfigure}
      \lstinputlisting{./code/read-mysql.txt}
      \captionof{figure}{Tempo de execução dos testes de leitura para o
      \texttt{MySQL}}
      \label{read-mysql}
    \end{mfigure}

    \begin{mfigure}
      \lstinputlisting{./code/read-cassandra.txt}
      \captionof{figure}{Tempo de execução dos testes de leitura para o
      \texttt{Cassandra}}
      \label{read-cassandra}
    \end{mfigure}

  \section{Conclusão}

    A ideia geral do NoSQL é permitir o armazenamento de imensas quantidades de
    dados, e no caso do Cassandra, entre múltiplos servidores. Os testes
    efetuados expressam os principais objetivos deste SGBD, que é o
    armazenamento rápido de dados.

    Sendo assim, a metodologia de escrita empregada é basicamente um anexo de
    dados contendo a nova informação em um formato ``chave-valor'' no sistema
    de armazenamento -- que em primeira instância é a memória principal e que
    periodicamente vai sendo transferida para o disco rígido.

    O único bloqueio de I/O esperado é o próprio acesso à este sistema de
    armazenamento e ao commit log, o que mantém a latência de escrita
    extremamente baixa principalmente quando feito em memória principal.

    De acordo com os resultados obtidos, a escrita sucessiva de aproximadamente
    16.000 entidades -- o que compõem vários conjuntos de dados com mais de uma
    chave estrangeira em cada um deles -- foi três vezes mais rápida no
    \texttt{Cassandra} do que no \texttt{MySQL}. Mesmo que os recursos ocupados
    pelo \texttt{Cassandra} fossem substancialmente maiores, a sua faixa de
    memória permanece estável e o pico de utilização de CPU é breve e se
    estabiliza progressivamente.

    Porém, ficou claro também que operações sucessivas de leitura e extração de
    relatórios são insustentáveis em ambientes \texttt{Cassandra}. Além da
    chave ter que ser buscada em memória principal -- pois pode não ter sido
    transferida para o disco ainda, o sistema de armazenamento final é uma
    tabela a ser inteiramente percorrida -- possivelmente entre diversos
    servidores, resultando em uma operação extremamente custosa e vagarosa.

    Esta deficiência para leitura de dados é projetada e vem atender ao
    requisito de armazenamento consistente e distribuído a que o
    \texttt{Cassandra} se propõe, o que não é possível de se fazer em outros
    SGBDs, cuja distribuição dos dados exige estratégias independentes.

    O cenário web neste caso é bastante peculiar. Soluções para visitação
    massiva até a chamada Web 2.0 não exigiam um aparato tecnológico tão
    complexo e integrado, pois implicavam somente na construção de um sistema
    de cache em \texttt{HTML} comprimido balanceado entre servidores, e não
    obrigatoriamente faziam com que os acessos dos usuários resultassem também
    em um acesso aos bancos de dados da aplicação.

    Com o crescimento das redes sociais, jogos online e plataformas de
    discussão, a interatividade é extremamente dinâmica, o que resulta também
    em inúmeras requisições a bancos de dados. Neste contexto é necessário que
    as aplicações armazenem todos os dados gerados continuamente, tanto para
    consulta posterior quanto para uso em estatísticas em estudos estratégicos.

    O \texttt{Cassandra} por atender à distributividade acaba perdendo bastante
    desempenho na realização de consultas, o que normalmente faz com que
    implementações usando este SGBD sejam também construídas com diversas
    camadas de cache em outros SGBDs convencionais e que possuem
    dimensionamento e otimização possível para realização de leituras. Assim
    pode-se ter diferentes fatias de dados servidas à frente do
    \texttt{Cassandra} sob demanda, fornecendo aos usuários buscas rápidas de
    acordo com o interesse de cada um, ao mesmo tempo em que se mantém a
    totalidade de todo o conjunto de dados gerado pela aplicação, por mais
    volumoso que ele possa ser.

    Esta implementação, não por acaso, é o que está presente no que conhece-se
    publicamente das grandes estruturas de armazenamento de dados de redes
    sociais e outras aplicações da Internet que lidam com geração massiva de
    dados.

    O \texttt{Django} neste caso possui uma arquitetura bastante interessante,
    já que é possível aproveitar exatamente o mesmo esquema de dados que se
    utiliza para SGBDs relacionais no \texttt{Cassandra}. As operações de
    conversão das diferentes relações em famílias de colunas é suportada por
    uma implementação com o \texttt{Django-nonrel} a partr do mesmo arquivo
    \texttt{models.py} que se utiliza, por exemplo, em um \texttt{PostgreSQL}.

    Com isso temos a possibilidades de configurar uma instância do
    \texttt{Django} com um SGBD convencional para serviço com os usuários
    aramazenando e disponibilizando os dados em uma primeira instância, ao
    mesmo tempo que há uma replicação em segundo nível para bancos
    \texttt{NoSQL} -- assim como foi feito para este projeto. Uma arquitetura
    destas necessitaria também de rotinas de eliminação dos dados que estão nos
    bancos em primeira instância e buscas especializadas para recuperação de
    dados persistentes em \texttt{NoSQL}, disponibilizando aos usuários fatias
    não persistentes de dados que, apesar de não serem completas, são as que
    interessam a eles.

  \end{multicols}

\end{document}
