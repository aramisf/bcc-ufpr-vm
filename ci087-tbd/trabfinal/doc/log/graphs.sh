##!/bin/bash

# Write CPU

gnuplot << EOF
set terminal pngcairo color enhanced
set output "write-cpu.png"
set title "Uso de CPU pelos daemons"
set xlabel "Segundos"
set xrange [0:225]
set ylabel "CPU (%)"
set yrange [0:50]
plot \
    "write-mysql.log" using (\$0):1 with lines title "mysqld", \
    "write-cassandra.log" using (\$0):1 with lines title "jsvc.exe"
EOF

gnuplot << EOF
set terminal pngcairo color enhanced
set output "write-cpu-py.png"
set title "Uso de CPU pelo script Python"
set xlabel "Segundos"
set xrange [0:225]
set ylabel "CPU (%)"
set yrange [0:50]
plot \
    "write-mysql-py.log" using (\$0):1 with lines title "MySQL", \
    "write-cassandra-py.log" using (\$0):1 with lines title "Cassandra"
EOF

# Write MEM

gnuplot << EOF
set terminal pngcairo color enhanced
set output "write-mem.png"
set title "Uso de Memória pelos daemons"
set xlabel "Segundos"
set xrange [0:225]
set ylabel "Memória (Mbytes)"
set yrange [0:1400]
set format y "%.0f"
plot \
    "write-mysql.log" using (\$0):(\$2/1000) with lines title "mysqld", \
    "write-cassandra.log" using (\$0):(\$2/1000) with lines title "jsvc.exe"
EOF

gnuplot << EOF
set terminal pngcairo color enhanced
set output "write-mem-py.png"
set title "Uso de Memória pelo script Python"
set xlabel "Segundos"
set xrange [0:225]
set ylabel "Memória (Mbytes)"
set yrange [0:1400]
set format y "%.0f"
plot \
    "write-mysql-py.log" using (\$0):(\$2/1000) with lines title "MySQL", \
    "write-cassandra-py.log" using (\$0):(\$2/1000) with lines title "Cassandra"
EOF
