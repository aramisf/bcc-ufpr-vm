+ cassandra            # Projeto em Cassandra
  - cassandra          # Arquivos de conexao gerados pela Thrift API
  - cassandra.thrift   # Definicoes de interface com a Thrift API
  - django             # Django-nonrel 1.3.0 (baseado na 1.3.0 beta 1 do Django)
  - django_cassandra   # Aplicativo django_cassandra_backend revisao fc1ea10aa1
  - djangotoolbox      # Aplicativo djangotoolbox 0.9.2 para Django-nonrel
  + project            # Pasta de enclausuramento do projeto
    - app              # Link simbolico para common/app
    - settings.py      # Configuracoes do projeto

+ mysql                # Projeto em MySQL
  + django             # Django 1.3.0
  + project            # Pasta de enclausuramento do projeto
    - app              # Link simbolico para common/app
    - settings.py      # Configuracoes do projeto e de conexao no MySQL

+ common               # Projeto e Testes
  + app                # Aplicativo com o esquema de dados base
    - models.py        # Descricao do esquema de dados em objetos Python
  - tests_params.py    # Parametros para quantidade de operacoes a serem efetuadas
  - tests_write.py     # Script Python em console do Django para testes de escrita
  - tests_read.py      # Script Python em console do Django para testes de leitura
