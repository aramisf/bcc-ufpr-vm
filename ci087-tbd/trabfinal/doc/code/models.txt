class City(models.Model):
    name = models.CharField('Name',
        max_length = 255)
    state = models.ForeignKey(State)

class CompanyType(models.Model):
    type = models.CharField('Type',
        max_length = 255)

class Company(models.Model):
    name = models.CharField('Name',
        max_length = 255)
    type = models.ForeignKey(CompanyType)
    address = models.CharField('Address',
        max_length = 255)
    city = models.ForeignKey(City)
