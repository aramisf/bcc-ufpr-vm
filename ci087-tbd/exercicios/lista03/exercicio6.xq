(: Exercicio 6: Faça a juncao de items e bids por itemno. Retorne no
resultado: Userid, Description e itemno. :)

<bids>
{
    for $i in doc('items.xml')/items/item_tuple
    for $b in doc('bids.xml')/bids/bid_tuple
    where $i/itemno = $b/itemno
    return
    <bid>
        { $b/userid }
        { $i/description }
        { $i/itemno }
    </bid>
}
</bids>
