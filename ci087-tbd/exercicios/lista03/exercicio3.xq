{-- Exercicio 3 Selecionar todos os bids cujo itemno="1001". Retorne o
resultado no seguinte formato: --}

<result>
{
    for $b in $bids/bids/bid_tuple
    where $b/itemno = '1001'
    return
        <bid>
            <user>{ $b/userid }</user>
            <bid>{ $b/bid }</bid>
        </bid>
}
</result>
