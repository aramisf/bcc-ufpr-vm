(: Exercicio 4: Monte uma consulta que retorna todos os items (com sua
estrutura completa) que tem reserve_price=25 e foram oferecidos por U02. :)

for $i in doc('items.xml')/items/item_tuple
where $i/offered_by = 'U02'
    and $i/reserve_price = 25
return $i
