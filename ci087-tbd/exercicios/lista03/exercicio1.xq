(: Exercicio 1: Selecionar itemno e description dos itens onde offered_by =
"U01". Salve num arquivo chamado exercicio1.xq :)

<result>
{
    for $i in doc('items.xml')/items/item_tuple
    where $i/offered_by = 'U01'
    return
        <item>
            {$i/itemno}{$i/description}
        </item>
}
</result>
