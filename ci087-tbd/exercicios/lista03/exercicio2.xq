(: Exercicio 2: Monte uma consulta que traga todos os usuários, com name e
rating, onde o rating="B". As tags devem aparecer no resultado em portugues.
Salve num arquivo chamado exercicio2.xq :)

<result>
{
    for $u in doc('users.xml')/users/user_tuple
    where $u/rating = 'B'
    return
        <usuario>
            <nome>{ $u/name/text() }</nome>
            <nota>{ $u/rating/text() }</nota>
        </usuario>
}
</result>
