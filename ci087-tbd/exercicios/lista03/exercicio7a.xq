(:
    Exercício 7 (a) Faca uma consulta que retorne a media de bids de todos os
    usuarios (usar o documento bids)

    306
:)

let $b := doc('bids.xml')/bids/bid_tuple
return { avg($b/bid) }
