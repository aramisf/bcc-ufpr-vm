define global $imdb { treat as document
(document("/home/vinicius/bcc/ci087-tbd/exercicios/lista04/imdb.xml")) }

define element imdb {
	element movie*,
	element person*,
	element company*
}

define element movie {
	attribute mid,
	element title,
	element soundmix*,
	element language*,
	element runtime?,
	element certification*,
	element genre+,
	element color*,
	element plot?,
	element originCountry*,
	element alternativeTitle*,
	element plotkeyword*,
	element plotwrittenby?,
	element productionYear?,
	element kind?,
	element episodes?,
	element movie_award*,
	element related_qualifications?
}

define attribute mid {xsd:string}
define element title {xsd:string}
define element soundmix {xsd:string}
define element language {xsd:string}
define element runtime {xsd:int}
define element certification {xsd:string}
define element genre {xsd:string}
define element color {xsd:string}
define element plot {xsd:string}
define element originCountry {xsd:string}
define element alternativeTitle {xsd:string}
define element plotkeyword {xsd:string}
define element plotwrittenby {xsd:string}
define element productionYear {xsd:int}
define element kind {xsd:string}
define element episodes {xsd:int}
define element movie_award {
	attribute idref {xsd:string} }
define element related_qualifications {element qualification*}

define element person {
	attribute pid,
	element Name,
	element BirthCountry,
	element Birthdate,
	element Deathdate?,
	element Deathcountry?,
	element Illness?,
	element Spouse*,
	element Personalquotes*,
	element Height?,
	element Biography?,
	element BiographyBy?,
	element BiographyfromLeonard?,
	element Trivia?,
	element CreditAs*,
	element Birthname?,
	element NickName*,
	element Awards_won?,
	element with_qualification*,
	element participates_in*
}

define attribute pid {xsd:string}
define element Name {xsd:string}
define element BirthCountry {xsd:string}
define element Birthdate {xsd:string}
define element Deathdate {xsd:string}
define element Deathcountry {xsd:string}
define element Illness {xsd:string}
define element Spouse {xsd:string}
define element Personalquotes {xsd:string}
define element Height {xsd:string}
define element Biography {xsd:string}
define element BiographyBy {xsd:string}
define element BiographyfromLeonard {xsd:string}
define element Trivia {xsd:string}
define element CreditAs {xsd:string}
define element Birthname {xsd:string}
define element NickName {xsd:string}
define element Awards_won {element Award*}
define element with_qualification {
	attribute idref {xsd:string}}
define element participates_in {
	attribute idref {xsd:string} }

define element Award {
	attribute aid,
	element awardOrganization,
	element awardYear,
	element awardResult,
	element awardName,
	element awardCategory,
	element awardAbout*,
	element urlOrganization
}

define attribute aid {xsd:string}
define element awardOrganization {xsd:string}
define element awardYear {xsd:int}
define element awardResult {xsd:string}
define element awardName {xsd:string}
define element awardCategory {xsd:string}
define element awardAbout {xsd:string}
define element urlOrganization {xsd:string}

define element company {
	attribute cid,
	element companyname,
	element companykind,
	element generates_movie*
}

define attribute cid {xsd:string}
define element companyname {xsd:string}
define element companykind {xsd:string}
define element generates_movie {
	attribute idref {xsd:string} }

define element qualification {
	attribute qid,
	element description?,
	element period?,
	element episodename?,
	element episodenumber?,
	element roledate,
	element kind_ofqualification
}

define attribute qid {xsd:string}
define element description {xsd:string}
define element period {xsd:string}
define element episodename {xsd:string}
define element episodenumber {xsd:int}
define element roledate {xsd:string}
define element kind_ofqualification {xsd:string}
