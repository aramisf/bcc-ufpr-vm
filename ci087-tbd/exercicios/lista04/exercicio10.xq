(: 10. Para cada pessoa (nome como atributo), encontre a quantidade de filmes
em que ela atuou (participates_in) e que foram produzidos (productionYear)
depois de 1994.  (Agrupamento) :)

<results>
{
    let $movies := fn:doc('imdb.xml')/imdb/movie
    for $p in fn:doc('imdb.xml')/imdb/person
    return
        <person name="{ $p/Name/text() }">
        {
            count(
                for $m in $movies
                where $m/productionYear > 1994
                    and $p/participates_in/@idref = $m/@mid
                return $m/title
            )
        }
        </person>
}
</results>
