(: 15. Encontre as qualificacoes (qualifications) que nao tem um numero de
eposidio (episodenumber).  (Teste de vazio) :)

<results>
{
    for $m in fn:doc('imdb.xml')/imdb/movie
    where not($m/related_qualifications/qualifications/episodenumber)
    return $m/title
}
</results>
