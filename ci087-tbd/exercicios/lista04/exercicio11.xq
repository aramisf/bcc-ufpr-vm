(: 11. Para cada pessoa (nome como atributo) que nasceu nos Estados Unidos
(USA), encontre a quantidade de filmes em que ela atuou que foram produzidos
depois de 1994.  (Agrupamento) :)

<results>
{
    let $movies := fn:doc('imdb.xml')/imdb/movie
    for $p in fn:doc('imdb.xml')/imdb/person
    return
        <person name="{ $p/Name/text() }">
        {   count(
                for $m in $movies
                where $m/productionYear > 1994
                    and $p/participates_in/@idref = $m/@mid
                    and replace(replace($p/BirthCountry/text(), '.*,', ''), '^ +', '') = 'USA'
                return $m/title
            )
        }
        </person>
}
</results>
