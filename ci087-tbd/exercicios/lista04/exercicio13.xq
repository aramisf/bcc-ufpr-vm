(: 13. Encontre o nome de todos as pessoas que nasceram em agosto (August). :)

<results>
{
    let $people := fn:doc('imdb.xml')/imdb/person
    for $p in $people
    where contains($p/Birthdate/text(), 'August')
    return
        <person>
            { $p/Name/text() }
        </person>
}
</results>
