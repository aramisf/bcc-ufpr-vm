(: 5. Quantas linguas (language) distintas existem nos filmes do documento?
(Agregacao) :)

<results>
{
    for $m in count(distinct-values(fn:doc('imdb.xml')/imdb/movie/language/text()))
    return $m
}
</results>
