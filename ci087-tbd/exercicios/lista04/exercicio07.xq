(: 7. Encontre o nome das pessoas (como um atributo) e os papeis
(qualification) que elas interpretaram nos filmes em que atuaram.  (Juncao) :)

<results>
{
    for $p in fn:doc('imdb.xml')/imdb/person,
        $m in fn:doc('imdb.xml')/imdb/movie/related_qualifications
    where $m/qualification/@qid = $p/with_qualification/@idref
    return
        <person
            name="{ $p/Name/text() }"
            qualification="{ $m/qualification[@qid =
                $p/with_qualification/@idref]/kind_ofqualification/text() }" />
}
</results>
