(: 8. Encontre o nome das pessoas e os filmes (se houver) em que elas atuaram.
(Juncao externa a esquerda) :)

<results>
{
    for $p in fn:doc('imdb.xml')/imdb/person,
        $m in fn:doc('imdb.xml')/imdb/movie
    where $m/related_qualifications/qualification/@qid = $p/with_qualification/@idref
    return
        <person
            name="{ $p/Name/text() }"
            movie="{ $m/title/text() }"
            qualification="{ $m/related_qualifications/qualification[@qid =
                $p/with_qualification/@idref]/kind_ofqualification/text() }" />
}
</results>
