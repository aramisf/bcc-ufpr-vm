(: 2. Para os filmes (movie) que tem "sound mix", escrever o titulo (title) e
todos os seus "sound mix".  (Construcao de elemento) :)

<results>
{
    for $m in fn:doc("imdb.xml")/imdb/movie
    where $m/soundmix
    return
        <result>
            { $m/title }
            { $m/soundmix }
        </result>
}
</results>
