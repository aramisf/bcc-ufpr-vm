(: 3. Encontre os titulos de filmes com qualificacoes (qualifications) com um
numero de episodio maior que 400.  (Selecao complexa) :)

<results>
{
    for $m in fn:doc('imdb.xml')/imdb/movie
    where $m/related_qualifications/qualification/episodenumber > 400
    return $m/title
}
</results>
