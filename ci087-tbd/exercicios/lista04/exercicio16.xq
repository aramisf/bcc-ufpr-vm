(: 16. Liste em ordem alfabetica o nome de todas as pessoas, junto com o seu
pais de nascimento (BirthCountry).  (Ordenacao - sort by) :)

<results>
{
    for $p in fn:doc('imdb.xml')/imdb/person
    order by $p/Name/text()
    return
        <person>
            { $p/Name }
            { $p/BirthCountry }
        </person>
}
</results>
