--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- Registrador genérico sensível à borda de subida do clock
-- com possibilidade de inicialização de valor
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
library IEEE;
use IEEE.std_logic_1164.all;
use work.p_MI0.all;

entity reg is
    generic( 
        width : integer := 32
    );
    port(
        clock, reset:   in std_logic;
        in_signal:      in std_logic_vector(width-1 downto 0);
        out_signal:     out std_logic_vector(width-1 downto 0)
    );
end reg;

architecture arq_reg of reg is 
begin

    process(clock, reset)
    begin
        if reset = '1' then
            out_signal <= (others => '0');
        elsif clock'event and clock = '1' then
            out_signal <= in_signal; 
        end if;
    end process;

end arq_reg;
