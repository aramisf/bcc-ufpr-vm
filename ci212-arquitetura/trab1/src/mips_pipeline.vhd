library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_bit.all;
use work.p_MI0.all;

entity mips_pipeline is
    port (
        clk:   in std_logic;
        reset: in std_logic
    );
end mips_pipeline;

architecture arq_mips_pipeline of mips_pipeline is
   
    -- IF Signals
    signal IF_fetched_instruction,
           IF_instr,
           IF_pc,
           IF_MuxOut_PCSrc,
           IF_MuxOut_PCBranch,
           IF_pc4: reg32 := (others => '0');

    -- ID Signals
    signal ID_instr,
           ID_PC4: reg32;
    signal ID_opcode,
           ID_funct: std_logic_vector (5 downto 0);
    signal ID_rs,
           ID_rt,
           ID_rd: std_logic_vector (4 downto 0);
    signal ID_immed: std_logic_vector (15 downto 0);
    signal ID_A,
           ID_B: reg32;
    signal ID_RegWrite,
           ID_Branch,
           ID_RegDst,
           ID_MemtoReg,
           ID_MemRead,
           ID_MemWrite,
           ID_ALUSrc: std_logic;
    signal ID_ALUOp: std_logic_vector (1 downto 0);

    --! control_branch
    signal ID_BranchCtrl,
           ID_BranchRegWrite,
           ID_RegWriteOr: std_logic;
    signal ID_BranchRegDest: std_logic_vector (4 downto 0);
    signal ID_BranchRegData,
           ID_RegWriteData:reg32;
    signal ID_extend,
           ID_shifted_extend,
           ID_PC_offseted: reg32;
    
    --! control_branch muxes
    signal ID_MuxOut_RegDest: std_logic_vector (4 downto 0);
    signal ID_MuxOut_RegData: reg32;
    
    -- EX Signals
    signal EX_opcode: std_logic_vector (5 downto 0);
    signal EX_pc4,
           EX_A,
           EX_B: reg32;
    signal EX_alub,
           EX_ALUOut: reg32;
    signal EX_Rs,
           EX_Rt,
           EX_Rd: std_logic_vector (4 downto 0);
    signal EX_RegRd: std_logic_vector (4 downto 0);
    signal EX_funct: std_logic_vector (5 downto 0);
    signal EX_RegWrite,
           EX_Branch,
           EX_RegDst,
           EX_MemtoReg,
           EX_MemRead,
           EX_MemWrite,
           EX_ALUSrc: std_logic;
    signal EX_Zero: std_logic;
    signal EX_ALUOp: std_logic_vector (1 downto 0);
    signal EX_Operation: std_logic_vector (2 downto 0);

    --! control_branch
    signal EX_BranchCtrl: std_logic;
    signal EX_extend,
           EX_shifted_extend,
           EX_PC_offseted: reg32;

    --! control_forward
    signal EX_FWD_ALUSrcA,
           EX_FWD_ALUSrcB: std_logic_vector (1 downto 0);
    signal EX_ALUSrcA,
           EX_ALUSrcB: reg32;

   -- MEM Signals
    signal MEM_PCSrc: std_logic_vector (1 downto 0);
    signal MEM_RegWrite,
           MEM_Branch,
           MEM_MemtoReg,
           MEM_MemRead,
           MEM_MemWrite,
           MEM_Zero: std_logic;
    signal MEM_PC_offseted,
           MEM_ALUOut,
           MEM_B: reg32;
    signal MEM_memout: reg32;
    signal MEM_RegRd: std_logic_vector (4 downto 0);
    signal MEM_Operation: std_logic_vector (2 downto 0);
   
    -- WB Signals
    signal WB_RegWrite,
           WB_MemtoReg: std_logic;
    signal WB_MemOut,
           WB_ALUOut: reg32;
    signal WB_WD: reg32;
    signal WB_RegRd: std_logic_vector (4 downto 0);

begin

    -- IF Stage

    -- registrador de pc
    IF_PC_REG: entity work.reg port map (
        clk, reset,
        IF_MuxOut_PCBranch,
        IF_pc
    );

    -- memória de instruções
    IF_INST_ROM: entity work.rom32 port map (
        IF_pc,
        IF_fetched_instruction
    );

    --! mux de seleção da nova instrução
    --! control_branch: gera um nop se houver desvio identificado em EX
    IF_MUX2_BRANCH: entity work.mux2 port map (
        EX_BranchCtrl,
        IF_fetched_instruction,
        "00000000000000000000000000100000", -- add $0, $0, $0
        IF_instr
    );

    -- somador PC + 4
    IF_PC4_ADD: entity work.add32 port map (
        IF_pc, x"00000004",
        IF_pc4
    );
    
    --! mux de seleção do novo PC
    --! control_branch: ativa a seleção de EX_PC_offseted se há desvio
    IF_MUX2_EX_BRANCH: entity work.mux2 port map (
        EX_BranchCtrl,
        IF_MuxOut_PCSrc, EX_PC_offseted,
        IF_MuxOut_PCBranch
    );

    --! mux de seleção do novo PC
    --! JR: envia o novo valor de PC do estágio MEM
    IF_MUX3_PCSRC: entity work.mux3 port map (
        MEM_PCSrc,
        IF_pc4, MEM_PC_offseted, MEM_ALUOut,
        IF_MuxOut_PCSrc
    );

    -- IF/ID Pipeline
    IF_ID_PIPELINE: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                ID_instr <= (others => '0');
                ID_pc4   <= (others => '0');
            else
                ID_instr <= IF_instr;
                ID_pc4   <= IF_pc4;
            end if;
        end if;
    end process;

    -- ID Stage

    ID_opcode <= ID_instr (31 downto 26);
    ID_rs     <= ID_instr (25 downto 21);
    ID_rt     <= ID_instr (20 downto 16);
    ID_rd     <= ID_instr (15 downto 11);
    ID_immed  <= ID_instr (15 downto 0);
  
    -- extensor de sinais
    ID_EXTENSOR: process (ID_immed)
    begin
        if ID_immed(15) = '1' then
            ID_extend <= x"FFFF" & ID_immed (15 downto 0);
        else
            ID_extend <= x"0000" & ID_immed (15 downto 0);
        end if;
    end process;

    --! deslocador de dois bits
    --! trocados os nomes dos sinais
    ID_SHIFT_EXTEND: entity work.shift_left port map (
        ID_extend, 2,
        ID_shifted_extend
    );

    --! deslocador de dois bits + PC + 4
    --! trocados os nomes dos sinais
    ID_PC4_OFFSET: entity work.add32 port map (
        ID_PC4, ID_shifted_extend,
        ID_PC_offseted
    );

    --! banco de registradores
    --! JAL: modificada a entrada do dado a ser gravado: `input write`
    ID_REG_BANK: entity work.reg_bank port map (
        clk, reset, ID_RegWriteOr,                         -- input signal
        ID_rs, ID_rt,                                      -- input read
        ID_MuxOut_RegDest,                                 -- input dest
        ID_A, ID_B,                                        -- output read
        ID_MuxOut_RegData                                  -- input write
    );

    --! porta OR para ativar a gravação no banco de registradores
    --! JAL: control_branch ativa o sinal
    ID_RegWriteOr <=
        '1' when ID_BranchRegWrite = '1' or WB_RegWrite = '1' else
        '0';

    --! mux de seleção do registrador de destino
    --! JAL: control_branch envia o sinal $31
    ID_MuxOut_RegDest <=
        ID_BranchRegDest when ID_BranchRegWrite = '1' else
        WB_RegRd;

    --! mux de seleção do dado a ser gravado no registrador
    --! JAL: control_branch envia o endereço de PC4 em ID_BranchRegData
    ID_MUX2_REG_DATA: entity work.mux2 port map (
        ID_BranchRegWrite,                                 -- input selector
        WB_WD, ID_BranchRegData,                           -- input options
        ID_MuxOut_RegData                                  -- output
    );

    --! unidade de controle
    --! control_branch: retirado o sinal branch não mais necessário
    ID_CTRL: entity work.control_pipeline port map (
        ID_opcode,                                         -- input
        ID_RegDst, ID_ALUSrc, ID_MemtoReg, ID_RegWrite,    -- output
        ID_MemRead, ID_MemWrite, ID_ALUOp
    );

    --! control_branch: unidade de controle de desvios
    ID_CTRL_BRANCH: entity work.control_branch port map (
        ID_opcode, ID_pc4, ID_A, ID_B,                     -- input
        ID_BranchCtrl, ID_BranchRegWrite,                  -- output signals
        ID_BranchRegDest, ID_BranchRegData                 -- output data
    );

    -- ID/EX Pipeline
    ID_EX_PIPELINE: process(clk)
    begin
        if rising_edge(clk) then

            if reset = '1' then
                EX_RegDst         <= '0';
                EX_ALUOp          <= (others => '0');
                EX_ALUSrc         <= '0';
                EX_Branch         <= '0';
                EX_MemRead        <= '0';
                EX_MemWrite       <= '0';
                EX_RegWrite       <= '0';
                EX_MemtoReg       <= '0';

                EX_pc4            <= (others => '0');
                EX_opcode         <= (others => '0');
                EX_A              <= (others => '0');
                EX_B              <= (others => '0');
                EX_extend         <= (others => '0');
                EX_Rs             <= (others => '0');
                EX_Rt             <= (others => '0');
                EX_Rd             <= (others => '0');

                --! control_branch: reset de sinais
                EX_BranchCtrl     <= '0';
                EX_extend         <= (others => '0');
                EX_shifted_extend <= (others => '0');
                EX_PC_offseted    <= (others => '0');

            else 
                EX_RegDst         <= ID_RegDst;
                EX_ALUOp          <= ID_ALUOp;
                EX_ALUSrc         <= ID_ALUSrc;
                EX_Branch         <= ID_Branch;
                EX_MemRead        <= ID_MemRead;
                EX_MemWrite       <= ID_MemWrite;
                EX_RegWrite       <= ID_RegWrite;
                EX_MemtoReg       <= ID_MemtoReg;
      
                EX_pc4            <= ID_pc4;
                EX_opcode         <= ID_opcode;
                EX_A              <= ID_A;
                EX_B              <= ID_B;
                EX_rs             <= ID_rs;
                EX_rt             <= ID_rt;
                EX_rd             <= ID_rd;

                --! control_branch: passagem de sinais
                EX_BranchCtrl     <= ID_BranchCtrl;
                EX_extend         <= ID_extend;
                EX_shifted_extend <= ID_shifted_extend;
                EX_PC_offseted    <= ID_PC_offseted;

            end if;
        end if;
    end process;

    -- EX Stage
    
    -- função da ALU
    EX_funct <= EX_extend(5 downto 0);
    
    -- mux seletor da entrada B da alu
    EX_ALU_MUX_B: entity work.mux2 port map (
        EX_ALUSrc,
        EX_B, EX_extend,
        EX_ALUB
    );
    
    -- ALU
    EX_ALU: entity work.alu port map (
        EX_Operation,
        EX_ALUSrcA, EX_ALUSrcB,
        EX_ALUOut,
        EX_Zero
    );
    
    -- registrador de destino
    EX_MUX2_REG_DEST: entity work.mux2 generic map (5) port map (
        EX_RegDst,
        EX_Rt, EX_Rd,
        EX_RegRd
    );
    
    -- unidade de controle da alu
    EX_CTRL_ALU: entity work.alu_ctl port map (
        EX_ALUOp, EX_funct,
        EX_Operation
    );

    --! mux para adiantamento na entrada A da ALU
    --! control_forward: sinal de seleção vem da unidade de desvios
    EX_MUX3_FWD_A: entity work.mux3 port map (
        EX_FWD_ALUSrcA,
        EX_A, MEM_ALUOut, WB_MemOut,
        EX_ALUSrcA
    );

    --! mux para adiantamento na entrada B da ALU
    --! control_forward: sinal de seleção vem da unidade de desvios
    EX_MUX3_FWD_B: entity work.mux3 port map (
        EX_FWD_ALUSrcB,
        EX_ALUB, MEM_ALUOut, WB_MemOut,
        EX_ALUSrcB
    );

    --! control_forward: unidade de controle de adiantamento
    EX_CTRL_FORWARD: entity work.control_forward port map (
        EX_opcode,                                         -- input opcode
        MEM_RegWrite, WB_RegWrite,                         -- input signals
        EX_Rs, EX_Rt, MEM_RegRd, WB_RegRd,                 -- input registers
        EX_FWD_ALUSrcA, EX_FWD_ALUSrcB                     -- output signals
    );

    -- EX/MEM Pipeline
    EX_MEM_PIPELINE: process (clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                MEM_Branch      <= '0';
                MEM_MemRead     <= '0';
                MEM_MemWrite    <= '0';
                MEM_RegWrite    <= '0';
                MEM_MemtoReg    <= '0';
                MEM_Zero        <= '0';

                MEM_PC_offseted <= (others => '0');
                MEM_ALUOut      <= (others => '0');
                MEM_B           <= (others => '0');
                MEM_RegRd       <= (others => '0');
            else
                MEM_Branch      <= EX_Branch;
                MEM_MemRead     <= EX_MemRead;
                MEM_MemWrite    <= EX_MemWrite;
                MEM_RegWrite    <= EX_RegWrite;
                MEM_MemtoReg    <= EX_MemtoReg;
                MEM_Zero        <= EX_Zero;
                
                --! sinal de operação da ALU
                --! JR: uso do valor para definir o desvio
                MEM_Operation   <= EX_Operation;

                MEM_PC_offseted <= EX_PC_offseted;
                MEM_ALUOut      <= EX_ALUOut;
                MEM_B           <= EX_B;
                MEM_RegRd       <= EX_RegRd;
            end if;
        end if;
    end process;

    -- MEM Stage
    
    -- memória
    MEM_ACCESS: entity work.mem32 port map (
        clk, MEM_MemRead, MEM_MemWrite,                    -- input
        MEM_ALUOut, MEM_B,
        MEM_memout                                         -- output
    );
    
    --! mux de seleção do PCSrc
    MEM_PCSrc <=
        "01" when MEM_Branch = '1' and MEM_Zero = '1' else
        "10" when MEM_Operation = JR else
        "00";
  
    -- MEM/WB Pipeline
    MEM_WB_PIPELINE: process (clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                WB_RegWrite <= '0';
                WB_MemtoReg <= '0';
                WB_ALUOut   <= (others => '0');
                WB_MemOut   <= (others => '0');
                WB_RegRd    <= (others => '0');
            else
                WB_RegWrite <= MEM_RegWrite;
                WB_MemtoReg <= MEM_MemtoReg;
                WB_ALUOut   <= MEM_ALUOut;
                WB_MemOut   <= MEM_MemOut;
                WB_RegRd    <= MEM_RegRd;
            end if;
        end if;
    end process;       

    -- WB Stage
    
    -- mux de seleção do dado a ser gravado
    WB_MUX2_DEST: entity work.mux2 port map (
        WB_MemtoReg,                                       -- input signal
        WB_ALUOut, WB_MemOut,                              -- input options
        WB_WD                                              -- output
    );

end arq_mips_pipeline;

