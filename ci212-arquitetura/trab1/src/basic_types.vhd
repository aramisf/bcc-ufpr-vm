library IEEE;
use IEEE.Std_Logic_1164.all;

package p_MI0 is
  
    subtype reg32 is   std_logic_vector (31 downto 0);

    constant ADDU:     std_logic_vector (2 downto 0) := "010";
    constant SUBU:     std_logic_vector (2 downto 0) := "110";
    constant AAND:     std_logic_vector (2 downto 0) := "000";
    constant OOR:      std_logic_vector (2 downto 0) := "001";
    constant SLT:      std_logic_vector (2 downto 0) := "111";
    constant JR:       std_logic_vector (2 downto 0) := "100"; --! JR

    constant R_FORMAT: std_logic_vector (5 downto 0) := "000000";
    constant LW:       std_logic_vector (5 downto 0) := "100101";
    constant SW:       std_logic_vector (5 downto 0) := "101011";
    constant BEQ:      std_logic_vector (5 downto 0) := "000100";
    constant ADDI:     std_logic_vector (5 downto 0) := "001000"; --! ADDI
    constant JAL:      std_logic_vector (5 downto 0) := "000011"; --! JAL

    type microinstruction is record
        ce:    std_logic;
        rw:    std_logic;
        wreg:  std_logic;
    end record;

end p_MI0;
