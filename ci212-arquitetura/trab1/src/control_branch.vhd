library IEEE;
use IEEE.std_logic_1164.all;
use work.p_MI0.all;

entity control_branch is
    port(
        opcode:        in  std_logic_vector(5 downto 0);
        PC4:           in  reg32;
        RegRs:         in  reg32;
        RegRt:         in  reg32;
        BranchCtrl:    out std_logic;
        BranchRegWr:   out std_logic;
        BranchRegDest: out std_logic_vector(4 downto 0);
        BranchRegData: out reg32;
        BranchPC:      out reg32;
        BranchBTGT:    out reg32
    );
end control_branch;

architecture arq_control_branch of control_branch is

    signal comp: std_logic;
    signal btgt: reg32;

begin
   
    comp <=
        '1' when RegRs = RegRt else
        '0';

    process (opcode)
    begin

        BranchBTGT <= btgt;

        case opcode is

            when JAL =>
                BranchCtrl    <= '1';
                BranchRegWr   <= '1';
                BranchRegDest <= "11111";
                BranchRegData <= PC4;

            when BEQ =>
                BranchCtrl    <= comp;
                BranchRegWr   <= '0';
                BranchRegDest <= (others => 'X');
                BranchRegData <= (others => 'X');

            when others =>
                BranchCtrl    <= '0';
                BranchRegWr   <= '0';
                BranchRegDest <= (others => 'X');
                BranchRegData <= (others => 'X');

        end case;
    end process;
end arq_control_branch;
