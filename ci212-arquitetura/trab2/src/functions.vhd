library ieee;
library std;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.ALL;

package functions is
    function hex_to_logic_vector(hex: string) return std_logic_vector;
end functions;
package body functions is

    function hex_to_logic_vector(hex: string) return std_logic_vector is
        variable vector: std_logic_vector(31 downto 0);
        variable c: character;
        variable j, k: integer;
    begin
        for i in 1 to hex'length loop
            c := hex(i);
            j := 31 - 4 * (i-1);
            k := j - 3;
            case c is
                when '0' => vector(j downto k) := "0000";
                when '1' => vector(j downto k) := "0001";
                when '2' => vector(j downto k) := "0010";
                when '3' => vector(j downto k) := "0011";
                when '4' => vector(j downto k) := "0100";
                when '5' => vector(j downto k) := "0101";
                when '6' => vector(j downto k) := "0110";
                when '7' => vector(j downto k) := "0111";
                when '8' => vector(j downto k) := "1000";
                when '9' => vector(j downto k) := "1001";
                when 'a' => vector(j downto k) := "1010";
                when 'b' => vector(j downto k) := "1011";
                when 'c' => vector(j downto k) := "1100";
                when 'd' => vector(j downto k) := "1101";
                when 'e' => vector(j downto k) := "1110";
                when 'f' => vector(j downto k) := "1111";
                when others => vector(j downto k) := "XXXX";
            end case;
        end loop;
        return vector;
    end hex_to_logic_vector;

end functions;
