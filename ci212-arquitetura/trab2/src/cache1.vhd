library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity ent_cache1 is
    port(
        clock:      in  std_logic;
        address:    in  std_logic_vector(31 downto 0);
        hit:        out std_logic;
        pos:        out integer
    );
end ent_cache1;

architecture arq_cache1 of ent_cache1 is

    -- Cache de 8Kbytes com 1 via = 8Kbytes
    constant cache1_size: integer := (8 * 1024);
    type cache1_type
        is array (0 to cache1_size-1)
        of std_logic_vector(14 downto 0);

    signal cache1_data: cache1_type;

begin

    process
        variable tag: std_logic_vector(13 downto 0);
        variable index: std_logic_vector(12 downto 0);
        
        variable n: integer;
    begin

        wait until clock = '1';

        --  31           18 17               5 4       2 1     0
        -- +---------------+------------------+---------+-------+
        -- |    14bits     |      13bits      |  3bits  | 2bits |
        -- +---------------+------------------+---------+-------+
        --       TAG              INDEX          BLOCO
        --                       8Kbytes

        tag := address(31 downto 18);
        index := address(17 downto 5);
        n := conv_integer(index);
        
        -- Acerto
        
        if cache1_data(n) = "1" & tag then
            hit <= '1';
        
        -- Falha
        
        else
            cache1_data(n) <= "1" & tag;
            hit <= '0';
            pos <= n;
        end if;

    end process;

end arq_cache1;
