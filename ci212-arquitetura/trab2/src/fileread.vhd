library ieee;
library std;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.std_logic_arith.ALL;
use std.textio.all;
use work.functions.all;

entity ent_fileread is
    port(
        clock:   in  std_logic;
        address: out std_logic_vector(31 downto 0)
    );
end ent_fileread;

architecture arq_fileread of ent_fileread is
    file filehandler: text open READ_MODE is "input.multmatrix.265.dat";
begin

    process
        variable inline: line;
        variable line_string: string (1 to 8);
    begin
               
        while NOT (endfile(filehandler)) loop

            wait until clock = '1';
          
            readline(filehandler, inline);
            read(inline, line_string(1 to line_string'length));
            address <= hex_to_logic_vector(line_string);

        end loop;

        wait until clock = '1';

    end process;

end arq_fileread;
