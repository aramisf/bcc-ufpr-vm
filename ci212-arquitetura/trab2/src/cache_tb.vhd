library ieee;
use ieee.std_logic_1164.all;

entity cache_tb is
end cache_tb;

architecture arq_cache_tb of cache_tb is

    component datapath is
        port(
            clock: in std_logic;
            reset: in std_logic
        );
    end component datapath;

    signal clock: std_logic := '0';
    signal reset: std_logic;

begin

    clock <= not clock after 200 ns;
    reset <= '1', '0'  after 250 ns;
    cache: datapath port map (clock, reset);

end arq_cache_tb;
