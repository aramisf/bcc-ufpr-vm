#include <stdlib.h>
#include <stdio.h>

#define MATRIX_SIZE 265

int main (void) {
	
	int a[MATRIX_SIZE][MATRIX_SIZE];
	int b[MATRIX_SIZE][MATRIX_SIZE];
	int c[MATRIX_SIZE][MATRIX_SIZE];
	
	register int i, j, k, sum;
	
	for (i = 0; i < MATRIX_SIZE; i++) {
		for (j = 0; j < MATRIX_SIZE; j++) {

			for (k = 0; k < MATRIX_SIZE; k++) {
				//2 memory accesses here
				printf ("%x\n", &(a[i][k]));
				printf ("%x\n", &(b[k][j]));
				sum = sum + a[i][k] * b[k][j];
			}
			
			//1 memory access
			printf ("%x\n", &(c[i][j]));
			c[i][j] = sum;
		}
	}
}
