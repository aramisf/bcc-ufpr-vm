Use esses dois programas para gerar os enderecos de acesso a cache. Para cada
acesso, por exemplo: x[i] = 1;

Imprima o endereco acessado em um arquivo: printf ("%x\n", &(x[i]));

Ao final, leia os valores atraves do testbench, gerando uma escrita ou uma
leitura dependendo da proporcao de escritas e leituras dadas por estatisticas
mostradas nas aulas e no livro da disciplina.
