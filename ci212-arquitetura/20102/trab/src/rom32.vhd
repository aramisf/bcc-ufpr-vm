library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use work.p_MI0.all;



entity rom32 is
	port (
		address: in reg32;
		data_out: out reg32
	);
end rom32;

architecture arq_rom32 of rom32 is

	signal mem_offset: std_logic_vector(5 downto 0);
	signal address_select: std_logic;
	

begin
	mem_offset <= address(7 downto 2);
        
	add_sel: process(address)
	begin
		if address(31 downto 8) = x"000000" then 	
			address_select <= '1';
		else
			address_select <= '0';
		end if;
	end process;

	access_rom: process(address_select, mem_offset)
	begin
		if address_select = '1' then
			case mem_offset is

				when 	"000000" => data_out <= "100000" & "00000" & "00001" & x"000a"; -- addi $1, $0, 10
				when 	"000001" => data_out <= "000000" & "00000" & "00000" & "00000" & "00000" & "100000"; -- nop
				when 	"000010" => data_out <= "000000" & "00000" & "00000" & "00000" & "00000" & "100000"; -- nop
				when 	"000011" => data_out <= "100000" & "00001" & "00001" & x"0014"; -- addi $1, $1, 20
				when 	"000100" => data_out <= "000000" & "00000" & "00000" & "00000" & "00000" & "100000"; -- nop
				when 	"000101" => data_out <= "000000" & "00000" & "00000" & "00000" & "00000" & "100000"; -- nop
				when 	"000110" => data_out <= "100000" & "00001" & "00001" & x"001e"; -- addi $1, $1, 30
				when 	"000111" => data_out <= "000000" & "00000" & "00000" & "00000" & "00000" & "100000"; -- nop
				when 	"001000" => data_out <= "000000" & "00000" & "00000" & "00000" & "00000" & "100000"; -- nop
				when 	"001001" => data_out <= "100000" & "01010" & "00010" & x"001e"; -- addi $2, $10, 30
				when 	"001010" => data_out <= "000000" & "00000" & "00000" & "00000" & "00000" & "100000"; -- nop
				when 	"001011" => data_out <= "000000" & "00000" & "00000" & "00000" & "00000" & "100000"; -- nop
				when 	"001100" => data_out <= "110000" & "00001" & "00000" & "00000" & "00000" & "011111"; -- jr $1
				when 	"001101" => data_out <= "000000" & "00000" & "00000" & "00000" & "00000" & "100000"; -- nop
				when 	"001110" => data_out <= "000000" & "00000" & "00000" & "00000" & "00000" & "100000"; -- nop
				when 	"001111" => data_out <= "000000" & "00000" & "00000" & "00000" & "00000" & "100000"; -- nop
				when 	others  => data_out <= (others => 'X');
			end case;
    		end if;
  	end process; 

end arq_rom32;
