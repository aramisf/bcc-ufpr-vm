#!/bin/bash

cd rom
./generate_rom.sh prog.asm
cp rom32.vhd ..
cd ..

ghdl -a --ieee=synopsys -fexplicit \
    basic_types.vhd \
    reg.vhd \
    add32.vhd \
    mux2.vhd \
    rom32.vhd \
    reg32_ce.vhd \
    reg_bank.vhd \
    control_pipeline.vhd \
    shift_left.vhd \
    alu.vhd \
    alu_ctl.vhd \
    mem32.vhd \
    mips_pipeline.vhd \
    mips_tb.vhd

ghdl -e --ieee=synopsys -fexplicit mips_tb 2> /dev/null
ghdl -r mips_tb --vcd=mips.vcd --stop-time=100000ns 2> /dev/null
rm -f *.o *.cf 2> /dev/null
gtkwave mips.vcd 2> /dev/null
