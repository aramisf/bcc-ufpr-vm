library IEEE;
use IEEE.std_logic_1164.all;
use work.p_MI0.all;

entity control_pipeline is
	port (
        opcode:   in  std_logic_vector(5 downto 0);
        RegDst:   out std_logic;
        ALUSrc:   out std_logic;
        MemtoReg: out std_logic;
        RegWrite: out std_logic;
        MemRead:  out std_logic;
        MemWrite: out std_logic;
        Branch:   out std_logic;
        ALUOp:    out std_logic_vector(1 downto 0)
    );
end control_pipeline;


architecture arq_control_pipeline of control_pipeline is

    --input [5:0] opcode;
    --output RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch;
    --output [1:0] ALUOp;
    --reg    RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch;
    --reg    [1:0] ALUOp;

begin

    process (opcode)
    begin
    case opcode is

        -- R Type
        when R_FORMAT =>
            RegDst   <= '1';
            ALUSrc   <= '0';
            MemtoReg <= '0';
            RegWrite <= '1';
            MemRead  <= '0';
            MemWrite <= '0';
            Branch   <= '0';
            ALUOp    <= "10";

        -- LW
        when LW =>
            RegDst   <= '0';
            ALUSrc   <= '1';
            MemtoReg <= '1';
            RegWrite <= '1';
            MemRead  <= '1';
            MemWrite <= '0';
            Branch   <= '0';
            ALUOp    <= "00";

        -- SW
        when SW =>
            RegDst   <= 'X';
            ALUSrc   <= '1';
            MemtoReg <= 'X';
            RegWrite <= '0';
            MemRead  <= '0';
            MemWrite <= '1';
            Branch   <= '0';
            ALUOp    <= "00";

        -- BEQ
        when BEQ =>
            RegDst   <= 'X';
            ALUSrc   <= '0';
            MemtoReg <= 'X';
            RegWrite <= '0';
            MemRead  <= '0';
            MemWrite <= '1';
            Branch   <= '1';
            ALUOp    <= "01";

        --! ADDI Atribuição dos sinais de controle
        when ADDI =>
            RegDst   <= '0';  --! ADDI Registrador de destino é o 'rt'
            ALUSrc   <= '1';  --! ADDI
            MemtoReg <= '0';  --! ADDI Os dados a serem escritos vêm da ALU
            RegWrite <= '1';  --! ADDI O resultado será escrito em registrador
            MemRead  <= '0';  --! ADDI Nada será lido da memória
            MemWrite <= '0';  --! ADDI Nada será escrito na memória
            Branch   <= '0';  --! ADDI Não haverá desvio
            ALUOp    <= "00"; --! ADDI Operação de soma na ALU [alu_ctl.vhd]

        --! JR Atribuição dos sinais de controle
        when JR =>
            RegDst   <= 'X';  --! JR Não há registrador de destino
            ALUSrc   <= '0';  --! JR
            MemtoReg <= 'X';  --! JR Não há escrita em registrador
            RegWrite <= '0';  --! JR Não há escrita em registrador
            MemRead  <= '0';  --! JR Nada será lido da memória
            MemWrite <= '1';  --! JR Ocorrerá uma escrita no PC
            Branch   <= '1';  --! JR Haverá um desvio
            ALUOp    <= "01"; --! JR Usa a função da ALU para esta instrução

        -- NOP
        when others =>
            RegDst   <= '0';
            ALUSrc   <= '0';
            MemtoReg <= '0';
            RegWrite <= '0';
            MemRead  <= '0';
            MemWrite <= '0';
            Branch   <= '0';
            ALUOp    <= "00";

	end case;
    end process;

end arq_control_pipeline;
