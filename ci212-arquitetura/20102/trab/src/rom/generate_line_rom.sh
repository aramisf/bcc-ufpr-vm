#!/bin/bash




function fill_zero() {

	WORD=$1
	NUM=$(expr length $WORD)



	while (( NUM < $2 ))
	do
        	WORD="0$WORD"
        	NUM=$((NUM + 1))
	done

	echo "$WORD"
}



vetor=()

i=0
for j in $*
do
	vetor[$i]=$j
	i=$((i + 1))
done



INST=${vetor[0]}




case $INST in
lw*)

	ADDRESS=${vetor[3]}
	BOP="100101"
  	RT=$(echo "${vetor[1]}"| sed -e "s/\$\([0-9]*\)/\1/g")
  	IMM=$(echo "${vetor[2]}"| sed -e "s/\(^-*[0-9]*\)(.*/\1/g")
  	RS=$(echo "${vetor[2]}"| sed -e "s/-*[0-9]*(\$\([0-9]*\))/\1/g")

	BRT=$(fill_zero $(echo "ibase=10;obase=2;$RT" | bc) 5)
	BRS=$(fill_zero $(echo "ibase=10;obase=2;$RS" | bc) 5)
	BIMM=$(./compl_num $IMM)
	BIMM=$(fill_zero $BIMM 4)

	echo "when 	\"$(fill_zero $(echo "ibase=10;obase=2;$ADDRESS" | bc) 6)\" => data_out <= \"$BOP\" & \"$BRS\" & \"$BRT\" & x\"$BIMM\"; -- lw \$$RT, $IMM(\$$RS)"


  	;;
sw*)

	ADDRESS=${vetor[3]}
	BOP="101011"
  	RT=$(echo "${vetor[1]}"| sed -e "s/\$\([0-9]*\)/\1/g")
  	IMM=$(echo "${vetor[2]}"| sed -e "s/\(^-*[0-9]*\)(.*/\1/g")
  	RS=$(echo "${vetor[2]}"| sed -e "s/-*[0-9]*(\$\([0-9]*\))/\1/g")

	BRT=$(fill_zero $(echo "ibase=10;obase=2;$RT" | bc) 5)
	BRS=$(fill_zero $(echo "ibase=10;obase=2;$RS" | bc) 5)
	BIMM=$(./compl_num $IMM)
	BIMM=$(fill_zero $BIMM 4)

	echo "when 	\"$(fill_zero $(echo "ibase=10;obase=2;$ADDRESS" | bc) 6)\" => data_out <= \"$BOP\" & \"$BRS\" & \"$BRT\" & x\"$BIMM\"; -- sw \$$RT, $IMM(\$$RS)"


  	;;

addi*)
	ADDRESS=${vetor[4]}
	BOP="100000"    # --! ADDI Modificado o opcode da instrução
	RT=$(echo "${vetor[1]}"| sed -e "s/\$\([0-9]*\)/\1/g")
	RS=$(echo "${vetor[2]}"| sed -e "s/\$\([0-9]*\)/\1/g")
 	IMM=$(echo "${vetor[3]}"| sed -e "s/\(^-*[0-9]*\)(.*/\1/g")

	BRT=$(fill_zero $(echo "ibase=10;obase=2;$RT" | bc) 5)
	BRS=$(fill_zero $(echo "ibase=10;obase=2;$RS" | bc) 5)
	BIMM=$(./compl_num $IMM)
	BIMM=$(fill_zero $BIMM 4)

	echo "when 	\"$(fill_zero $(echo "ibase=10;obase=2;$ADDRESS" | bc) 6)\" => data_out <= \"$BOP\" & \"$BRS\" & \"$BRT\" & x\"$BIMM\"; -- addi \$$RT, \$$RS, $IMM"

  	;;

# --! JR Adicionado o parse para esta instrução
jr*)
	ADDRESS=${vetor[2]}
	BOP="110000"                                            # --! Opcode escolhido para a instrução
	RS=$(echo "${vetor[1]}"| sed -e "s/\$\([0-9]*\)/\1/g")  # --! Parse para 'rs'
    RT="00000"                                              # --! 'rt' não será usado
    RD="00000"                                              # --! 'rd' não será usado
	SHAMT="00000"                                           # --! 'shamt' não será usado
	FUNCT="011111"                                          # --! 'funct' habilita a operação na ALU

	BRS=$(fill_zero $(echo "ibase=10;obase=2;$RS" | bc) 5)
	BRT=$(fill_zero $(echo "ibase=10;obase=2;$RT" | bc) 5)
	BRD=$(fill_zero $(echo "ibase=10;obase=2;$RD" | bc) 5)

	echo "when 	\"$(fill_zero $(echo "ibase=10;obase=2;$ADDRESS" | bc) 6)\" => data_out <= \"$BOP\" & \"$BRS\" & \"$BRT\" & \"$BRD\" & \"$SHAMT\" & \"$FUNCT\"; -- jr \$$RS"

  	;;


add*)

	ADDRESS=${vetor[4]}
	BOP="000000"
  	RD=$(echo "${vetor[1]}"| sed -e "s/\$\([0-9]*\)/\1/g")
	RS=$(echo "${vetor[2]}"| sed -e "s/\$\([0-9]*\)/\1/g")
	RT=$(echo "${vetor[3]}"| sed -e "s/\$\([0-9]*\)/\1/g")
	SHAMT="00000"
	FUNCT="100000"

	BRD=$(fill_zero $(echo "ibase=10;obase=2;$RD" | bc) 5)
	BRS=$(fill_zero $(echo "ibase=10;obase=2;$RS" | bc) 5)
	BRT=$(fill_zero $(echo "ibase=10;obase=2;$RT" | bc) 5)

	echo "when 	\"$(fill_zero $(echo "ibase=10;obase=2;$ADDRESS" | bc) 6)\" => data_out <= \"$BOP\" & \"$BRS\" & \"$BRT\" & \"$BRD\" & \"$SHAMT\" & \"$FUNCT\"; -- add \$$RD, \$$RS, \$$RT"

  	;;
slt*)
	ADDRESS=${vetor[4]}
	BOP="000000"
  	RD=$(echo "${vetor[1]}"| sed -e "s/\$\([0-9]*\)/\1/g")
	RS=$(echo "${vetor[2]}"| sed -e "s/\$\([0-9]*\)/\1/g")
	RT=$(echo "${vetor[3]}"| sed -e "s/\$\([0-9]*\)/\1/g")
	SHAMT="00000"
	FUNCT="101010"

	BRD=$(fill_zero $(echo "ibase=10;obase=2;$RD" | bc) 5)
	BRS=$(fill_zero $(echo "ibase=10;obase=2;$RS" | bc) 5)
	BRT=$(fill_zero $(echo "ibase=10;obase=2;$RT" | bc) 5)

	echo "when 	\"$(fill_zero $(echo "ibase=10;obase=2;$ADDRESS" | bc) 6)\" => data_out <= \"$BOP\" & \"$BRS\" & \"$BRT\" & \"$BRD\" & \"$SHAMT\" & \"$FUNCT\"; -- slt \$$RD, \$$RS, \$$RT"

  	;;
beq*)
	ADDRESS=${vetor[4]}
	BOP="000100"
	RS=$(echo "${vetor[1]}"| sed -e "s/\$\([0-9]*\)/\1/g")
	RT=$(echo "${vetor[2]}"| sed -e "s/\$\([0-9]*\)/\1/g")
 	IMM=$(echo "${vetor[3]}"| sed -e "s/\(^-*[0-9]*\)(.*/\1/g")

	BRT=$(fill_zero $(echo "ibase=10;obase=2;$RT" | bc) 5)
	BRS=$(fill_zero $(echo "ibase=10;obase=2;$RS" | bc) 5)
	BIMM=$(./compl_num $IMM)
	BIMM=$(fill_zero $BIMM 4)

	echo "when 	\"$(fill_zero $(echo "ibase=10;obase=2;$ADDRESS" | bc) 6)\" => data_out <= \"$BOP\" & \"$BRS\" & \"$BRT\" & x\"$BIMM\"; -- beq \$$RS, \$$RT, $IMM"

  	;;
nop*)

	# NOP is implemented on old MIPS as an "add $0, $0, $0". As you can't write in $0, this instruction do nothing as expected

	ADDRESS=${vetor[1]}

	BOP="000000"
  	SHAMT="00000"
	FUNCT="100000"

	BRD="00000"
	BRS="00000"
	BRT="00000"

	echo "when 	\"$(fill_zero $(echo "ibase=10;obase=2;$ADDRESS" | bc) 6)\" => data_out <= \"$BOP\" & \"$BRS\" & \"$BRT\" & \"$BRD\" & \"$SHAMT\" & \"$FUNCT\"; -- nop"

	;;
*)
	echo "Invalid Instruction"
  	;;
esac
