# include <stdio.h>
# include <stdlib.h>

/*Fun��o usada para imprimir vetor.*/
void ImprimirVetor(int *v,int tam) {
	int i;
	for (i = 0; i < tam; i++) printf("%d\n",v[i]);
}


/*Fun��o intercala usada pelo MergeSort.*/
void Intercala(int *v, int tam) {
	int meio;
	int i, j, k;
	int* tmp;

	tmp = (int*) malloc(tam * sizeof(int));
	if (tmp == NULL) exit(1);

	meio = tam / 2;
	i = 0;
	j = meio;
	k = 0;

	while (i < meio && j < tam) {
		if (v[i] < v[j]) {
			tmp[k] = v[i];
			++i;
		} else {
			tmp[k] = v[j];
			++j;
		}
		++k;
	}

	if (i == meio) {
		while (j < tam) {
			tmp[k] = v[j];
			++j;
			++k;
		}
	} else {
		while (i < meio) {
			tmp[k] = v[i];
			++i;
			++k;
		}
	}

	for (i = 0; i < tam; ++i) {
		v[i] = tmp[i];
	}

	free(tmp);
}


/*Fun��o de ordena��o por sele��o.*/
void SelectionSort(int *v, int tam) {
	int i, j, min, aux;
	for(i=0; i<tam-1; i++) {
		min = i;
		aux = v[i];
		for(j=i+1; j<tam; j++) {
			if (v[j] < aux) {
				min=j;
				aux=v[j];
			}
		}
		aux = v[i];
		v[i] = v[min];
		v[min] = aux;
	}
}


/*Fun��o de ordena��o MergeSort.*/
/*Enquanto o tamanho do "peda�o" do vetor (que � "particionado" pelo MergeSort) for maior ou igual a 9, a ordena��o segue sendo pelo Merge. Caso contr�rio, a ordena��o � feita pelo SelectionSort, que tem custo inferior para vetores de tamanho menor a 9.*/
void MergeSort(int *v, int tam) {
	int meio;
	if (tam >= 7) {
		meio = tam / 2;
		MergeSort(v, meio);
		MergeSort(v + meio, tam - meio);
		Intercala(v, tam);
	} else {
		SelectionSort(v,tam);
	}
}


int main() {
	int vtam, i = 1;
	int *v;

/*Aloca��o e preenchimento do vetor a ser ordenado.*/
	v = malloc (i + sizeof(int));
	v[0] = 0;
	while (scanf("%d",&v[i-1]) != EOF) {
		++i;
		v = (int*) realloc(v, i * sizeof(int));
	}

	--i; --i;
	vtam = i;

	/*Ordena��o e impress�o do vetor.*/
	MergeSort(v,vtam);
	ImprimirVetor(v,vtam);
	return 0;
}
