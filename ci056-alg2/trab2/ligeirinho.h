/*
--------------------------------------------
CI056 - 2009/2
Segundo trabalho pr�tico

Arquivo com a defini��o de relacao e os prot�tipos das fun��es

N�O ALTERE O CONTE�DO DESTE ARQUIVO
--------------------------------------------
*/

#ifndef _ligeirinholib_
#define _ligeirinholib_

#include "biblioteca.h"

#define MAXTUBOS 100

/* tipo relacao */
typedef struct
{
  int n_pares; /* N�mero de pares de indiv�duos */

  lista *adjacentes[MAXTUBOS]; /* vetor de listas de adjac�ncias */
} relacao;


/*
A lista que cont�m um caminho m�nimo pode ser uma lista de pares
onde cada elemento � um tubo e uma linha de ligeirinho. Isto �
o esperado como sa�da das duas fun��es de caminho m�nimo
*/

/* Devolve um caminho m�nimo (menor n�mero de tubos) */
lista * caminho_minimo_tubos(relacao *R, int tuboA, int tuboB);

/* Devolve um caminho m�nimo (menor n�mero de ligeirinhos) */
lista * caminho_minimo_linhas(relacao *R, int tuboA, int tuboB);

/* Escreve o caminho m�nimo no formato de sa�da */
void escreve_caminho(lista *c);

#endif

