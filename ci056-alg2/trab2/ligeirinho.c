#include <stdio.h>
#include <stdlib.h>

#include "ligeirinho.h"

/* Busca do fecho transitivo. */
int busca_fecho (relacao *R, relacao *S)
{
	no *n1, *n2;
	lista *lst, *lst2;
	par *p1, *p2, *ptemp;
	int i, k, trajeto, tubo, trajeto2, tubo2;

	/* Para todos os tubos */
	for (i = 1; i < MAXTUBOS; i++)
	{
		lst = R->adjacentes[i];
		/* Para todas as listas. */
		for (n1 = primeiro_no(lst); n1; n1 = sucessor_no(n1,lst))
		{
			p1 = objeto_no (n1);
			trajeto = p1->primeiro;
			tubo = p1->segundo;
			ptemp = constroi_objeto_par(trajeto, tubo);
			if (! insere_lista(ptemp, S->adjacentes[i]))
			{
				return (0);
			}
			for (k = i + 1; k < MAXTUBOS; k++)
			{
				lst2 = R->adjacentes[k];
				for (n2 = primeiro_no(lst2); n2; n2 = sucessor_no(n2,lst2))
				{
					p2 = objeto_no (n2);
					trajeto2 = p2->primeiro;
					tubo2 = p2->segundo;
					if (trajeto == trajeto2)
					{
						ptemp = constroi_objeto_par(trajeto2, tubo2);
						if (! insere_lista(ptemp, S->adjacentes[i]))
						{
							return (0);
						}
					}
				}
			}
		}
	}
	return (1);
}

/* Retorna o caminho com o menos ligeirinhos. */
lista * caminho_minimo_linhas(relacao *R, int tuboA, int tuboB)
{
	relacao S;
	int i, trajeto, tubo, trajeto2, tubo2, primeiro_tubo;
	lista *fecho, *lst, *caminho;
	no *n1, *n2;
	par *p1, *p2;

	/* Para todos os tubos */
	for (i = 0; i < MAXTUBOS; i++)
		S.adjacentes[i] = constroi_lista ();
	if (busca_fecho(R, &S) == 0)
		return (NULL);
	fecho = caminho_minimo_tubos(&S,tuboA,tuboB);
	caminho = constroi_lista ();
	n1 = primeiro_no(fecho);
	p1 = objeto_no(n1);
	primeiro_tubo = p1->segundo;
	if (! insere_final(p1, caminho))
		return (NULL);
	/* Para todas as listas. */
	for (n1=sucessor_no(n1, fecho); n1; n1=sucessor_no(n1,fecho))
	{
		p1 = objeto_no(n1);
		trajeto = p1->primeiro;
		tubo = p1->segundo;
		lst = R->adjacentes[primeiro_tubo];
		n2 = primeiro_no(lst);
		while (n2)
		{
			p2 = objeto_no (n2);
			trajeto2 = p2->primeiro;
			tubo2 = p2->segundo;
			if (trajeto == trajeto2)
			{
				if (! insere_final(p2, caminho))
					return (NULL);
				if (tubo == tubo2)
				{
					n2 = sucessor_no(n2, lst);
				}
				else
				{
					lst = R->adjacentes[tubo2];
					n2 = primeiro_no(lst);
				}
			}
			else
				n2 = sucessor_no(n2, lst);
		}
		primeiro_tubo = tubo;
	}
	return (caminho);
}

/* Retorna o caminho com menos tubos possiveis. */
lista * caminho_minimo_tubos(relacao *R, int tuboA, int tuboB)
{
	no *n;
	lista *fila = constroi_lista ();
	par *p;
	int i, pre[MAXTUBOS], st[MAXTUBOS], trajetos[MAXTUBOS], *tubo1, *tubo2, *tubo, t;
	
	t = 0;
	for (i = 1; i < MAXTUBOS; i++)
		pre[i] = st[i] = trajetos[i] = -1;
	pre[tuboA] = t++;
	tubo = constroi_objeto_int (tuboA);  
	if (! insere_lista(tubo, fila))
		return (NULL);
	while ((tubo1 = remove_lista(fila)))
	{
		for (n = primeiro_no(R->adjacentes[*tubo1]); n; n = sucessor_no(n,R->adjacentes[*tubo1]))
		{
			p = objeto_no(n);
			tubo2 = &p->segundo;
			if (pre[*tubo2] == -1)
			{
				tubo = constroi_objeto_int(*tubo2);
				if (! insere_lista(tubo, fila))
					return (NULL);
				pre[*tubo2] = t++;
				st[*tubo2] = *tubo1;
				trajetos[*tubo2] = p->primeiro;
			}
		}
	}
	for (i = tuboB; i != tuboA; i = st[i])
	{
		p = constroi_objeto_par(trajetos[i], i);
		if (! insere_lista(p, fila))
			return (NULL);
	}
	p = constroi_objeto_par(trajetos[i], i);
	if (! insere_lista(p, fila))
		return (NULL);
	return (fila);
}

/* Imprime o resultado como na especificacao. */
void escreve_caminho(lista *c)
{
	no *n;
	par *p;

	if (!c)
		exit(1);
	n = primeiro_no(c);
	p = objeto_no (n);
	printf ("(%d)", p->segundo);
	for (n = sucessor_no(n,c); n; n = sucessor_no(n,c))
	{
		p = objeto_no (n);
		printf ("-%d-(%d)", p->primeiro, p->segundo);
	}
	puts("");
}
